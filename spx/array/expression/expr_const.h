#ifndef SPX_EXPR_CONST_H
#define SPX_EXPR_CONST_H

namespace spx
{

template <typename T>
  struct expr_const
  {
    using value_type       = T;
    using reference        = T&;
    using const_reference  = const T&;

    static constexpr unbound rank() { return unbound(); }
   
    expr_const( T&& v ) 
      : val( std::forward<T>(v) ) 
    {}
    
    // evaluation interface 
    reference       operator*()       { return val; }
    const_reference operator*() const { return val; }

    // Input_iteerrator    
    expr_const& operator++() { return *this; }
    
    std::size_t  suggest_stride( std::size_t dim ) 
    { 
      return 1; 
    }
    
    bool is_stride( std::size_t dim, std::size_t stride ) 
    { 
      return true; 
    }
    
    bool can_collapse( std::size_t outer_dim, std::size_t inner_dim ) 
    { 
      return true; 
    }

    // TODO: test & need to deduce difference type
    void advance_data( std::ptrdiff_t n )
    {}
    
    void advance_data()
    {}

    void advance_stride( std::size_t d, std::ptrdiff_t step)
    {}
    
    void advance_stride( std::size_t d )
    {}
    
    unbound descriptor() const;
      
    unbound extent( std::size_t dim );
    
    // Shift iterator by index vector
    //
    template <Range R>
      expr_const shift( R&& idx ) const
      { 
        return *this;
      }
 
    template <Range R>
      expr_const operator()( R&& idx ) const
      {
        return shift( idx );
      }

  private:
    T val;
  };

//////////////////////////////////////////////////////////////////////////////
// global factory method
//////////////////////////////////////////////////////////////////////////////
 
template <typename A>
requires Dense_array<Main_type<A>>()
  constexpr decltype(auto) as_const( A&& a )
  {
    return expr_const<A>( std::forward<A>(a) );
  }

template <typename A>
requires not Dense_array<Main_type<A>>()
  constexpr decltype(auto) as_const( A&& a )
  {
    return a; 
  }

} // namespace spx

#endif // SPX_EXPR_CONST_H
