#ifndef SPX_EXPRESSION_CONCEPTS_H
#define SPX_EXPRESSION_CONCEPTS_H

namespace spx
{

struct unbound {};

template <typename T>
  concept bool Has_unbound_rank()
  {
    return Same<decltype(T::rank()), unbound>();
  }
  
template <typename T>
  concept bool Expression_rank()
  {
    return Same<T, unbound>()
        || Same<T, std::size_t>(); 
  }
 
namespace type_impl
{
  template <typename T>
    struct static_rank
    {
    private:
      template <typename X>
        static constexpr auto check( X&& x ) -> decltype( X::rank() );
      static constexpr subst_failure check( ... );

    public:
      using type = decltype( check(std::declval<T>()) );
    };
}

template <typename T>
  concept bool Expression_iterator()
  {
    return requires( T t ) {
             { *t } -> const Value_type<T>&;
             //{ ++t } -> T&; // TODO: internal compiler error
             { t.suggest_stride( std::size_t() ) } -> std::size_t;
             { t.is_stride( std::size_t(), std::size_t() ) } -> bool;
             { t.can_collapse( std::size_t(), std::size_t() ) } -> bool;
             { t.advance_data( std::ptrdiff_t() ) };
             { t.advance_data() };
             { t.advance_stride( std::size_t(), std::ptrdiff_t() ) };
             { t.advance_stride( std::size_t() ) };
             requires Expression_rank<typename type_impl::static_rank<T>::type>();
             // { t.extent( std::size_t() ) } -> decltype(T::rank()); // <-- this leads extremely slow compilation
           };
  }

template <typename T>
  concept bool Unbound_expression_iterator()
  {
    return Expression_iterator<T>()
        && Has_unbound_rank<T>();
  }
  
template <typename T>
  concept bool Ranked_expression_iterator()
  {
    return Expression_iterator<T>()
        && not Has_unbound_rank<T>();
  }

template <typename T>
  concept bool Describable()
  {
    return requires( T t ) {
             requires Descriptor<typename T::descriptor_type>();
             { t.descriptor() } -> typename T::descriptor_type;
           };
  }

template <typename T>
  concept bool Dense_array()
  {
    return Range<T>()
        && Describable<T>()
        && requires( T t ) {
             typename Value_type<T>;
             { *(t.data()) } -> Value_type<T>;             
             { t.descriptor().offset( t.lbounds() ) };
             requires Random_access_iterator<decltype(t.data())>();
             { T::rank() } -> std::size_t;
             { t.size() } -> Size_type<T>;
           };
  }
  
// as_expr: a factory method to make expression iterator

// head forward
template <typename T> class expr_const;

template <typename T>
requires not Expression_iterator<T>() 
      && not Dense_array<T>() 
  constexpr expr_const<T> as_expr( T t )
  {
    return expr_const<T>( std::forward<T>(t) );
  }

template <Expression_iterator T>
  constexpr T as_expr( T t )
  {
    return t;
  }

// head forward
template <Descriptor Desc, Random_access_iterator I>
requires Dense_iter_draggable<Desc, I>()
  struct expr_dense; 
  
// head forward
template <Descriptor Desc, Random_access_iterator I>
requires Dense_iter_draggable<Desc, I>()
  expr_dense<Desc, I>
  make_expr_dense( const Desc& s, 
                   I it, 
                   const typename expr_dense<Desc, I>::id_vec_t& id_init );
  
template <typename T>
requires Dense_array<Main_type<T>>()
  constexpr decltype(auto) as_expr( T&& t )
  {
    return make_expr_dense( t.descriptor(), t.data(), t.lbounds() );
  }

// Expressible:check as_expr( T() ) is defined and returns Expression_iterator

namespace type_impl
{
  template <typename T>
    struct as_expr_t
    {
    private:
      template <typename X>
        static constexpr auto check( X&& x ) -> decltype( as_expr(x) );
      static constexpr subst_failure check( ... );
    
    public:
      using type = decltype( check(std::declval<T>()) ); 
    };
}

template <typename T>
  concept bool Expressible()
  {
    return requires( T t ) {
             requires Expression_iterator<typename type_impl::as_expr_t<T>::type>();
           };
  }

template <typename T>
  concept bool Nonconst_expressible()
  {
    return Dense_array<T>()
        || Expression_iterator<T>(); 
  }

template <typename T>
  concept bool Const_expressible()
  {
    return Expressible<T>()
        && not Nonconst_expressible<T>(); 
  }
/*
template <typename E, typename C, typename F>
  concept bool Binary_expressible_check()
  {
    return requires( Value_type<E> val_e, C val_c ) {
             typename function_result<F, Value_type<E>, C>;
           };
  }
*/
template <typename E, typename C, typename F>
  concept bool Binary_expressible_check()
  {
    return requires( Value_type<E> e, C c, F f ) {
             { f( e, c ) };
             //typename function_result<F, Value_type<E>, C>;
           };
  }

template <typename T, typename U, typename F>
  concept bool Binary_expressible()
  {
    return ( Nonconst_expressible<T>() && Nonconst_expressible<U>() )
        || ( Nonconst_expressible<T>() && Binary_expressible_check<T, U, F>() )
        || ( Nonconst_expressible<U>() && Binary_expressible_check<U, T, F>() );
  }


// template <typename T>
//   using expression_iterator_type = decltype( as_expr( std::declval<T>() ) );

// template <typename T, typename U, typename F>
//   concept bool Binary_expressible()
//   {
//     return ( Expressible<T>() && Expressible<U>() )
//         && Some( Nonconst_expressible<T>(), Nonconst_expressible<U>() );
//         && requires( T t, U u, F f ) {
//               { f( *as_expr(t), *as_expr(u) ) } -> function_result<F, Value_type<expression_iterator_type<T>>, Value_type<expression_iterator_type<U>>>;
//            };
//   }

} // namespace spx

#endif // SPX_EXPRESSION_CONCEPTS_H
