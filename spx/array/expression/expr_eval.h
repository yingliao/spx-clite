#ifndef SPX_EXPR_EVAL_H
#define SPX_EXPR_EVAL_H

namespace spx
{
template <Expression_iterator E, typename F, typename X, typename C>
  inline void linear_evaluate( E& expr, 
                               F&& f,
                               bool use_common_stride, 
                               X extent, 
                               C common_stride,
                               std::size_t outer_dim )
  {
    if( use_common_stride )
    {
      std::size_t len = extent * common_stride;
      std::size_t i = 0;

      if( common_stride == 1 )
      {
        std::size_t n1 = len & 3;
        switch (n1)
        {
          case 3: 
            f( *expr );
            expr.advance_data();
            ++i;
          case 2: 
            f( *expr );  
            expr.advance_data();
            ++i;
          case 1: 
            f( *expr );  
            expr.advance_data();
            ++i;
          case 0:
            ;          
        }
        
        for(; i < len; i += 4 )
        {
          f( *expr );  expr.advance_data();
          f( *expr );  expr.advance_data();
          f( *expr );  expr.advance_data();
          f( *expr );  expr.advance_data();
        }
      }
      else 
      {
        std::size_t n1 = extent & 3;
        switch (n1)
        {
          case 3: 
            f( *expr );  
            expr.advance_data( common_stride );
            i += common_stride;
          case 2: 
            f( *expr );  
            expr.advance_data( common_stride );
            i += common_stride;
          case 1: 
            f( *expr );  
            expr.advance_data( common_stride );
            i += common_stride;
          case 0:
            ;          
        }

        for(; i < len; i += 4 * common_stride )
        {
          f( *expr );  expr.advance_data( common_stride );
          f( *expr );  expr.advance_data( common_stride );
          f( *expr );  expr.advance_data( common_stride );
          f( *expr );  expr.advance_data( common_stride );
        }
      }
    }
    else 
    {
      // std::cout << "[WARNING] slow linear evaluation." << std::endl;      
      for( std::size_t i = 0; i < extent; ++i )
      {
        f( *expr );
        expr.advance_stride( outer_dim );
      }
    }    
  }
  
template <Expression_iterator E, typename F>
requires ( E::rank() == 1 )
  inline void eval( E expr, F&& f )
  {
    std::size_t d0 = 0;   
    auto common_stride = expr.suggest_stride(d0);
    bool use_common_stride = expr.is_stride( d0, common_stride );
    linear_evaluate_async( expr, 
                           std::forward<F>(f), 
                           use_common_stride, 
                           expr.extent( d0 ), 
                           common_stride, d0 );
  }
  
template <Expression_iterator E, typename F>
requires ( E::rank() > 1 )
  inline void eval( E expr, F&& f )
  {
    decltype(auto) desc = expr.descriptor();
    const auto max_dim = desc.store_dim(0);

    auto common_stride = expr.suggest_stride( max_dim );
    bool use_common_stride = expr.is_stride( max_dim, common_stride );
    auto last_length = desc.extent( max_dim );

    std::size_t first_non_collapsed_loop = 1;
    for ( std::size_t i = 1; i < E::rank(); ++i)
    {
      auto outer_dim = desc.store_dim( i );
      auto inner_dim = desc.store_dim( i-1 );

      if ( expr.can_collapse( outer_dim, inner_dim ) )
      {
        last_length *= desc.extent( outer_dim );
        first_non_collapsed_loop = i + 1;
      }
      else  
        break;
    }
    
    auto idx = spx::lbounds( desc );

    while (true) 
    {
      linear_evaluate_async( expr, 
                             std::forward<F>(f), 
                             use_common_stride, 
                             last_length, 
                             common_stride, 
                             max_dim );

      expr.advance_stride( max_dim, -std::ptrdiff_t( last_length ) );
      
      auto j = first_non_collapsed_loop;
      for(; j < E::rank(); ++j )
      {
        auto r = desc.store_dim(j);
        expr.advance_stride( r, 1 );        
        ++idx[r];
        
        if( idx[r] <= desc.ubound(r) )
          break;
        else
        {
          idx[r] = desc.lbound( r );
          expr.advance_stride( r, -desc.extent(r) );
        }
      }
      
      if( j == E::rank() )
        break;
    }
  }  

struct no_lump
{
  template <typename T>
    constexpr decltype(auto) operator()( T&& t )
    {
      return t;
    }
};

template <Expression_iterator E>
  inline void eval( E expr )  
  {
    eval( expr, no_lump() );
  }
  
// Reduce
//
template <Expressible E, typename T>
  inline decltype(auto) sum( E&& e, T& s ) // s = result
  {
    s = T{0};
    eval( as_expr( std::forward<E>(e)  ), 
          [&]( auto&& t ) { s += t; } );
  }

template <Expressible E>
  inline decltype(auto) sum( E&& e ) 
  {
    using T = Value_type<Main_type<E>>;
    T s = T{0};
    eval( as_expr( std::forward<E>(e)  ), 
          [&]( auto&& t ) { s += t; } );
    return s;
  }

// For each
//
template <typename F, Expressible... E>
  inline void for_each( F&& f, E&&... expr )
  {
    eval( make_expr_func( std::forward<F>(f), std::forward<E>(expr)... ) );
  }

} // namespace spx

#endif // SPX_EXPR_EVAL_H
