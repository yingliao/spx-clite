#ifndef SPX_EXPR_OPS_H
#define SPX_EXPR_OPS_H

namespace spx
{

// [NOTE] ignore assignment operators: =, +=, -=, *=, /=, %=, &=, |=, ^=, <<=, >>=

template <Expressible T, Expressible U>
requires Binary_expressible<Main_type<T>, Main_type<U>, ops::equal>()
  constexpr decltype(auto) operator == ( T&& t, U&& u )
  {
    return make_expr_func( ops::equal(), std::forward<T>(t), std::forward<U>(u) );
  }
 
template <Expressible T, Expressible U>
requires Binary_expressible<Main_type<T>, Main_type<U>, ops::not_equal>()
  constexpr decltype(auto) operator != ( T&& t, U&& u )
  {
    return make_expr_func( ops::not_equal(), std::forward<T>(t), std::forward<U>(u) );  
  }
    
template <Expressible T, Expressible U>
requires Binary_expressible<Main_type<T>, Main_type<U>, ops::less>()
  constexpr decltype(auto) operator < ( T&& t, U&& u )
  {
    return make_expr_func( ops::less(), std::forward<T>(t), std::forward<U>(u) );
  }
   
template <Expressible T, Expressible U>
requires Binary_expressible<Main_type<T>, Main_type<U>, ops::greater>()
  constexpr decltype(auto) operator > ( T&& t, U&& u )
  {
    return make_expr_func( ops::greater(), std::forward<T>(t), std::forward<U>(u) );
  }
 
template <Expressible T, Expressible U>
requires Binary_expressible<Main_type<T>, Main_type<U>, ops::less_equal>()
  constexpr decltype(auto) operator <= ( T&& t, U&& u )
  {
    return make_expr_func( ops::less_equal(), std::forward<T>(t), std::forward<U>(u) );
  }
   
template <Expressible T, Expressible U>
requires Binary_expressible<Main_type<T>, Main_type<U>, ops::greater_equal>()
  constexpr decltype(auto) operator >= ( T&& t, U&& u )
  {
    return make_expr_func( ops::greater_equal(), std::forward<T>(t), std::forward<U>(u) );
  }
  
template <Expressible T, Expressible U>
requires Binary_expressible<Main_type<T>, Main_type<U>, ops::plus>()
  constexpr decltype(auto) operator + ( T&& t, U&& u )
  {
    return make_expr_func( ops::plus(), std::forward<T>(t), std::forward<U>(u) );
  }

template <Expressible T, Expressible U>
requires Binary_expressible<Main_type<T>, Main_type<U>, ops::minus>()
  constexpr decltype(auto) operator - ( T&& t, U&& u )
  {
    return make_expr_func( ops::minus(), std::forward<T>(t), std::forward<U>(u) );
  }
  
template <Expressible T, Expressible U>
requires Binary_expressible<Main_type<T>, Main_type<U>, ops::multiplies>()
  constexpr decltype(auto) operator * ( T&& t, U&& u )
  {
    return make_expr_func( ops::multiplies(), std::forward<T>(t), std::forward<U>(u) );
  }

template <Expressible T, Expressible U>
requires Binary_expressible<Main_type<T>, Main_type<U>, ops::divides>()
  constexpr decltype(auto) operator / ( T&& t, U&& u )
  {
    return make_expr_func( ops::divides(), std::forward<T>(t), std::forward<U>(u) );
  }
 
template <Expressible T, Expressible U>
requires Binary_expressible<Main_type<T>, Main_type<U>, ops::modulus>()
  constexpr decltype(auto) operator % ( T&& t, U&& u )
  {
    return make_expr_func( ops::modulus(), std::forward<T>(t), std::forward<U>(u) );
  }

template <Expressible T>
requires Nonconst_expressible<Main_type<T>>()
  constexpr decltype(auto) operator + ( T&& t )
  {
    return make_expr_func( ops::unary_plus(), std::forward<T>(t) );
  }
              
template <Expressible T>
requires Nonconst_expressible<Main_type<T>>()
  constexpr decltype(auto) operator - ( T&& t )
  {
    return make_expr_func( ops::unary_minus(), std::forward<T>(t) );
  }
    
template <Expressible T>
requires Nonconst_expressible<Main_type<T>>() 
  constexpr decltype(auto) operator ++ ( T&& t )
  {
    return make_expr_func( ops::pre_increment(), std::forward<T>(t) );
  }

template <Expressible T>
requires Nonconst_expressible<Main_type<T>>()
  constexpr decltype(auto) operator ++ ( T&& t, int )
  {
    return make_expr_func( ops::post_increment(), std::forward<T>(t) );
  }
  
template <Expressible T>
requires Nonconst_expressible<Main_type<T>>()
  constexpr decltype(auto) operator -- ( T&& t )
  {
    return make_expr_func( ops::pre_decrement(), std::forward<T>(t) );
  }
   
template <Expressible T>
requires Nonconst_expressible<Main_type<T>>()
  constexpr decltype(auto) operator -- ( T&& t, int )
  {
    return make_expr_func( ops::post_decrement(), std::forward<T>(t) );
  }

template <Expressible T, Expressible U>
requires Binary_expressible<Main_type<T>, Main_type<U>, ops::binary_and>()
  constexpr decltype(auto) operator && ( T&& t, U&& u )
  {
    return make_expr_func( ops::binary_and(), std::forward<T>(t), std::forward<U>(u) );
  }

template <Expressible T, Expressible U>
requires Binary_expressible<Main_type<T>, Main_type<U>, ops::binary_or>()
  constexpr decltype(auto) operator || ( T&& t, U&& u )
  {
    return make_expr_func( ops::binary_or(), std::forward<T>(t), std::forward<U>(u) );
  }

//template <Expressible T>
//requires not Const_expressible<Main_type<T>>()
//  constexpr decltype(auto) operator ! ( T&& t )
//  {
//    return make_expr_func( ops::unary_not(), std::forward<T>(t) );
//  }

template <Expressible T, Expressible U>
requires Binary_expressible<Main_type<T>, Main_type<U>, ops::bit_and>()
  constexpr decltype(auto) operator & ( T&& t, U&& u )
  {
    return make_expr_func( ops::bit_and(), std::forward<T>(t), std::forward<U>(u) );
  } 
  
template <Expressible T, Expressible U>
requires Binary_expressible<Main_type<T>, Main_type<U>, ops::bit_or>()
  constexpr decltype(auto) operator | ( T&& t, U&& u )
  {
    return make_expr_func( ops::bit_or(), std::forward<T>(t), std::forward<U>(u) );
  }
 
template <Expressible T, Expressible U>
requires Binary_expressible<Main_type<T>, Main_type<U>, ops::bit_xor>()
  constexpr decltype(auto) operator ^ ( T&& t, U&& u )
  {
    return make_expr_func( ops::bit_xor(), std::forward<T>(t), std::forward<U>(u) );
  }
 
template <Expressible T, Expressible U>
requires Binary_expressible<Main_type<T>, Main_type<U>, ops::left_shift>()
  constexpr decltype(auto) operator << ( T&& t, U&& u )
  {
    return make_expr_func( ops::left_shift(), std::forward<T>(t), std::forward<U>(u) );
  }
  
template <Expressible T, Expressible U>
requires Binary_expressible<Main_type<T>, Main_type<U>, ops::right_shift>()
  constexpr decltype(auto) operator >> ( T&& t, U&& u )
  {
    return make_expr_func( ops::right_shift(), std::forward<T>(t), std::forward<U>(u) );
  }
  
template <Expressible T>
requires Nonconst_expressible<Main_type<T>>()
  constexpr decltype(auto) operator ~ ( T&& t )
  {
    return make_expr_func( ops::complement(), std::forward<T>(t) );
  }
  
template <Expressible T>
requires Nonconst_expressible<Main_type<T>>()
  constexpr decltype(auto) operator & ( T&& t )
  {
    return make_expr_func( ops::address(), std::forward<T>(t) );
  }
  
// [NOTE] ignore dereference (*): will confuse with the evaluation of expression iterator
// [NOTE] ignore subscript ([]): 
//          static_array: static subscript in linear storage space (as std::array)
//          array: multi-component support (element-wise)

} // namespace spx

#endif // SPX_EXPR_OPS_H
