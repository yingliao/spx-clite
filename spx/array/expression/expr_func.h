#ifndef SPX_EXPR_FUNC_H
#define SPX_EXPR_FUNC_H

namespace spx
{

namespace expr_func_impl
{
  // Expression_iterator::rank()
  //
  template <Unbound_expression_iterator E>
    constexpr decltype(auto) expr_rank()
    {
      return unbound();
    }

  template <Ranked_expression_iterator E, Expression_iterator... R>
    constexpr decltype(auto) expr_rank()
    {
      return E::rank();
    }
    
  template <Unbound_expression_iterator E, Expression_iterator... R>
  requires Not_empty<R...>()
    constexpr decltype(auto) expr_rank()
    {
      return expr_rank<R...>();
    }

//   template <Ranked_expression_iterator E, Expression_iterator... R>
//   requires Not_empty<R...>()
//         && Some( Ranked_expression_iterator<R>()... )
//     constexpr decltype(auto) expr_rank()
//     {
//       static_assert( E::rank() == expr_rank<R...>(), "expression ranks does not match" );
//       return E::rank();
//     }
   
  // Expression_iterator::descriptor()
  //
  struct expr_descriptor
  {
    template <Unbound_expression_iterator... E>
      constexpr decltype(auto) operator()( E... exprs )
      {
        return unbound();
      }
            
    template <Ranked_expression_iterator E, Expression_iterator... R>
      constexpr decltype(auto) operator()( E e, R... exprs )
      {
        return e.descriptor();
      }
      
    template <Unbound_expression_iterator E, Expression_iterator... R>
      constexpr decltype(auto) operator()( E e, R... exprs )
      {
        return (*this)( exprs... );
      }    
  };
  
  // Expression_iterator::extent( dim )
  //
  template <Unbound_expression_iterator I>
    inline void
    update_extent( std::size_t& s, std::size_t dim, I i )
    {}

  template <Ranked_expression_iterator I>
    inline decltype(auto) 
    update_extent( std::size_t& s, std::size_t dim, I i )
    {
      s = std::max( s, i.extent( dim ) );
    }

  // Partial Invokation
  //
  struct partial_invoke
  {
    template <Expression_iterator F, Expression_iterator... E>
      constexpr decltype(auto)
      operator()( F&& f, E&&... e )
      {
        return (*f)( std::forward<E>(e)... );
      }
  };

  template <typename F, Expression_iterator... E>
  requires not Same<F, partial_invoke>()
    constexpr decltype(auto) deref( F&&f, E&&... e )
    {
      return f( *e... ); 
    }

  template <typename F, Expression_iterator... E>
  requires Same<F, partial_invoke>()
    constexpr decltype(auto) deref( F&&f, E&&... e )
    {
      return f( std::forward<E>(e)... );
    }
}

// head forward
template <typename F, Expressible... E>
  constexpr decltype(auto) make_expr_func( F&& f, E&&... expr );
  
// Expression iterator E... for function F
//
template <typename F, Expression_iterator... E>
  struct expr_func
  { 
    //using reference  = decltype( std::declval<F>()( *std::declval<E>()... ) );
    using reference = decltype( expr_func_impl::deref( std::declval<F>(), std::declval<E>()... ) ); 
    using value_type = Remove_reference<reference>;
    
    static constexpr decltype(auto) rank() 
    { 
      return expr_func_impl::expr_rank<E...>();
    }
    
  private:
    
    F f;
    std::tuple<E...> exprs;
    
  public:
   
    expr_func( E... e ) 
      : exprs( std::make_tuple(e...) ) 
    {}

    expr_func( F&& f, E... e ) 
      : f( std::forward<F>(f) ), exprs( std::make_tuple(e...) ) 
    {}
    
    // Copyable
    expr_func( const expr_func& ) = default;
    
    // if value_type is convertible to bool
    // evaluate *this and return true if all are true
    // [WARNING] this is very BOLD implementation
    template <bool dummy = true>
    requires Convertible<value_type, bool>()
      operator bool() const
      {
        bool b = true;
        eval( *this, [&]( bool v ) { b &= v; } );
        return b;
      }
    
    // Input_iterator
    expr_func& operator++() 
    { 
      tuple_for_each( [&]( auto& e ) { ++e; }, exprs );
      return *this;
    }

    // evaluation interface
    decltype(auto) operator*()
    {
      return tuple_unpack_and_subst( [&]( auto&&... e ) -> decltype(auto)
                                     { return expr_func_impl::deref( std::forward<F>(f), std::forward<E>(e)... ); },
                                     exprs );
    }

//    template <bool dummy = true>
//    requires Function<F, E...>() // F can handle de-reference by itself
//      decltype(auto) operator*() 
//      {
//        return tuple_unpack_and_subst( [&]( auto&... e ) -> decltype(auto)
//                                       { return f( e... ); },
//                                       exprs );
//      }
//
    std::size_t suggest_stride( std::size_t dim ) 
    { 
      std::size_t s = 0;
      tuple_for_each( [&]( auto& e ) { s = std::max( s, e.suggest_stride( dim ) ); }, exprs ); 
      return s;
    }
    
    bool is_stride( std::size_t dim, std::size_t stride ) 
    { 
      bool b = true;
      tuple_for_each( [&]( auto& e ) { b &= e.is_stride( dim, stride ); }, exprs );; 
      return b;
    }
    
    bool can_collapse( std::size_t outer_dim, std::size_t inner_dim ) 
    { 
      bool b = true;
      tuple_for_each( [&]( auto& e ) { b &= e.can_collapse( outer_dim, inner_dim ); }, exprs );; 
      return b;
    }
    
    // TODO: test & need to deduce difference type
    void advance_data( std::ptrdiff_t n )
    {
      tuple_for_each( [&]( auto& e ) { e.advance_data(n); }, exprs );
    }
    
    void advance_data()
    {
      tuple_for_each( [&]( auto& e ) { e.advance_data(); }, exprs );
    }
    
    void advance_stride( std::size_t d, std::ptrdiff_t step)
    {
      tuple_for_each( [&]( auto& e ) { e.advance_stride(d, step); }, exprs );
    }
    
    void advance_stride( std::size_t d )
    {
      tuple_for_each( [&]( auto& e ) { e.advance_stride( d ); }, exprs );
    }
    
    // WARNING: return the first available descriptor in exprs
    decltype(auto) descriptor() const
    {
      return tuple_unpack_and_subst( expr_func_impl::expr_descriptor(), exprs );
    }
      
    template <bool dummy = true>
    requires All( Unbound_expression_iterator<E>()... )  
      unbound extent( std::size_t dim )
      {
        return unbound();
      }
    
    template <bool dummy = true>
    requires not All( Unbound_expression_iterator<E>()... )  
      decltype(auto) extent( std::size_t dim )
      {
        std::size_t s = 0;
        tuple_for_each( [&]( auto& e ) { expr_func_impl::update_extent( s, dim, e ); }, exprs ); 
        return s;
      }
     
    // operator =
    template <Expressible X>
      void operator=( X&& e )
      {
        eval( make_expr_func( ops::assign(), *this, std::forward<X>(e) ) );
      }

    // operator [] overloading      
    template <Expressible X>
      decltype(auto) operator[]( X&& e )
      {
        return make_expr_func( ops::subscript(), *this, std::forward<X>(e) );
      }
      
    template <Expressible X>
      decltype(auto) operator[]( X&& e ) const
      {
        return make_expr_func( ops::subscript(), *this, std::forward<X>(e) );
      }
      
    // operator ()
    //
    template <Expressible... X>
    requires Function<value_type, Value_type<X>...>()
      decltype(auto) operator()( X&&... e )
      {
        return make_expr_func( ops::invoke(), *this, std::forward<X>(e)... );    
      }

    template <Expressible... X>
    requires Function<value_type, Value_type<X>...>()
      decltype(auto) operator()( X&&... e ) const
      {
        return make_expr_func( ops::invoke(), *this, std::forward<X>(e)... );    
      }

    // operator (): de-reference handled by value_type itself
    //
    template <Expressible... X>
    requires Function<value_type, decltype(as_expr(std::declval<X>()))...>()
      decltype(auto) operator()( X&&... e )
      {
        return make_expr_func( expr_func_impl::partial_invoke(), *this, std::forward<X>(e)... );    
      }

    template <Expressible... X>
    requires Function<value_type, decltype(as_expr(std::declval<X>()))...>()
      decltype(auto) operator()( X&&... e ) const
      {
        return make_expr_func( expr_func_impl::partial_invoke(), *this, std::forward<X>(e)... );    
      }

    // Shift iterator by index vector
    //
    template <Range R>
      expr_func shift( R&& idx ) const
      { 
        using std::begin;
        auto it = begin( idx );
        expr_func x( *this );
        for( std::size_t d = 0; d < expr_func::rank(); ++d, ++it )
          x.advance_stride( d, *it );
        return x;
      }

    template <Range R>
      expr_func operator()( R&& idx ) const
      {
        return shift( idx );
      }
  };

//////////////////////////////////////////////////////////////////////////////
// global factory method
//////////////////////////////////////////////////////////////////////////////
  
template <typename F, Expressible... E>
  constexpr decltype(auto) make_expr_func( F&& f, E&&... expr )
  {
    return expr_func<F, decltype(as_expr( std::forward<E>(expr) ))...>( std::forward<F>(f), 
                                                                        as_expr( std::forward<E>(expr) )... );
  }
 
//////////////////////////////////////////////////////////////////////////////
// STL math function adapters
//////////////////////////////////////////////////////////////////////////////

// Trigonometric functions

template <Expressible... E>
  decltype(auto) cos( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::cos( v ); }, 
                           std::forward<E>(expr)... );
  }
  
template <Expressible... E>
  decltype(auto) sin( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::sin( v ); }, 
                           std::forward<E>(expr)... );
  }  

template <Expressible... E>
  decltype(auto) tan( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::tan( v ); }, 
                           std::forward<E>(expr)... );
  }  

template <Expressible... E>
  decltype(auto) acos( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::acos( v ); }, 
                           std::forward<E>(expr)... );
  }  
    
template <Expressible... E>
  decltype(auto) asin( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::asin( v ); }, 
                           std::forward<E>(expr)... );
  }  
      
template <Expressible... E>
  decltype(auto) atan( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::atan( v ); }, 
                           std::forward<E>(expr)... );
  }  
        
template <Expressible... E>
  decltype(auto) atan2( E&&... expr )
  {
    return make_expr_func( []( auto... v ) { return std::atan2( v... ); }, 
                           std::forward<E>(expr)... );
  }  
     
// Hyperbolic functions
    
template <Expressible... E>
  decltype(auto) cosh( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::cosh( v ); }, 
                           std::forward<E>(expr)... );
  }
  
template <Expressible... E>
  decltype(auto) sinh( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::sinh( v ); }, 
                           std::forward<E>(expr)... );
  }  

template <Expressible... E>
  decltype(auto) tanh( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::tanh( v ); }, 
                           std::forward<E>(expr)... );
  }  

template <Expressible... E>
  decltype(auto) acosh( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::acosh( v ); }, 
                           std::forward<E>(expr)... );
  }  
    
template <Expressible... E>
  decltype(auto) asinh( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::asinh( v ); }, 
                           std::forward<E>(expr)... );
  }  
      
template <Expressible... E>
  decltype(auto) atanh( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::atanh( v ); }, 
                           std::forward<E>(expr)... );
  }  
     
template <Expressible... E>
  decltype(auto) sech( E&&... expr )
  {
    return make_expr_func( []( auto v ) 
                           {
                             using T = decltype(v);
                             return T(1) / std::cosh( v ); 
                           }, 
                           std::forward<E>(expr)... );
  }

// Exponential and logarithmic functions

template <Expressible... E>
  decltype(auto) exp( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::exp( v ); }, 
                           std::forward<E>(expr)... );
  }  
    
template <Expressible... E>
  decltype(auto) frexp( E&&... expr )
  {
    return make_expr_func( []( auto x, auto& p ) { return std::frexp( x, &p ); }, 
                           std::forward<E>(expr)... );
  }
      
template <Expressible... E>
  decltype(auto) ldexp( E&&... expr )
  {
    return make_expr_func( []( auto... v ) { return std::ldexp( v... ); }, 
                           std::forward<E>(expr)... );
  }
      
template <Expressible... E>
  decltype(auto) log( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::log( v ); }, 
                           std::forward<E>(expr)... );
  }

template <Expressible... E>
  decltype(auto) log10( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::log10( v ); }, 
                           std::forward<E>(expr)... );
  }
            
template <Expressible... E>
  decltype(auto) exp2( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::exp2( v ); }, 
                           std::forward<E>(expr)... );
  }
     
template <Expressible... E>
  decltype(auto) expm1( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::expm1( v ); }, 
                           std::forward<E>(expr)... );
  }

template <Expressible... E>
  decltype(auto) ilogb( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::ilogb( v ); }, 
                           std::forward<E>(expr)... );
  }
    
template <Expressible... E>
  decltype(auto) log1p( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::log1p( v ); }, 
                           std::forward<E>(expr)... );
  }    

template <Expressible... E>
  decltype(auto) log2( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::log2( v ); }, 
                           std::forward<E>(expr)... );
  }
  
template <Expressible... E>
  decltype(auto) logb( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::logb( v ); }, 
                           std::forward<E>(expr)... );
  }
      
template <Expressible... E>
  decltype(auto) scalbn( E&&... expr )
  {
    return make_expr_func( []( auto... v ) { return std::scalbn( v... ); }, 
                           std::forward<E>(expr)... );
  }
     
template <Expressible... E>
  decltype(auto) scalbln( E&&... expr )
  {
    return make_expr_func( []( auto... v ) { return std::scalbln( v... ); }, 
                           std::forward<E>(expr)... );
  }

// Power functions
          
template <Expressible... E>
  decltype(auto) pow( E&&... expr )
  {
    return make_expr_func( []( auto... v ) { return std::pow( v... ); }, 
                           std::forward<E>(expr)... );
  }
  
template <Expressible... E>
  decltype(auto) sqrt( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::sqrt( v ); }, 
                           std::forward<E>(expr)... );
  }
    
template <Expressible... E>
  decltype(auto) cbrt( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::cbrt( v ); }, 
                           std::forward<E>(expr)... );
  }
  
template <Expressible... E>
  decltype(auto) hypot( E&&... expr )
  {
    return make_expr_func( []( auto... v ) { return std::hypot( v... ); }, 
                           std::forward<E>(expr)... );
  }
    
// Error and gamma functions

template <Expressible... E>
  decltype(auto) erf( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::erf( v ); }, 
                           std::forward<E>(expr)... );
  }
   
template <Expressible... E>
  decltype(auto) erfc( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::erfc( v ); }, 
                           std::forward<E>(expr)... );
  }

template <Expressible... E>
  decltype(auto) tgamma( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::tgamma( v ); }, 
                           std::forward<E>(expr)... );
  }
  
template <Expressible... E>
  decltype(auto) lgamma( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::lgamma( v ); }, 
                           std::forward<E>(expr)... );
  }
  
// Rounding, absolute value and remainder functions  

template <Expressible... E>
  decltype(auto) ceil( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::ceil( v ); }, 
                           std::forward<E>(expr)... );
  }

template <Expressible... E>
  decltype(auto) floor( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::floor( v ); }, 
                           std::forward<E>(expr)... );
  }
   
template <Expressible... E>
  decltype(auto) fmod( E&&... expr )
  {
    return make_expr_func( []( auto... v ) { return std::fmod( v... ); }, 
                           std::forward<E>(expr)... );
  }
  
template <Expressible... E>
  decltype(auto) trunc( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::trunc( v ); }, 
                           std::forward<E>(expr)... );
  }  
  
template <Expressible... E>
  decltype(auto) round( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::round( v ); }, 
                           std::forward<E>(expr)... );
  } 
     
template <Expressible... E>
  decltype(auto) lround( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::lround( v ); }, 
                           std::forward<E>(expr)... );
  } 
     
template <Expressible... E>
  decltype(auto) llround( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::llround( v ); }, 
                           std::forward<E>(expr)... );
  }
  
template <Expressible... E>
  decltype(auto) rint( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::rint( v ); }, 
                           std::forward<E>(expr)... );
  }
 
template <Expressible... E>
  decltype(auto) lrint( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::lrint( v ); }, 
                           std::forward<E>(expr)... );
  }

template <Expressible... E>
  decltype(auto) llrint( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::llrint( v ); }, 
                           std::forward<E>(expr)... );
  }
    
template <Expressible... E>
  decltype(auto) nearbyint( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::nearbyint( v ); }, 
                           std::forward<E>(expr)... );
  }
    
template <Expressible... E>
  decltype(auto) remainder( E&&... expr )
  {
    return make_expr_func( []( auto... v ) { return std::remainder( v... ); }, 
                           std::forward<E>(expr)... );
  }                

template <Expressible... E>
  constexpr decltype(auto) remquo( E&&... expr )
  {
    return make_expr_func( []( auto n, auto d, auto& q ) { return std::remquo( n, d, &q ); }, 
                           std::forward<E>(expr)... );
  }                

// Floating-point manipulation functions

template <Expressible... E>
  decltype(auto) copysign( E&&... expr )
  {
    return make_expr_func( []( auto... v ) { return std::copysign( v... ); }, 
                           std::forward<E>(expr)... );
  }  
  
template <Expressible... E>
  decltype(auto) nextafter( E&&... expr )
  {
    return make_expr_func( []( auto... v ) { return std::nextafter( v... ); }, 
                           std::forward<E>(expr)... );
  }  
  
template <Expressible... E>
  decltype(auto) nexttoward( E&&... expr )
  {
    return make_expr_func( []( auto... v ) { return std::nexttoward( v... ); }, 
                           std::forward<E>(expr)... );
  }
   
// Minimum, maximum, difference functions
   
template <Expressible... E>
  decltype(auto) fdim( E&&... expr )
  {
    return make_expr_func( []( auto... v ) { return std::fdim( v... ); }, 
                           std::forward<E>(expr)... );
  }  
 
template <Expressible... E>
  decltype(auto) fmax( E&&... expr )
  {
    return make_expr_func( []( auto... v ) { return std::fmax( v... ); }, 
                           std::forward<E>(expr)... );
  }  
   
template <Expressible... E>
  decltype(auto) fmin( E&&... expr )
  {
    return make_expr_func( []( auto... v ) { return std::fmin( v... ); }, 
                           std::forward<E>(expr)... );
  }  
   
// Other functions

template <Expressible... E>
  decltype(auto) fabs( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::fabs( v ); }, 
                           std::forward<E>(expr)... );
  }
  
template <Expressible... E>
  decltype(auto) abs( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::abs( v ); }, 
                           std::forward<E>(expr)... );
  }
    
template <Expressible... E>
  decltype(auto) fma( E&&... expr )
  {
    return make_expr_func( []( auto... v ) { return std::fma( v... ); }, 
                           std::forward<E>(expr)... );
  }     
    
// Complex functions

template <Expressible... E>
  decltype(auto) arg( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::arg( v ); }, 
                           std::forward<E>(expr)... );
  }

template <Expressible... E>
  decltype(auto) norm( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::norm( v ); }, 
                           std::forward<E>(expr)... );
  }
    
template <Expressible... E>
  decltype(auto) conj( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::conj( v ); }, 
                           std::forward<E>(expr)... );
  }
     
template <Expressible... E>
  decltype(auto) proj( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::proj( v ); }, 
                           std::forward<E>(expr)... );
  }      
 
template <Expressible E>
  decltype(auto) polar( E&& expr )
  {
    return make_expr_func( []( auto r ) { return std::polar( r ); }, 
                           std::forward<E>(expr) );
  }
    
template <Expressible... E>
  decltype(auto) polar( E&&... expr )
  {
    return make_expr_func( []( auto... v ) { return std::polar( v... ); }, 
                           std::forward<E>(expr)... );
  }
  
template <Expressible... E>
  decltype(auto) real( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::real( v ); }, 
                           std::forward<E>(expr)... );
  }   

template <Expressible... E>
  decltype(auto) imag( E&&... expr )
  {
    return make_expr_func( []( auto v ) { return std::imag( v ); }, 
                           std::forward<E>(expr)... );
  }

} // namespace spx

#endif // SPX_EXPR_FUNC_H
