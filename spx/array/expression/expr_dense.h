#ifndef SPX_EXPR_DENSE_H
#define SPX_EXPR_DENSE_H

namespace spx
{

template <Descriptor Desc, Random_access_iterator I>
requires Dense_iter_draggable<Desc, I>()
  struct expr_dense
  {
    using value_type        = typename std::iterator_traits<I>::value_type;
    using reference         = typename std::iterator_traits<I>::reference;
    using pointer           = typename std::iterator_traits<I>::pointer;
    using difference_type   = typename std::iterator_traits<I>::difference_type;

    using index_type = typename Desc::index_type;
    using id_vec_t   = std::array<index_type, Desc::rank()>;
    
    static constexpr decltype(auto) rank() { return Desc::rank(); }
    
  private:
    
    const Desc*      desc; // Describes the iterator range, use pointer to make copyable
    I                iter;
    
  public:
   
    // Copyable
    expr_dense( const expr_dense& ) = default;
    expr_dense& operator=( const expr_dense& ) = default;
    
    // Moveable
    expr_dense( expr_dense&& ) = default;
    expr_dense& operator=( expr_dense&& ) = default;

    // Constructor
    //
    // "beg_it" always points to the begining position. After construction, 
    // this descriptor iterator will shift beg_it to the position at "id_init"
    
    expr_dense( const Desc& s, I beg_it, const id_vec_t& id_init );
    expr_dense( const Desc& s, I beg_it );
    
    // Returns the iterators describing slice.
    const Desc& descriptor() const { return *desc; }
    const I&    wrap_iter()  const { return iter; }
          I&    wrap_iter()        { return iter; }
          
    // ExpressionIterator
    //
    
    // Readable
    decltype(auto) operator*()  const { return *iter; }
    decltype(auto) operator->() const { return &(*iter); }
    
    // Input_iterator
    expr_dense& operator++() 
    { 
      advance_stride( desc->store_dim(0) ); 
      return *this;
    }
    
    std::size_t suggest_stride( std::size_t dim ) 
    { 
      return descriptor().stride( dim );
    }
    
    bool is_stride( std::size_t dim, std::size_t s ) 
    { 
      return descriptor().stride( dim ) == s; 
    }
    
    bool can_collapse( std::size_t outer_dim, std::size_t inner_dim ) 
    { 
      return descriptor().stride( inner_dim )
           * descriptor().extent( inner_dim ) 
          == descriptor().stride( outer_dim );
    }
    
    // TODO: test & need to deduce difference type
    void advance_data( std::ptrdiff_t n )
    {
      iter += n;
    }
    
    void advance_data()
    {
      ++iter;
    }
    
    void advance_stride( std::size_t d, std::ptrdiff_t step)
    {
      iter += desc->strides[d] * step;
    }
    
    void advance_stride( std::size_t d )
    {
      iter += desc->strides[d];
    }
    
    decltype(auto) extent( std::size_t dim )
    {
      return descriptor().extent( dim );
    }    
    
    // operator [] overloading
    template <Expressible X>
      decltype(auto) operator[]( X&& e )
      {
        return make_expr_func( ops::subscript(), *this, std::forward<X>(e) );
      }
      
    template <Expressible X>
      decltype(auto) operator[]( X&& e ) const
      {
        return make_expr_func( ops::subscript(), *this, std::forward<X>(e) );
      }
     
    // operator ()
    //
    template <Expressible... X>
    requires Function<value_type, Value_type<X>...>()
      decltype(auto) operator()( X&&... e )
      {
        return make_expr_func( ops::invoke(), *this, std::forward<X>(e)... );    
      }

    template <Expressible... X>
    requires Function<value_type, Value_type<X>...>()
      decltype(auto) operator()( X&&... e ) const
      {
        return make_expr_func( ops::invoke(), *this, std::forward<X>(e)... );    
      }

    // operator (): de-reference handled by value_type itself
    //
    template <Expressible... X>
    requires Function<value_type, decltype(as_expr(std::declval<X>()))...>()
      decltype(auto) operator()( X&&... e )
      {
        return make_expr_func( expr_func_impl::partial_invoke(), *this, std::forward<X>(e)... );    
      }

    template <Expressible... X>
    requires Function<value_type, decltype(as_expr(std::declval<X>()))...>()
      decltype(auto) operator()( X&&... e ) const
      {
        return make_expr_func( expr_func_impl::partial_invoke(), *this, std::forward<X>(e)... );    
      }

    // Shift iterator by index vector
    //
    template <Range R>
      expr_dense shift( R&& idx ) const
      { 
        using std::begin;
        auto it = begin( idx );
        expr_dense x( *this );
        for( std::size_t d = 0; d < Desc::rank(); ++d, ++it )
          x.advance_stride( d, *it );
        return x;
      }
 
    template <Range R>
      expr_dense operator()( R&& idx ) const
      {
        return shift( idx );
      }
  };
  
//////////////////////////////////////////////////////////////////////////////
// Constructors
//////////////////////////////////////////////////////////////////////////////

template <Descriptor Desc, Random_access_iterator I>
requires Dense_iter_draggable<Desc, I>()
  expr_dense<Desc, I>::expr_dense( const Desc& s, 
                                   I it, 
                                   const id_vec_t& id_init )
    : desc(&s), iter(it)
  {
    iter  += s.offset( id_init );
  }

//////////////////////////////////////////////////////////////////////////////
// global factory function
//////////////////////////////////////////////////////////////////////////////
  
template <Descriptor Desc, Random_access_iterator I>
requires Dense_iter_draggable<Desc, I>()
  expr_dense<Desc, I>
  make_expr_dense( const Desc& s, 
                   I it, 
                   const typename expr_dense<Desc, I>::id_vec_t& id_init )
  {
    return expr_dense<Desc, I>( s, it, id_init );
  }
  
} // namespace spx

#endif // SPX_EXPR_DENSE_H
