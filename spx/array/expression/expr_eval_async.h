#ifndef SPX_EXPR_EVAL_ASYNC_H
#define SPX_EXPR_EVAL_ASYNC_H

#include <future>
#include <thread>

namespace spx
{

// head forward
//
template <Expression_iterator E, typename F, typename X, typename C>
  inline void linear_evaluate( E& expr, 
                               F&& f,
                               bool use_common_stride, 
                               X extent, 
                               C common_stride,
                               std::size_t outer_dim );
  
template <Expression_iterator E, typename F, typename X, typename C>
  inline void linear_evaluate_async( E& expr, 
                                     F&& f,
                                     bool use_common_stride, 
                                     X extent, 
                                     C common_stride,
                                     std::size_t outer_dim )
  {
    if( use_common_stride )
    {
      std::size_t np = runtime_consts::num_threads;
      
      if( np <= 1 || extent < 1024 )
      {
        linear_evaluate( expr, 
                         std::forward<F>(f), 
                         true, 
                         extent, 
                         common_stride, 
                         outer_dim );
        return;
      }

      std::size_t remain = extent % np;

      if( common_stride == 1 )
      {
        for( std::size_t i = 0; i < remain; ++i )
        {
          f( *expr ); 
          expr.advance_data();
        }
      }
      else
      {
        for( std::size_t i = 0; i < remain; ++i )
        {
          f( *expr );
          expr.advance_data( common_stride );
        }
      }
      
      if( extent < np )
        return;
      
      // async parallel
      //
      std::size_t load = extent - remain; // total work load without remain
      std::size_t part = load / np;       // load MUST be able to be equally partitioned

      std::vector<E> vec_e;
      for( std::size_t n = 0; n < np; ++n )
      {
        E e = expr;
        if( common_stride == 1 )
          e.advance_data( n * part );
        else
          e.advance_data( n * part * common_stride );
        vec_e.push_back( e );
      }

      auto task = [&]( auto th_id )
      {
        linear_evaluate( vec_e[ th_id ], 
                         std::forward<F>(f), 
                         true, 
                         part, 
                         common_stride, 
                         outer_dim );
      };

      std::vector<std::thread> thds;
      for( std::size_t n = 0; n < np; ++n )
        thds.push_back( std::thread( task, n ) ); // launch thread

      for( std::size_t n = 0; n < np; ++n )
        if( thds[ n ].joinable() )
          thds[ n ].join();
      
      // finalize -- advance expr itself
      //
      if( common_stride == 1 )
        expr.advance_data( remain );
      else
        expr.advance_data( remain * common_stride );
    }
    else 
    {
std::cout << "[WARNING] slow linear evaluation." << std::endl;      
      for( std::size_t i = 0; i < extent; ++i )
      {
        f( *expr );
        expr.advance_stride( outer_dim );
      }
    }    
  }
 
}

#endif // SPX_EXPR_EVAL_ASYNC_H

