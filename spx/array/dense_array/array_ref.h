#ifndef SPX_ARRAY_REF_H
#define SPX_ARRAY_REF_H

namespace spx
{

// -------------------------------------------------------------------------- //
// Array reference

template <Random_access_iterator P, Descriptor Desc>
  class array_ref : public arithmetic<array_ref<P, Desc>, Value_type<P>>
  {
  public:
    static constexpr std::size_t rank() { return Desc::rank(); }

    using descriptor_type = Desc;
    using value_type      = typename std::iterator_traits<P>::value_type;
    using size_type       = Size_type<Desc>;
    // Dense iterator
    using desc_iterator          = dense_iterator<descriptor_type, P>;
    using desc_const_iterator    = const dense_iterator<descriptor_type, P>;
    // Normal data iterator
    using iterator               = desc_iterator;
    using const_iterator         = desc_const_iterator;

  private:
    descriptor_type desc; // Descirbes the array_ref
    P ptr;                // Points to the first element of a array

  public:
    // Default construction
    array_ref() = delete;

    // Move semantics
    // FIXME: The implementation is not exception-safe.
    //array_ref(array_ref&& x);
    //array_ref& operator=(array_ref&& x);

    // Copy semantics
    //array_ref(const array_ref& x);
    //array_ref& operator=(const array_ref& x);

    // Descriptor initialization
    array_ref(const descriptor_type& s, P p);

    // Subarray conversion
    //
    // Allow implicit conversion from a non-const array_ref to a const
    // array_ref.
    template <typename U, typename Desc2>
      array_ref(const array_ref<U, Desc2>& x);

    template <typename U, typename Desc2>
      array_ref& operator=(const array_ref<U, Desc2>& x);

    // Destruction
    ~array_ref() = default;

    // Properties

    // Return the slice describing the array.
    const descriptor_type& descriptor() const { return desc; }

    // Returns the extent of the array in the nth dimension. 
    std::size_t extent(std::size_t n) const { return desc.extents[n]; }
    decltype(auto) extents() { return spx::extents(desc); }

    // Returns the number of rows (0th extent) in the array.
    std::size_t rows() const { return extent(0); }

    // Returns the number of columns (1st extent) in the array.
    std::size_t cols() const { return extent(1); }

    // Returns the total number of elements contained in the array.
    std::size_t size() const { return desc.size(); }

    // Data access
    P       data()       { return ptr; }
    const P data() const { return ptr; }

    // Subscripting
    //
    // Returns a reference to the element at the index given by the sequence
    // of indexes, args.
    
    template <Indexable... Args>
      decltype(auto) operator()( Args&&... args )
      {  
        return array_impl::subscript( *this, std::forward<Args>(args)... );
      }
      
    template <Indexable... Args>
      decltype(auto) operator()( Args&&... args ) const
      {  
        return array_impl::subscript( *this, std::forward<Args>(args)... );  
      }

    // Bouonds
    //
    // Returns lower bound index and upper bound index
    
    decltype(auto) lbounds() const { return spx::lbounds(desc); }
    decltype(auto) ubounds() const { return spx::ubounds(desc); }
    
    // Iterator
    //
    // Returns dense_iterator
    
    iterator       begin()       { return { desc, data(), lbounds() }; }
    iterator       end()         { return { desc, data(), spx::iter_ubounds(desc) }; }
    const_iterator begin() const { return { desc, data(), lbounds() }; }
    const_iterator end()   const { return { desc, data(), spx::iter_ubounds(desc) }; }

    desc_iterator       dense_begin()       { return { desc, data(), lbounds() }; }
    desc_iterator       dense_end()         { return { desc, data(), spx::iter_ubounds(desc) }; }
    desc_const_iterator dense_begin() const { return { desc, data(), lbounds() }; }
    desc_const_iterator dense_end()   const { return { desc, data(), spx::iter_ubounds(desc) }; }   
    
    // Assignment
    //
    template <typename U>
    requires Assignable<value_type&, U>()
          && not Expression_iterator<U>()
      decltype(auto) operator=( const U& u );
      
    template <Range R>
    requires Assignable<value_type&, range_value_type<R>>()
      decltype(auto) operator=( R&& r );
      
    template <typename U>
    requires Assignable<value_type&, U>()
      decltype(auto) operator=( std::initializer_list<U> r );
      
    template <Expression_iterator E>
      decltype(auto) operator=( E expr );
      
    // Apply functions
    //
    // [TODO]: constrain F = Binary_operation
    template <typename U, typename F>
    requires Assignable<value_type&, U>()
          && not Expression_iterator<U>()
      decltype(auto) apply( const U& u, F&& f );

    // [TODO]: constrain F = Binary_operation
    template <Range R, typename F>
    requires not Dense_array<R>()
          && Assignable<value_type&, range_value_type<R>>()
      decltype(auto) apply( R&& r, F&& f );

    // [TODO]: constrain F = Binary_operation
    template <Range R, typename F>
    requires Dense_array<R>()
          && Assignable<value_type&, range_value_type<R>>()
      decltype(auto) apply( R&& r, F&& f );
      
    // [TODO]: constrain F = Binary_operation
    template <typename U, typename F>
    requires Assignable<value_type&, U>()
      decltype(auto) apply( std::initializer_list<U> r, F&& f );
      
    // [TODO]: constrain F = Binary_operation
    template <Expression_iterator E, typename F>
      decltype(auto) apply( E expr, F&& f );
      
    // Slice @ a specific dimension I
    template <std::size_t I, Indexable_argument S>
      decltype(auto) operator()( size_constant<I>, S&& s )
      {
        return array_impl::subscript( *this, size_constant<I>(), std::forward<S>(s) );
      }
      
    template <std::size_t I, Indexable_argument S>
      decltype(auto) operator()( size_constant<I>, S&& s ) const
      {
        return array_impl::subscript( *this, size_constant<I>(), std::forward<S>(s) );
      }
      
    // operator [s]: slice s @ dimension 0
    template <Indexable_argument S>
    requires not Has_subscript<value_type, S>()
      decltype(auto) operator[]( S&& s )
      {
        return (*this)( size_constant<0>(), std::forward<S>(s) );
      }
      
    template <Indexable_argument S>
    requires not Has_subscript<value_type, S>()
      decltype(auto) operator[]( S&& s ) const
      {
        return (*this)( size_constant<0>(), std::forward<S>(s) );
      }   
      
    // operator [x]: expressible x
    template <Expressible X>
    requires Has_subscript<value_type, decltype( *as_expr(std::declval<X>()) )>()
      decltype(auto) operator[]( X&& x )
      {
        // return make_expr_func( ops::subscript(), *this, std::forward<X>(x) );
        return make_expr_func( []( auto&& u, auto&& v ){ return u[v]; }, 
                               *this, 
                               std::forward<X>(x) );
      }
    
    template <Expressible X>
    requires Has_subscript<value_type,  decltype( *as_expr(std::declval<X>()) )>() 
      decltype(auto) operator[]( X&& x ) const
      {
        // return make_expr_func( ops::subscript(), *this, std::forward<X>(x) );
         return make_expr_func( []( auto&& u, auto&& v ){ return u[v]; }, 
                                *this, 
                                std::forward<X>(x) );
      }

  
/*
    // Row
    //
    // Return a array_ref referring to the nth row.
    array_ref<T, N-1>       row(std::size_t n);
    array_ref<const T, N-1> row(std::size_t n) const;

    // Column
    //
    // Return a array_ref referring to the nth column.
    array_ref<T, N-1>       col(std::size_t n);
    array_ref<const T, N-1> col(std::size_t n) const;

    // Apply
    template <typename F>
      array_ref& apply(F f);

    template <typename M, typename F>
      array_ref& apply(const M& m, F f);


    // Scalar arithmetic
    array_ref& operator=(const value_type& x);
    array_ref& operator+=(const value_type& x);
    array_ref& operator-=(const value_type& x);
    array_ref& operator*=(const value_type& x);
    array_ref& operator/=(const value_type& x);
    array_ref& operator%=(const value_type& x);

    // Matrix arithmetic
    template <typename M>
      array_ref& operator+=(const M& m);
    
    template <typename M>
      array_ref& operator-=(const M& m);


    // Iterators
    iterator begin() { return {desc, ptr}; }
    iterator end()   { return {desc, ptr, true}; }

    const_iterator begin() const { return {desc, ptr}; }
    const_iterator end() const   { return {desc, ptr, true}; }


    void swap(array_ref& x);
    void swap_rows(std::size_t m, std::size_t n);
*/
  };
  
////////////////////////////////////////////////////////////////////////////
// factory methods
//////////////////////////////////////////////////////////////////////////////

template <Random_access_iterator P, Descriptor Desc>
  inline decltype(auto)
  make_array_ref( const Desc& desc, P first )
  {
    return array_ref<P, Desc>{ desc, first };
  }
 
/////////////////////////////////////////////////////////////////////////////
// Move semantics
//////////////////////////////////////////////////////////////////////////////
/*
template <typename T, std::size_t N>
  inline
  array_ref<T, N>::array_ref(array_ref&& x)
    : desc(x.desc), ptr(x.ptr)
  { }

template <typename T, std::size_t N>
  inline array_ref<T, N>&
  array_ref<T, N>::operator=(array_ref&& x)
  {
    assert(same_extents(desc, x.desc));
    // FIXME: This does not guarantee exception safety. If move throws, this
    // operator may leak resources. The algorithm must be specialized on
    // whether T is nothrow-movable.
    std::move(x.begin(), x.end(), begin());
    return *this;
  }
*/
/////////////////////////////////////////////////////////////////////////////
// Copy semantics
//////////////////////////////////////////////////////////////////////////////
/*
template <typename T, std::size_t N>
  inline
  array_ref<T, N>::array_ref(const array_ref& x)
    : desc(x.desc), ptr(x.ptr)
  { }

template <typename T, std::size_t N>
  inline array_ref<T, N>&
  array_ref<T, N>::operator=(const array_ref& x)
  {
    assert(same_extents(desc, x.desc));
    copy(x.begin(), x.end(), begin());
    return *this;
  }
  
template <typename T, std::size_t N>
  template <typename U>
    inline
    array_ref<T, N>::array_ref(const array_ref<U, N>& x)
      : desc(x.descriptor()), ptr(x.ptr)
    { }

template <typename T, typename Desc>
  template <typename U>
    inline array_ref<T, N>&
    array_ref<T, N>::operator=(const array_ref<U, N>& x)
    {
      // FIXME: Is this right? Should we just assign values or resize the
      // vector based o what x is?
      assert(same_extents(desc, x.descriptor()));
      apply(x, [](T& a, const U& b) { a = b; });
      return *this;
    }
*/

/////////////////////////////////////////////////////////////////////////////
// Constructors
//////////////////////////////////////////////////////////////////////////////

template <Random_access_iterator P, Descriptor Desc>
  inline
  array_ref<P, Desc>::array_ref(const Desc& s, P p)
    : desc(s), ptr(p)
  { }


//////////////////////////////////////////////////////////////////////////////
// member function
//////////////////////////////////////////////////////////////////////////////

// Assignment

template <Random_access_iterator P, Descriptor Desc>
  template <typename U>
  requires Assignable<typename array_ref<P, Desc>::value_type&, U>()
        && not Expression_iterator<U>()  
    inline decltype(auto) 
    array_ref<P, Desc>::operator=( const U& u )
    {
      return apply( u, ops::assign() );
    }
      
template <Random_access_iterator P, Descriptor Desc>
  template <Range R>
  requires Assignable<typename array_ref<P, Desc>::value_type&, range_value_type<R>>()
    inline decltype(auto) 
    array_ref<P, Desc>::operator=( R&& r )
    {
      return apply( std::forward<R>(r), ops::assign() );
    }
    
template <Random_access_iterator P, Descriptor Desc>
  template <typename U>
  requires Assignable<typename array_ref<P, Desc>::value_type&, U>()
    inline decltype(auto) 
    array_ref<P, Desc>::operator=( std::initializer_list<U> r )
    {
      return apply( r, ops::assign() );
    }
     
template <Random_access_iterator P, Descriptor Desc>
  template <Expression_iterator E>
    inline decltype(auto) 
    array_ref<P, Desc>::operator=( E expr )
    {
      return apply( expr, ops::assign() );
    }
    
// Apply functions

template <Random_access_iterator P, Descriptor Desc>
  template <typename U, typename F>
  requires Assignable<typename array_ref<P, Desc>::value_type&, U>()
        && not Expression_iterator<U>()  
    inline decltype(auto) 
    array_ref<P, Desc>::apply( const U& u, F&& f )
    {
      return apply( as_expr(u), std::forward<F>(f) );
    }

template <Random_access_iterator P, Descriptor Desc>
  template <Range R, typename F>
  requires not Dense_array<R>()
        && Assignable<typename array_ref<P, Desc>::value_type&, range_value_type<R>>()
    inline decltype(auto) 
    array_ref<P, Desc>::apply( R&& r, F&& f )
    {
      using std::begin;
      using std::end;
      decltype(auto) iter_i = begin(*this);
      decltype(auto) iter_j = begin(r);
      for( ; iter_j != end(r); iter_i++, iter_j++ )
        f( *iter_i, *iter_j );
      return *this;
    }

template <Random_access_iterator P, Descriptor Desc>
  template <Range R, typename F>
  requires Dense_array<R>()
        && Assignable<typename array_ref<P, Desc>::value_type&, range_value_type<R>>()
    inline decltype(auto) 
    array_ref<P, Desc>::apply( R&& r, F&& f )
    {
      return apply( as_expr(r), std::forward<F>(f) );
    }
          
template <Random_access_iterator P, Descriptor Desc>
  template <typename U, typename F>
  requires Assignable<typename array_ref<P, Desc>::value_type&, U>()
    inline decltype(auto) 
    array_ref<P, Desc>::apply( std::initializer_list<U> r, F&& f )
    {
      using std::begin;
      using std::end;
      decltype(auto) iter_i = begin(*this);
      decltype(auto) iter_j = begin(r);
      for( ; iter_j != end(r); iter_i++, iter_j++ )
        f( *iter_i, *iter_j );
      return *this;
    }
      
template <Random_access_iterator P, Descriptor Desc>
  template <Expression_iterator E, typename F>
    inline decltype(auto) 
    array_ref<P, Desc>::apply( E expr, F&& f )
    {
      eval( make_expr_func( std::forward<F>(f), 
                            as_expr(*this), 
                            expr ) );
      return *this;
    }
      
/*
template <typename T, std::size_t N>
  inline array_ref<T, N-1>
  array_ref<T, N>::row(std::size_t n)
  {
    assert(n < extent(0));
    array_slice<N-1> row(desc, size_constant<0>(), n);
    return {ptr, row};
  }

template <typename T, std::size_t N>
  inline array_ref<const T, N-1>
  array_ref<T, N>::row(std::size_t n) const
  {
    assert(n < extent(0));
    array_slice<N-1> row(desc, size_constant<0>(), n);
    return {row, ptr};
  }


template <typename T, std::size_t N>
  inline array_ref<T, N-1>
  array_ref<T, N>::col(std::size_t n)
  {
    assert(n < extent(1));
    array_slice<N-1> col(desc, size_constant<1>(), n);
    return {ptr, col};
  }

template <typename T, std::size_t N>
  inline array_ref<const T, N-1>
  array_ref<T, N>::col(std::size_t n) const
  {
    assert(n < extent(1));
    array_slice<N-1> col(desc, size_constant<1>(), n);
    return {ptr, col};
  }



template <typename T, std::size_t N>
  template <typename F>
    inline array_ref<T, N>&
    array_ref<T, N>::apply(F f)
    {
      for (auto i = begin(); i != end(); ++i)
        f(*i);
      return *this;
    }

template <typename T, std::size_t N>
  template <typename M, typename F>
    inline array_ref<T, N>&
    array_ref<T, N>::apply(const M& m, F f)
    {
      assert(same_extents(desc, m.descriptor()));
      auto i = begin();
      auto j = m.begin();
      while (i != end()) {
        f(*i, *j);
        ++i;
        ++j;
      }
      return *this;
    }


// Scalar assignment
template <typename T, std::size_t N>
  inline array_ref<T, N>& 
  array_ref<T, N>::operator=(const value_type& value)  
  {
    return apply([&](T& x) { x = value; });
  }
    
// Scalar addition.
template <typename T, std::size_t N>
  inline array_ref<T, N>& 
  array_ref<T, N>::operator+=(const value_type& value) 
  { 
    return apply([&](T& x) { x += value; });
  }

// Scalar subtraction
template <typename T, std::size_t N>
  inline array_ref<T, N>& 
  array_ref<T, N>::operator-=(value_type const& value) 
  { 
    return apply([&](T& x) { x -= value; });
  }

// Scalar multiplication
template <typename T, std::size_t N>
  inline array_ref<T, N>& 
  array_ref<T, N>::operator*=(value_type const& value) 
  { 
    return apply([&](T& x) { x *= value; });
  }

// Scalar division
template <typename T, std::size_t N>
  inline array_ref<T, N>& 
  array_ref<T, N>::operator/=(value_type const& value) 
  { 
    return apply([&](T& x) { x /= value; });
  }

// Scalar modulus
// This operation is only defined when T is a model of Euclidean domain.
template <typename T, std::size_t N>
  inline array_ref<T, N>& 
  array_ref<T, N>::operator%=(value_type const& value) 
  { 
    return apply([&](T& x) { x %= value; });
  }

// Matrix addition
template <typename T, std::size_t N>
  template <typename M>
    inline array_ref<T, N>&
    array_ref<T, N>::operator+=(const M& m)
    {
      using U = Value_type<M>;
      return apply(m, [&](T& t, const U& u) { t += u; });
    }

// Matrix subtraction
template <typename T, std::size_t N>
  template <typename M>
    inline array_ref<T, N>&
    array_ref<T, N>::operator-=(const M& m)
    {
      using U = Value_type<M>;
      return apply(m, [&](T& t, const U& u) { t -= u; });
    }


  // Swap
  template <typename T, std::size_t N>
    inline void
    array_ref<T, N>::swap(array_ref& x)
    {
      using std::swap;
      swap(desc, x.desc);
      swap(ptr, x.ptr);
    }

  // Swap rows
  template <typename T, std::size_t N>
    inline void
    array_ref<T, N>::swap_rows(std::size_t m, std::size_t n)
    {
      auto a = (*this)[m];
      auto b = (*this)[n];
      std::swap_ranges(a.begin(). a.end(), b.begin());
    }

*/

} // namespace spx

#endif // SPX_ARRAY_REF_H
