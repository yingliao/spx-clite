#ifndef SPX_SPARSE_ITERATOR_H
#define SPX_SPARSE_ITERATOR_H

namespace spx
{

template <Sparse_data_generator S>
  struct sparse_iterator
  {
    using value_type        = typename S::value_type;
    using reference         = value_type&;
    using pointer           = value_type*;
    using difference_type   = std::ptrdiff_t;
    using iterator_category = std::random_access_iterator_tag;

  private:
    S*             data;
    std::ptrdiff_t id;

  public:
      
    // Copyable
    sparse_iterator( const sparse_iterator& ) = default;
    sparse_iterator& operator=( const sparse_iterator& ) = default;
    
    // Moveable
    sparse_iterator( sparse_iterator&& ) = default;
    sparse_iterator& operator=( sparse_iterator&& ) = default;

    // Constructor
    //    
    sparse_iterator( S& data, std::ptrdiff_t id )
      : data( &data ), id( id )
    {}

    // Return wrapping data
    decltype(auto) descriptor() const { return data->descriptor(); }

    // Readable
    decltype(auto) operator*()  const { return data->get( id ); }
    decltype(auto) operator->() const { return &(data->get( id )); }

    // Forward Iterator
    sparse_iterator& operator++()
    {
      ++id;
      return *this;
    }

    sparse_iterator  operator++(int)
    {
      sparse_iterator x = *this;
      ++(*this);
      return x;
    }

    sparse_iterator& operator--()
    {
      --id;
      return *this;
    }

    sparse_iterator  operator--(int)
    {
      sparse_iterator x = *this;
      --(*this);
      return x;
    }
    
    sparse_iterator& operator+=( const difference_type& n )
    {
      id += n;
      return *this;
    }

    sparse_iterator& operator-=( const difference_type& n )
    {
      id -= n;
      return *this;
    }

    sparse_iterator  operator+( const difference_type& n ) const
    { 
      return sparse_iterator( *data, id + n );;
    }

    friend 
    sparse_iterator operator+( const difference_type& n,  const sparse_iterator& x )
    {
        return x + n;
    }
    
    sparse_iterator  operator-( const difference_type& n ) const
    { 
      return sparse_iterator( *data, id - n );
    }
    
    // Difference between two iterators
    difference_type operator-( const sparse_iterator& x ) const
    {
      return id - x.id;
    }

    decltype(auto) operator[]( const difference_type& n ) const
    {
      return data->get( id + n );
    }

    // Equality_comparable

    bool operator==( const sparse_iterator<S>& b ) const
    {
      return id == b.id;
    }

    bool operator!=( const sparse_iterator<S>& b ) const
    {
      return id != b.id;
    }

  };

} // namespace spx

#endif // SPX_SPARSE_ITERATOR_H
