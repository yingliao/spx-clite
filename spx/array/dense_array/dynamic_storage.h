#ifndef SPX_DYNAMIC_STORAGE_H
#define SPX_DYNAMIC_STORAGE_H

namespace spx
{
template <typename T, Descriptor Desc>
  class dynamic_storage
  {
  public:
    static constexpr std::size_t rank() { return Desc::rank(); }
  
    using base_t                 = std::vector<T>;
    using value_type             = typename base_t::value_type;
    using pointer                = typename base_t::pointer;
    using const_pointer          = typename base_t::const_pointer;
    using reference              = typename base_t::reference;
    using const_reference        = typename base_t::const_reference;
    using size_type              = typename base_t::size_type;
    using difference_type        = typename base_t::difference_type;
    // Normal data iterator
    using iterator               = typename base_t::iterator;
    using const_iterator         = typename base_t::const_iterator;
    using reverse_iterator       = typename base_t::reverse_iterator;
    using const_reverse_iterator = typename base_t::const_reverse_iterator;
    // descriptor
    using descriptor_type        = Desc;

  protected:
    descriptor_type desc; // dense descriptor
    base_t elems;
    
  public:
    // Default construction
    dynamic_storage() = default;
    
    // Destruction
    ~dynamic_storage() = default;
    
    // Move semantics
    dynamic_storage( dynamic_storage&& ) = default;
    dynamic_storage& operator=( dynamic_storage&& ) = default;

    // Copy semantics
    dynamic_storage( const dynamic_storage& x ) = default;
    dynamic_storage& operator=( const dynamic_storage& x ) = default;
    
    // Constructors      
    //
    // 1D: v is init values; ND: v is shape vector
    template <typename D>
    requires (Desc::rank() == 1)
      dynamic_storage( std::initializer_list<D> v );
 
    template <typename D>
    requires (Desc::rank() != 1)
      dynamic_storage( std::initializer_list<D> v );
      
    template <typename... D>
    requires (sizeof...(D) == Desc::rank())
          && All(Convertible<D, size_type>()...)
      dynamic_storage( D... shape );
      
    template <Input_range R>
    requires not Ranked_storage<Main_type<R>>()
      dynamic_storage( R&& shape );

    template <Input_range R>
    requires Ranked_storage<Main_type<R>>()
          && (Desc::rank() > Main_type<R>::rank())
      dynamic_storage( R&& x ); // x is a shape vector
 
    template <Input_range R>
    requires Ranked_storage<Main_type<R>>()
          && (Desc::rank() == Main_type<R>::rank())
      dynamic_storage( R&& x ); // COPY!
    
    explicit dynamic_storage( size_type shape );
      
    // Member functions
    //  
    decltype(auto) size()       const { return desc.size(); }

    const descriptor_type& descriptor() const { return desc; }
          descriptor_type& descriptor()       { return desc; }

    // Resize by Range R
    template <Range R>
      void resize( R&& shape );
      
    // Resize by std::initializer_list
    template <typename D>
      void resize( std::initializer_list<D> shape );

    // Resize by variadic size_type
    template <typename... D>
    requires (sizeof...(D) == Desc::rank())      
      void resize( D... shape );
      
    // Resize by the shape iterator
    template <Input_iterator I>
      void resize( I first, I last );
          
    // Apply functions
    //
    // [TODO]: constrain F = Binary_operation
    template <Input_iterator I, typename F>
    requires Assignable<T&, Value_type<I>>()
      void apply( I first, I last, F&& f );
      
    // wrapper functions
    T*                     data()          { return elems.data(); }
    const T*               data()    const { return elems.data(); }
    iterator               begin()         { return elems.begin(); }
    const_iterator         begin()   const { return elems.begin(); }
    const_iterator         cbegin()  const { return elems.cbegin(); }
    iterator               end()           { return elems.end(); }
    const_iterator         end()     const { return elems.end(); }
    const_iterator         cend()    const { return elems.cend(); }
    reverse_iterator       rbegin()        { return elems.rbegin(); }
    const_reverse_iterator rbegin()  const { return elems.rbegin(); }
    const_reverse_iterator crbegin() const { return elems.crbegin(); }
    reverse_iterator       rend()          { return elems.rend(); }
    const_reverse_iterator rend()    const { return elems.rend(); }
    const_reverse_iterator crend()   const { return elems.crend(); }
  };
  
//////////////////////////////////////////////////////////////////////////////
// constructor
//////////////////////////////////////////////////////////////////////////////

template <typename T, Descriptor Desc>
  template <typename D>
  requires (Desc::rank() == 1)
    dynamic_storage<T, Desc>::dynamic_storage( std::initializer_list<D> v )
    {
      desc = descriptor_type( {v.size()} ); // 1D: v is init values
      elems.resize( size() );
      elems.assign( v.begin(), v.end() );
    }
 
template <typename T, Descriptor Desc>
  template <typename D>
  requires (Desc::rank() != 1)
    dynamic_storage<T, Desc>::dynamic_storage( std::initializer_list<D> v )
    {
      static_assert( rank() >= 1, "rank() >= 1 is required" );
      desc = descriptor_type( v ); // ND: v is shape vector
      elems.resize( size() );
    }
         
template <typename T, Descriptor Desc>
  template <typename... D>
  requires (sizeof...(D) == Desc::rank())
        && All(Convertible<D, typename dynamic_storage<T, Desc>::size_type>()...)
    dynamic_storage<T, Desc>::dynamic_storage( D... shape )
      : desc( {shape...} )
    {
      elems.resize( size() );
    }
    
template <typename T, Descriptor Desc>
  template <Input_range R>
  requires not Ranked_storage<Main_type<R>>()
    dynamic_storage<T, Desc>::dynamic_storage( R&& shape )
      : desc( std::forward<R>(shape) )
    {
      elems.resize( size() );
    }

template <typename T, Descriptor Desc>
  template <Input_range R>
  requires Ranked_storage<Main_type<R>>()
        && (Desc::rank() > Main_type<R>::rank())
    dynamic_storage<T, Desc>::dynamic_storage( R&& x )
    {
      using std::begin;
      using std::end;
      desc.resize( begin(x), end(x) );
      elems.resize( size() );
    }

template <typename T, Descriptor Desc>
  template <Input_range R>
  requires Ranked_storage<Main_type<R>>()
        && (Desc::rank() == Main_type<R>::rank())
    dynamic_storage<T, Desc>::dynamic_storage( R&& x )
    {
      using std::begin;
      using std::end;
      decltype(auto) desc_x = x.descriptor(); 
      auto b = begin( desc_x.extents );
      auto e = end( desc_x.extents );
      desc.resize( b, e );
      elems.resize( size() );
      std::copy( begin(x), end(x), begin(elems) );
  }

template <typename T, Descriptor Desc>
  dynamic_storage<T, Desc>::dynamic_storage( typename dynamic_storage<T, Desc>::size_type shape )
  {
    std::array<size_type, rank()> d;
    d.fill( shape );
    desc = descriptor_type( d );
    elems.resize( size() );
  }    
  
//////////////////////////////////////////////////////////////////////////////
// member functions
//////////////////////////////////////////////////////////////////////////////

template <typename T, Descriptor Desc>
  template <Input_iterator I, typename F>
  requires Assignable<T&, Value_type<I>>()
    inline void
    dynamic_storage<T, Desc>::apply( I first, I last, F&& f )
    {
      decltype(auto) iter = elems.begin();
      for( ; first != last; ++first, ++iter )
        f( *iter, *first );
    }

template <typename T, Descriptor Desc>
  template <Range R>
    inline void 
    dynamic_storage<T, Desc>::resize( R&& shape )
    {
      using std::begin;
      using std::end;
      resize( begin(shape), end(shape) );
    }
    
template <typename T, Descriptor Desc>
  template <typename D>
    inline void 
    dynamic_storage<T, Desc>::resize( std::initializer_list<D> shape )
    {
      resize( shape.begin(), shape.end() );
    }

template <typename T, Descriptor Desc>
  template <typename... D>
  requires (sizeof...(D) == Desc::rank())
    inline void 
    dynamic_storage<T, Desc>::resize( D... shape )
    {
      resize( {shape...} );
    }
    
template <typename T, Descriptor Desc>
  template <Input_iterator I>
    inline void 
    dynamic_storage<T, Desc>::resize( I first, I last )
    {
      desc.resize( first, last );
      elems.resize( size() );
    }
    
} // namespace spx

#endif // SPX_DYNAMIC_STORAGE_H
