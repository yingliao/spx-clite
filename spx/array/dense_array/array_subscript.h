#ifndef SPX_ARRAY_SUBSCRIPT_H
#define SPX_ARRAY_SUBSCRIPT_H

namespace spx
{

// Header forward
template <Random_access_iterator P, Descriptor Desc>
  inline decltype(auto) make_array_ref( const Desc& desc, P first );

namespace type_impl
{
  template <typename A, typename... Args>
    struct subarray_desc
    {
    private:
      template <typename X, typename... Y>
        static constexpr auto check( X&& arr, Y&&... args ) 
          -> decltype( arr.descriptor()( std::forward<Y>(args)... ) );
      static constexpr subst_failure check( ... );

    public:
      using type = decltype( check(std::declval<A>(), std::declval<Args>()...) ); 
    };

  template <typename A, std::size_t I, typename S>
    struct subarray_desc<A, size_constant<I>, S>
    {
    private:
      template <typename X, typename Y>
        static constexpr auto check( X&& arr, Y&& s ) 
          -> decltype( arr.descriptor()( size_constant<I>(), std::forward<S>(s) ) );
      static constexpr subst_failure check( ... );

    public:
      using type = decltype( check(std::declval<A>(), std::declval<S>()) ); 
    };
}

template <typename A, typename... Args>
  concept bool Subarray()
  {
    return Dense_array<A>()
        && All( Indexable<Args>()... )
        && requires( A arr, Args... args ) {
             requires Descriptor<typename type_impl::subarray_desc<A, Args...>::type>();
           };
  }
     
template <typename A, std::size_t I, typename S>
  concept bool Subarray()
  {
    return Dense_array<A>()
        && Indexable_argument<S>()
        && requires( A arr, S s ) {
             requires Descriptor<typename type_impl::subarray_desc<A, size_constant<I>, S>::type>();
           };
  }

// Array subscript for all operator()( ... ) in dense-like multi-array
namespace array_impl
{    
  template <Dense_array A, Indexable... Args>
  requires not Subarray<A, Args...>()
    inline decltype(auto) 
    subscript( A& arr, Args&&... args )
     {
       //TODO: check bounds
       return *( arr.data() + arr.descriptor()( args... ) );
     }
    
  template <Dense_array A, Indexable... Args>
  requires Subarray<A, Args...>()
    inline decltype(auto) subscript( A& arr, Args&&... args )
     {
       return make_array_ref( arr.descriptor()( args... ), arr.data() );
     }

  // subscript @ dimension I
  template <Dense_array A, std::size_t I, Indexable_argument S>
  requires not Subarray<A, I, S>()
    inline decltype(auto) subscript( A& arr, size_constant<I>, S&& s )
    {
       return subscript( arr, std::forward<S>(s) );
    }

  // slicing @ dimension I
  template <Dense_array A, std::size_t I, Indexable_argument S>
  requires Subarray<A, I, S>()
    inline decltype(auto) subscript( A& arr, size_constant<I>, S&& s )
    {
       return make_array_ref( arr.descriptor()( size_constant<I>(), 
                                                std::forward<S>(s) ), 
                              arr.data() );
    }

} // namespace array_impl
} // namespace spx

#endif // SPX_ARRAY_SUBSCRIPT_H
