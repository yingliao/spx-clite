#ifndef SPX_STATIC_STORAGE_H
#define SPX_STATIC_STORAGE_H

namespace spx
{
template <typename T, Descriptor Desc, std::size_t... D>
requires (sizeof...(D) == Desc::rank())
  class static_storage : 
    public std::array<T, product<std::size_t, D...>()>
  {
  public:
    static constexpr std::size_t rank() { return Desc::rank(); }
    static constexpr std::size_t size() { return product<std::size_t, D...>(); }
  
    using base_t                 = std::array<T, size()>;
    using value_type             = typename base_t::value_type;
    using pointer                = typename base_t::pointer;
    using const_pointer          = typename base_t::const_pointer;
    using reference              = typename base_t::reference;
    using const_reference        = typename base_t::const_reference;
    using size_type              = typename base_t::size_type;
    using difference_type        = typename base_t::difference_type;
    // Normal data iterator
    using iterator               = typename base_t::iterator;
    using const_iterator         = typename base_t::const_iterator;
    using reverse_iterator       = typename base_t::reverse_iterator;
    using const_reverse_iterator = typename base_t::const_reverse_iterator;
    // descriptor
    using descriptor_type        = Desc;
  
  protected:
    static descriptor_type desc; // static dense descriptor

  public:
    // Default construction
    static_storage() = default;
    
    // Destruction
    ~static_storage() = default;
    
    // Move semantics
    static_storage(static_storage&&) = default;
    static_storage& operator=(static_storage&&) = default;

    // Copy semantics
    static_storage(const static_storage&) = default;
    static_storage& operator=(const static_storage&) = default;      
    
    // Constructors
    template <typename U>
    requires Assignable<T&, U>()
      static_storage( std::initializer_list<U> init_vals );
      
    template <Input_range R>
    requires Assignable<T&, range_value_type<R>>()
      static_storage( R&& init_vals );
      
    template <typename U>
    requires Assignable<T&, U>()
      explicit static_storage( const U& init_val );
     
    // Member functions
    //
    const descriptor_type& descriptor() const { return desc; }

    // Apply functions
    //
    // [TODO]: constrain F = Binary_operation
    template <Input_iterator I, typename F>
    requires Assignable<T&, Value_type<I>>()
          && (static_storage<T, Desc, D...>::size() < compile_options::max_template_depth())
      void apply( I first, I last, F&& f );

    template <Input_iterator I, typename F>
    requires Assignable<T&, Value_type<I>>()
          && (static_storage<T, Desc, D...>::size() >= compile_options::max_template_depth())
      void apply( I first, I last, F&& f );    
  };
    
// static member descriptor
template <typename T, Descriptor Desc, std::size_t... D>
requires (sizeof...(D) == Desc::rank())
  Desc static_storage<T, Desc, D...>::desc = Desc({D...});
    
//////////////////////////////////////////////////////////////////////////////
// constructor
//////////////////////////////////////////////////////////////////////////////
  
template <typename T, Descriptor Desc, std::size_t... D>
requires (sizeof...(D) == Desc::rank())
  template <typename U>
  requires Assignable<T&, U>()
    static_storage<T, Desc, D...>::static_storage( std::initializer_list<U> init_vals )
    {
      assert( init_vals.size() == size() || init_vals.size() == 1 );
      
      if( init_vals.size() == 1 )
        base_t::fill( *init_vals.begin() );
      else
        std::copy( init_vals.begin(), init_vals.end(), base_t::begin() );
    }  

template <typename T, Descriptor Desc, std::size_t... D>
requires (sizeof...(D) == Desc::rank())
  template <Input_range R>
  requires Assignable<T&, range_value_type<R>>()
    static_storage<T, Desc, D...>::static_storage( R&& init_vals )
    {
      std::copy( begin(init_vals), end(init_vals), base_t::begin() );
    }
    
template <typename T, Descriptor Desc, std::size_t... D>
requires (sizeof...(D) == Desc::rank())
  template <typename U>
  requires Assignable<T&, U>()
    static_storage<T, Desc, D...>::static_storage( const U& init_val )
    {
      std::fill( base_t::begin(), base_t::end(), init_val );
    }
  
//////////////////////////////////////////////////////////////////////////////
// member functions
//////////////////////////////////////////////////////////////////////////////
 
namespace array_impl
{
  template <std::size_t I, std::size_t N, typename F, typename X, typename Y>
  requires (I == N)
    void apply_impl( F&& f, X iter_i, Y iter_j )
    {}

  template <std::size_t I, std::size_t N, typename F, typename X, typename Y>
  requires (I < N)
    void apply_impl( F&& f, X iter_i, Y iter_j )
    {
      f( *iter_i++, *iter_j++ );
      apply_impl<I+1, N>( f, iter_i, iter_j );
    }
}
  
template <typename T, Descriptor Desc, std::size_t... D>
requires (sizeof...(D) == Desc::rank())
  template <Input_iterator I, typename F>
  requires Assignable<T&, Value_type<I>>()
        && (static_storage<T, Desc, D...>::size() < compile_options::max_template_depth())
    inline void
    static_storage<T, Desc, D...>::apply( I first, I last, F&& f )
    {
      array_impl::apply_impl<0, size()>( std::forward<F>(f), base_t::begin(), first );
    }

template <typename T, Descriptor Desc, std::size_t... D>
requires (sizeof...(D) == Desc::rank())
  template <Input_iterator I, typename F>
  requires Assignable<T&, Value_type<I>>()
        && (static_storage<T, Desc, D...>::size() >= compile_options::max_template_depth())
    inline void
    static_storage<T, Desc, D...>::apply( I first, I last, F&& f )
    {
      decltype(auto) iter = base_t::begin();
      for( ; first != last; ++first, ++iter )
        f( *iter, *first );
    }
 
} // namespace spx

#endif // SPX_STATIC_STORAGE_H
