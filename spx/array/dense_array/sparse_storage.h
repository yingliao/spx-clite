#ifndef SPX_SPARSE_STORAGE_H
#define SPX_SPARSE_STORAGE_H

#include <iterator>

namespace spx
{

// S: data generator
template <Sparse_data_generator S>
  class sparse_storage
  {
  public:
    using T                      = typename S::value_type; 
    using value_type             = T;
    using pointer                = T*;
    using const_pointer          = const T*;
    using reference              = T&;
    using const_reference        = const T&;
    using size_type              = std::size_t;
    using difference_type        = std::ptrdiff_t;
    // descriptor
    using descriptor_type        = typename S::descriptor_type;
    // Normal data iterator
    using iterator               = sparse_iterator<S>;
    using const_iterator         = sparse_iterator<const S>;
    using reverse_iterator       = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;
    
    static constexpr std::size_t rank() { return descriptor_type::rank(); }
  
  protected:
    S datagen;
    
  public:
    // Destruction
    ~sparse_storage() = default;
    
    // Move semantics
    sparse_storage( sparse_storage&& ) = default;
    sparse_storage& operator=( sparse_storage&& ) = default;

    // Copy semantics
    sparse_storage( const sparse_storage& ) = default;
    sparse_storage& operator=( const sparse_storage& ) = default;      
    
    // Constructors      
    //
    sparse_storage( S&& s )
      : datagen( std::move(s) )
    {}
 
    // Member functions
    //  
    decltype(auto) size()       const { return descriptor().size(); }
    decltype(auto) descriptor() const { return datagen.descriptor(); } 

    const S& data_gen() const { return datagen; }
          S& data_gen()       { return datagen; }
   
    template <bool dummy = true>
    requires Sparse_databank<S>()
      void insert_on() { datagen.insert_on(); }

    template <bool dummy = true>
    requires Sparse_databank<S>()
      void insert_off() { datagen.insert_off(); }

    // Resize by Range R
    template <Range R>
      void resize( R&& shape );
      
    // Resize by std::initializer_list
    template <typename D>
      void resize( std::initializer_list<D> shape );

    // Resize by variadic size_type
    template <typename... D>
    requires (sizeof...(D) == sparse_storage<S>::rank())      
      void resize( D... shape );
      
    // Resize by the shape iterator
    template <Input_iterator I>
      void resize( I first, I last );
          
    // Apply functions
    //
    // [TODO]: constrain F = Binary_operation
    template <Input_iterator I, typename F>
    requires Assignable<T&, Value_type<I>>()
      void apply( I first, I last, F&& f );
      
    // wrapper functions
    iterator               data()          { return begin(); }
    const_iterator         data()    const { return begin(); }
    iterator               begin()         { return iterator( datagen, 0 ); }
    const_iterator         begin()   const { return const_iterator( datagen, 0 ); }
    // const_iterator         cbegin()  const { return elems.cbegin(); }
    iterator               end()           { return iterator( datagen, size() ); }
    const_iterator         end()     const { return const_iterator( datagen, size() ); }
    // const_iterator         cend()    const { return elems.cend(); }
    // reverse_iterator       rbegin()        { return elems.rbegin(); }
    // const_reverse_iterator rbegin()  const { return elems.rbegin(); }
    // const_reverse_iterator crbegin() const { return elems.crbegin(); }
    // reverse_iterator       rend()          { return elems.rend(); }
    // const_reverse_iterator rend()    const { return elems.rend(); }
    // const_reverse_iterator crend()   const { return elems.crend(); }
  };
  
//////////////////////////////////////////////////////////////////////////////
// member functions
//////////////////////////////////////////////////////////////////////////////

template <Sparse_data_generator S>
  template <Input_iterator I, typename F>
  requires Assignable<typename sparse_storage<S>::T&, Value_type<I>>()
    inline void
    sparse_storage<S>::apply( I first, I last, F&& f )
    {
      decltype(auto) iter = begin();
      for( ; first != last; ++first, ++iter )
        f( *iter, *first );
    }

template <Sparse_data_generator S>
  template <Range R>
    inline void 
    sparse_storage<S>::resize( R&& shape )
    {
      using std::begin;
      using std::end;
      resize( begin(shape), end(shape) );
    }
    
template <Sparse_data_generator S>
  template <typename D>
    inline void 
    sparse_storage<S>::resize( std::initializer_list<D> shape )
    {
      resize( shape.begin(), shape.end() );
    }

template <Sparse_data_generator S>
  template <typename... D>
  requires (sizeof...(D) == sparse_storage<S>::rank())
    inline void 
    sparse_storage<S>::resize( D... shape )
    {
      resize( {shape...} );
    }
    
template <Sparse_data_generator S>
  template <Input_iterator I>
    inline void 
    sparse_storage<S>::resize( I first, I last )
    {
      datagen.resize( first, last );
    }
    
} // namespace spx

#endif // SPX_SPARSE_STORAGE_H
