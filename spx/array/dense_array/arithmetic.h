#ifndef SPX_ARITHMETIC_H
#define SPX_ARITHMETIC_H

namespace spx
{

template <typename D, typename T>
  struct arithmetic
  {
    // Scalar arithmetic
    
    template <typename U>
    requires Has_plus_assign<T, U>()
      decltype(auto) operator+=( const U& u )
      {  return static_cast<D&>(*this).apply( u, ops::plus_assign() );  }
      
    template <typename U>
    requires Has_minus_assign<T, U>()
      decltype(auto) operator-=( const U& u )
      {  return static_cast<D&>(*this).apply( u, ops::minus_assign() );  }
      
    template <typename U>
    requires Has_multiplies_assign<T, U>()
      decltype(auto) operator*=( const U& u )
      {  return static_cast<D&>(*this).apply( u, ops::multiplies_assign() );  }
      
    template <typename U>
    requires Has_divides_assign<T, U>()
      decltype(auto) operator/=( const U& u )
      {  return static_cast<D&>(*this).apply( u, ops::divides_assign() );  }
      
    template <typename U>
    requires Has_modulus_assign<T, U>()
      decltype(auto) operator%=( const U& u )
      {  return static_cast<D&>(*this).apply( u, ops::modulus_assign() );  }
      
    template <typename U>
    requires Has_bit_and_assign<T, U>()
      decltype(auto) operator&=( const U& u )
      {  return static_cast<D&>(*this).apply( u, ops::bit_and_assign() );  }
      
    template <typename U>
    requires Has_bit_or_assign<T, U>()
      decltype(auto) operator|=( const U& u )
      {  return static_cast<D&>(*this).apply( u, ops::bit_or_assign() );  }
      
    template <typename U>
    requires Has_bit_xor_assign<T, U>()
      decltype(auto) operator^=( const U& u )
      {  return static_cast<D&>(*this).apply( u, ops::bit_xor_assign() );  }
      
    template <typename U>
    requires Has_left_shift_assign<T, U>()
      decltype(auto) operator<<=( const U& u )
      {  return static_cast<D&>(*this).apply( u, ops::left_shift_assign() );  }
      
    template <typename U>
    requires Has_right_shift_assign<T, U>()
      decltype(auto) operator>>=( const U& u )
      {  return static_cast<D&>(*this).apply( u, ops::right_shift_assign() );  }
      
    // Vector (Range) arithmetic
    
    template <Range R>
    requires Has_plus_assign<T, range_value_type<R>>()
      decltype(auto) operator+=( R&& r )
      {  return static_cast<D&>(*this).apply( std::forward<R>(r), ops::plus_assign() );  }
      
    template <Range R>
    requires Has_minus_assign<T, range_value_type<R>>()
      decltype(auto) operator-=( R&& r )
      {  return static_cast<D&>(*this).apply( std::forward<R>(r), ops::minus_assign() );  }
      
    template <Range R>
    requires Has_multiplies_assign<T, range_value_type<R>>()
      decltype(auto) operator*=( R&& r )
      {  return static_cast<D&>(*this).apply( std::forward<R>(r), ops::multiplies_assign() );  }
      
    template <Range R>
    requires Has_divides_assign<T, range_value_type<R>>()
      decltype(auto) operator/=( R&& r )
      {  return static_cast<D&>(*this).apply( std::forward<R>(r), ops::divides_assign() );  }
      
    template <Range R>
    requires Has_modulus_assign<T, range_value_type<R>>()
      decltype(auto) operator%=( R&& r )
      {  return static_cast<D&>(*this).apply( std::forward<R>(r), ops::modulus_assign() );  }
      
    template <Range R>
    requires Has_bit_and_assign<T, range_value_type<R>>()
      decltype(auto) operator&=( R&& r )
      {  return static_cast<D&>(*this).apply( std::forward<R>(r), ops::bit_and_assign() );  }
      
    template <Range R>
    requires Has_bit_or_assign<T, range_value_type<R>>()
      decltype(auto) operator|=( R&& r )
      {  return static_cast<D&>(*this).apply( std::forward<R>(r), ops::bit_or_assign() );  }
      
    template <Range R>
    requires Has_bit_xor_assign<T, range_value_type<R>>()
      decltype(auto) operator^=( R&& r )
      {  return static_cast<D&>(*this).apply( std::forward<R>(r), ops::bit_xor_assign() );  }
      
    template <Range R>
    requires Has_left_shift_assign<T, range_value_type<R>>()
      decltype(auto) operator<<=( R&& r )
      {  return static_cast<D&>(*this).apply( std::forward<R>(r), ops::left_shift_assign() );  }
      
    template <Range R>
    requires Has_right_shift_assign<T, range_value_type<R>>()
      decltype(auto) operator>>=( R&& r )
      {  return static_cast<D&>(*this).apply( std::forward<R>(r), ops::right_shift_assign() );  }
        
    // Initializer_list arithmetic
    
    template <typename U>
      decltype(auto) operator+=( std::initializer_list<U> r ) 
      {  return static_cast<D&>(*this).apply( r, ops::plus_assign() );  }
      
    template <typename U>
      decltype(auto) operator-=( std::initializer_list<U> r ) 
      {  return static_cast<D&>(*this).apply( r, ops::minus_assign() );  }
      
    template <typename U>
      decltype(auto) operator*=( std::initializer_list<U> r ) 
      {  return static_cast<D&>(*this).apply( r, ops::multiplies_assign() );  }
      
    template <typename U>
      decltype(auto) operator/=( std::initializer_list<U> r ) 
      {  return static_cast<D&>(*this).apply( r, ops::divides_assign() );  }
      
    template <typename U>
      decltype(auto) operator%=( std::initializer_list<U> r ) 
      {  return static_cast<D&>(*this).apply( r, ops::modulus_assign() );  }
      
    template <typename U>
      decltype(auto) operator&=( std::initializer_list<U> r ) 
      {  return static_cast<D&>(*this).apply( r, ops::bit_and_assign() );  }
      
    template <typename U>
      decltype(auto) operator|=( std::initializer_list<U> r ) 
      {  return static_cast<D&>(*this).apply( r, ops::bit_or_assign() );  }
      
    template <typename U>
      decltype(auto) operator^=( std::initializer_list<U> r ) 
      {  return static_cast<D&>(*this).apply( r, ops::bit_xor_assign() );  }
      
    template <typename U>
      decltype(auto) operator<<=( std::initializer_list<U> r ) 
      {  return static_cast<D&>(*this).apply( r, ops::left_shift_assign() );  }
      
    template <typename U>
      decltype(auto) operator>>=( std::initializer_list<U> r ) 
      {  return static_cast<D&>(*this).apply( r, ops::right_shift_assign() );  }
      
    // Expression_iterator
    
    template <Expression_iterator E>
    requires Has_plus_assign<T, Value_type<E>>()
      decltype(auto) operator+=( E e ) 
      {  return static_cast<D&>(*this).apply( e, ops::plus_assign() );  }
      
    template <Expression_iterator E>
    requires Has_minus_assign<T, Value_type<E>>()
      decltype(auto) operator-=( E e )
      {  return static_cast<D&>(*this).apply( e, ops::minus_assign() );  }
      
    template <Expression_iterator E>
    requires Has_multiplies_assign<T, Value_type<E>>()
      decltype(auto) operator*=( E e )
      {  return static_cast<D&>(*this).apply( e, ops::multiplies_assign() );  }
      
    template <Expression_iterator E>
    requires Has_divides_assign<T, Value_type<E>>()
      decltype(auto) operator/=( E e )
      {  return static_cast<D&>(*this).apply( e, ops::divides_assign() );  }
      
    template <Expression_iterator E>
    requires Has_modulus_assign<T, Value_type<E>>()
      decltype(auto) operator%=( E e )
      {  return static_cast<D&>(*this).apply( e, ops::modulus_assign() );  }
      
    template <Expression_iterator E>
    requires Has_bit_and_assign<T, Value_type<E>>()
      decltype(auto) operator&=( E e )
      {  return static_cast<D&>(*this).apply( e, ops::bit_and_assign() );  }
      
    template <Expression_iterator E>
    requires Has_bit_or_assign<T, Value_type<E>>()
      decltype(auto) operator|=( E e )
      {  return static_cast<D&>(*this).apply( e, ops::bit_or_assign() );  }
      
    template <Expression_iterator E>
    requires Has_bit_xor_assign<T, Value_type<E>>()
      decltype(auto) operator^=( E e )
      {  return static_cast<D&>(*this).apply( e, ops::bit_xor_assign() );  }
      
    template <Expression_iterator E>
    requires Has_left_shift_assign<T, Value_type<E>>()
      decltype(auto) operator<<=( E e )
      {  return static_cast<D&>(*this).apply( e, ops::left_shift_assign() );  }
      
    template <Expression_iterator E>
    requires Has_right_shift_assign<T, Value_type<E>>()
      decltype(auto) operator>>=( E e )
      {  return static_cast<D&>(*this).apply( e, ops::right_shift_assign() );  }
              
  };

} // namespace spx

#endif // SPX_ARITHMETIC_H
