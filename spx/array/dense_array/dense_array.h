#ifndef SPX_DENSE_ARRAY_H
#define SPX_DENSE_ARRAY_H

namespace spx
{
  
// [TODO] make concep Storage G
template <typename T, Dense_storage G>
  class dense_array 
    : public G,
      public arithmetic<dense_array<T, G>, T>
  {
  public:
    using base_t                 = G;
    using value_type             = typename base_t::value_type;
    using pointer                = typename base_t::pointer;
    using const_pointer          = typename base_t::const_pointer;
    using reference              = typename base_t::reference;
    using const_reference        = typename base_t::const_reference;
    using size_type              = typename base_t::size_type;
    using difference_type        = typename base_t::difference_type;
    // Normal data iterator
    using iterator               = typename base_t::iterator;
    using const_iterator         = typename base_t::const_iterator;
    using reverse_iterator       = typename base_t::reverse_iterator;
    using const_reverse_iterator = typename base_t::const_reverse_iterator;
    // Dense iterator
    using descriptor_type        = typename base_t::descriptor_type;
    using index_type             = typename descriptor_type::index_type;
    using desc_iterator          = dense_iterator<descriptor_type, iterator>;
    using desc_const_iterator    = dense_iterator<descriptor_type, const_iterator>;
    // reuse constructors
    using base_t::base_t;
    
  public:
    // Default construction
    dense_array() = default;
    
    // Destruction
    ~dense_array() = default;
    
    // Move semantics
    dense_array( dense_array&& ) = default;
    dense_array& operator=( dense_array&& ) = default;

    // Copy semantics
    dense_array( const dense_array& x ) 
      : base_t(x) 
    {}
    dense_array& operator=( const dense_array& ) = default;
    
    // Initialize elements by evaluating expression
    template <Expression_iterator E>
      dense_array( E expr ) { *this = expr; }
      
    // Properties
    //
    // NOTE: base class std::array provides size() and data()
      
    // Returns the extent of the array in the nth dimension. 
    std::size_t extent(std::size_t n) const { return base_t::descriptor().extents[n]; }
    decltype(auto) extents() const { return spx::extents( base_t::descriptor() ); }
    
    // Subscripting
    //
    // Returns a reference to the element at the index given by the sequence
    // of indexes, args.
    template <Indexable... Args>
      decltype(auto) operator()( Args&&... args )
      {  
        return array_impl::subscript( *this, std::forward<Args>(args)... );  
      }
      
    template <Indexable... Args>
      decltype(auto) operator()( Args&&... args ) const
      {  
        return array_impl::subscript( *this, std::forward<Args>(args)... );  
      }

    // Bouonds
    //
    // Returns lower bound index and upper bound index
    
    decltype(auto) lbounds() const { return spx::lbounds( base_t::descriptor() ); }
    decltype(auto) ubounds() const { return spx::ubounds( base_t::descriptor() ); }
    
    // Iterator
    //
    // Returns dense_iterator
    
    desc_iterator       dense_begin()       { return { base_t::descriptor(), base_t::begin(), lbounds() }; }
    desc_iterator       dense_end()         { return { base_t::descriptor(), base_t::begin(), spx::iter_ubounds( base_t::descriptor() ) }; }
    desc_const_iterator dense_begin() const { return { base_t::descriptor(), base_t::begin(), lbounds() }; }
    desc_const_iterator dense_end()   const { return { base_t::descriptor(), base_t::begin(), spx::iter_ubounds( base_t::descriptor() ) }; }   
    
    // Assignment
    //
    template <typename U>
    requires Assignable<T&, U>()
         && not Expression_iterator<U>()
      decltype(auto) operator=( const U& u );
      
    template <Range R>
    requires Assignable<T&, range_value_type<R>>()
          && not Has_extent<R>()
      decltype(auto) operator=( R&& r );
 
    template <Range R>
    requires Assignable<T&, range_value_type<R>>()
          && Has_extent<R>()
          && Dynamic_dense_storage<G>()
      decltype(auto) operator=( R&& r );
      
    template <typename U>
    requires Assignable<T&, U>()
      decltype(auto) operator=( std::initializer_list<U> r );
      
    template <Expression_iterator E>
    requires Assignable<T&, Value_type<E>>()
      decltype(auto) operator=( E expr );
      
    // Apply functions
    //
    // [TODO]: constrain F = Binary_operation
    template <typename U, typename F>
    requires Assignable<T&, U>()
          && not Expression_iterator<U>()
      decltype(auto) apply( const U& u, F&& f );

    // [TODO]: constrain F = Binary_operation
    template <Range R, typename F>
    requires Assignable<T&, range_value_type<R>>()
      decltype(auto) apply( R&& r, F&& f );
      
    // [TODO]: constrain F = Binary_operation
    template <typename U, typename F>
    requires Assignable<T&, U>()
      decltype(auto) apply( std::initializer_list<U> r, F&& f );
      
    // [TODO]: constrain F = Binary_operation
    template <Expression_iterator E, typename F>
    requires Dynamic_dense_storage<G>()
          && Ranked_expression_iterator<E>()
      decltype(auto) apply( E expr, F&& f );

    // [TODO]: constrain F = Binary_operation
    template <Expression_iterator E, typename F>
    requires Dynamic_dense_storage<G>()
          && not Ranked_expression_iterator<E>()
      decltype(auto) apply( E expr, F&& f );

    // [TODO]: constrain F = Binary_operation
    template <Expression_iterator E, typename F>
    requires not Dynamic_dense_storage<G>()
      decltype(auto) apply( E expr, F&& f );
      
    // Rebase -- Enable only if Flex_descriptor
    //
    template <bool dummy = true>
    requires Flex_descriptor<descriptor_type>()
      void rebase( size_type d, index_type id )
      {
        base_t::descriptor().rebase( d, id );
      }
      
    template <Range R>
    requires Flex_descriptor<descriptor_type>() 
      void rebase( R&& r )
      {
        base_t::descriptor().rebase( std::forward<R>(r) );
      }
      
    // Slice @ a specific dimension I
    //
    template <std::size_t I, Indexable_argument S>
      decltype(auto) operator()( size_constant<I>, S&& s )
      {
        return array_impl::subscript( *this, size_constant<I>(), std::forward<S>(s) );
      }
      
    template <std::size_t I, Indexable_argument S>
      decltype(auto) operator()( size_constant<I>, S&& s ) const
      {
        return array_impl::subscript( *this, size_constant<I>(), std::forward<S>(s) );
      }
      
    // operator [s]: slice s @ dimension 0
    template <Indexable_argument S>
    requires not Has_subscript<value_type, S>()
      decltype(auto) operator[]( S&& s )
      {
        return (*this)( size_constant<0>(), std::forward<S>(s) );
      }
      
    template <Indexable_argument S>
    requires not Has_subscript<value_type, S>()
      decltype(auto) operator[]( S&& s ) const
      {
        return (*this)( size_constant<0>(), std::forward<S>(s) );
      }

    // operator [x]: expressible x
    template <Expressible X>
    requires Has_subscript<value_type, decltype( *as_expr(std::declval<X>()) )>()
      decltype(auto) operator[]( X&& x )
      {
        return make_expr_func( ops::subscript(), *this, std::forward<X>(x) );
      }
    
    template <Expressible X>
    requires Has_subscript<value_type,  decltype( *as_expr(std::declval<X>()) )>() 
      decltype(auto) operator[]( X&& x ) const
      {
        return make_expr_func( ops::subscript(), *this, std::forward<X>(x) );
      }
  };


////////////////////////////////////////////////////////////////////////////
// type alias
//////////////////////////////////////////////////////////////////////////////

// general static array
template <typename T, Descriptor Desc, std::size_t... D>
  using g_static_array = dense_array<T, static_storage<T, Desc, D...>>;
  
// general dynamic array  
template <typename T, Descriptor Desc>
  using g_dynamic_array = dense_array<T, dynamic_storage<T, Desc>>;
  
// C static array
template <typename T, std::size_t... D>
  using c_static_array = g_static_array<T, c_descriptor<(sizeof...(D))>, D...>;

// Fortran static array  
template <typename T, std::size_t... D>
  using fort_static_array = g_static_array<T, fortran_descriptor<(sizeof...(D))>, D...>;

// C dynamic array
template <typename T, std::size_t N>
  using c_dynamic_array = g_dynamic_array<T, c_descriptor<N>>;

// Fortran dynamic array
template <typename T, std::size_t N>
  using fort_dynamic_array = g_dynamic_array<T, fortran_descriptor<N>>;

// C static vector
template <typename T, std::size_t D>
  using c_static_vector = c_static_array<T, D>;

// Fortran static vector
template <typename T, std::size_t D>
  using fort_static_vector = fort_static_array<T, D>;
  
// C dynamic vector
template <typename T>
  using c_dynamic_vector = c_dynamic_array<T, 1>;

// Fortran dynamic vector
template <typename T>
  using fort_dynamic_vector = fort_dynamic_array<T, 1>;

// Common type alias
//
// static_array = c_static_array
template <typename T, std::size_t... D>
  using static_array = c_static_array<T, D...>;

// dynamic_array = c_dynamic_array  
template <typename T, std::size_t N>
  using dynamic_array = c_dynamic_array<T, N>;
  
// static_vector = c_static_vector
template <typename T, std::size_t D>
  using static_vector = c_static_vector<T, D>;

// dynamic_vector = c_dynamic_vector  
template <typename T>
  using dynamic_vector = c_dynamic_vector<T>;

// d_array = dynamic_array
template <typename T, std::size_t N>
  using d_array = dynamic_array<T, N>;

// d_vector = static_vector
template <typename T>
  using d_vector = dynamic_vector<T>;

// d_arr = d_array
template <typename T, std::size_t N>
  using d_arr = d_array<T, N>;

// d_vec = d_vector
template <typename T>
  using d_vec = d_vector<T>;

////////////////////////////////////////////////////////////////////////////
// static_vector & dynamic_vector factory methods
//////////////////////////////////////////////////////////////////////////////
  
template <typename T, typename... Args>
requires Same<T, Args...>()
  constexpr decltype(auto) make_static_vector( T&& v, Args&&... args )
  {
    return static_vector<T, (sizeof...(Args)+1)>
           { std::forward<T>(v), std::forward<Args>(args)... };
  }

template <typename T, typename... Args>
requires Same<T, Args...>()
  constexpr decltype(auto) make_dynamic_vector( T&& v, Args&&... args )
  {
    return dynamic_vector<T>
           { std::forward<T>(v), std::forward<Args>(args)... };
  }
    
//////////////////////////////////////////////////////////////////////////////
// member function
//////////////////////////////////////////////////////////////////////////////

// Assignment

template <typename T, Dense_storage G>
  template <typename U>
  requires Assignable<T&, U>()
        && not Expression_iterator<U>()
    decltype(auto) dense_array<T, G>::operator=( const U& u )
    {
      return apply( u, ops::assign() );
    }
      
template <typename T, Dense_storage G>
  template <Range R>
  requires Assignable<T&, range_value_type<R>>()
        && not Has_extent<R>()
    decltype(auto) dense_array<T, G>::operator=( R&& r )
    {
      using std::begin;
      using std::end;
      std::copy( begin(r), end(r), base_t::begin() );
      return *this;
    }
 
template <typename T, Dense_storage G>
  template <Range R>
  requires Assignable<T&, range_value_type<R>>()
        && Has_extent<R>() 
        && Dynamic_dense_storage<G>()
    decltype(auto) dense_array<T, G>::operator=( R&& r )
    {
      // resize if needed
      constexpr auto D = base_t::rank();
      std::array<std::size_t, D> shape;
      for( std::size_t d = 0; d < D; ++d )
        shape[d] = r.extent( d );
      base_t::resize( shape );
      // copy elements
      using std::begin;
      using std::end;
      std::copy( begin(r), end(r), base_t::begin() );
      return *this;
    }
    
template <typename T, Dense_storage G>
  template <typename U>
  requires Assignable<T&, U>()
    decltype(auto) dense_array<T, G>::operator=( std::initializer_list<U> r )
    {
      using std::begin;
      using std::end;
      std::copy( begin(r), end(r), base_t::begin() );
      return *this;      
    }
     
template <typename T, Dense_storage G>
  template <Expression_iterator E>
  requires Assignable<T&, Value_type<E>>()
    decltype(auto) dense_array<T, G>::operator=( E expr )
    {
      return apply( expr, ops::assign() );
    }
          
// Apply functions

template <typename T, Dense_storage G>
  template <typename U, typename F>
  requires Assignable<T&, U>()
        && not Expression_iterator<U>()
    inline decltype(auto) 
    dense_array<T, G>::apply( const U& u, F&& f )
    {
      std::for_each( base_t::begin(), base_t::end(), [&](T& v){ f(v, u); } );
      return *this;
    }

template <typename T, Dense_storage G>
  template <Range R, typename F>
  requires Assignable<T&, range_value_type<R>>()    
    inline decltype(auto) 
    dense_array<T, G>::apply( R&& r, F&& f )
    {
      using std::begin;
      using std::end;
      base_t::apply( begin(r), end(r), std::forward<F>(f) );
      return *this;
    }

template <typename T, Dense_storage G>
  template <typename U, typename F>
  requires Assignable<T&, U>()
    inline decltype(auto) 
    dense_array<T, G>::apply( std::initializer_list<U> r, F&& f )
    {
      using std::begin;
      using std::end;
      base_t::apply( begin(r), end(r), std::forward<F>(f) );
      return *this;
    }

template <typename T, Dense_storage G>
  template <Expression_iterator E, typename F>
  requires Dynamic_dense_storage<G>()
        && Ranked_expression_iterator<E>()
    inline decltype(auto) 
    dense_array<T, G>::apply( E expr, F&& f )
    {
      constexpr auto D = base_t::rank();
      std::array<std::size_t, D> shape;
      for( std::size_t d = 0; d < D; ++d )
        shape[d] = expr.extent( d );      
      base_t::resize( shape );
      eval( make_expr_func( std::forward<F>(f),
                            as_expr(*this), 
                            expr ) );
      return *this;      
    }
 
template <typename T, Dense_storage G>
  template <Expression_iterator E, typename F>
  requires Dynamic_dense_storage<G>()
        && not Ranked_expression_iterator<E>()
    inline decltype(auto) 
    dense_array<T, G>::apply( E expr, F&& f )
    {
      eval( make_expr_func( std::forward<F>(f),
                            as_expr(*this), 
                            expr ) );
      return *this;      
    }
    
template <typename T, Dense_storage G>
  template <Expression_iterator E, typename F>
  requires not Dynamic_dense_storage<G>()
    inline decltype(auto) 
    dense_array<T, G>::apply( E expr, F&& f )
    {
      eval( make_expr_func( std::forward<F>(f),
                            as_expr(*this), 
                            expr ) );
      return *this;      
    }

} // namespace spx

#endif // SPX_DENSE_ARRAY_H
