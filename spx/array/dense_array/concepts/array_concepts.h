#ifndef SPX_ARRAY_CONCEPTS_H
#define SPX_ARRAY_CONCEPTS_H

namespace spx
{
  
template <typename T, std::size_t N>
  constexpr decltype(auto) static_size( T(&)[N] ) 
  {  
    return N;
  }

template <typename T>
requires requires() { std::tuple_size<Main_type<T>>::value; }
  constexpr decltype(auto) static_size( T&& ) 
  {  
    return std::tuple_size<Main_type<T>>::value;  
  }

template <typename T>
requires requires() { Main_type<T>::size(); }
  constexpr decltype(auto) static_size( T&& ) 
  {  
    return Main_type<T>::size();
  }

template <typename T>
  concept bool Has_static_size()
  {
    return requires( T& t ) {
             {static_size( t )} -> Size_type<T>;
           };
  }

template <Has_static_size T>
  constexpr decltype(auto) size = static_size( T() );

namespace type_impl
{
template <typename S, std::size_t N>
requires ( N>0 )
  struct resizable
  {
    template <typename T, typename... X>
      static constexpr auto determine( T&& t, X... ) 
        -> decltype( t.resize( std::declval<X>()... ) );
      
    static subst_failure determine( ... );

    template <typename T, typename... X>
    requires ( sizeof...(X) == N )
      static constexpr decltype(auto) check( T&& t, X... args )
      {
        using R = decltype( determine( std::forward<T>(t), args... ) );
        return !Same<R, subst_failure>();
      }

    template <typename T, typename... X>
    requires ( sizeof...(X) < N )
      static constexpr decltype(auto) check( T&& t, X... args )
      {
        return check( std::forward<T>(t), S(), args... );
      }
          
    template <typename T>
      static constexpr decltype(auto) check( T&& t )
      {
        return check( std::forward<T>(t), S() );
      }
  };
}
 
template <typename T>
  concept bool Resizable()
  {
    return requires( T t ) {
             requires type_impl::resizable<Size_type<T>, T::rank()>::check(t);
             { t.resize( std::array<Size_type<T>, T::rank()>() ) };
           };
  }
 
namespace type_impl
{
  // member_func_data
  //
  template <typename T>
    struct member_func_data
    {
    public:
      template <typename X>
        static constexpr auto check( X&& x ) -> decltype( x.data() );
      static constexpr subst_failure check( ... );

    private:
      using type = decltype( check(std::declval<T>()) );
    };

  template <typename T>
    using member_func_data_t = typename member_func_data<T>::type;
}

template <typename T>
  concept bool Ranked_storage()
  {
    return Range<T>()
        && Describable<T>()
        && requires( T t ) {
             Value_type<T>();
             { *(t.data()) } -> Value_type<T>;             
             requires Random_access_iterator<decltype(t.data())>();
             //requires Random_access_iterator<type_impl::member_func_data_t<T>>();
             { T::rank() } -> Size_type<T>;
             { t.size() } -> Size_type<T>;
           };
  }
      
template <typename T>
  concept bool Dynamic_dense_storage()
  {
    return Ranked_storage<T>()
        && Resizable<T>()
        && requires( T t ) {
             { t.size() } -> Size_type<T>;
           };
  }
  
template <typename T>
  concept bool Static_dense_storage()
  {
    return Ranked_storage<T>()
        && requires( T t ) {
              { T::size() } -> Size_type<T>;
           };
  }
    
template <typename T>
  concept bool Dense_storage()
  {
    return Static_dense_storage<T>()
        || Dynamic_dense_storage<T>();
  }

template <typename T>
  concept bool Has_extent()
  {
    return requires( T t ) {
             { t.extent( std::size_t() ) } -> std::size_t;
           };
  }

template <typename T>
  concept bool Sparse_data_generator()
  {
    return Describable<T>()
        && requires( T t ) {
             { t.get( std::ptrdiff_t() ) } -> Value_type<T>;
           };
  }

template <typename T>
  concept bool Sparse_databank()
  {
    return Sparse_data_generator<T>()
        && requires( T t ) {
             { t.insert_on() };
             { t.insert_off() };
             { t.elems() };
           };
  }


} // namespace spx
  

#endif // SPX_ARRAY_CONCEPTS_H
