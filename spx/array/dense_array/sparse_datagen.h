#ifndef SPX_SPARSE_DATAGEN_H
#define SPX_SPARSE_DATAGEN_H

#include <map>
#include <mutex>

namespace spx
{

std::mutex lock;

// -------------------------------------------------------------------------- //
// sparse_default_gen
//
// A default sparse data generator
//
template <typename T, Descriptor Desc>
  class sparse_default_gen
  {
  public:
    static constexpr std::size_t rank() { return Desc::rank(); }
    
    using value_type      = T; 
    using descriptor_type = Desc; 
    using size_type       = std::size_t;

  private:
    descriptor_type             desc;
    T                           zero = T{0};
    std::map<std::ptrdiff_t, T> data;
    bool                        insert = false;

  public:
    // Default construction
    sparse_default_gen() = default;
    
    // Destruction
    ~sparse_default_gen() = default;
    
    // Move semantics
    sparse_default_gen( sparse_default_gen&& ) = default;
    sparse_default_gen& operator=( sparse_default_gen&& ) = default;

    // Copy semantics
    sparse_default_gen( const sparse_default_gen& x ) = default;
    sparse_default_gen& operator=( const sparse_default_gen& x ) = default;
    
    // Constructors      
    //
    // 1D: v is init values; ND: v is shape vector
    template <typename D>
    requires (Desc::rank() == 1)
      sparse_default_gen( std::initializer_list<D> v )
      {
        desc = descriptor_type( {v.size()} ); // 1D: v is init values
        std::cerr << SPX_WARNING_PRE << "Using 1D initialization to construct sparse_array will be slow." << std::endl;
        auto it = v.begin();
        for( auto i = desc.lbound(0); i < desc.ubound(0); ++i  )
          data[ desc(i) ] = *it++;
      }
 
    template <typename D>
    requires (Desc::rank() != 1)
      sparse_default_gen( std::initializer_list<D> v )
      {
        static_assert( rank() >= 1, "rank() >= 1 is required" );
        desc = descriptor_type( v ); // ND: v is shape vector
      }
      
    template <typename... D>
    requires (sizeof...(D) == Desc::rank())
          && All(Convertible<D, size_type>()...)
      sparse_default_gen( D... shape )
        : desc( {shape...} )
      {}
      
    template <Input_range R>
    requires not Ranked_storage<Main_type<R>>()
      sparse_default_gen( R&& shape )
        : desc( std::forward<R>(shape) )
      {}

    template <Input_range R>
    requires Ranked_storage<Main_type<R>>()
      sparse_default_gen( R&& x )
      {
        constexpr auto D = Main_type<R>::rank();
        init_range<D>( std::forward<R>(x) );
      }

    explicit sparse_default_gen( size_type shape )
    {
      std::array<size_type, rank()> d;
      d.fill( shape );
      desc = descriptor_type( d );
    }

    // Member functions
    //
    const descriptor_type& descriptor() const { return desc; }
          descriptor_type& descriptor()       { return desc; }

    
    T& get( std::ptrdiff_t id )
    {
      decltype(auto) x = data.find( id );
      //return x!= data.end() ? 
      //           x->second : 
      //           insert ? data[id] : zero;
      if( x != data.end() )
        return x->second;
      else if( insert )
      {
        std::lock_guard<std::mutex> guard( lock );
        T& v = data[id];
        return v;
      }
      else
        return zero;
    }

    const T& get( std::ptrdiff_t id ) const
    {
      decltype(auto) x = data.find( id );
      return x!= data.end() ? x->second : zero;
    }

    decltype(auto) elems() const           { return data; }
    
    void insert_on()  { insert = true; }
    void insert_off() { insert = false; }
  
  private:
    
    template <std::size_t D, Input_range R> 
    requires (Desc::rank() > D)
      void init_range( R&& x ) // x = shape
      {
        using std::begin;
        using std::end;
        desc.resize( begin(x), end(x) ); 
      }

    template <std::size_t D, Input_range R>
    requires (Desc::rank() == D)
      void init_range( R&& x ) // COPY!
      {
         std::cerr << SPX_WARNING_PRE << "Copying full range to construct sparse_array will be slow." << std::endl;
        using std::begin;
        using std::end;
        decltype(auto) desc_x = x.descriptor(); 
        auto b = begin( desc_x.extents );
        auto e = end( desc_x.extents );
        desc.resize( b, e );
        auto it = begin( x );
        for( auto i = desc.lbound(0); i <= desc.ubound(0); ++i, ++it  )
          data[ desc(i) ] = *it;
      }
 };

//////////////////////////////////////////////////////////////////////////////
// type alias 
//////////////////////////////////////////////////////////////////////////////

template <Sparse_data_generator S>
  using sparse_array = dense_array<Value_type<S>, sparse_storage<S>>;
 
template <typename T, std::size_t N>
  using c_sparse_default = sparse_default_gen<T, c_descriptor<N>>; 
 
//////////////////////////////////////////////////////////////////////////////
// global factory function
//////////////////////////////////////////////////////////////////////////////

template <typename T, std::size_t N, typename D>
  constexpr decltype(auto) make_sparse_array( std::initializer_list<D> v )
  {
    using S = c_sparse_default<T, N>;
    return sparse_array<S>( S(v) );
  }

template <typename T, std::size_t N, typename... Args>
requires None( Sparse_data_generator<Args>()... )
  constexpr decltype(auto) make_sparse_array( Args&&... args )
  {
    using S = c_sparse_default<T, N>;
    return sparse_array<S>( S( std::forward<Args>(args)... ) );
  }

template <Sparse_data_generator S>
  constexpr decltype(auto) make_sparse_array( S&& s )
  {
    return sparse_array<S>( std::move(s) );
  }
 
// -------------------------------------------------------------------------- //
// sparse_const
//
// A sparse data generator for constant value
//
template <typename T, Descriptor Desc>
  class sparse_const
  {
  public:
    static constexpr std::size_t rank() { return Desc::rank(); }
    
    using value_type      = T; 
    using descriptor_type = Desc; 
    using size_type       = std::size_t;

  private:
    descriptor_type desc;
    T elem = T{0};

  public:
    // Default construction
    sparse_const() = default;
    
    // Destruction
    ~sparse_const() = default;
    
    // Move semantics
    sparse_const( sparse_const&& ) = default;
    sparse_const& operator=( sparse_const&& ) = default;

    // Copy semantics
    sparse_const( const sparse_const& x ) = default;
    sparse_const& operator=( const sparse_const& x ) = default;
    
    // Constructors      
    //
    template <typename D>
      sparse_const( std::initializer_list<D> v )
      {
        desc = descriptor_type( v ); // v is shape vector
      }
      
    template <typename... D>
    requires (sizeof...(D) == Desc::rank())
          && All(Convertible<D, size_type>()...)
      sparse_const( D... shape )
        : desc( {shape...} )
      {}
      
    template <Input_range R>
    requires not Ranked_storage<Main_type<R>>()
      sparse_const( R&& shape )
        : desc( std::forward<R>(shape) )
      {}

    template <Input_range R>
    requires Ranked_storage<Main_type<R>>()
      sparse_const( R&& x )
      {
        static_assert( Main_type<R>::rank() == 1, "Not a shape vector." );
        using std::begin;
        using std::end;
        desc.resize( begin(x), end(x) ); 
      }

    explicit sparse_const( size_type shape )
    {
      std::array<size_type, rank()> d;
      d.fill( shape );
      desc = descriptor_type( d );
    }

    // Member functions
    //
    const descriptor_type& descriptor() const { return desc; }
          descriptor_type& descriptor()       { return desc; }

    const T& get( std::ptrdiff_t id ) const { return elem; }
          T& get( std::ptrdiff_t id )       { return elem; }

    const T& value() const { return elem; }
          T& value()       { return elem; }

};
//////////////////////////////////////////////////////////////////////////////
// type alias 
//////////////////////////////////////////////////////////////////////////////

template <typename T, std::size_t N>
  using c_sparse_const = sparse_const<T, c_descriptor<N>>; 
 
//////////////////////////////////////////////////////////////////////////////
// global factory function
//////////////////////////////////////////////////////////////////////////////

template <std::size_t N, typename T, typename D>
  decltype(auto) make_const_array( T&& init, std::initializer_list<D> v )
  {
    auto gen = c_sparse_const<Main_type<T>, N>( v );
    gen.value() = std::forward<T>( init );
    return make_sparse_array( std::move(gen) ); 
  }

template <std::size_t N, typename T, typename... Args>
requires None( Sparse_data_generator<Args>()... )
  decltype(auto) make_const_array( T&& init, Args&&... args )
  {
    auto gen = c_sparse_const<Main_type<T>, N>( std::forward<Args>(args)... );
    gen.value() = std::forward<T>( init );
    return make_sparse_array( std::move(gen) );
  }

// -------------------------------------------------------------------------- //
// sparse_func_gen
//
// A sparse array data generator by function callback
//
template <Descriptor Desc, typename F>
  class sparse_func_gen
  {
  public:
    using value_type      = Main_type<decltype(std::declval<F>()( std::declval<Desc&>(), std::ptrdiff_t() ))>;
    using descriptor_type = Desc; 

  private:
    Desc desc;
    F    f;

  public:
    sparse_func_gen( const Desc& desc, F&& f )
      : desc( desc ), f( std::forward<F>(f) )
    {}

    // Member functions
    //
    const descriptor_type& descriptor() const { return desc; }
          descriptor_type& descriptor()       { return desc; }

    decltype(auto) get( std::ptrdiff_t id ) const
    {
      return f( descriptor(), id );
    }
  };

//////////////////////////////////////////////////////////////////////////////
// global factory function
//////////////////////////////////////////////////////////////////////////////

template <Descriptor Desc, typename F>
  decltype(auto) make_callback_array( const Desc& desc, F&& f )
  {
    auto gen = sparse_func_gen<Desc, F>( desc, std::forward<F>(f) );
    return make_sparse_array( std::move(gen) );
  }

template <std::size_t D, typename F>
  decltype(auto) make_callback_array( std::initializer_list<std::size_t> shape, F&& f )
  {
    using Desc = c_descriptor<D>;
    auto gen = sparse_func_gen<Desc, F>( Desc( shape ), std::forward<F>(f) );
    return make_sparse_array( std::move(gen) );
  }

template <Expression_iterator E>
  decltype(auto) make_callback_array( E expr )
  {
    return make_callback_array( expr.descriptor(), 
                                [ e = expr ]( auto&& desc, auto idx ) -> decltype(auto)
                                {
                                  return *e( desc.index_of( idx ) );
                                } );
  }

} // namespace spx

#endif // SPX_SPARSE_DATAGEN_H
