#ifndef SPX_NAMED_POSITION_H
#define SPX_NAMED_POSITION_H

#include <spx/array/descriptor/concepts/slice_concepts.h>
#include <spx/util/operators.h>

namespace spx
{

struct first
{
  template <Can_be_signed B, Can_be_size_t L>
    constexpr B index( B base, L length ) const { return base; }
};

struct half
{
  template <Can_be_signed B, Can_be_size_t L>
    constexpr B index( B base, L length ) const { return (base + (length-1) / 2); }
};

struct last
{
  template <Can_be_signed B, Can_be_size_t L>
    constexpr B index( B base, L length ) const { return (base + length - 1); }
};

// subscript_index

// (Subscript) S = Named_position
template <Can_be_signed B, Can_be_size_t L, Named_position S>
  constexpr B subscript_index( B base, L length, const S& p )
  {
    return p.index( base, length );
  }

// (Subscript) S = Can_be_signed
template <Can_be_signed B, Can_be_size_t L, Can_be_signed S>
requires Convertible<S, B>()
  constexpr B subscript_index( B base, L length, const S& p )
  {
    return p;
  }
      
// uniary wrapped pos
template <Subscript S, typename F>
  struct uni_wrap_pos
  {    
    // SECTION - member data
    S pos;
    F trans;
    
    // SECTION - member function
    uni_wrap_pos( S&& s, F&& f ) 
        : pos( std::forward<S>(s) ), trans( std::forward<F>(f) ) 
    {}

    // Default constructible
    uni_wrap_pos() = default;
    
    // Copyable
    uni_wrap_pos( const uni_wrap_pos& ) = default;
    uni_wrap_pos& operator=( const uni_wrap_pos& ) = default;
    
    // Movable
    uni_wrap_pos( uni_wrap_pos&& ) = default;
    uni_wrap_pos& operator=( uni_wrap_pos&& ) = default;

    // Named_position
    template <Can_be_signed B, Can_be_size_t L>
    requires Unary_operation<F, B>()
      constexpr B index( B base, L length ) 
      { 
        return trans( subscript_index( base, length, pos) );
      }
  };

// binary wrapped pos
template <Subscript S1, Subscript S2, typename F>
  struct bi_wrap_pos
  {    
    // SECTION - member data
    S1 pos1;
    S2 pos2;
    F  trans;
    
    // SECTION - member function    
    bi_wrap_pos( S1&& s1, S2&& s2, F&& f ) 
      : pos1( std::forward<S1>(s1) ), 
        pos2( std::forward<S2>(s2) ), 
        trans( std::forward<F>(f) ) 
    {}
    
    // Default constructible
    bi_wrap_pos() = default;
    
    // Copyable
    bi_wrap_pos( const bi_wrap_pos& ) = default;
    bi_wrap_pos& operator=( const bi_wrap_pos& ) = default;
    
    // Movable
    bi_wrap_pos( bi_wrap_pos&& ) = default;
    bi_wrap_pos& operator=( bi_wrap_pos&& ) = default;
    
    // Named_position
    template <Can_be_signed B, Can_be_size_t L>
    requires Binary_operation<F, B, B>()
      constexpr B index( B base, L length ) 
      { 
        return trans( subscript_index( base, length, pos1 ), 
                      subscript_index( base, length, pos2 ) );
      }
  };
  
// PBC wrapped pos
template <Subscript S>
  struct period_pos
  {    
    // SECTION - member data
    S pos;
    
    // SECTION - member function (public)
    period_pos( S&& s ) : pos( std::forward<S>(s) )
    {}

    // Default constructible
    period_pos() = default;
    
    // Copyable
    period_pos( const period_pos& ) = default;
    period_pos& operator=( const period_pos& ) = default;
    
    // Movable
    period_pos( period_pos&& ) = default;
    period_pos& operator=( period_pos&& ) = default;

    // Named_position
    template <Can_be_signed B, Can_be_size_t L>
      constexpr B index( B base, L length )
      { 
        return cal_real_id( subscript_index( base, length, pos ), 
                            base, 
                            length);
      } 
    
  private:
    // SECTION - member function (private)
    template <Can_be_signed B, Can_be_size_t L>
    requires Convertible<L, B>()
      constexpr B cal_real_id( B id, B base, L length )
      {
        return id >= base ? 
              base + ( id - base ) % B(length) :
              base + B(length) - ( base - id ) % B(length);
      }
  };

template <Subscript S>
  constexpr auto period( S&& s )
  {
    return period_pos<S>( std::forward<S>(s) );
  }

// out-class unary operators

template <Named_position P>
  constexpr auto operator - ( P&& p )
  {
    return uni_wrap_pos<P, ops::unary_minus>( std::forward<P>(p), 
                                              ops::unary_minus() );
  }
  
template <Named_position P>
  constexpr auto operator + ( P&& p )
  {
    return uni_wrap_pos<P, ops::unary_plus>( std::forward<P>(p), 
                                             ops::unary_plus() );
  }

// out-class operators for "Pos" and "int"

template <Subscript P1, Subscript P2>
requires not (Can_be_signed<P1>() && Can_be_signed<P2>())
  constexpr auto operator + ( P1&& p1, P2&& p2 )
  {
    return bi_wrap_pos<P1, P2, ops::plus>( std::forward<P1>(p1), 
                                           std::forward<P2>(p2), 
                                           ops::plus() );
  }

template <Subscript P1, Subscript P2>
requires not (Can_be_signed<P1>() && Can_be_signed<P2>())
  constexpr auto operator - ( P1&& p1, P2&& p2 )
  {
    return bi_wrap_pos<P1, P2, ops::minus>( std::forward<P1>(p1), 
                                            std::forward<P2>(p2), 
                                            ops::minus() );
  }
    
template <Subscript P1, Subscript P2>
requires not (Can_be_signed<P1>() && Can_be_signed<P2>())
  constexpr auto operator * ( P1&& p1, P2&& p2 )
  {
    return bi_wrap_pos<P1, P2, ops::multiplies>( std::forward<P1>(p1), 
                                                 std::forward<P2>(p2), 
                                                 ops::multiplies() );
  }

template <Subscript P1, Subscript P2>
requires not (Can_be_signed<P1>() && Can_be_signed<P2>())
  constexpr auto operator / ( P1&& p1, P2&& p2 )
  {
    return bi_wrap_pos<P1, P2, ops::divides>( std::forward<P1>(p1), 
                                              std::forward<P2>(p2), 
                                              ops::divides() );
  }

} // namespace spx
#endif // SPX_NAMED_POSITION_H
