#ifndef SPX_STORAGE_MAJOR_H
#define SPX_STORAGE_MAJOR_H

namespace spx
{

// -------------------------------------------------------------------------- //
// row_major
//
template <std::size_t D>
  struct row_major
  {
    constexpr std::size_t store_dim( std::size_t d ) const { return D - d - 1; }
  };

// -------------------------------------------------------------------------- //
// column_major
//
template <std::size_t D>
  struct column_major
  {
    constexpr std::size_t store_dim( std::size_t d ) const { return d; }
  };

// -------------------------------------------------------------------------- //
// any_major
//
template <std::size_t D>
  struct any_major
  {
    std::size_t storage[D]; // the sequence of dimension for storage ordering
    
    constexpr std::size_t store_dim( std::size_t n ) const { return storage[n]; }
    
    any_major();
    any_major( const any_major& ) = default;

    template <Range R>
    requires Range_of_type<R, std::size_t>()
      any_major( R&& store_order );
      
    template <typename U>
    requires Convertible<U, std::size_t>()
      any_major( std::initializer_list<U> store_order );
      
    // constructed any_major with default storage order
    // O = row_major or O = column_major
    template <template <std::size_t> class O>
    requires Row_major<O<D>>() or Column_major<O<D>>()
      static inline any_major<D> create_by_default();
  };

template <std::size_t D>
  template <template <std::size_t> class O>
  requires Row_major<O<D>>() or Column_major<O<D>>()
    inline any_major<D>
    any_major<D>::create_by_default()
    {
      O<D> o;
      std::size_t store[D];
      for( std::size_t d = 0; d < D; ++d )
        store[d] = o.store_dim( d );
      return any_major<D>( store );
    }

template <std::size_t D>
  any_major<D>::any_major() 
  {
    *this = any_major<D>::create_by_default<row_major>();
  }

template <std::size_t D>
  template <Range R>
  requires Range_of_type<R, std::size_t>()
    any_major<D>::any_major( R&& store_order )
    {
      using std::begin;
      using std::end;
      std::copy( begin(store_order), end(store_order), begin(storage) );  
    }

template <std::size_t D>
  template <typename U>
  requires Convertible<U, std::size_t>()
    any_major<D>::any_major( std::initializer_list<U> store_order )
    {
      using std::begin;
      using std::end;
      std::copy( begin(store_order), end(store_order), begin(storage) );  
    }

} // namespace spx

#endif // SPX_STORAGE_MAJOR_H
