#ifndef SPX_NONUNIFORM_DESCRIPTOR_H
#define SPX_NONUNIFORM_DESCRIPTOR_H

namespace spx
{

// -------------------------------------------------------------------------- //
// nonuniform_descriptor
//
// A nonuniform_descriptor supports nonuniform ID spaces.
//
// Template Parameters:
//    Desc - The base type of any Descriptor except Nonuniform_descriptor itself
//

template <Descriptor Desc>
requires not Nonuniform_descriptor<Desc>()
  class nonuniform_descriptor : public Desc
  {
  public:
    using base_t     = Desc;
    using size_type  = typename base_t::size_type;
    using index_type = typename base_t::index_type;
    using size_vec   = std::vector<std::size_t>;
    //using base_t::base_t;
    
    // type as new rank D_new
    template <std::size_t D_new> 
      using as_rank = nonuniform_descriptor<typename Desc::template as_rank<D_new>>;
    
    std::array<size_vec, base_t::rank()> shift;
    std::array<bool,     base_t::rank()> is_uniform;
    
    // Default Constructible
    nonuniform_descriptor()
    {
      std::for_each( is_uniform.begin(), is_uniform.end(), 
                     []( bool& v ){ v = true; } );
    }
    
    // Copy constructor for base type
    nonuniform_descriptor( const base_t& desc )
    {
      static_cast<base_t&>(*this) = desc;
      std::for_each( is_uniform.begin(), is_uniform.end(), 
                     []( bool& v ){ v = true; } );
    }
    
    // Returns is_uniform[d]
    constexpr bool uniform( std::size_t d ) const { return is_uniform[d]; }

    // Resize by shape iterator
    template <Input_iterator I>
      void resize( I first, I last )
      {
        base_t::resize( first, last );
        for( size_type d = 0; d < base_t::rank(); ++d )
        {
          shift[d].clear();
          is_uniform[d] = true;
        }
      }
      
    // Returns the shift by stride * ID at dimension d
    decltype(auto) stride_shift( std::size_t d, index_type id ) const
    {
      return is_uniform[d] ? 
             base_t::stride_shift( d, id ) : 
             shift[d][ id - base_t::lbound(d) ] * base_t::stride(d);
    }
    
    // Returns the shift by dot(strides, ID)
    template <Input_iterator I, Can_be_signed Rtn>
      Rtn stride_shift( I id_beg, I id_end, Rtn init ) const
      {
        std::size_t d = 0;
        for( ; id_beg != id_end; ++id_beg ) 
          init += stride_shift( d++, *id_beg );
        return init;
      }
    
    // id -> offset
    template <Range R>
    requires Range_of_type<R, index_type>()
      decltype(auto) offset( R&& id ) const
      {
        using std::begin;
        using std::end;
        return stride_shift( begin(id), end(id), base_t::data_offset() ); 
      }
    
    // id -> offset
    template <typename U>
    requires Convertible<U, index_type>()
      decltype(auto) offset( std::initializer_list<U> id ) const
      { 
        return stride_shift( id.begin(), id.end(), base_t::data_offset() ); 
      }

    // offset -> id
    std::array<index_type, Desc::rank()> index_of( std::size_t offset ) const
    {
      return spx::index_of( *this, offset );
    }
    
    // subscript && slice
    // either return offset or new slice, always const (never change "this")
    template <Indexable... Args>
      decltype(auto) operator()( Args&&... args ) const
      {
        return descriptor_slice::get( *this, std::forward<Args>(args)... );
      }
      
    // std::tuple support
    template <Indexable... Args>
      decltype(auto) operator()( std::tuple<Args...>&& tup ) const
      {
        return tuple_unpack_and_subst( *this, tup );
      }
      
    // slice @ a specific dimension I
    template <std::size_t I, Indexable_argument S>
      decltype(auto) operator()( size_constant<I>, S&& s )
      {
        return (*this)( tuple_slot_type<slice_all, Desc::rank(), I>( std::forward<S>(s) ) );
      }
      

    // Equality Comparable
    bool operator==( const nonuniform_descriptor& x ) const
    {
      return this == &x ? true :
             static_cast<const base_t&>(*this) == static_cast<const base_t&>(x)
          && std::equal( begin(shift), end(shift), begin(x.shift) )
          && std::equal( begin(is_uniform), end(is_uniform), begin(x.is_uniform) );
    }
    
    bool operator!=( const nonuniform_descriptor& x ) const
    {
      return !(*this == x);
    }      
  };

} // namespace spx

#endif // SPX_NONUNIFORM_DESCRIPTOR_H