#ifndef SPX_DESCRIPTOR_BASE_H
#define SPX_DESCRIPTOR_BASE_H

namespace spx
{

// -------------------------------------------------------------------------- //
// descriptor_base
//
// The descriptor base class provides core features to all descriptor
// implementations in this library. By default descriptors use row-major
// ordering.
//
// This class is internal to the library. Non-constructible. Constructors are
// protected, which is initialized by sub-classes:
//
//    descriptor_base<D, row_major>    desc( shape );
//    descriptor_base<D, column_major> desc( shape );
//    descriptor_base<D, any_major>    desc( shape, storage_order );

template<std::size_t D, template <std::size_t> class O = row_major>
requires Dense_storage_major<O<D>>()
  class descriptor_base : public O<D>
  {
  public:
    using size_type = std::size_t;

    // Returns the rank of the descriptor.
    static constexpr size_type rank() { return D; }

    size_type extents[D]; // The number of elements in each dimension
    size_type strides[D]; // The distance between elemetns in each dimension

  protected:
    
    // args... can be empty or range_of_storage_order if O = any_major
    template<Range R, typename... Args>
    requires Range_of_type<R, std::size_t>()
      explicit descriptor_base( R&& shape, Args&&... args );
      
    template<typename U, typename... Args>
    requires Convertible<U, std::size_t>()
      descriptor_base( std::initializer_list<U> shape, Args&&... args );
      
    descriptor_base() = default;
    descriptor_base( const descriptor_base& ) = default; 
  
  public:
    
    // Returns the total number of elements.
    constexpr auto size() const;

    // Returns the extent in the nth dimension.
    size_type extent( size_type n ) const { return extents[n]; }

    // Returns the stride in the nth dimension.
    size_type stride( size_type n ) const { return strides[n]; }

    // Returns the shift by stride * ID at dimension d
    // FIXME: Integral<T> is not correct. We need a real "Integer<T>" instead.
    template <Can_be_signed T>
      auto stride_shift( std::size_t d, T id ) const;
    
    // Returns the shift by dot(strides, ID)
    template <Input_iterator I, Can_be_signed Rtn>
      Rtn stride_shift( I id_beg, I id_end, Rtn init ) const;
      
    // Resize by shape iterator
    template <Input_iterator I>
      void resize( I first, I last );
      
    // Equality Comparable
    bool operator==( const descriptor_base& x ) const
    {
      return this == &x ? true :
             std::equal(extents, extents + D, x.extents)
          && std::equal(strides, strides + D, x.strides);
    }
      
    bool operator!=( const descriptor_base& x ) const
    {
      return !(*this == x);
    }
    
  private:
    void init_strides();
  };

template<std::size_t D, template <std::size_t> class O>
requires Dense_storage_major<O<D>>()
  template<Range R, typename... Args>
  requires Range_of_type<R, std::size_t>()
    descriptor_base<D, O>::descriptor_base( R&& s, Args&&... args )
      : O<D>( std::forward<Args>(args)... )
    {
      using std::begin;
      using std::end;
      std::copy( begin(s), end(s), begin(extents) );
      init_strides();
    }

template<std::size_t D, template <std::size_t> class O>
requires Dense_storage_major<O<D>>()
  template<typename U, typename... Args>
  requires Convertible<U, std::size_t>()
    descriptor_base<D, O>::descriptor_base( std::initializer_list<U> s, Args&&... args )
      : O<D>( std::forward<Args>(args)... )
    {
      using std::begin;
      using std::end;
      std::copy( begin(s), end(s), begin(extents) );
      init_strides();
    }
    
template<std::size_t D, template <std::size_t> class O>
requires Dense_storage_major<O<D>>()
  constexpr auto
  descriptor_base<D, O>::size() const
  {
    return extents[ O<D>::store_dim(D-1) ] 
         * strides[ O<D>::store_dim(D-1) ];
  }

template<std::size_t D, template <std::size_t> class O>
requires Dense_storage_major<O<D>>()
  template <Can_be_signed T>
    inline auto 
    descriptor_base<D, O>::stride_shift( std::size_t d, T id ) const
    {
      return id * strides[d];
    }

template<std::size_t D, template <std::size_t> class O>
requires Dense_storage_major<O<D>>()
  template <Input_iterator I, Can_be_signed Rtn>
    inline Rtn 
    descriptor_base<D, O>::stride_shift( I id_beg, I id_end, Rtn init ) const
    {
      using std::begin;
      using std::end;
      return std::inner_product( id_beg, id_end, begin(strides), init );
    } 
      
template<std::size_t D, template <std::size_t> class O>
requires Dense_storage_major<O<D>>()
  inline void
  descriptor_base<D, O>::init_strides()
  {
    strides[ O<D>::store_dim(0) ] = 1;
    auto off = extents[ O<D>::store_dim(0) ];
    for( std::size_t d = 1; d < D; d++)
    {
      strides[ O<D>::store_dim(d) ] = off;
      off *= extents[ O<D>::store_dim(d) ];
    }
  }

template<std::size_t D, template <std::size_t> class O>
requires Dense_storage_major<O<D>>()
  template <Input_iterator I>
  inline void
  descriptor_base<D, O>::resize( I first, I last )
  {
    using std::begin;
    std::copy( first, last, begin(extents) );
    init_strides();
  }

} // namespace spx

#endif // SPX_DESCRIPTOR_BASE_H
