#ifndef SPX_FLEX_DESCRIPTOR_H
#define SPX_FLEX_DESCRIPTOR_H

namespace spx
{

// -------------------------------------------------------------------------- //
// flex_descriptor
//
// A flex descriptor describes an array with arbitrary bounds. T = index_type
//
// Construct for 0-based descriptor, lbounds[D] = T(0):
//    flex_descriptor<D, T, row_major>    desc( shape );
//    flex_descriptor<D, T, column_major> desc( shape );
//    flex_descriptor<D, T, any_major>    desc( shape, storage_order );
//
// Construct for any-based descriptor:
//    flex_descriptor<D, T, row_major>    desc( lbound, shape );
//    flex_descriptor<D, T, column_major> desc( lbound, shape );
//    flex_descriptor<D, T, any_major>    desc( lbound, shape, storage_order );
//

template<std::size_t D, Can_be_signed T, template <std::size_t> class O>
requires Dense_storage_major<O<D>>()
  class flex_descriptor : public descriptor_base<D, O>
  {
  public:
    using base_t     = descriptor_base<D, O>;  
    using size_type  = typename base_t::size_type;
    using index_type = T;

    // type as new rank D_new
    template <std::size_t D_new> 
      using as_rank = flex_descriptor<D_new, T, O>;

    std::ptrdiff_t            data_off = 0;  // data_off = dot( first_id, stride ) - dot( lbound, stride )
    std::array<index_type, D> lbounds; // lower bounds
    
  public:
    // Default Constructible
    flex_descriptor() = default;

    // Copy Constructible
    flex_descriptor( const flex_descriptor& desc ) = default;
    
    // Constructors
    //
    // construct from shape, as well as storage_order
    
    template<Range R, typename... Args>
    requires Range_of_type<R, size_type>()
      explicit flex_descriptor( R&& shape, Args&&... args )
        : base_t( std::forward<R>(shape), std::forward<Args>(args)... ) 
      {
        using std::begin;
        using std::end;
        std::for_each( lbounds.begin(), lbounds.end(), []( index_type& v ){ v = 0; } );
      }
      
    template<typename U, typename... Args>
    requires Convertible<U, size_type>()
      flex_descriptor( std::initializer_list<U> shape, Args&&... args )
        : base_t( shape, std::forward<Args>(args)... ) 
      {
        using std::begin;
        using std::end;
        std::for_each( lbounds.begin(), lbounds.end(), []( index_type& v ){ v = 0; } );
      }
      
    // Constructors
    //
    // construct from lower_bounds && shape, as well as storage_order

    template <Range RL, Range RE, typename... Args>
    requires Range_of_type<RE, size_type>()
          && Range_of_type<RL, T>()
      explicit flex_descriptor( RL&& lb, RE&& e, Args&&... args )
        : base_t( std::forward<RE>(e), std::forward<Args>(args)... )
      {
        using std::begin;
        using std::end;
        std::copy( begin(lb), end(lb), begin(lbounds) );
        init_dataoff();
      }

    template <typename UL, typename UE, typename... Args>
    requires Convertible<UL, T>() 
          && Convertible<UE, size_type>()
      flex_descriptor( std::initializer_list<UL> lb, std::initializer_list<UE> e, Args&&... args )
        : base_t( e, std::forward<Args>(args)... )
      {
        using std::begin;
        using std::end;
        std::copy( begin(lb), end(lb), begin(lbounds) );
        init_dataoff();
      }


    // Returns the lower and upper bound in the nth dimension.
    index_type lbound( size_type n ) const { return lbounds[n]; }
    index_type ubound( size_type n ) const { return lbound(n) + base_t::extent(n) - 1; }

    // Rebase lower bound in the nth dimension
    void rebase( size_type n, index_type id ) 
    { 
      data_off -= (id - lbound(n)) * base_t::stride(n);
      lbounds[n] = id;
    }
    
    template <Range R>
    requires Range_of_type<R, index_type>()
      void rebase( R&& r ) 
      { 
        using std::begin;
        using std::end;
        auto it = begin(r);
        for( std::size_t d = 0; d < D; ++d, ++it )
          rebase( d, *it );
      }
      
    // Returns data_off
    std::ptrdiff_t data_offset() const { return data_off; }

    // Resize by shape iterator
    template <Input_iterator I>
      void resize( I first, I last )
      {
        base_t::resize( first, last );
        init_dataoff();
      }
      
    // id -> offset
    template <Range R>
    requires Range_of_type<R, index_type>()
      decltype(auto) offset( R&& id ) const 
      { 
        using std::begin;
        using std::end;
        return base_t::stride_shift( begin(id), end(id), data_off );
      }
    
    // id -> offset
    template <typename U>
    requires Convertible<U, index_type>()
      decltype(auto) offset( std::initializer_list<U> id ) const
      { 
        using std::begin;
        using std::end;
        return base_t::stride_shift( begin(id), end(id), data_off ); 
      }
      
    // offset -> id
    decltype(auto) index_of( std::size_t offset ) const
    {
      return spx::index_of( *this, offset );
    }
    
    // subscript && slice
    // either return offset or new slice, always const (never change "this")
    template <Indexable... Args>
      decltype(auto) operator()( Args&&... args ) const
      {
        return descriptor_slice::get( *this, std::forward<Args>(args)... );
      }
      
    // std::tuple support
    template <Indexable... Args>
      decltype(auto) operator()( std::tuple<Args...>&& tup ) const
      {
        return tuple_unpack_and_subst( *this, tup );
      }
      
    // slice @ a specific dimension I
    template <std::size_t I, Indexable_argument S>
      decltype(auto) operator()( size_constant<I>, S&& s ) const
      {
        return (*this)( tuple_slot_type<slice_all, D, I>( std::forward<S>(s) ) );
      }
      
    // Equality Comparable
    bool operator==( const flex_descriptor& x ) const
    {
      return this == &x ? true :
             static_cast<const base_t&>(*this) == static_cast<const base_t&>(x)
          && data_off == x.data_off
          && std::equal( begin(lbounds), end(lbounds), begin(x.lbounds) );
    }
    
    bool operator!=( const flex_descriptor& x ) const
    {
      return !(*this == x);
    }
    
    private:
      void init_dataoff();
  };

template<std::size_t D, Can_be_signed T, template <std::size_t> class O>
requires Dense_storage_major<O<D>>()
  inline void
  flex_descriptor<D, T, O>::init_dataoff()
  {
    // initialize data_off
    data_off = 0;
    for( std::size_t d = 0; d < D; ++d )
        data_off -= lbound(d) * base_t::stride(d);    
  }

} // namespace spx

#endif // SPX_FLEX_DESCRIPTOR_H