#ifndef SPX_INDEX_SPACE_H
#define SPX_INDEX_SPACE_H

#include <vector>

namespace spx
{

// uniform_id_space

template <Subscript TS, Subscript TE>
  class uniform_id_space
  {     
  private:
    TS s;
    TE e;
    std::size_t r = 1;
    
  public:
    // Default constructor
    uniform_id_space() = default;
    
    uniform_id_space( TS start, TE end, std::size_t stride = 1 ) 
      : s(start), e(end), r(stride) 
    {}
    
    TS          start()  const { return s; }
    TE          end()    const { return e; }
    std::size_t stride() const { return r; }  
  };


template <Subscript TS, Subscript TE>
  constexpr auto slice( TS start, TE end, std::size_t stride = 1 )
  {
    return uniform_id_space<TS, TE>( start, end, stride );
  }

using slice_all = uniform_id_space<first, last>;
using all       = slice_all;

// nonuniform_id_space

template <Subscript TS>
  class nonuniform_id_space
  {
  private:
    using TR = std::vector<std::size_t>;
    
  public:
    // Default constructible
    nonuniform_id_space() = default;
    
    template <typename R>
    requires Range_of_type<R, std::size_t>()
      nonuniform_id_space( TS start, R&& stride ) : s(start)
      {
        using std::begin;
        using std::end;
        for( auto it = begin( stride ); it != end( stride ); ++it )
          r.push_back( *it );
      }
    
          TS  start()  const { return s; }
    const TR& stride() const { return r; }
    
  private:
    TS s;
    TR r;
  };

namespace descriptor_impl
{
  template <Forward_range R>
    inline auto idx_to_stride( R&& idx )
    {
      using std::begin;
      using std::end;
      std::vector<std::size_t> stride;
      auto iter = begin( idx );
      auto old  = *iter++;
      std::size_t acu = 0;
      stride.push_back( acu );
      for( ; iter != end( idx ); ++iter )
      {
        acu += *iter - old;
        old  = *iter;
        stride.push_back( acu );
      }
      return stride;
    }
}

template <Forward_range R>
requires Can_be_signed<range_value_type<R>>()
  inline auto idx_slice( R&& idx )
  {
    decltype(auto) s = descriptor_impl::idx_to_stride( std::forward<R>(idx) );
    return nonuniform_id_space<range_value_type<R>>( *begin( idx ), std::move( s ) );
  }

template <Can_be_signed T>
  constexpr auto idx_slice( std::initializer_list<T> idx )
  {
    decltype(auto) s = descriptor_impl::idx_to_stride( idx );
    return nonuniform_id_space<Make_signed<T>>( *begin( idx ), std::move( s ));
  }

} // namespace spx 
#endif // SPX_INDEX_SPACE_H
  
