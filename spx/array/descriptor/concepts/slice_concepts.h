#ifndef SPX_SLICE_CONCEPTS_H
#define SPX_SLICE_CONCEPTS_H

namespace spx
{

template <Input_range R>
  using range_value_type = Value_type<Iterator_type<R>>;

template <typename R, typename T>
  concept bool Range_of_type()
  {
      return Input_range<R>()
          && Convertible<range_value_type<R>, T>();
          //&& requires() {
          //     requires Convertible<Value_type<Iterator_type<R>>, T>();
          //   };
  }

template <typename T>
  concept bool Can_be_signed()
  {
    return requires() {
             typename Make_signed<Main_type<T>>;
           };
  }

template <typename T>
  concept bool Can_be_size_t()
  {
    return std::is_convertible<T, std::size_t>::value;
  }

//template <typename T>
//  concept bool Named_position()
//  {
//    return requires( Main_type<T> t ) {
//             requires Signed_type<decltype(t.index( int{}, std::size_t{} ))>()
//                   || Unsigned_type<decltype(t.index( std::size_t{}, std::size_t{} ))>();
//           };
//  }

namespace type_impl
{
  template <typename T>
    struct named_pos_check 
    {
    private:
      template <typename X>
        static constexpr auto check( X&& x ) 
          -> decltype( x.index( int(), std::size_t() )  );

      static constexpr subst_failure check( ... );

    public:
      static constexpr bool value = 
           std::is_signed<decltype( check(std::declval<T>()) )>::value
        || std::is_unsigned<decltype( check(std::declval<T>()) )>::value;
    };
}

template <typename T>
  concept bool Named_position()
  {
    return type_impl::named_pos_check<Main_type<T>>::value;
  }

template <typename T>
  concept bool Subscript()
  {
    return Named_position<T>()
        || Can_be_signed<T>();
  }


namespace type_impl
{
  // id_space_start
  //
  template <typename T>
    struct id_space_start
    {
    private:
      template <typename X>
        static constexpr auto check( X&& x ) -> decltype( x.start() );
      
      static constexpr subst_failure check( ... );

    public:
      using type = decltype( check(std::declval<T>()) );
    };

  template <typename T>
    using id_space_start_t = typename id_space_start<T>::type;

  // id_space_end
  //
  template <typename T>
    struct id_space_end
    {
    private:
      template <typename X>
        static constexpr auto check( X&& x ) -> decltype( x.end() );
      
      static constexpr subst_failure check( ... );

    public:
      using type = decltype( check(std::declval<T>()) );
    };

  template <typename T>
    using id_space_end_t = typename id_space_end<T>::type;

  // id_space_stride
  //
  template <typename T>
    struct id_space_stride
    {
    private:
      template <typename X>
        static constexpr auto check( X&& x ) -> decltype( x.stride() );
      
      static constexpr subst_failure check( ... );

    public:
      using type = decltype( check(std::declval<T>()) );
    };

  template <typename T>
    using id_space_stride_t = typename id_space_stride<T>::type;

  // subscript_zero
  //
  template <typename T>
    struct subscript_zero
    {
    private:
      template <typename X>
        static constexpr auto check( X&& x ) -> decltype( x[0] );
      
      static constexpr subst_failure check( ... );

    public:
      using type = decltype( check(std::declval<T>()) );
    };

  template <typename T>
    using subscript_zero_t = typename subscript_zero<T>::type;
}

template <typename T>
  concept bool Uniform_id_space()
  {
    return requires( T t ) {
             requires Subscript<type_impl::id_space_start_t<T>>();
             requires Subscript<type_impl::id_space_end_t<T>>();
             requires Unsigned_type<type_impl::id_space_stride_t<T>>();
           };
  }
  
template <typename T>
  concept bool Determined_uniform_idx()
  {
    return Uniform_id_space<T>()
        && requires( T t ) {
             requires Can_be_signed<type_impl::id_space_start_t<T>>();
             requires Can_be_signed<type_impl::id_space_end_t<T>>();
             requires Unsigned_type<type_impl::id_space_stride_t<T>>();
           };
  }

template <typename T>
  concept bool Undetermined_uniform_idx()
  {
    return Uniform_id_space<T>()
        && not Determined_uniform_idx<T>();
  }

template <typename T>
  concept bool Nonuniform_id_space()
  {
    return requires( T t ) {
             requires Subscript<Main_type<type_impl::id_space_start_t<T>>>();
             requires Unsigned_type<Main_type<type_impl::subscript_zero_t<type_impl::id_space_stride_t<T>>>>();
           };
  }

template <typename T>
  concept bool Slice()
  {
      return Uniform_id_space<T>() 
          || Nonuniform_id_space<T>();
  }

template <typename T>
  concept bool Indexable_argument()
  {
    return Subscript<T>() || Slice<T>();
  }

template <typename T>
  concept bool Indexable_range()
  {
    return Input_range<T>()
        && requires() {
             typename range_value_type<T>;
             typename Value_type<Iterator_type<T>>;
             requires Indexable_argument<Value_type<Iterator_type<T>>>();
           };
  }
  
template <typename T>
  concept bool Indexable()
  {
    return Indexable_argument<T>()
        || Indexable_range<T>();
  }

} // namespace spx

#endif // SPX_SLICE_CONCEPTS_H
