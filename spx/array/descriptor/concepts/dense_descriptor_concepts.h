#ifndef SPX_DENSE_DESCRIPTOR_CONCEPTS_H
#define SPX_DENSE_DESCRIPTOR_CONCEPTS_H

namespace spx
{

////////////////////////////////////////////////////////////////////////////
// *-major
////////////////////////////////////////////////////////////////////////////

namespace type_impl
{
  // check row_major
  template <std::size_t D>
    constexpr defined_t as_row_major( row_major<D>&& );
    
  // check column_major
  template <std::size_t D>
    constexpr defined_t as_column_major( column_major<D>&& );
    
  // check any_major
  template <std::size_t D>
    constexpr defined_t as_any_major( any_major<D>&& );
}

template <typename T>
  concept bool Dense_storage_major()
  {
    return requires (T t) {
      { t.store_dim( std::size_t() ) } -> std::size_t;
    };
  }

template <typename T>
  concept bool Row_major()
  {
    return Dense_storage_major<T>()
        && requires() {
          type_impl::as_row_major( Main_type<T>() );
        };
  }

template <typename T>
  concept bool Column_major()
  {
    return Dense_storage_major<T>()
        && requires() {
          type_impl::as_column_major( Main_type<T>() );
        };
  }

template <typename T>
  concept bool Any_major()
  {
    return Dense_storage_major<T>()
        && requires() {
          type_impl::as_any_major( Main_type<T>() );
        };
  }

////////////////////////////////////////////////////////////////////////////
// Descriptor
////////////////////////////////////////////////////////////////////////////

template <typename T>
requires requires() { typename T::index_type; }
  using Index_type = typename T::index_type;

template <typename T>
  concept bool Descriptor()
  {
    return Dense_storage_major<T>()
        && requires( Main_type<T> t ) {
          typename Size_type<T>;
          typename Index_type<T>;
          { T::rank() } -> Size_type<T>;
          { t.extent( Size_type<T>() ) } -> Size_type<T>;
          { t.stride( Size_type<T>() ) } -> Size_type<T>;
          { t.lbound( Size_type<T>() ) } -> Index_type<T>;
        };
  }

template <typename T>
  concept bool Flex_descriptor()
  {
    return Descriptor<T>() 
        && requires( Main_type<T> t ) {
          // t.rebase( dim, new_base_id )
          t.rebase( Size_type<T>(), Index_type<T>() );
        };
  }

template <typename T>
  concept bool Permute_descriptor()
  {  
    return Descriptor<T>()
        && Any_major<T>();
  }

//FIXME: temporarily by checking uniform[d] -> bool
template <typename T>
  concept bool Nonuniform_descriptor()
  {
    return Descriptor<T>() 
        && requires( Main_type<T> t ) {
          { t.uniform( Size_type<T>() ) } -> bool;
        };
  }

////////////////////////////////////////////////////////////////////////////
// Zero_lbound
////////////////////////////////////////////////////////////////////////////

template <typename Desc, std::size_t D>
  concept bool Zero_lbound()
  {
    return Descriptor<Desc>()
        && requires( Desc desc ) {
              requires (desc.lbound(D) == 0);
            };
  }

namespace type_impl
{
  template <typename Desc, std::size_t I>
  requires (I == Desc::rank())
    constexpr bool Zero_lbound_all()
    {
      return true;
    }
    
  template <typename Desc, std::size_t I>
  requires (I != Desc::rank())
    constexpr bool Zero_lbound_all()
    {
      return Zero_lbound<Desc, I>()
          && Zero_lbound<Desc, I+1>();
    }
}

template <typename Desc>
  concept bool Zero_lbound()
  {
    return Descriptor<Desc>()
        && type_impl::Zero_lbound_all<Desc, 0>();
  }  

} // namespace spx

#endif // SPX_DENSE_DESCRIPTOR_CONCEPTS_H
