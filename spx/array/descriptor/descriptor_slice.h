#ifndef SPX_DESCRIPTOR_H
#define SPX_DESCRIPTOR_H

#include <cassert>

// Headers Forward

namespace spx
{
  template <Descriptor Desc> requires not Nonuniform_descriptor<Desc>()
    class nonuniform_descriptor;
}

// Implementation Body

namespace spx
{

namespace descriptor_slice
{

  //////////////////////////////////////////////////////////////////////////////
  // 1) N-argument subscript:
  // (int, int, int, int...)
  //////////////////////////////////////////////////////////////////////////////
  
  template <Descriptor Desc, Subscript... Args>
  requires None( Named_position<Args>()... )
    inline decltype(auto) get( const Desc& desc, Args&&... args )
    {
      using id_t = typename Desc::index_type;      
      static_assert( Count( Convertible<Args, id_t>()... ) == Desc::rank(),
                     "number of index does not match rank" );
      id_t idx[ Desc::rank() ] { id_t(args)... };
      return desc.offset( idx );
    }

  namespace impl
  {
    // T = Named_position
    template <std::size_t D, Descriptor Desc, Named_position T>
      constexpr decltype(auto) determine_index( const Desc& desc, T&& v )
      {
        return v.index( desc.lbound(D), desc.extent(D) );
      }

    // T = index_type
    template <std::size_t D, Descriptor Desc>
      //constexpr decltype(auto) determine_index( const Desc& desc, typename Desc::index_type&& v )
      constexpr decltype(auto) determine_index( const Desc& desc, typename Desc::index_type v )
      {
        return v;
      }
    
    template <std::size_t D, Descriptor Desc, Subscript T>
      constexpr decltype(auto) stride_shift( const Desc& desc, T&& v )
      {
        return desc.stride_shift( D, determine_index<D>( desc, std::forward<T>(v) ) );
      }
    
    template <std::size_t D, Descriptor Desc, Subscript T, typename... Args>
      constexpr decltype(auto) stride_shift( const Desc& desc, T&& v, Args&&... args )
      {
        return stride_shift<D  >( desc, std::forward<T>(v) )
             + stride_shift<D+1>( desc, std::forward<Args>(args)... );
      }
  }
    
  //////////////////////////////////////////////////////////////////////////////
  // 2) N-argument subscript:
  // (int, tag, tag, int...)
  //////////////////////////////////////////////////////////////////////////////
  
  template <Descriptor Desc, Subscript... Args>
  requires Some( Named_position<Args>()... )
    constexpr decltype(auto) get( const Desc& desc, Args&&... args )
    {
      using id_t = typename Desc::index_type;      
      static_assert( Count( Convertible<Args, id_t>()... )
                   + Count( Named_position<Args>()... ) == Desc::rank(),
                     "number of index does not match rank" );
      return desc.data_offset() 
           + impl::stride_shift<0>( desc, std::forward<Args>(args)... );
    }


  //////////////////////////////////////////////////////////////////////////////
  // Sliced_type:
  // deduce the result type of slicing
  //////////////////////////////////////////////////////////////////////////////
    
  namespace impl
  {
    template <typename X>
    requires Nonuniform_descriptor<X>()
      static auto sliced_type_base_check( X&& ) -> X;
      
    template <typename X>
    requires not Nonuniform_descriptor<X>()
      static auto sliced_type_base_check( X&& ) -> nonuniform_descriptor<X>;
          
    template <Descriptor Desc, Indexable... Args>
      struct sliced_type_base
      {
        constexpr static std::size_t n_slice = Count( Slice<Args>()... );
        constexpr static std::size_t n_sbcpt = Count( Subscript<Args>()... );
        constexpr static std::size_t D       = Desc::rank();
        constexpr static std::size_t D_new   = D - n_sbcpt;
        
        constexpr static bool valid = (n_slice + n_sbcpt == D);
        static_assert( valid, "invalid slicing" );
                  
      public:
        using Desc_new = typename Desc::template as_rank<D_new>;        
        using type = type_if<None( Nonuniform_id_space<Args>()... ),
                             Desc_new,
                            // Some Args are Nonuniform_id_space
                            decltype( sliced_type_base_check(std::declval<Desc_new>()) )>;
      };
    
    template <Descriptor Desc, Indexable... Args>
      static typename sliced_type_base<Desc, Args...>::type
      range_sliced_type_check( Desc&&, std::tuple<Args...>&& );
      
    template <Descriptor Desc, Indexable_range R>
      struct range_sliced_type
      {
        using value_type = range_value_type<R>;
        using tup = repeat_type_as_tuple<value_type, Desc::rank()>;
        using type = decltype( range_sliced_type_check( std::declval<Desc>(), 
                                                        std::declval<tup>() ) );
      };
    
    // the following 2 types are the concrete types to give the sliced restuls
    template <Descriptor Desc, Indexable... Args>
      struct sliced_type : sliced_type_base<Desc, Args...>
      {};

    template <Descriptor Desc, Indexable_range R>
      struct sliced_type<Desc, R> : range_sliced_type<Desc, R>
      {};
  }
  
  template <Descriptor Desc, Indexable... Args>
    using sliced_type = typename impl::sliced_type<Desc, Args...>::type;

  namespace impl
  {
    //////////////////////////////////////////////////////////////////////////////
    // do_uniform_slice
    //////////////////////////////////////////////////////////////////////////////
    
    // Not Zero_lbound Descriptor
    template <std::size_t D, Descriptor Desc>
    requires not Zero_lbound<Desc>()
      constexpr decltype(auto)
      data_off_due_to_lbound( Desc& desc, std::size_t stride )
      {
        return -desc.lbound(D) * stride;
      }

    // Zero_lbound Descriptor 
    template <std::size_t D, Descriptor Desc>
    requires Zero_lbound<Desc>()
      constexpr decltype(auto)
      data_off_due_to_lbound( Desc& desc, std::size_t stride )
      {
        return 0;
      }

    // Determined_uniform_idx = slice( int(s), int(e), int(r) )
    template <std::size_t D, Descriptor Desc, Determined_uniform_idx S>
      inline void do_uniform_slice( Desc& desc, S&& v )
      {
        auto        new_s = v.start();
        auto        new_e = v.end();
        std::size_t new_r = v.stride();
        
        std::size_t& ss = desc.strides[D];
        desc.data_off  += (new_s + data_off_due_to_lbound<D>( desc, new_r ) ) * ss;
        ss *= new_r;             

        std::size_t new_l = std::size_t( new_e - new_s ) / new_r + 1; 
        desc.extents[D] = new_l;
      }

    //////////////////////////////////////////////////////////////////////////////
    // do_nonuniform_slice_dim
    //////////////////////////////////////////////////////////////////////////////
      
    // Nonuniform_id_space
    // @ nonuniform_descriptor
    template <std::size_t D, Nonuniform_descriptor Desc, Nonuniform_id_space S>
      inline void do_nonuniform_slice_dim( Desc& desc, S&& v )
      {
        auto start_off = v.start() - desc.lbound(D);
        auto shift_off = desc.shift[D][ start_off ];
        desc.data_off += shift_off * desc.stride(D);
        using shift_vec_t = Main_type<decltype( desc.shift[D] )>;
        shift_vec_t new_shift( v.stride().size() );
        for( std::size_t i = 0; i < new_shift.size(); ++i )
          new_shift[i] = desc.shift[D][ v.stride()[i] + start_off ] - shift_off;
        desc.shift[D].resize( new_shift.size() );
        std::copy( new_shift.begin(), new_shift.end(), desc.shift[D].begin() );
        desc.extents[D] = desc.shift[D].size();
      }

    //////////////////////////////////////////////////////////////////////////////
    // do_slice_dim
    //////////////////////////////////////////////////////////////////////////////
      
    // Determined_uniform_idx = slice( int(s), int(e), int(r) )
    // @ uniform descriptor (NOT nonuniform_descriptor)
    template <std::size_t D, Descriptor Desc, Determined_uniform_idx S>
    requires not Nonuniform_descriptor<Desc>()
      inline void do_slice_dim( Desc& desc, S&& v )
      {
        do_uniform_slice<D>( desc, std::forward<S>(v) );
      }

    // Determined_uniform_idx = slice( int(s), int(e), int(r) )
    // @ nonuniform_descriptor
    template <std::size_t D, Nonuniform_descriptor Desc, Determined_uniform_idx S>
      inline void do_slice_dim( Desc& desc, S&& v )
      {
        // dimension D is still uniform
        if( desc.is_uniform[D] )
          do_uniform_slice<D>( desc, std::forward<S>(v) );
        // dimension D has already sliced to nonuniform
        else
        {
          auto first = v.start();
          auto end = v.end();
          std::size_t stride = v.stride();
          std::size_t len = std::size_t( end - first ) / stride + 1;
          using id_t = decltype( v.start() );
          std::vector<id_t> idx( len );
          for( std::size_t i = 0; i < len; ++i )
            idx[i] = first + i*stride;
          do_nonuniform_slice_dim<D>( desc, idx_slice( idx ) );
        }
      }

    // Undetermined_uniform_idx = slice( int || tag, int || tag, int )
    template <std::size_t D, Descriptor Desc, Undetermined_uniform_idx S>
      inline void do_slice_dim( Desc& desc, S&& v )
      {
        auto start = determine_index<D>( desc, v.start() );
        auto end   = determine_index<D>( desc, v.end() );
        do_slice_dim<D>( desc, slice( start, end, v.stride() ) );
      }

    // Nonuniform_id_space = idx_slice{ int... }
    // @ nonuniform_descriptor
    template <std::size_t D, Nonuniform_descriptor Desc, Nonuniform_id_space S>
      inline void do_slice_dim( Desc& desc, S&& v )
      {
        assert( v.stride().size() > 0 );
        
        // first slice to nonuniform
        if( desc.is_uniform[D] )
        {
          desc.data_off += v.start() * desc.stride(D);
          desc.shift[D].resize( v.stride().size() );
          std::copy( v.stride().begin(), v.stride().end(), desc.shift[D].begin() );
          desc.is_uniform[D] = false;                
          desc.extents[D] = desc.shift[D].size();
        }
        // dimension D has already sliced to nonuniform
        else
          do_nonuniform_slice_dim<D>( desc, std::forward<S>(v) );
      }

    //////////////////////////////////////////////////////////////////////////////
    // do_slice
    //////////////////////////////////////////////////////////////////////////////
    
    template <Descriptor Desc>
      inline void do_slice( Desc& desc )
      {
        return;
      }
        
    template <Descriptor Desc, typename T, typename... Args>
      inline void do_slice( Desc& desc, T&& v, Args&&... args )
      {
        constexpr std::size_t N = Desc::rank();
        constexpr std::size_t D = N - sizeof...(Args) - 1;
        do_slice_dim<D>( desc, std::forward<T>(v) );
        do_slice( desc, std::forward<Args>(args)... );
      }
  }

  //////////////////////////////////////////////////////////////////////////////
  // 3) N-argument slicing:
  // all Args are slices
  //////////////////////////////////////////////////////////////////////////////
  
  template <Descriptor Desc, Slice... Args>
    inline decltype(auto) get( const Desc& desc, Args&&... args )
    {
      sliced_type<Desc, Args...> desc_new( desc );
      impl::do_slice( desc_new, std::forward<Args>(args)... );
      return desc_new;
    }

  namespace impl
  {
    // re-map extents and strides
    template <std::size_t D_new, std::size_t D_old, Descriptor Desc_new, Descriptor Desc_old>
      inline void
      remap_extent_stride( Desc_new& desc_new, Desc_old& desc_old)
      {
        desc_new.extents[ D_new ] = desc_old.extents[ D_old ];
        desc_new.strides[ D_new ] = desc_old.strides[ D_old ];
      } 

    inline void remap_extent_stride( ... ) {}
    
    // re-map lower bound
    template <std::size_t D_new, std::size_t D_old, Descriptor Desc_new, Descriptor Desc_old>
    requires Flex_descriptor<Desc_new>() 
          && Flex_descriptor<Desc_old>()
      inline void
      remap_lbound( Desc_new& desc_new, Desc_old& desc_old)
      {
        desc_new.lbounds[ D_new ] = desc_old.lbounds[ D_old ];
      } 
    
    template <std::size_t D_new, std::size_t D_old, Descriptor Desc_new, Descriptor Desc_old>
    requires not (Flex_descriptor<Desc_new>() 
               && Flex_descriptor<Desc_old>())
      inline void
      remap_lbound( Desc_new& desc_new, Desc_old& desc_old)
      {}
    
    // re-map nonuniform_descriptor
    template <std::size_t D_new, std::size_t D_old, Descriptor Desc_new, Descriptor Desc_old>
    requires Nonuniform_descriptor<Desc_new>() 
          && Nonuniform_descriptor<Desc_old>()
      inline void
      remap_nonuniform_desc( Desc_new& desc_new, Desc_old& desc_old)
      {
        desc_new.is_uniform[ D_new ] = desc_old.is_uniform[ D_old ];
        auto& new_shift = desc_new.shift[ D_new ];
        auto& old_shift = desc_old.shift[ D_old ];
        new_shift.resize( old_shift.size() );
        std::copy( old_shift.begin(), old_shift.end(), new_shift.begin() );
      } 
    
    template <std::size_t D_new, std::size_t D_old, Descriptor Desc_new, Descriptor Desc_old>
    requires not (Nonuniform_descriptor<Desc_new>() 
               && Nonuniform_descriptor<Desc_old>())
      inline void
      remap_nonuniform_desc( Desc_new& desc_new, Desc_old& desc_old)
      {}
      
    // re-map storage order
    template <std::size_t D, Descriptor Desc_new, Descriptor Desc_old, typename Dim_map>
    requires Permute_descriptor<Desc_new>()
      inline void
      remap_order( Desc_new& desc_new, Desc_old& desc_old, Dim_map& dim_map )
      {            
          std::size_t j = 0;
          for ( std::size_t i = 0; i < D; ++i)
          {
              auto old_order = desc_old.store_dim(i);
              if ( dim_map[ old_order ] != -1 )
              {
                  desc_new.storage[j] = dim_map[ old_order ];
                  ++j;
              }
          }            
      }
    
    template <std::size_t D, Descriptor Desc_new, Descriptor Desc_old, typename Dim_map>
    requires not Permute_descriptor<Desc_new>()
      inline void
      remap_order( Desc_new& desc_new, Desc_old& desc_old, Dim_map& dim_map )
      {}
      
    // [WARNING] just a header
    template <std::size_t D_new,
              std::size_t D_old, 
              Descriptor Desc_new,
              Descriptor Desc_old, 
              typename Dim_map, 
              Subscript T, 
              typename... Args>
      inline void
      do_reduced_slice( Desc_new& desc_new, 
                        Desc_old& desc_old,
                        Dim_map& dim_map,
                        T&& v,
                        Args&&... args );
    
    // end of recursive
    template <std::size_t D_new,
              std::size_t D_old, 
              Descriptor Desc_new, 
              Descriptor Desc_old, 
              typename Dim_map>
      inline void
      do_reduced_slice( Desc_new& desc_new, 
                        Desc_old& desc_old, 
                        Dim_map& dim_map )
      {
        return;
      }
        
    // (old) descriptor -> (new) uniform or nonuniform descriptor
    template <std::size_t D_new, 
              std::size_t D_old,
              Descriptor Desc_new, 
              Descriptor Desc_old, 
              typename Dim_map, 
              Slice S, 
              typename... Args>
      inline void
      do_reduced_slice( Desc_new& desc_new, 
                        Desc_old& desc_old,                      
                        Dim_map& dim_map,
                        S&& s,
                        Args&&... args )
      {
        dim_map[ D_old ] = D_new;
        
        remap_extent_stride<D_new, D_old>( desc_new, desc_old );
        remap_lbound<D_new, D_old>( desc_new, desc_old );
        remap_nonuniform_desc<D_new, D_old>( desc_new, desc_old );

        // do slice @ D_new
        do_slice_dim<D_new>( desc_new, std::forward<S>(s) );
    
        do_reduced_slice<D_new+1, D_old+1>( 
          desc_new, desc_old, dim_map, std::forward<Args>(args)... );
      }
    
    // int or tag
    template <std::size_t D_new,
              std::size_t D_old, 
              Descriptor Desc_new,
              Descriptor Desc_old, 
              typename Dim_map, 
              Subscript T, 
              typename... Args>
      inline void
      do_reduced_slice( Desc_new& desc_new, 
                        Desc_old& desc_old,
                        Dim_map& dim_map,
                        T&& v,
                        Args&&... args )
      {
        dim_map[ D_old ] = -1;
        desc_new.data_off += stride_shift<D_old>( desc_old, std::forward<T>(v) );
    
        do_reduced_slice<D_new, D_old+1>( 
            desc_new, desc_old, dim_map, std::forward<Args>(args)... );
      }          
      
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // 4) N-argument slicing:
  // slices + subcription (reduced slicing)
  //////////////////////////////////////////////////////////////////////////////
  
  template <Descriptor Desc, typename... Args>
  requires Some(Slice<Args>()...)
        && Some(Subscript<Args>()...)
    inline decltype(auto) get( const Desc& desc, Args&&... args )
    {      
      sliced_type<Desc, Args...> desc_new;      
      desc_new.data_off = desc.data_off;
      
      constexpr std::size_t D = Desc::rank();
      //static_vector<int, D> dim_map;
      int dim_map[D];
      impl::do_reduced_slice<0,0>( desc_new, desc, dim_map, std::forward<Args>(args)... );
      impl::remap_order<D>( desc_new, desc, dim_map );
        
      return desc_new;
    }
    
  //////////////////////////////////////////////////////////////////////////////
  // take care of "1-argument", i.e., Range of something.
  //////////////////////////////////////////////////////////////////////////////
  
  // 5) range of slices
  template <Descriptor Desc, Indexable_range R>
  requires Slice<range_value_type<R>>()
    inline decltype(auto) get( const Desc& desc, R&& r )
    {
      decltype(auto) tup = to_tuple<Desc::rank()>( std::forward<R>(r) );
      return tuple_unpack_and_subst( desc, tup );
    }

  // 6) range of subscript
  template <Descriptor Desc, Indexable_range R>
  requires Subscript<range_value_type<R>>()
    inline decltype(auto) get( const Desc& desc, R&& r )
    {
      decltype(auto) tup = to_tuple<Desc::rank()>( std::forward<R>(r) );
      return tuple_unpack_and_subst( desc, tup );
    }
    
} // namespace descriptor_slice

//////////////////////////////////////////////////////////////////////////////
// global type alias to deduce descriptor sliced results
//////////////////////////////////////////////////////////////////////////////

template <Descriptor Desc, Indexable... Args>
  using sliced_type = typename descriptor_slice::sliced_type<Desc, Args...>;

} // namespace spx

#endif // SPX_DESCRIPTOR_H
