#ifndef SPX_BASIC_DESCRIPTOR_H
#define SPX_BASIC_DESCRIPTOR_H

namespace spx
{

// -------------------------------------------------------------------------- //
// basic_descriptor
//
// A basic descriptor is one whose lower bounds are static L-based
//
//    basic_descriptor<D, L, row_major>    desc( shape );
//    basic_descriptor<D, L, column_major> desc( shape );
//    basic_descriptor<D, L, any_major>    desc( shape, storage_order );
//

template<std::size_t D, std::ptrdiff_t L, template <std::size_t> class O>
requires Dense_storage_major<O<D>>()
  class basic_descriptor : public descriptor_base<D, O>
  {
  public:
    using base_t     = descriptor_base<D, O>;
    using size_type  = typename base_t::size_type;
    using index_type = type_if<(L==0), std::size_t, std::ptrdiff_t>;

    // type as new rank D_new
    template <std::size_t D_new> 
      using as_rank = basic_descriptor<D_new, L, O>;
    
    index_type data_off = 0;  // The offset of the first element past a base pointer
  
  public:    
    // Default Constructible
    basic_descriptor() = default;

    // Copy Constructible
    basic_descriptor( const basic_descriptor& desc )
      : base_t( desc ), data_off( desc.data_off )
    {}
    
    // Constructors
    //
    // construct from shape, as well as storage_order
    
    template <Range R, typename... Args>
    requires Range_of_type<R, size_type>()
      explicit basic_descriptor( R&& shape, Args&&... args )
        : base_t( std::forward<R>(shape), std::forward<Args>(args)... ) 
      {
        init_dataoff();
      }
      
    template <typename U, typename... Args>
    requires Convertible<U, size_type>()
      basic_descriptor( std::initializer_list<U> shape, Args&&... args )
        : base_t( shape, std::forward<Args>(args)... ) 
      {
        init_dataoff();
      }

    // Returns the lower and upper bound in the nth dimension. As a fixed
    // descriptor, the lower bound is constant.
    static constexpr index_type lbound( size_type n )       { return L; }
                     index_type ubound( size_type n ) const { return L + base_t::extent(n) - 1; }
                 
    // Returns data_off
    index_type data_offset() const { return data_off; }
    
    // Resize by shape iterator
    template <Input_iterator I>
      void resize( I first, I last )
      {
        base_t::resize( first, last );
        init_dataoff();
      }
      
    // id -> offset
    template <Range R>
    requires Range_of_type<R, index_type>()
      decltype(auto) offset( R&& id ) const 
      { 
        using std::begin;
        using std::end;
        return base_t::stride_shift( begin(id), end(id), data_off );
      }
    
    // id -> offset
    template <typename U>
    requires Convertible<U, index_type>()
      decltype(auto) offset( std::initializer_list<U> id ) const
      { 
        using std::begin;
        using std::end;
        return base_t::stride_shift( begin(id), end(id), data_off ); 
      }

    // offset -> id
    decltype(auto) index_of( std::size_t offset ) const
    {
      return spx::index_of( *this, offset );
    }

    // subscript && slice
    // either return offset or new slice, always const (never change "this")
    template <Indexable... Args>
      decltype(auto) operator()( Args&&... args ) const
      {
        return descriptor_slice::get( *this, std::forward<Args>(args)... );
      }
      
    // std::tuple support
    template <Indexable... Args>
      decltype(auto) operator()( std::tuple<Args...>&& tup ) const
      {
        return tuple_unpack_and_subst( *this, tup );
      }

    // slice @ a specific dimension I
    template <std::size_t I, Indexable_argument S>
      decltype(auto) operator()( size_constant<I>, S&& s ) const
      {
        return (*this)( tuple_slot_type<slice_all, D, I>( std::forward<S>(s) ) );
      }
  
    // Equality Comparable
    bool operator==( const basic_descriptor& x ) const
    {
      return this == &x ? true :
             static_cast<const base_t&>(*this) == static_cast<const base_t&>(x)
          && data_off == x.data_off;
    }
    
    bool operator!=( const basic_descriptor& x ) const
    {
      return !(*this == x);
    }
    
  private:
    
    template <bool dummy = true>
    requires (L!=0)
      void init_dataoff()
      {
        using std::begin;
        using std::end;
        std::for_each( begin( base_t::strides ), end( base_t::strides ), 
                      [&]( size_type ss ){ data_off -= L * ss; } );
      }    

    template <bool dummy = true>
    requires (L==0)
      void init_dataoff() const
      {}
  };

// -------------------------------------------------------------------------- //
// type alias
//
// Provide some useful type alias
//

// As rank
template <Descriptor Desc, std::size_t D>
  using as_rank = typename Desc::template as_rank<D>;

// descriptor for C-array
template <std::size_t D>
  using c_descriptor = basic_descriptor<D, 0, row_major>;
  
// descriptor for Fortran-array
template <std::size_t D>
  using fortran_descriptor = basic_descriptor<D, 1, column_major>;

} // namespace spx

#endif // SPX_BASIC_DESCRIPTOR_H
