#ifndef SPX_DENSE_ITERATOR_H
#define SPX_DENSE_ITERATOR_H

namespace spx
{

template <typename Desc, typename I>
  concept bool Dense_iter_draggable()
  {
    return Descriptor<Desc>()
        && Random_access_iterator<I>()
        && requires( Desc desc, I i ) {
             // desc.stride_shift( size_t d, id_t id ) -> storage_stride[d] * id;
             { i += desc.stride_shift(std::size_t(), typename Desc::index_type()) } -> I&;
             { i +  desc.stride_shift(std::size_t(), typename Desc::index_type()) } -> I;
             { desc.stride_shift(std::size_t(), typename Desc::index_type()) + i  } -> I;
             { i -= desc.stride_shift(std::size_t(), typename Desc::index_type()) } -> I&;
             { i  - desc.stride_shift(std::size_t(), typename Desc::index_type()) } -> I;
             { i[ desc.stride_shift(std::size_t(), typename Desc::index_type()) ] } -> decltype(*i);
           };
  }

template <Descriptor Desc, Random_access_iterator I>
requires Dense_iter_draggable<Desc, I>()
  struct dense_iterator
  {
    using value_type        = typename std::iterator_traits<I>::value_type;
    using reference         = typename std::iterator_traits<I>::reference;
    using pointer           = typename std::iterator_traits<I>::pointer;
    using difference_type   = typename std::iterator_traits<I>::difference_type;
    using iterator_category = std::random_access_iterator_tag;

    using index_type        = typename Desc::index_type;
    using id_vec_t          = std::array<index_type, Desc::rank()>;
    
  protected:
    
    const Desc* desc; // Describes the iterator range, use pointer to make copyable
    I           iter; // The current element
    id_vec_t    idx;  // Counting indexes

  public:
      
    // Copyable
    dense_iterator( const dense_iterator& ) = default;
    dense_iterator& operator=( const dense_iterator& ) = default;
    
    // Moveable
    dense_iterator( dense_iterator&& ) = default;
    dense_iterator& operator=( dense_iterator&& ) = default;

    // Constructor
    //
    // "beg_it" always points to the begining position. After construction, 
    // this descriptor iterator will shift beg_it to the position at "id_init"
    
    dense_iterator( const Desc& s, I beg_it, const id_vec_t& id_init );
    dense_iterator( const Desc& s, I beg_it );

    // Returns the iterators describing slice.
    const Desc& descriptor() const { return *desc; }
    const I&    wrap_iter()  const { return iter; }
    
    // Returns corrent indexes
    const id_vec_t& index() const { return idx; }
    
    // Readable
    decltype(auto) operator*()  const { return *iter; }
    decltype(auto) operator->() const { return &(*iter); }

    // Forward Iterator
    dense_iterator& operator++();
    dense_iterator  operator++(int);

    dense_iterator& operator--();
    dense_iterator  operator--(int);
    
    dense_iterator& operator+=( const difference_type& n );
    dense_iterator& operator-=( const difference_type& n );

    dense_iterator  operator+( const difference_type& n ) const
    { 
      dense_iterator x( *this );
      x.shift( n );
      return x;
    }

    friend 
    dense_iterator operator+( const difference_type& n,  const dense_iterator& x )
    {
        return x + n;
    }
    
    dense_iterator  operator-( const difference_type& n ) const
    { 
      dense_iterator x( *this );
      x.shift( -n );
      return iter;
    }
    
    // Difference between two iterators
    difference_type operator-( const dense_iterator& x ) const
    {
      constexpr std::size_t D = Desc::rank();
      difference_type diff = 0;
      for( std::size_t sd = 0, ss = 1; sd < D; ++sd )
      {
        std::size_t d = desc->store_dim( sd );
        diff += (idx[d] - x.idx[d]) * ss;
        ss *= desc->extent(d);
      }
      return diff;
    }

    reference operator[]( const difference_type& n ) const
    {
      auto sh_res = compute_shift(n);
      return *(iter + std::get<0>(sh_res) );
    }

    // Shift iterator by index vector
    //
    template <Range R>
      dense_iterator  operator+( R&& r ) const
      { 
        dense_iterator x( *this );
        x.shift( std::forward<R>(r) );
        return x;
      }

    template <Range R>
      friend 
      dense_iterator operator+( R&& r,  const dense_iterator& x )
        {
          return x + std::forward<R>(r);
        }
    
    template <Range R>
      dense_iterator  operator-( R&& r ) const
      { 
        using std::begin;
        auto it = begin( r );
        dense_iterator x( *this );
        for( std::size_t d = 0; d < Desc::rank(); ++d, ++it )
          x.shift( d, -(*it) );
        return x;
      }

    void shift( std::size_t, difference_type );
    template <Range R> void shift( R&& );
            
  private:
    void increment();
    void decrement();
    void shift( difference_type );
    std::pair<difference_type, id_vec_t> compute_shift( difference_type ) const;
  };
               
//////////////////////////////////////////////////////////////////////////////
// Constructors
//////////////////////////////////////////////////////////////////////////////

template <Descriptor Desc, Random_access_iterator I>
requires Dense_iter_draggable<Desc, I>()
  dense_iterator<Desc, I>::dense_iterator( const Desc& s, 
                                           I it, 
                                           const id_vec_t& id_init )
    : desc(&s), iter(it), idx(id_init)
  {
    iter += desc->offset( idx );
  }

template <Descriptor Desc, Random_access_iterator I>
requires Dense_iter_draggable<Desc, I>()
  dense_iterator<Desc, I>::dense_iterator( const Desc& s, 
                                           I it )
    : desc(&s), iter(it)
  {
    for( std::size_t d = 0; d < Desc::rank(); ++d )
      idx[d] = desc->lbound(d);
    iter += desc->offset( idx );
  }

//////////////////////////////////////////////////////////////////////////////
// member function
//////////////////////////////////////////////////////////////////////////////

template <Descriptor Desc, Random_access_iterator I>
requires Dense_iter_draggable<Desc, I>()
  inline dense_iterator<Desc, I>&
  dense_iterator<Desc, I>::operator++()
  {
    increment();
    return *this;
  }

template <Descriptor Desc, Random_access_iterator I>
requires Dense_iter_draggable<Desc, I>()
  inline dense_iterator<Desc, I>
  dense_iterator<Desc, I>::operator++(int)
  {
    dense_iterator x = *this;
    increment();
    return x;
  }

template <Descriptor Desc, Random_access_iterator I>
requires Dense_iter_draggable<Desc, I>()
  inline dense_iterator<Desc, I>&
  dense_iterator<Desc, I>::operator--()
  {
    decrement();
    return *this;
  }

template <Descriptor Desc, Random_access_iterator I>
requires Dense_iter_draggable<Desc, I>()
  inline dense_iterator<Desc, I>
  dense_iterator<Desc, I>::operator--(int)
  {
    dense_iterator x = *this;
    decrement();
    return x;
  }
    
template <Descriptor Desc, Random_access_iterator I>
requires Dense_iter_draggable<Desc, I>()
  inline dense_iterator<Desc, I>&
  dense_iterator<Desc, I>::operator+=( const difference_type& n )
  {
    shift( n );
    return *this;
  }

template <Descriptor Desc, Random_access_iterator I>
requires Dense_iter_draggable<Desc, I>()
  inline dense_iterator<Desc, I>&
  dense_iterator<Desc, I>::operator-=( const difference_type& n )
  {
    shift( -n );
    return *this;
  }

// Move to the next element in the range.
template <Descriptor Desc, Random_access_iterator I>
requires Dense_iter_draggable<Desc, I>()
  inline void
  dense_iterator<Desc, I>::increment()
  {
    // innermost dimension (storage dimension = 0)
    std::size_t sd = 0;
    
    while (true) {
      std::size_t d  = desc->store_dim(sd);
      iter += desc->strides[d];
      ++idx[d];

      // If have not yet counted to the extent of the current dimension, then
      // we will continue to do so in the next iteration.
      if( idx[d] <= desc->ubound(d) )
        break;

      // Otherwise, if we have not counted to the extent in the outermost
      // dimension, move to the next dimension and try again. If d is 0, then
      // we have counted through the entire slice.
      if( ++sd != Desc::rank() ) {
        iter -= desc->strides[d] * desc->extents[d];
        idx[d] = desc->lbound(d);
      } else {
        break;
      }
    }
  }

// Move to the previous element in the range.
template <Descriptor Desc, Random_access_iterator I>
requires Dense_iter_draggable<Desc, I>()
  inline void
  dense_iterator<Desc, I>::decrement()
  {
    // innermost dimension (storage dimension = 0)
    std::size_t sd = 0;
    
    while (true) {
      std::size_t d  = desc->store_dim(sd);
      iter -= desc->strides[d];
      --idx[d];

      if( idx[d] >= desc->lbound(d) )
        break;

      if( ++sd != Desc::rank() ) {
        iter += desc->strides[d] * desc->extents[d];
        idx[d] = desc->ubound(d);
      } else {
        break;
      }
    }
  }
  
template <Descriptor Desc, Random_access_iterator I>
requires Dense_iter_draggable<Desc, I>()
  inline void
  dense_iterator<Desc, I>::shift( difference_type n )
  {
    auto sh_res = compute_shift(n);
    iter += std::get<0>( sh_res );
    for( std::size_t d = 0; d < Desc::rank(); ++d )
      idx[d] += std::get<1>( sh_res )[d];
  }

template <Descriptor Desc, Random_access_iterator I>
requires Dense_iter_draggable<Desc, I>()
  inline void
  dense_iterator<Desc, I>::shift( std::size_t d, difference_type s )
  {
    idx[d] += s;
    iter += s * desc->strides[d];    
  }
    
template <Descriptor Desc, Random_access_iterator I>
requires Dense_iter_draggable<Desc, I>()
  template <Range R>
    inline void
    dense_iterator<Desc, I>::shift( R&& ids )
    {
      using std::begin;
      auto it = begin( ids );
      for( std::size_t d = 0; d < Desc::rank(); ++d, ++it )
        shift( d, *it );
    }
    
template <Descriptor Desc, Random_access_iterator I>
requires Dense_iter_draggable<Desc, I>()
  inline std::pair<typename dense_iterator<Desc, I>::difference_type, 
                   typename dense_iterator<Desc, I>::id_vec_t>
  dense_iterator<Desc, I>::compute_shift( difference_type n ) const
  {
    id_vec_t ids, id_new;
    auto old_off = desc->offset( idx );
    difference_type ns = desc->size();
    for( std::size_t sd = Desc::rank(); sd > 0; --sd )
    {
      std::size_t d = desc->store_dim( sd-1 );
      ns /= desc->extent(d);
      ids[d] = n / ns;
      id_new[d] = idx[d] + ids[d];
      n %= ns;
    }
    auto new_off = desc->offset( id_new );
    difference_type off = new_off - old_off;
    return std::make_pair( off, ids );
  }
    
// Equality_comparable
//
// Two slice iterators are equality comparable when their slices compute the
// same sequence of elements, and the iterators refer to the same element.
//
// For efficiency, we assume the first requirement. It is undefined behavior
// to compare slice iterators from different slices.

template <Descriptor D, Random_access_iterator I>
requires Dense_iter_draggable<D, I>()
  inline bool
  operator==(const dense_iterator<D, I>& a, const dense_iterator<D, I>& b)
  {
    // WARNING: this might be too expensive
    assert(a.descriptor() == b.descriptor()); 
    return a.wrap_iter() == b.wrap_iter();
  }

template <Descriptor D, Random_access_iterator I>
requires Dense_iter_draggable<D, I>()
  inline bool
  operator!=(const dense_iterator<D, I>& a, const dense_iterator<D, I>& b)
  {
    return !(a == b);
  }

//////////////////////////////////////////////////////////////////////////////
// Factory methods
//////////////////////////////////////////////////////////////////////////////

template <Descriptor Desc, Random_access_iterator I>
requires Dense_iter_draggable<Desc, I>()
  inline decltype(auto)
  make_dense_iterator( const Desc& desc, I iter )
  {
    return dense_iterator<Desc, I>( desc, iter );
  }

} // namespace spx

#endif // SPX_DENSE_ITERATOR_H
