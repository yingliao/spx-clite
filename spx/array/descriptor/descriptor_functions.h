#ifndef SPX_DESCRIPTOR_FUNCTIONS_H
#define SPX_DESCRIPTOR_FUNCTIONS_H

namespace spx
{

template <Descriptor Desc>
requires not Nonuniform_descriptor<Desc>()
  inline decltype(auto) 
  index_of( const Desc& desc, std::size_t offset )
  {
    constexpr std::size_t D = Desc::rank();
    using id_t              = typename Desc::index_type;
    using id_vec            = std::array<id_t, D>;
    decltype(auto) d_off    = desc.data_offset();
    
    id_vec id;
    std::for_each( id.begin(), id.end(), []( id_t& v ){ v = 0; } );    
    std::size_t mod  = offset;
    std::size_t d = D;
    while( d-- > 0 )
    {
      decltype(auto) ss = desc.stride( desc.store_dim(d) );
      id[ desc.store_dim(d) ] = mod / ss - d_off / ss;
      mod %= ss;
      d_off %= ss;
    }
    
    return id;  
  }

template <Nonuniform_descriptor Desc>
  inline decltype(auto) 
  index_of( const Desc& desc, std::size_t offset )
  {
    constexpr std::size_t D = Desc::rank();
    using id_t              = typename Desc::index_type;
    using id_vec            = std::array<id_t, D>;
    decltype(auto) d_off    = desc.data_offset();

    id_vec id;
    std::for_each( id.begin(), id.end(), []( id_t& v ){ v = 0; } );
    std::size_t mod  = offset;    
    std::size_t d = D;
    while( d-- > 0 )
    {
      decltype(auto) map_d = desc.store_dim(d);
      decltype(auto) ss = desc.stride( map_d );
      decltype(auto) uid = mod / ss - d_off / ss;
      if( desc.is_uniform[ map_d ] )
        id[ map_d ] = uid;
      else
      {
        decltype(auto) iter = std::find_if( desc.shift[map_d].begin(), desc.shift[map_d].end(),
                                  [&](id_t v){ return v == uid - desc.lbound( map_d ); } );
        
        if( iter == desc.shift[map_d].end() )
          throw std::out_of_range( "index out of range: " + std::to_string(offset) );

        id[ map_d ] = (iter - desc.shift[map_d].begin()) + desc.lbound( map_d );
      }
      mod %= ss;
      d_off %= ss;
    }
    return id;
}

// extents
template <Descriptor Desc>
  inline decltype(auto)
  extents( const Desc& desc )
  {
    using size_type = typename Desc::size_type;
    constexpr auto D = Desc::rank();
    
    std::array<size_type, D> e;
    for( std::size_t n = 0; n < D; ++n ) 
      e[n] = desc.extent(n);
    return e;
  }
  
// lbounds
template <Descriptor Desc>
  inline decltype(auto)
  lbounds( const Desc& desc )
  {
    using index_type = typename Desc::index_type;
    constexpr auto D = Desc::rank();
    
    std::array<index_type, D> idx;
    for( std::size_t n = 0; n < D; ++n ) 
      idx[n] = desc.lbound(n);
    return idx;
  }

// ubounds
template <Descriptor Desc>
  inline decltype(auto)
  ubounds( const Desc& desc )
  {
    using index_type = typename Desc::index_type;
    constexpr auto D = Desc::rank();
    
    std::array<index_type, D> idx;
    for( std::size_t n = 0; n < D; ++n ) 
      idx[n] = desc.ubound(n);
    return idx;
  }
  
// iterator ubounds
template <Descriptor Desc>
requires (Desc::rank() > 1)
  inline decltype(auto)
  iter_ubounds( const Desc& desc )
  {
    auto idx = lbounds( desc );
    auto d   = desc.store_dim( Desc::rank() -1 ); // outermost storage dim
    idx[d]   = desc.ubound(d) + 1;
    return idx;
  }
  
// iterator ubounds
template <Descriptor Desc>
requires (Desc::rank() == 1)
  inline decltype(auto)
  iter_ubounds( const Desc& desc )
  {
    auto idx = ubounds( desc );
    idx[0]  += 1;
    return idx;
  }
  
// idx helpers
//
// generate list of starting idx along axis
template <typename V>
  decltype(auto) starting_idx_of_axis( V lbounds, V ubounds, std::size_t axis )
  {
    auto D = lbounds.size();
    V id;
    for( std::size_t d = 0; d < D; ++d )
      id[d] = lbounds[d];

    std::vector<V> idx;

    // special case for 1D
    if (D == 1) 
    {
      for(; id[0] <= ubounds[0]; ++id[0] )
        idx.push_back( V(id) );
      return idx;
    }

    // loop for ND
    bool stop = false;
    int  dmin = (axis == 0) ? 1 : 0;
    while( !stop ) 
    {
      idx.push_back( V(id) );
      for( int d = D - 1; d >= dmin; --d ) 
      {
        if ( d == int(axis) )
          continue;

        if ( id[d] < ubounds[d] ) 
        {
          ++id[d];
          break;
        } 
        else 
        {
          id[d] = lbounds[d];
          stop = (d == dmin);
        }
      }
    }
    return idx;
  }


} // namespace spx

#endif // SPX_DESCRIPTOR_FUNCTIONS_H
