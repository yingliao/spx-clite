#ifndef SPX_HEAD_FORWARD_ARRAY_H
#define SPX_HEAD_FORWARD_ARRAY_H

namespace spx
{

// -------------------------------------------------------------------------- //
// *-major
//
// USED in:
//
//    concepts/dense_descriptor_concepts.h
//    descriptor/storage_major.h

template <std::size_t> class row_major;
template <std::size_t> class column_major;
template <std::size_t> class any_major;

} // namespace spx

#endif // SPX_HEAD_FORWARD_ARRAY_H