#ifndef SPX_FIRSTHEADER_H
#define SPX_FIRSTHEADER_H

#ifdef __GNUC__
//  #pragma GCC diagnostic ignored "-Wsign-compare"
//  #pragma GCC diagnostic ignored "-Warray-bounds"
//  #pragma GCC diagnostic ignored "-Wnarrowing"
  #ifdef SPX_DEBUG
  #else
    #define NDEBUG
    #warning "Non-Debug Mode"
  #endif

#endif

#ifdef _DEBUG // for Visual C++

    #include <cassert>
    #define SPX_ASSERT( expr, err_msg )            \
        if( !( expr ) )                            \
            std::cerr << "[ERROR MESSAGE]: "       \
                      << (err_msg) << std::endl;   \
        assert( expr )
    #define SPX_PRE_CONDITION( expr )    assert( expr )
    #define SPX_POST_CONDITION( expr )   assert( expr )

#else

    #include <cassert>
    #define SPX_ASSERT( expr, err_msg )
    #define SPX_PRE_CONDITION( expr )
    #define SPX_POST_CONDITION( expr )

#endif
 
#define SPX_STRINGIZE( L )      #L 
#define SPX_MAKE_STRING( M, L ) M(L)
#define $LINE SPX_MAKE_STRING( SPX_STRINGIZE, __LINE__ )
#define SPX_WARNING_PRE   __FILE__ "(" $LINE ") : [!! SPX WARNING !!] "
#define SPX_DO_PRAGMA( X ) _Pragma( #X )
#define SPX_WARNING( X ) SPX_DO_PRAGMA( message SPX_WARNING_PRE X ) 

#include <thread>

namespace spx
{
  struct compile_options 
  {
    static constexpr std::size_t max_template_depth() { return 512; }
  };

  struct runtime_consts
  {
    static const std::size_t num_threads;
  };
  
  const std::size_t runtime_consts::num_threads 
    = std::thread::hardware_concurrency();
}

#endif //! SPX_FIRSTHEADER_H
