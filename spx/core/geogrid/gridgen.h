#ifndef SPX_GRIDGEN_H
#define SPX_GRIDGEN_H

namespace spx
{

template <typename T>
  struct one_sided_uniform
  {
    using value_type = T;
    
    // 1D coordinate generation 
    //
    // lmin: min. length of mesh size
    // lmax: max. length of mesh size
    // n   : number of points
    //
    decltype(auto) operator()( T lmin, T lmax, std::size_t n )
    {
      return linspace( lmin, lmax, n );
    }
  };

template <typename T>
  struct one_sided_compression
  {
    using value_type = T;

    T prm = T(2.5);
    
    d_vec<T> operator()( T lmin, T lmax, std::size_t n )
    {
      auto L       = lmax - lmin;
      auto ksi_b   = linspace( T(0), T(1), n );
      auto a       = prm / T(2);
      d_vec<T> s_b = T(1) + tan( a*(ksi_b - T(1)) ) / tan(a);
      s_b *= L;
      s_b += lmin;
      return s_b;
    }
  };

template <typename T>
  struct one_sided_expansion
  {
    using value_type = T;

    T prm = T(5.0);
    
    d_vec<T> operator()( T lmin, T lmax, std::size_t n )
    {
      auto L       = lmax - lmin;
      auto ksi_b   = linspace( T(0), T(1), n );
      auto b       = prm / T(2);
      d_vec<T> s_b = T(1) + tanh( b*(ksi_b - T(1)) ) / tanh(b);
      s_b *= L;
      s_b += lmin;
      return s_b;
    }
  };


//  define two-sided vinokur basis
//
//  scl_1: the grid spacing ratio of the FIRST grid to uniform grid
//  scl_2: the grid spacing ratio of the LAST grid to uniform grid
//
template <typename T>
  struct two_sided_vinokur
  {
    using value_type = T;

    T scl_1 = T(1);
    T scl_2 = T(1);

    d_vec<T> operator()( T lmin, T lmax, std::size_t n )
    {
      //auto ds_star = s_max / (T)n;
      auto s_max = lmax - lmin;
      auto s0    = T(1) / scl_1; //ds_star / _ds1;
      auto s1    = T(1) / scl_2; //ds_star / _ds2;
      auto A     = std::sqrt( s0 / s1 );
      auto B     = std::sqrt( s0 * s1 );

      auto prm   = (B < 1) ? cal_prm_cmprs( B ) : cal_prm_expan( B );

      d_vec<T> ksi_b = linspace( T(0), T(1), n );
      d_vec<T> u( n );
      d_vec<T> s( n );

      if( B < 1 )
        u = T(0.5) + T(0.5) * tan ( prm*(ksi_b - T(0.5) ) ) / std::tan (T(0.5) * prm);
      else
        u = T(0.5) + T(0.5) * tanh( prm*(ksi_b - T(0.5) ) ) / std::tanh(T(0.5) * prm);

      s = lmin + s_max * u / (A + (T(1) - A) * u );

      return s;
    }

  private:
    //
    //  [B < 1] calculate parameter "a" for compression grid, solved by Newton method
    //
    T cal_prm_cmprs( T B )
    {
      auto f      = [B]( T a ) { return B - std::sin(a) / a; };
      auto f_derv = [B]( T a ) { return std::sin(a) / a / a - std::cos(a) / a; };
      T a     = pi<T>;
      T a_new = a - f(a) / f_derv(a);
      std::size_t it = 0;
      while( !almost_zero( std::fabs(a - a_new) ) && it++ < 100 )
      {
        a     = a_new;
        a_new = a - f(a) / f_derv(a);
      }
      return a;
    }

    //
    //  [B >= 1] calculate parameter "b" for expansion grid, solved by Newton method
    //
    T cal_prm_expan( T B )
    {
      auto f      = [B]( T b ) { return B - std::sinh(b) / b; };
      auto f_derv = [B]( T b ) { return std::sinh(b) / b / b - std::cosh(b) / b; };
      T b     = T(1);
      T b_new = b - f(b) / f_derv(b);
      std::size_t it = 0;
      while( !almost_zero( std::fabs(b - b_new) ) && it++ < 100)
      {
        b     = b_new;
        b_new = b - f(b) / f_derv(b);
      }
      return b;
    }
  };

template <typename T>
  concept bool Grid_generator_1d()
  {
    return requires( T t, Value_type<T> x ) {
             { t( x, x, std::size_t() ) };
           };
  }

template <Vector V>
  decltype(auto) cal_length( const V& x )
  {
    using T = Value_type<V>;
    using std::begin;
    using std::end;

    using R = decltype( norm2( *(begin(x)) ) );
    
    d_vec<R> l( x.size() );

    auto it_x0 = begin( x );
    auto it_x1 = it_x0 + 1;
    auto it_o  = begin( l );
    *it_o = R{0};
    for( ++it_o; it_x1 != end( x ); ++it_x0, ++it_x1, ++it_o )
    {
      T d   = *it_x1 - *it_x0;
      *it_o = norm2( d );
    }
    return l;
  }

template <Vector V>
  decltype(auto) accumulate( const V& x )
  {
    using std::begin;
    using std::end;

    V y( x.size() );

    auto it_x  = begin( x );
    auto it_y0 = begin( y );
    auto it_y1 = it_y0 + 1;
    
    *it_y0 = *it_x;
    for( ++it_x; it_x != end( x ); ++it_x, ++it_y0, ++it_y1 )
      *it_y1 = *it_x + *it_y0;
    
    return y;
  }

template <Vector V>
  decltype(auto) accumulate_length( const V& x )
  {
    auto len = cal_length( x );
    auto acu = accumulate( len );
    return acu;
  }

template <Vector X, Vector Y>
  decltype(auto) linear_interp_1d( const X& x, const Y& y, const X& x_int )
  {
    using std::begin;
    using std::end;

    Y y_int( x_int.size() );
    auto it   = begin( x_int );
    auto it_o = begin( y_int );
    for( ; it_o != end( y_int ); ++it, ++it_o )
    {
      auto it_x0 = begin( x );
      auto it_x1 = it_x0 + 1;
      auto it_y0 = begin( y );
      auto it_y1 = it_y0 + 1;
      for( ; it_x1 != end( x ); ++it_x0, ++it_x1, ++it_y0, ++it_y1 )
      {
        if( (*it >= *it_x0) && (*it <= *it_x1) )
        {
          auto w = (*it - *it_x0) / ( *it_x1 - *it_x0 );
          *it_o  = *it_y0 + w * (*it_y1 - *it_y0);
          break;
        }
      }
    }
    return y_int;
  }

// generate grid coordinates on 1D linked edges
//
// vtx : vertices representing linked edges
// n   : number of grid points being generated
// g   : Grid_generator_1d, a generator
//
template <Vector V, Grid_generator_1d G>
  decltype(auto) gen_grids( const V& vtx, std::size_t n, G&& g  )
  {
    using T = Value_type<G>;
    
    auto len = cal_length( vtx );
    auto acu = accumulate( len );
    auto s   = g( T(0), acu[ last() ], n );
    auto x   = linear_interp_1d( acu, vtx, s );
    return x;
  }


//
//  generate SonyTFI for 4-edge domain (parameterized 2D domain)
//  
//  T: any numerical type, i.e., double, float, ...
//
//  [NOTE]: In spite of parameterized 2D domain (ksi-eta coordinates), 
//  it's not necessarily DIM = 2 since each edge can be 
//  represented in any higher dimensions, i.e., DIM = 3 to present a 
//  4-edge boundary in 3D domain.
//
//  e_ksi_min: edge T1 at ksi_min
//  e_eta_min: edge S1 at eta_min
//  e_ksi_max: edge T2 at ksi_max
//  e_eta_max: edge S2 at eta_max
//
//  Configuration:
//
//                S2 (ksi)
//     eta_max -------------->
//             ^             ^
//             |             |
//    T1 (eta) |             | T2 (eta)
//             |             |
//             |             |
//     eta_min -------------->
//                S1 (ksi)
//         ksi_min        ksi_max
//
//
template <Vector V>
  decltype(auto) soni_tfi( const V& e_ksi_min,   // edge T1
                           const V& e_eta_min,   // edge S1
                           const V& e_ksi_max,   // edge T2
                           const V& e_eta_max )  // edge S2
  {
    using R = Value_type<V>;
    using T = Value_type<R>;
    using S = static_vector<T, 2>;

    using std::begin;
    using std::end;

    auto eta_n = e_ksi_min.size(); 
    auto ksi_n = e_eta_min.size(); 

    auto al_ksi_min = accumulate_length( e_ksi_min );
    auto al_eta_min = accumulate_length( e_eta_min );
    auto al_ksi_max = accumulate_length( e_ksi_max );
    auto al_eta_max = accumulate_length( e_eta_max );

    d_vec<T> T1 = al_ksi_min / al_ksi_min[ last() ];
    d_vec<T> S1 = al_eta_min / al_eta_min[ last() ];
    d_vec<T> T2 = al_ksi_max / al_ksi_max[ last() ];
    d_vec<T> S2 = al_eta_max / al_eta_max[ last() ];

    d_arr<R, 2> grid( eta_n, ksi_n );
    d_arr<S, 2> ksi_bar_2d( eta_n, ksi_n ); 

    // assign 4 boundaries equal to edges
    auto s_eta_min = grid( first(), slice_all() );
    auto s_eta_max = grid( last(),  slice_all() );
    auto s_ksi_min = grid( slice_all(), first() );
    auto s_ksi_max = grid( slice_all(), last() );
    std::copy( begin( e_eta_min ), end( e_eta_min ), begin( s_eta_min ) );
    std::copy( begin( e_eta_max ), end( e_eta_max ), begin( s_eta_max ) );
    std::copy( begin( e_ksi_min ), end( e_ksi_min ), begin( s_ksi_min ) );
    std::copy( begin( e_ksi_max ), end( e_ksi_max ), begin( s_ksi_max ) );

    ksi_bar_2d( first(), slice_all() )[0] = S1;   // ksi_bar at eta_min
    ksi_bar_2d( first(), slice_all() )[1] = T(0); // eta_bar at eta_min
    ksi_bar_2d( last(),  slice_all() )[0] = S2;   // ksi_bar at eta_max
    ksi_bar_2d( last(),  slice_all() )[1] = T(1); // eta_bar at eta_max

    ksi_bar_2d( slice_all(), first() )[0] = T(0); // ksi_bar at ksi_min
    ksi_bar_2d( slice_all(), first() )[1] = T1;   // eta_bar at ksi_min
    ksi_bar_2d( slice_all(), last()  )[0] = T(1); // ksi_bar at ksi_max
    ksi_bar_2d( slice_all(), last()  )[1] = T2;   // eta_bar at ksi_max

    // compute domain nodes
    for( std::size_t eta = 1; eta < eta_n - 1; eta++ )
      for( std::size_t ksi = 1; ksi < ksi_n - 1; ksi++ )
      {
        T P       = T(1) - ( S2[ksi] - S1[ksi] ) * ( T2[eta] - T1[eta] );
        T ksi_bar = ( S1[ksi] + T1[eta] * (S2[ksi] - S1[ksi]) ) / P;
        T eta_bar = ( T1[eta] + S1[ksi] * (T2[eta] - T1[eta]) ) / P;
        ksi_bar_2d( eta, ksi )[0] = ksi_bar;
        ksi_bar_2d( eta, ksi )[1] = eta_bar;
      }

    // generate coordinates
    for( std::size_t eta = 1; eta < eta_n - 1; eta++ )
      for( std::size_t ksi = 1; ksi < ksi_n - 1; ksi++ )
      {
        T ksi_bar = ksi_bar_2d( eta, ksi )[0];
        T eta_bar = ksi_bar_2d( eta, ksi )[1];

        grid( eta, ksi ) =
           ( T(1) - ksi_bar ) * grid( eta, first() )
         + ksi_bar            * grid( eta, last()  )
         + ( T(1) - eta_bar ) * grid( first(), ksi )
         + eta_bar            * grid( last(),  ksi )
         - (  (T(1) - ksi_bar) * (T(1) - eta_bar) * grid( first(), first() )
            + (T(1) - ksi_bar) * eta_bar          * grid( last(),  first() )
            + ksi_bar          * (T(1) - eta_bar) * grid( first(), last()  )
            + ksi_bar          * eta_bar          * grid( last(),  last()  ) );
      }

    return grid;
  }

} // namespace spx

#endif // SPX_GRIDGEN_H

