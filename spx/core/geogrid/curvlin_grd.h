#ifndef SPX_CURVLIN_GRD_H
#define SPX_CURVLIN_GRD_H

namespace spx
{

template <typename T, bool Crstfl, Diff_operator... O>
  class curvilin_grid
  {
  public:
    static constexpr std::size_t rank() { return sizeof...(O); }
    using value_type = T; 
    
    // for convenience
    //
    static constexpr auto D = rank();
    using R = static_array<T, D, D, D>;
    using M = static_array<T, D, D>;
    using S = static_vector<T, D>;
    
  private:
    std::tuple<O...> dksi_op;
    d_arr<S, D>      ksi_crd;
    d_arr<M, D>      dx_dksi;
    d_arr<M, D>      dksi_dx;
    d_arr<T, D>      J;       // Jacobian
    d_arr<M, D>      g_con;   // contravariant g_ij (superscript)
    d_arr<M, D>      g_cov;   // covariant     g_ij (subscript)
    d_arr<R, D>      crstfl;  // cristoffel

  public:
    // Default Constructible
    curvilin_grid() = default;

    // Copyable
    curvilin_grid( const curvilin_grid& ) = default;
    curvilin_grid& operator=( const curvilin_grid& ) = default;

    // Movable
    curvilin_grid( curvilin_grid&& ) = default;
    curvilin_grid& operator=( curvilin_grid&& ) = default;

    curvilin_grid( O&&... op )
      : dksi_op( std::forward_as_tuple( std::forward<O>(op)... ) ) 
    {
      ksi_crd = tuple_unpack_and_subst( []( auto&&... u ) 
          { 
            return meshgrid( u.data_gen().diff_basis().coords()... ); 
          }, dksi_op );
    }

    // Member data access
    //
    const d_arr<S, D>& coords()     const { return ksi_crd; }
    const d_arr<M, D>& b_covrnt()   const { return dx_dksi; }
    const d_arr<M, D>& b_contra()   const { return dksi_dx; }
    const d_arr<T, D>& jacobian()   const { return J; }
    const d_arr<M, D>& g_contra()   const { return g_con; }
    const d_arr<M, D>& g_covrnt()   const { return g_cov; }

    template <bool dummy = true>
    requires (Crstfl)
      const d_arr<R, D>& cristoffel() const { return crstfl; }

    // update coordinates
    //
    template <Dense_array A>
      void update( const A& x )
      {
        update_basis( x );
        update_metric( x );
        update_cristoffel( x );
      }

    // operators
    //
    template <Stencil_array... P>
      constexpr decltype(auto) make_laplace_j( P&&... dksi_op )
      {
        return make_stencil_array( impl_laplace_j( std::forward<P>(dksi_op)... ) );
      }

    template <Stencil_array... P>
      constexpr decltype(auto) make_laplace( P&&... dksi_op )
      {
        return make_stencil_array( impl_laplace_j( std::forward<P>(dksi_op)... ) / this->jacobian() );    
      }

  private:
    template <Stencil_array... P>
      constexpr decltype(auto) impl_laplace_j( P&&... dksi_op )
      {
        return varargs_trans( 
            [&, this]( auto&&... x ) 
            {
              return varargs_sum( std::forward<decltype(x)>(x)... );
            },
            [&, this]( auto&& dksi_p, auto p )
            {
              return varargs_trans( 
                [&]( auto&&... x ) 
                {
                  return dksi_p( this->jacobian() * varargs_sum( std::forward<decltype(x)>(x)... ) );
                }, 
                [&]( auto&& dksi_q, auto q )
                {
                  return this->g_contra()[p][q] * dksi_q;
                },
                std::forward<P>(dksi_op)... );
            },
            std::forward<P>(dksi_op)... );
      }

    template <Dense_array A>
      void update_basis( const A& x )
      {
        // compute dx_dksi (cov. basis)
        //
        dx_dksi.resize( x.extents() );
        for( std::size_t i = 0; i < D; ++i )
        {
          d_arr<T, D> xi = x[i];
          tuple_for_each_with_id( [&]( auto j, auto&& dksi_j )
              {
                dx_dksi[ i ][ j() ] = dksi_j( xi );
              }, 
              dksi_op );
        }

        // compute dksi_dx & Jacobian
        //
        dksi_dx.resize( x.extents() );
        J.resize( x.extents() );
        for_each( []( auto&& u, auto&& v, auto&& j ) 
            {
              return u = inv( v, j );
            }, 
            dksi_dx, dx_dksi, J );
      }

    template <Dense_array A>
      void update_metric( const A& x )
      {
        g_con.resize( x.extents() );
        g_cov.resize( x.extents() );
        auto u = d_arr<T, D>( x.extents() );
        auto v = d_arr<T, D>( x.extents() );
        for( std::size_t i = 0; i < D; ++i )
          for( std::size_t j = 0; j < D; ++j )
          {
            u = 0;
            v = 0;
            for( std::size_t k = 0; k < D; ++k )
            {
              u += dksi_dx[i][k] * dksi_dx[j][k];
              v += dx_dksi[k][i] * dx_dksi[k][j];
            }
            g_con[i][j] = u;
            g_cov[i][j] = v;
          }
      }

    template <Dense_array A>
    requires (Crstfl)
      void update_cristoffel( const A& x )
      {
        d_arr<R, D> dg_dksi( x.extents() );
        for( std::size_t i = 0; i < D; ++i )
          for( std::size_t j = 0; j < D; ++j )
            tuple_for_each_with_id( [&]( auto k, auto&& dksi_k )
                {
                  dg_dksi[ i ][ j ][ k() ] = dksi_k( g_cov[i][j] );
                }, 
                dksi_op );

        crstfl.resize( x.extents() );
        auto u = d_arr<T, D>( x.extents() );
        for( std::size_t i = 0; i < D; ++i )
          for( std::size_t j = 0; j < D; ++j )
            for( std::size_t k = 0; k < D; ++k )
            {
              u = 0;
              for( std::size_t l = 0; l < D; ++l )
              {
                auto dg_lj_dksi_k = dg_dksi[l][j][k];
                auto dg_kl_dksi_j = dg_dksi[k][l][i];
                auto dg_jk_dksi_l = dg_dksi[j][k][l];
                u +=  g_con[i][l] / T(2) * ( dg_lj_dksi_k + dg_kl_dksi_j - dg_jk_dksi_l );
              }
              crstfl[i][j][k] = u;
            }
      }

    template <Dense_array A>
    requires not (Crstfl)
      void update_cristoffel( const A& x )
      {} 
  };

//////////////////////////////////////////////////////////////////////////////
// global factory function
//////////////////////////////////////////////////////////////////////////////

template <typename T, Diff_operator... O>
  decltype(auto) make_curvilin_grid( O&&... op )
  {
    return curvilin_grid<T, false, O...>( std::forward<O>(op)... );
  }

template <typename T, Diff_operator... O>
  decltype(auto) make_curvilin_grid_crstfl( O&&... op )
  {
    return curvilin_grid<T, true, O...>( std::forward<O>(op)... );
  }

// -------------------------------------------------------------------------- //
// curvilin_dyn_grid
//
// Time-varied curvilinear grid
//
template <typename T, bool Crstfl, Diff_operator... O>
  class curvilin_dyn_grid : public curvilin_grid<T, Crstfl, O...>
  {
  public:
    using base_t     = curvilin_grid<T, Crstfl, O...>;
    using value_type = T; 
    static constexpr auto D = base_t::rank();
    
    using S = static_vector<T, D>;
    using A = d_arr<S, D>;
    using V = std::vector<A>;
    using B = basis_fd<T>;

  private:
    B diff_t; // time diff. operator
    V hist;   // grid history
    A w;      // grid velocity
    A w_con;  // grid velocity contravariant

  public:
    // Default Constructible
    curvilin_dyn_grid() = default;

    // Copyable
    curvilin_dyn_grid( const curvilin_dyn_grid& ) = default;
    curvilin_dyn_grid& operator=( const curvilin_dyn_grid& ) = default;

    // Movable
    curvilin_dyn_grid( curvilin_dyn_grid&& ) = default;
    curvilin_dyn_grid& operator=( curvilin_dyn_grid&& ) = default;

    // dt:    time step
    // acrcy: 1-st order time diff. order of accuracy
    //
    curvilin_dyn_grid( T dt, std::size_t acrcy, O&&... op )
      : base_t( std::forward<O>(op)... ) 
    {
      // num nodes required = order + acrcy
      auto N = 1 + acrcy; 

      // init time = 0, -dt, -2*dt, ...
      auto t = linspace( 0, -T(N - 1) * dt, N );
      diff_t.update( t );
 
      diff_t.set_diff_order( 1 );
      diff_t.set_accuracy( acrcy );
   }

    // Member data access
    //
    decltype(auto) grid_vel()        const { return w; }
    decltype(auto) grid_vel_contra() const { return w_con; }
    decltype(auto) grid_history()    const { return hist; }

    // update coordinates
    //
    template <Dense_array A>
      void update( const A& x )
      {
        base_t::update( x );

        if( hist.size() == 0 ) // not init.
          init( x );
        else
          hist[0] = x;

        update_grid_velocity( x );
      }

    void roll_back()
    {
      for( std::size_t n = hist.size()-1; n >= 1; --n )
        hist[ n ] = hist[ n - 1 ];
    }

    template <Dense_array A>
      void advance( const A& x )
      {
        if( hist.size() == 0 ) // not init.
          init( x ); 
        roll_back();
        update( x );
      }

  private:
    
    template <Dense_array A>
      void init( const A& x )
      {
        // init history
        auto N = diff_t.coords().size();
        hist.resize( N );
        for( std::size_t t = 0; t < N; ++t )
          hist[t] = x;

        // init grid velocity
        w     = A( x.extents() );
        w_con = A( x.extents() );
      } 
    
    template <Dense_array A>
      void update_grid_velocity( const A& x )
      {
        // update grid velocity
        auto stn_0 = diff_t.diff_stencil()[0];
        w = 0;
        for( auto kv : stn_0 )
          w += kv.second * hist[ kv.first[0] ];

        // update contravariant grid velocity
        auto wi = d_arr<T, D>( x.extents() );
        for( std::size_t i = 0; i < D; ++i )
        {
          wi = 0;
          for( std::size_t j = 0; j < D; ++j )
            wi += base_t::dksi_dx[i][j] * w[j];
          w_con[i] = wi;
        }
      }
  };

//////////////////////////////////////////////////////////////////////////////
// global factory function
//////////////////////////////////////////////////////////////////////////////

template <typename T, Diff_operator... O>
  decltype(auto) make_curvilin_dyn_grid( T dt, O&&... op )
  {
    return curvilin_dyn_grid<T, false, O...>( dt, 2, std::forward<O>(op)... );
  }

template <typename T, Diff_operator... O>
  decltype(auto) make_curvilin_dyn_grid( T dt, std::size_t acrcy, O&&... op )
  {
    return curvilin_dyn_grid<T, false, O...>( dt, acrcy, std::forward<O>(op)... );
  }

template <typename T, Diff_operator... O>
  decltype(auto) make_curvilin_dyn_grid_crstfl( T dt, O&&... op )
  {
    return curvilin_dyn_grid<T, true, O...>( dt, 2, std::forward<O>(op)... );
  }

template <typename T, Diff_operator... O>
  decltype(auto) make_curvilin_dyn_grid_crstfl( T dt, std::size_t acrcy, O&&... op )
  {
    return curvilin_dyn_grid<T, true, O...>( dt, acrcy, std::forward<O>(op)... );
  }


} // namespace spx

#endif // SPX_CURVLIN_GRD_H

