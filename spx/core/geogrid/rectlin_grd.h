#ifndef SPX_RECTLIN_GRD_H
#define SPX_RECTLIN_GRD_H

namespace spx
{

template <Vector V, Vector... Vs>
  class rectlin_grid
  {
  public:
    static constexpr std::size_t rank() { return sizeof...(Vs) + 1; }

  private:
    static constexpr auto D = rank();
    using T = Value_type<V>;
    using S = static_vector<T, D>;
    
  private:
    std::tuple<V, Vs...> x;
    d_arr<S, D> crd;

  public:
    // Default Constructible
    rectlin_grid() = default;

    // Copyable
    rectlin_grid( const rectlin_grid& ) = default;
    rectlin_grid& operator=( const rectlin_grid& ) = default;

    // Movable
    rectlin_grid( rectlin_grid&& ) = default;
    rectlin_grid& operator=( rectlin_grid&& ) = default;

    rectlin_grid( const V& x0, const Vs&... xn )
      : x( std::make_tuple( x0, xn... ) )
    {
      crd = tuple_unpack_and_subst( []( auto&&... u ) { return meshgrid( u... ); }, x );
    }

    decltype(auto) coords()  const { return crd; }
    decltype(auto) extents() const { return crd.extents(); }

    template <Dense_array A>
    requires ( A::rank() == rank() - 1 )
      decltype(auto) perturb_surface( const A& eta, std::size_t axis, bool at_upper_face = true )
      {
        d_arr<S, D> crd_cp = crd;

        using L = typename d_arr<S, D>::index_type;
        using X = static_vector<L, D>;

        auto l_idx = crd_cp.lbounds();
        auto u_idx = crd_cp.ubounds();
        auto n     = u_idx[ axis ] - l_idx[ axis ] + 1;

        auto it = eta.dense_begin();
        for( ; it != eta.dense_end(); ++it )
        {
          auto idx_eta = it.index();
          auto eta     = *it;

          X idx_b;
          X idx_t;
          std::size_t i = 0;
          for( std::size_t d = 0; d < D; ++d )
          {
            idx_b[d] = (d == axis) ? l_idx[d] : idx_eta[i++];
            idx_t[d] = (d == axis) ? u_idx[d] : idx_b[d];
          }

          auto x_b = crd_cp( idx_b );
          auto x_t = crd_cp( idx_t );
          auto len = std::abs( x_t[ axis ] - x_b[ axis ] );
          auto scl = 1 + eta / len;
          
          X idx = idx_b;
          for( std::size_t j = 0; j < n; ++j, ++idx[ axis ] )
          {
            auto& xn = crd_cp( idx );
            if( at_upper_face )
              xn[ axis ] = x_b[ axis ] + scl * (xn[ axis ] - x_b[ axis ]);
            else
              xn[ axis ] = x_t[ axis ] + scl * (xn[ axis ] - x_t[ axis ]);
          }
        }
        
        return crd_cp;
      }
  };


template <Vector V, Vector... Vs>
  decltype(auto) make_rectlin_grid( const V& x0, const Vs&... x )
  {
    return rectlin_grid<V, Vs...>( x0, x... );
  }

} // namespace spx

#endif // SPX_RECTLIN_GRD_H

