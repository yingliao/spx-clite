#ifndef SPX_CORE_CONCEPTS_H
#define SPX_CORE_CONCEPTS_H

namespace spx
{

// template <ename T>
//   concept bool Continuous_scalar_field<T>()
//   {
//     return requires( T t ) {
//              typename T::coord_type;
//              Value_type<T>();
//              { t( std::declval<typename T::coord_type>() ) } -> Value_type<T>;
//            };
//   }
//   
// template <typename T>
//   concept bool Scalar_field()
//   {
//     return Discrete_scalar_field<T>()
//         || Continuous_scalar_field<T>;
//   }

template <typename T>
  concept bool Stencil()
  {
    return requires( T t ) {
             { T::rank() } -> Size_type<T>;
             Index_type<T>();
             typename T::weight_type;
             requires Range_of_type<typename T::idx_vec_t, Index_type<T>>();
             requires Range<T>();
             { t[ typename T::idx_vec_t{} ] } -> typename T::weight_type;
           };
  }

//template <typename T>
//  concept bool StencilGenerator()
//  {
//    return requires( T t ) {
//             { T::rank() } -> Size_type<T>;
//             Index_type<T>();
//             requires Range_of_type<typename T::idx_vec_t, Index_type<T>>();
//             requires Stencil<typename T::stencil_t>();
//             { t.stencil_at( typename T::idx_vec_t{} ) } -> typename T::stencil_t;
//           };
//  }

template <typename T>
  concept bool Stencil_generator()
  {
    return requires( T t ) {
             requires Expressible<T>();
             requires Stencil<Value_type<T>>();
           };
  }

template <typename T>
  concept bool Stencil_array()
  {
    return Dense_array<Main_type<T>>()
        && Stencil<Value_type<T>>();
  }

template <typename T, typename U>
  concept bool Linear_combinable()
  {
    return Has_multiplies<T, U>()
        && requires( T t, U u ) {
             requires Has_plus<decltype(t * u)>();
             requires Has_plus_assign<decltype(t * u)>();
             requires Default_constructible<decltype(t * u)>();
           };
  }

template <typename T, typename U>
  using linear_combine_type = decltype( (std::declval<T>() * std::declval<U>()) 
                                      + (std::declval<T>() * std::declval<U>()) ); 
  
template <typename T>
  concept bool Basis_1d()
  {
    return Default_constructible<T>()
        && Movable<T>()
        && Copyable<T>()
        && requires( T t ) {
             { T::periodic() } -> bool;
             { t.set_diff_order( std::size_t() ) };
             { t.coords()[ std::size_t() ] } -> Value_type<T>;
             { t.length() } -> Value_type<T>;
             { t.diff_stencil()[ std::size_t() ] } -> typename T::stencil_t;
           };
  }

template <typename T>
  concept bool Multistep_scheme()
  {
    return requires( T t ) {
             { T::implicit } -> bool;
             requires Static_vector<decltype(t.coef_u())>();
             requires Static_vector<decltype(t.coef_f())>();
             requires (decltype(t.coef_u())::size() == T::num_u);
             requires (decltype(t.coef_f())::size() == T::num_f);
           };
  }

template <typename T>
  concept bool Runge_kutta_scheme()
  {
    return requires( T t ) {
             { T::N } -> std::size_t;
             { t.a( std::size_t(), std::size_t() ) } -> Value_type<T>;
             { t.b( std::size_t() ) } -> Value_type<T>;
             { t.c( std::size_t() ) } -> Value_type<T>;
           };
  }


template <typename T>
  concept bool Implicit_multistep_scheme()
  {
    return Multistep_scheme<T>()
        && requires( T t ) {
             requires (T::implicit);
           };
  }

template <typename T>
  concept bool Explicit_multistep_scheme()
  {
    return Multistep_scheme<T>()
        && not Implicit_multistep_scheme<T>();
  }

// FIXME: just temporary
//
template <typename O, typename V>
  concept bool Diff_operator()
  {
    return requires( O op, V v ) {
             { op( v ) } -> V;
           };
  }

template <typename O>
  concept bool Diff_operator()
  {
    return Stencil_array<Main_type<O>>();
  }

template <typename T>
  concept bool Coordinate_generator_1d()
  {
    return requires( T t ) {
             Value_type<T>();
             requires Vector<decltype(t( Value_type<T>(), Value_type<T>(), std::size_t() ))>();
           };
  }

} // namespace spx

#endif // SPX_CORE_CONCEPTS_H
