#ifndef SPX_KRYLOV_H
#define SPX_KRYLOV_H

namespace spx
{

template <typename O, typename V>
  concept bool Krylov_operatable()
  {
    return requires( O op, V v ) {
             { op( v ) } -> V;
           };
  }

namespace krylov_impl
{
  template <typename T>
  requires not Range<T>()
    constexpr decltype(auto) a_over_b( T a, T b )
    { 
      return almost_zero(a) ? 
               T{0} :
               almost_zero(b) ? 
                 a / epsilon<T> : 
                 a / b; 
    }

  template <Range T>
    decltype(auto) a_over_b( const T& a, const T& b)
    {
      using std::begin;
      using std::end;
      T v;
      std::transform( begin(a), end(a), begin(b), begin(v), 
                      []( auto x, auto y ) { return a_over_b( x, y ); } );
      return v;
    }

  template <typename T>
  requires not Range<T>()
    constexpr decltype(auto) cal_epsilon( T x2, T y2 )
    {
      return almost_zero( y2 ) ? 
               std::sqrt( epsilon<T> ) : 
               std::sqrt( (T{1} + x2) * epsilon<T> ) / y2;
    }

  template <Range T>
    decltype(auto) cal_epsilon( const T& a, const T& b)
    {
      using std::begin;
      using std::end;
      T v;
      std::transform( begin(a), end(a), begin(b), begin(v), 
                      []( auto x, auto y ) { return cal_epsilon( x, y ); } );
      return v;
    }

}

struct cg
{
  template <typename O, typename V, typename F>
  requires Krylov_operatable<O, V>()
    static inline decltype(auto) 
    solve( const O& op, const V& b, V& x, F&& f )
    {
      using T = Value_type<V>;
      V r     = b - op( x );
      V r_old = r;
      V p     = r;

      while( true )
      {
        T r2, pAp, r_old2;
        decltype(auto) Ap = op( p );
        spx::sum( r * r, r2 );
        spx::sum( p * Ap, pAp );
        T alpha = krylov_impl::a_over_b( r2, pAp );
        x += as_const( alpha ) * p;
        r -= as_const( alpha ) * Ap;
        spx::sum( r * r, r2 );
        spx::sum( r_old * r_old, r_old2 );
        T beta = krylov_impl::a_over_b( r2, r_old2 );
        p = r + as_const( beta ) * p;
        r_old = r;
        auto err = norm2( r );
        if( f( err ) )
          break;
      };
    }
};

struct bicgstab
{
  template <typename O, typename V, typename F>
  requires Krylov_operatable<O, V>()
    static inline decltype(auto) 
    solve( const O& op, const V& b, V& x, F&& f )
    {
      using T = Value_type<V>;
      V v     = b;
      V p     = b;
      V r     = b - op(x);
      V r_hat = r;
      V s     = b;

      T rho   = T{1};
      T alpha = T{1};
      T w     = T{1};

      T beta    = T{0};
      T up      = T{0};
      T dwn     = T{0};
      T rho_old = rho;

      v = T{0};
      p = T{0};

      while( true )
      {
        rho_old = rho;
        spx::sum( r_hat * r, rho );
        up      = rho * alpha;
        dwn     = rho_old * w;
        beta    = krylov_impl::a_over_b( up, dwn );
        s       = p - (as_const( w ) * v);
        p       = r + as_const( beta ) * s;
        v       = op( p );
        up      = rho;
        spx::sum( r_hat * v, dwn );
        alpha   = krylov_impl::a_over_b( up, dwn );
        s       = r - ( as_const(alpha) * v );
        decltype(auto) t = op( s );
        spx::sum( t * s, up );
        spx::sum( t * t, dwn );
        w       = krylov_impl::a_over_b( up, dwn );
        x      += ( as_const( alpha ) * p ) + ( as_const( w ) * s );
        r       = s - ( as_const( w ) * t );
        auto err = norm2( r );
        if( f( err ) )
          break;
      }
    }
};

struct jfnk
{
  template <typename O, typename V, typename F, typename S>
  requires Krylov_operatable<O, V>()
    static inline decltype(auto) 
    solve( const O& op, V& x, F&& f, S&& lin_sol )
    {
      using T = Value_type<V>;
      
      while( true )
      {
        V    b   = T{-1} * op( x );
        auto err = norm2( b );
        
        if( f( err ) )
          break;

        T x2 = norm2( x );
  
        auto lin_op = [&]( auto& y )
        {
          // T eps = epsilon<T> * spx::sum( spx::abs( y ) + (T)1 ) / (T)y.size() / y2;
          T y2 = norm2( y );
          T eps  = krylov_impl::cal_epsilon( x2, y2 );
          V xPdx = x + eps * y;
          V xMdx = x - eps * y;
          V Jy   = ( op( xPdx ) + b ) / eps;
          // V Jy   = ( op( xPdx ) - op( xMdx ) ) / eps;
          return Jy;
        };
  
        decltype(auto) y = lin_sol( lin_op, b );
        x += y;
      }
    }
};

struct nonlin_cg
{
  template <typename O, typename G, typename V, typename T>
    static inline decltype(auto) 
    linemin( const O& op, const G& grad, V& x0, T alpha, V& p )
    {
      T rho = T{0.5};   // rho = 0 ~ 1
      T c   = T{1.e-4}; // c = 0 ~ 1
      V x   = x0;

      T fk  = op(x); 
      V gk  = grad(x);
      V xx  = x;
      
      x += alpha * p;

      T fk1 = op(x); 
      while( fk1 > fk + c * alpha * spx::dot(gk, p) )
      {
        alpha *= rho;
        x = xx + alpha * p;
        fk1 = op(x); 
      }
      return alpha;
    }
 
  template <typename O, typename G, typename V, typename F, typename S>
  requires Krylov_operatable<O, V>() // O: obj. func.
        && Krylov_operatable<G, V>() // G: grad. func.
    static inline decltype(auto) 
    solve( const O& op, const G& grad, V& x, F&& f, S&& lin_sol )
    {
      using T = Value_type<V>;
      V g = grad( x );
      V p = -g;
      V g_old = g;
      V y = g - g_old;
      V r = p;

      T alpha = T{1};
      T beta  = T{0};

      while( true )
      {
        alpha = linemin( op, grad, x, T{1}, p );
        std::cout << "alpha = " << alpha << std::endl;
        r = alpha * p;
        x += r;
        g = grad(x);
        std::cout << "x = " << x[0] << ", " << x[1] << ", " << x[2] << std::endl;
        std::cout << "g = " << g[0] << ", " << g[1] << ", " << g[2] << std::endl;
        // Beta (Polak-Ribiere)
        y = g - g_old;
        beta = krylov_impl::a_over_b( spx::dot( g, y ), spx::dot( g_old, g_old ) );
        beta = std::max( beta, T{0} );
        std::cout << "beta = " << beta << std::endl;
        p = beta * p - g;
        g_old = g;
        auto err = norm2( g );
        if( f( err ) )
          break;
      }
    }

  template <typename O, typename V, typename F, typename S>
  requires Krylov_operatable<O, V>() // O: obj. func.
    static inline decltype(auto) 
    solve( const O& op, V& x, F&& f, S&& lin_sol )
    {
      auto grad = [&]( auto& y )
      {
        using T = Value_type<V>;
        T y2   = norm2( y );
        T eps  = krylov_impl::cal_epsilon( y2, y2 ); 
        T fy0  = op( y );
        using std::begin;
        using std::end;
        V y_cp = y;
        auto it_y = begin( y_cp );
        V g = y;
        for( auto& v : g )
        {
          *it_y += eps;
          T fy1 = op( y_cp );
          v = (fy1  - fy0) / eps;
          *it_y -= eps;
          ++it_y;
        }
        return g;
      };
      return solve( op, grad, x, std::forward<F>(f), std::forward<S>(lin_sol) );
    }
};

template <typename T>
  class iter_solver_base
  {
  protected:
    T           tol;
    std::size_t max_it;
    bool        verb;

  public:
    // Default constructor
    iter_solver_base( T tol = T{1.e-16}, std::size_t max_it = 10000, bool verbose = true )
      : tol( tol ), max_it( max_it ), verb( verbose )
    {}

    // Movable 
    iter_solver_base( iter_solver_base&& ) = default;
    iter_solver_base& operator=( iter_solver_base& ) = default;

    // Copyable
    iter_solver_base( const iter_solver_base& ) = default;
    iter_solver_base& operator=( const iter_solver_base& ) = default;

    // Verbose
    //
    void set_verbose( bool v ) { verb = v; }
    bool verbose() const       { return verb; }

    // Tolerance
    //
    void set_tolerance( T t ) { tol = t; }
    T    tolerance() const    { return tol; }

    // Max. iteration
    // 
    void set_max_iter( std::size_t n ) { max_it = n; }
    std::size_t max_iter() const { return max_it; }
  };

template <typename T, typename S = bicgstab>
  class iter_solver : public iter_solver_base<T>
  {
  public:
    using base_t = iter_solver_base<T>;
    using base_t::base_t;

    // Solve
    //
    template <typename O, typename V, typename F>
    requires Krylov_operatable<O, V>()
    decltype(auto) operator()( const O& op, const V& b, F&& f )
    {
      V x = b;
      std::size_t it = 0;
      S::solve( op, b, x, [&]( auto err )
           {
             f( x ); // adjust x
             if( this->verbose() )
               std::cerr << "[ITER = " << it << "] Err. L2-norm = " << err << std::endl;
             return ( std::abs(err) <= this->tolerance() ) 
                 || ( ++it == this->max_iter() )
                 || std::isnan( std::abs(err) );
           });
      return x;
    }
 
    template <typename O, typename V>
    requires Krylov_operatable<O, V>()
    decltype(auto) operator()( const O& op, const V& b )
    {
      return (*this)( op, b, []( auto& ){} );
    }
  };

// S: nonlinear solver
// L: linear solver
template <typename T, typename S = jfnk, typename L = iter_solver<T>>
  class nonlin_solver : public iter_solver_base<T>
  {
  public:
    using base_t = iter_solver_base<T>;
    using base_t::base_t;

  private:
    L lin_sol;

  public:
    // Constructor
    //
    template <typename... Args>
      explicit nonlin_solver( L&& lin_sol, Args&&... args )
        : base_t( std::forward<Args>(args)... ),
          lin_sol( std::forward<L>(lin_sol) )
      {}

    // Linear solver
    //
    void     set_lin_solver( const L& s ) { lin_sol = s; }
    const L& lin_solver() const           { return lin_sol; }

    // Solve
    //
    template <typename O, typename V, typename F>
    requires Krylov_operatable<O, V>()
    decltype(auto) operator()( const O& op, V& x,  F&& f )
    {
      std::size_t it = 0;
      auto g = [&]( auto err )
      {
        f( x ); // adjust x
        if( this->verbose() )
          std::cerr << "[NONLIN_ITER = " << it << "] Err. L2-norm = " << err << std::endl;
        return ( std::abs(err) <= this->tolerance() ) 
            || ( ++it == this->max_iter() )
            || std::isnan( err );
      };
      S::solve( op, x, g, lin_sol ); 
      return x;
    }
 
    template <typename O, typename V>
    requires Krylov_operatable<O, V>()
    decltype(auto) operator()( const O& op, V& x )
    {
      return (*this)( op, x, []( auto& ){} );
    }
  };

template <typename T, typename S = bicgstab>
  decltype(auto) make_iter_solver( T tol = T{1.e-16}, 
                                   std::size_t max_it = 10000, 
                                   bool verbose = true,
                                   S = S() )
  {
    return iter_solver<T, S>( tol, max_it, verbose );
  }

template <typename T, typename S = jfnk, typename L = iter_solver<T>>
  decltype(auto) make_nonlin_solver( T tol = T{1.e-16}, 
                                     std::size_t max_it = 10000, 
                                     bool verbose = true,
                                     L&& lin_sol = L(),
                                     S = S() )
  {
    return nonlin_solver<T, S, L>( std::forward<L>(lin_sol), tol, max_it, verbose );
  }


template <Matrix A, Vector V, typename T = Value_type<V>, typename S = bicgstab>
  decltype(auto) iter_solve( const A& a, 
                             const V& b, 
                             T tol = 1.e-16, 
                             std::size_t max_it = 10000, 
                             bool verbose = true, 
                             S krv = S() ) 
  {
    auto op = [&]( auto& x ) 
    {
      return MxV( a, x );
    };
    auto f = make_iter_solver( tol, max_it, verbose, krv );
    auto x = f( op, b );
    return x;
  }

} // namespace spx

#endif // SPX_KRYLOV_H

