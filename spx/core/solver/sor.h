#ifndef SPX_SOR_H
#define SPX_SOR_H

namespace spx
{

// Jacobi
//
template <Stencil_array S, Dense_array A>
  void jacobi( const S& stnarr, const A& rhs, A& u  )
  {
    using W = typename Value_type<S>::weight_type;
    using X = typename Value_type<S>::idx_vec_t;
    using T = Value_type<A>;
    using C = linear_combine_type<W, T>;

    A uk( u.descriptor().extents );
    auto it  = stnarr.dense_begin();
    auto end = stnarr.dense_end();

    for( ; it != end; it++ ) 
    {
      auto           idx = X( it.index() );
      decltype(auto) stn = *it;
      decltype(auto) r   = stn.range_self_else();
      
      assert( stn.use_self() ); 

      C c{0};
      for( auto& kv : r ) 
      {
        X id = idx + kv.first;
        c += kv.second * u( id );
      }
      uk( idx ) = ( rhs( idx ) - c ) / stn.self(); 
    }
    u = uk;
  }

// Point Successive over-Relaxation updater (PSOR)
//
// omega:
//   0 to 1: under relaxation
//   1 to 2: over  relaxation
//
template <Stencil_array S, Dense_array A, typename W = typename Value_type<S>::weight_type>
  decltype(auto) psor_updater( const S& stnarr, const A& rhs, A& u, W omega = W{1}  )
  {
    using X = typename Value_type<S>::idx_vec_t;
    using T = Value_type<A>;
    using C = linear_combine_type<W, T>;

    return [&]( X idx )
    {
      decltype(auto) stn = stnarr( idx );
      decltype(auto) r   = stn.range_self_else();
      
      assert( stn.use_self() ); 

      C c{0};
      for( auto& kv : r ) 
      {
        X id = idx + kv.first;
        c += kv.second * u( id );
      }
      auto w = almost_zero( stn.self() ) ?  W(0) : (omega / stn.self());
      u( idx ) = (W(1) - omega) * u( idx ) + w * (rhs( idx ) - c); 
    };
  }

// Point Successive over-Relaxation (PSOR)
//
// omega:
//   0 to 1: under relaxation
//   1 to 2: over  relaxation
//
template <Stencil_array S, Dense_array A, typename W = typename Value_type<S>::weight_type>
  void psor( const S& stnarr, const A& rhs, A& u, W omega = W{1}  )
  {
    using X = typename Value_type<S>::idx_vec_t;
    using T = Value_type<A>;
    using C = linear_combine_type<W, T>;

    auto it  = stnarr.dense_begin();
    auto end = stnarr.dense_end();

    for( ; it != end; it++ ) 
    {
      auto           idx = X( it.index() );
      decltype(auto) stn = *it;
      decltype(auto) r   = stn.range_self_else();
      
      assert( stn.use_self() ); 

      C c{0};
      for( auto& kv : r ) 
      {
        X id = idx + kv.first;
        c += kv.second * u( id );
      }
      auto w = almost_zero( stn.self() ) ?  W(0) : (omega / stn.self());
      u( idx ) = (W(1) - omega) * u( idx ) + w * (rhs( idx ) - c); 
    }
  }

// Gauss-Seidel
//
template <Stencil_array S, Dense_array A>
  void point_gauss_seidel( const S& stnarr, const A& rhs, A& u  )
  {
    psor( stnarr, rhs, u, 1 ); 
  }

// Line Successive over-Relaxation (LSOR)
//
struct lsor_mat_slvr
{
  template <Matrix A, Vector V>
    void operator()( A& a, V& b ) const
    {
      lu_solve( a, b );
    }
};

template <typename V, Stencil_array S, Dense_array A, typename W = typename Value_type<S>::weight_type, typename M = lsor_mat_slvr>
  void lsor( const std::vector<V>& idx_starts, 
             const S& stnarr, 
             const A& rhs, 
             A& u, 
             std::size_t axis = 0, 
             W omega = W{1},
             const M& mat_slvr = M() )
  {
    using X = typename Value_type<S>::idx_vec_t;
    using T = Value_type<A>;
    using C = linear_combine_type<W, T>;
    
    std::size_t D = axis;

    auto l_idx = stnarr.lbounds();
    auto u_idx = stnarr.ubounds();
   
    for( auto id0 : idx_starts )
    {
      auto l_id = l_idx[D];
      auto u_id = u_idx[D];
      auto n    = u_id - l_id + 1;
      
      X              idx( id0 );
      d_array<C, 2>  a( n, n );
      d_vector<C>    b( n );
      a = C{0};
      b = C{0};

      for( auto id = l_id; id <= u_id; ++id )
      {
        idx[D] = id;
        decltype(auto) stn   = stnarr( idx );
        decltype(auto) r_off = stn.range_line_else( D );
        decltype(auto) r_axs = stn.range_line( D );

        C w_ii = stn.self();

        // ensemble A
        std::size_t i = id - l_id;
        for( auto kv : r_axs )
        {
          X           idj = idx + kv.first;
          std::size_t j   = idj[D] - l_id;
          a( i, j )       = C( omega * kv.second );
        }
        a( i, i ) = w_ii;
        
        // ensemble B
        C c{0};
        for( auto kv : r_off )
        {
          X idj = idx + kv.first;
          c += kv.second * u( idj );
        }
        b(i) = (W(1) - omega) * w_ii * u( idx ) + omega * (rhs( idx ) - c);
      }

      // solve
      mat_slvr( a, b );

      // assign back
      for( auto id = l_id; id <= u_id; ++id )
      {
        idx[D] = id;
        u( idx ) = b( id - l_id );
      } 
    }
  }

template <Stencil_array S, Dense_array A, typename W = typename Value_type<S>::weight_type, typename M = lsor_mat_slvr >
  void lsor( const S& stnarr, const A& rhs, A& u, std::size_t axis = 0, W omega = W{1}, const M& mat_slvr = M() )
  {
    auto l_idx      = stnarr.lbounds();
    auto u_idx      = stnarr.ubounds();
    auto idx_starts = starting_idx_of_axis( l_idx, u_idx, axis );
    lsor( idx_starts, stnarr, rhs, u, axis, omega, mat_slvr );
  }

// Line Gauss-Seidel
//
template <Stencil_array S, Dense_array A>
  void line_gauss_seidel( const S& stnarr, const A& rhs, A& u, std::size_t axis = 0  )
  {
    lsor( stnarr, rhs, u, axis, 1 ); 
  }

// ADI
//
template <Stencil_array S, Dense_array A, typename W = typename Value_type<S>::weight_type>
  void adi( const S& stnarr, const A& rhs, A& u, W omega = W{1}  )
  {
    for( std::size_t d = 0; d < S::rank(); ++d )
    {
      lsor( stnarr, rhs, u, d, omega );
    }
  } 

} // namespace spx

#endif // SPX_SOR_H
