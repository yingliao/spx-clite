#ifndef SPX_SOR_ASYNC_H
#define SPX_SOR_ASYNC_H

#include <future>
#include <thread>

namespace spx 
{

// Line Successive over-Relaxation (LSOR)
// 
template <Stencil_array S, Dense_array A, typename W = typename Value_type<S>::weight_type, typename M = lsor_mat_slvr>
  void lsor_async( const S& stnarr, const A& rhs, A& u, std::size_t axis = 0, W omega = W{1}, const M& mat_slvr = M() )
  {
    auto np = runtime_consts::num_threads; 
    if( np == 0 )
    {
      lsor( stnarr, rhs, u, axis, omega, mat_slvr );
      return;
    }

    auto task = [&]( auto idx_starts )
    {
      lsor( idx_starts, stnarr, rhs, u, axis, omega, mat_slvr );
    };
 
    auto l_idx       = stnarr.lbounds();
    auto u_idx       = stnarr.ubounds();
    auto idx_starts  = starting_idx_of_axis( l_idx, u_idx, axis );
    using V          = Value_type<decltype(idx_starts)>;
    std::size_t part = idx_starts.size() / np; 

    std::vector<std::thread> thds;
    for( std::size_t n = 0; n < np; ++n )
    {
      std::size_t beg_id = n * part;
      std::size_t end_id = ( n == np-1 ) ? idx_starts.size() : beg_id + part;

      using std::begin;
      using std::end;
      auto beg_it = begin( idx_starts ) + beg_id;
      auto end_it = begin( idx_starts ) + end_id;
      std::vector<V> sub_idx( beg_it, end_it );
      thds.push_back( std::thread( task, sub_idx ) ); // launch thread
    }

    for( std::size_t n = 0; n < np; ++n )
      if( thds[ n ].joinable() )
        thds[ n ].join();
  }

} // namespace spx

#endif // SPX_SOR_ASYNC_H
