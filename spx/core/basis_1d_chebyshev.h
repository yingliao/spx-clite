#ifndef SPX_BASIS_1D_CHEBYSHEV_H
#define SPX_BASIS_1D_CHEBYSHEV_H

namespace spx
{

template <typename T>
  class basis_1d_chebyshev
  {
  public:
    using weight_type = T;
    using index_type  = std::ptrdiff_t;
    using stencil_t   = stencil<weight_type, 1, index_type>;
    using value_type  = T;

    constexpr static bool periodic() { return false; }
 
  private:
    d_vec<T>    coord;
    T           len;
    std::size_t diff_od  = 1; // order of differentiation
    std::size_t diff_max = 1; // order of differentiation (max.)
    std::vector<d_vec<stencil_t>> stencils; 

  public:
    // Default Constructible
    basis_1d_chebyshev() = default;

    // Movable
    basis_1d_chebyshev( basis_1d_chebyshev&& ) = default;
    basis_1d_chebyshev& operator=( basis_1d_chebyshev&& ) = default;

    // Copyable
    basis_1d_chebyshev( const basis_1d_chebyshev& ) = default;
    basis_1d_chebyshev& operator=( const basis_1d_chebyshev& ) = default;

    // Constructors
    //
    basis_1d_chebyshev( std::size_t N, T min = T{-1}, T len = T{2} )
    {
      update( N, min, len ); 
    }

    // Order of differentiation
    //
    void set_diff_order( std::size_t o )
    {
      if ( o >= diff_max ) 
      {
        diff_max = o;
        update_stencil();
      }
      diff_od = o;
    }
    
    // Update coordinates
    //
    void update( std::size_t N, T min = T{-1}, T l = T{2} )
    {
      coord  = chebyshev_quad<T>::gauss_lobatto_nodes_and_weights( N )[0];
      coord *= l / (coord[ last() ] - coord[ first() ]); 
      coord += min - coord[ first() ];
      len    = l;
      update_stencil();
    } 

    // Member data access
    //
    const d_vec<T>& coords() const { return coord; } // coordinates
    T               length() const { return len; }   // bounding length

    // Get Stencil
    //
    const d_vec<stencil_t>& diff_stencil() const
    {
      return stencils[ diff_od ];
    }

    void print() const;
    
  private:
    void update_stencil()
    {
      std::size_t N = coord.size();
      
      stencils.clear();
      stencils.push_back( d_vec<stencil_t>( N ) ); // fake 0-th order
      
      for( std::size_t o = 1; o <= diff_max; ++o )
      {
        d_vec<stencil_t> ss( N );
        auto derv_mat = mth_order_polynomial_derivative_matrix( coord, len, o );
        for( std::size_t n = 0; n < N; ++n )
        {
          decltype(auto) stn = ss[n];
          for( std::size_t i = 0; i < N; ++i )
            stn[ {i - n} ] = derv_mat( n, i );
        }
        stencils.push_back( std::move( ss ) );
      }
    }
  };

//////////////////////////////////////////////////////////////////////////////
// global factory function
//////////////////////////////////////////////////////////////////////////////

template <typename T = double>
  constexpr decltype(auto) make_chebyshev_basis( std::size_t N, T min = T{-1}, T len = T{2} )
  {
    return basis_1d_chebyshev<T>( N, min, len );
  }

template <typename T = double>
  constexpr decltype(auto) make_chebyshev_basis( std::size_t N, std::size_t o, T min = T{-1}, T len = T{2} )
  {
    auto b = basis_1d_chebyshev<T>( N, min, len );
    b.set_diff_order( o );
    return b;
  }

//////////////////////////////////////////////////////////////////////////////
// member function
//////////////////////////////////////////////////////////////////////////////

template <typename T>
  void basis_1d_chebyshev<T>::print() const
  {
    // print
    for( std::size_t o = 0; o <= this->diff_max; ++o ) 
    {
      std::cout << "ORDER = " << o << std::endl;
      for( std::size_t n = 0; n < this->coords().size(); ++n )
      {
        std::cout << "NODE = " << n << std::endl;
        decltype(auto) stn = this->stencils[o][n];
        for( auto& kv : stn )
        {
          std::cout << "  [";
          for(auto k: kv.first)
            std::cout << k << ", ";
          std::cout << "]" << " : " << kv.second << std::endl;
        }
      }
      std::cout << std::endl;
    }
  }

} // namespace spx

#endif // SPX_BASIS_1D_CHEBYSHEV_H

