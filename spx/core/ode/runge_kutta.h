#ifndef SPX_RUNGE_KUTTA_H
#define SPX_RUNGE_KUTTA_H

namespace spx
{

template <typename F, typename V, typename... U>
  concept bool RK_operator()
  {
    return All( Dense_array<U>()... )
        && requires( F f, V v, U... u ) {
             { f( std::size_t(), v, u... ) };
           };
  }

// -------------------------------------------------------------------------- //
// runge_kutta
//
// Head forward
//
template <typename... Args>
  class runge_kutta;

// -------------------------------------------------------------------------- //
// runge_kutta
//
// Single-variable
//
template <typename V, Runge_kutta_scheme T, Dense_array U>
  class runge_kutta<V, T, U>
  {
  public:
    using value_type = V;
  
  private:
    V dt;        // time step
    T ts;        // time scheme
    U u0, u1;    // init / final U
    U k[ T::N ]; // innser values

  public:
    runge_kutta( V dt, T ts, const U& init )
      : dt( dt ), 
        ts( ts ), 
        u0( init ), 
        u1( init )
    {}

    // time step
    //
    void set_step( V dt ) { this->dt = dt; }
    V    step() const     { return dt; }

    // init cond
    //
    void     set_init_cond( const U& init ) { u0 = init; }
    const U& init_cond() const              { return u0; }

    // time scheme
    //
    const T& scheme() const { return ts; }

    // k
    //
    const U ( &get_k() const )[ T::N ] { return k; }
          U ( &get_k()       )[ T::N ] { return k; }

    // advance = roll_back + solve
    //
    template <typename F>
      const U& advance( F&& f )
      {
        roll_back();
        return solve( std::forward<F>(f) );
      }

    // roll_back
    //
    void roll_back()
    { 
      roll_back( u1 ); 
    }

    void roll_back( const U& u )
    {
      u0 = u;
    }

    // solve
    //
    template <typename F>
    requires RK_operator<F, V, U>()
      const U& solve( F&& f )
      {
        U un = u0;
        k[0] = f( 0, V(0), un );
        for( std::size_t s = 1; s < T::N; ++s )
        {
          un = u0 / dt;
          for( std::size_t i = 0; i < s; ++i )
            un += ts.a( s-1, i ) * k[ i ]; 
          un *= dt;

          k[s] = f( s, ts.c( s-1 )*dt, un );
        }

        u1 = u0;
        for( std::size_t s = 0; s < T::N; ++s )
          u1 += dt * ts.b( s ) * k[ s ];
        return u1;
      }

  };

//////////////////////////////////////////////////////////////////////////////
// global factory function
//////////////////////////////////////////////////////////////////////////////

template <typename V, Runge_kutta_scheme T, Dense_array U>
  decltype(auto) make_runge_kutta( V dt, T ts, const U& init )
  {
    return runge_kutta<V, T, U>( dt, ts, init );
  }

// -------------------------------------------------------------------------- //
// runge_kutta
//
// Multi-variables 
//
template <typename V, Runge_kutta_scheme T, Dense_array... U>
requires ( sizeof...(U) > 1 )
  class runge_kutta<V, T, U...>
  {
  public:
    using value_type = V;
  
  private:
    V dt;                       // time step
    T ts;                       // time scheme
    std::tuple<U...> u0, u1;    // init / final U
    std::tuple<U...> k[ T::N ]; // innser values

  public:
    runge_kutta( V dt, T ts, const U&... init )
      : dt( dt ), 
        ts( ts ),
        u0( std::make_tuple( init... ) ),
        u1( std::make_tuple( init... ) )
    {}

    // time step
    //
    void set_step( V dt ) { this->dt = dt; }
    V    step() const     { return dt; }

    // init cond
    //
    template <Dense_array X, std::size_t I = 0>
      void set_init_cond( const X& init ) { std::get<I>( u0 ) = init; }

    template <std::size_t I = 0>
      decltype(auto) init_cond() const { return std::get<I>( u0 ); }

    // time scheme
    //
    const T& scheme() const { return ts; }

    // k
    //
    template <std::size_t I>
      decltype(auto) get_k( std::size_t s, size_constant<I> x = size_constant<0>() ) const 
      { 
        return std::get<I>( k[s] ); 
      }

    template <std::size_t I>
      decltype(auto) get_k( std::size_t s, size_constant<I> x = size_constant<0>() )
      {
        return std::get<I>( k[s] ); 
      }

    // advance = roll_back + solve
    //
    template <typename F>
      decltype(auto) advance( F&& f )
      {
        roll_back();
        return solve( std::forward<F>(f) );
      }

    // roll_back
    //
    void roll_back()
    { 
      tuple_unpack_and_subst( [&]( auto&&... x ){ this->roll_back( x... ); }, u1 );
    }

    void roll_back( const U&... u )
    {
      auto u_tup = std::forward_as_tuple( std::forward<const U&>( u )... );
      tuple_for_each_with_id( [&]( auto I, auto& u0_v )
                              { 
                                u0_v = std::get<I.value>( u_tup ); 
                              }, 
                              u0 );
    }

    // solve
    //
    template <typename F>
    requires RK_operator<F, V, U...>()
      decltype(auto) solve( F&& f )
      {
        std::tuple<U...> un;

        tuple_for_each_with_id( [&]( auto I, auto& un_v ) 
            { 
              un_v = std::get<I.value>( u0 ); 
            },
            un );

        tuple_unpack_and_subst( [&]( auto&&... un_v )
            {
              decltype(auto) fu_tup = f( 0, V(0), std::forward<decltype(un_v)>( un_v )... );
              tuple_for_each_with_id( [&]( auto I, auto& k_v ) 
                { 
                  k_v = std::get<I.value>( fu_tup ); 
                }, 
                k[0] );
            },
            un );
        
        for( std::size_t s = 1; s < T::N; ++s )
        {
          tuple_for_each_with_id( [&]( auto I, auto& un_v ) 
              { 
                un_v = std::get<I.value>( u0 ); 
              },
              un );

          for( std::size_t i = 0; i < s; ++i )
          {
            tuple_for_each_with_id( [&]( auto I, auto& un_v ) 
                { 
                  un_v += dt * ts.a( s-1, i ) * std::get<I.value>( k[ i ] ); 
                },
                un );
          }

          tuple_unpack_and_subst( [&]( auto&&... un_v )
              {
                decltype(auto) fu_tup = f( s, 
                                           ts.c( s-1 ) * dt, 
                                           std::forward<decltype(un_v)>( un_v )... );

                tuple_for_each_with_id( [&]( auto I, auto& k_v ) 
                  { 
                    k_v = std::get<I.value>( fu_tup ); 
                  }, 
                  k[ s ] );
              },
              un );
        }

        tuple_for_each_with_id( [&]( auto I, auto& u1_v ) 
            { 
              u1_v = std::get<I.value>( u0 ); 
            },
            u1 );

        for( std::size_t s = 0; s < T::N; ++s )
        {
          tuple_for_each_with_id( [&]( auto I, auto& u1_v ) 
              { 
                u1_v += dt * ts.b( s ) * std::get<I.value>( k[ s ] ); 
              },
              u1 );
        }

        return u1;
      }
  };

//////////////////////////////////////////////////////////////////////////////
// global factory function
//////////////////////////////////////////////////////////////////////////////

template <typename V, Runge_kutta_scheme T, Dense_array... U>
requires ( sizeof...(U) > 1 )
  decltype(auto) make_runge_kutta( V dt, T ts, const U&... init )
  {
    return runge_kutta<V, T, U...>( dt, ts, init... );
  }


} // namespace spx

#endif // SPX_RUNGE_KUTTA_H
