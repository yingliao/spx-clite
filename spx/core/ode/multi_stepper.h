#ifndef SPX_MULTI_STEPPER_H
#define SPX_MULTI_STEPPER_H

namespace spx
{

// -------------------------------------------------------------------------- //
// multi_diff_term
//
// The implementation class for differential term: explicit or implicit 
//
template <Dense_array U, Multistep_scheme T, Diff_operator<U> O>
  class multi_diff_term
  {
  public:
    static constexpr bool implicit() { return Implicit_multistep_scheme<T>(); }

  private: 
    T ts;
    U fu[ T::num_f ];
    O op;

  public:
    multi_diff_term( const U& init, T ts_term, O&& diff_op )
      : ts( ts_term ),
        op( std::forward<O>(diff_op) )
    {
      fu[0] = op( init );
      for( std::size_t i = 1; i < T::num_f; ++i )
        fu[i] = fu[i-1];
    }

    decltype(auto) diff_op()     const { return op; }
    const T&       time_scheme() const { return ts; }

    decltype(auto) calculate_rhs()
    {
      decltype(auto) r   = ts.coef_f();
      std::size_t    beg = T::implicit ? 1 : 0;
      
      U rhs( fu[0].extents() );
      if( T::implicit && T::num_f <= 1 )
        rhs = 0;
      else
        rhs = r[beg] * fu[beg];

      for( std::size_t i = beg+1; i < T::num_f; ++i ) 
        rhs += r[i] * fu[i];
      return rhs;
    }
   
    void roll_back( const U& u )
    {
      if( T::implicit )
      {
        fu[ 0 ] = op( u );
        for( std::size_t i = T::num_f-1; i >= 1; --i )
          fu[ i ] = fu[ i-1 ];
      }
      else // Explicit
      {
         for( std::size_t i = T::num_f-1; i >= 1; --i )
          fu[ i ] = fu[ i-1 ];
         fu[ 0 ] = op( u );
      }
    }
  };

// -------------------------------------------------------------------------- //
// multi_stepper_term - base class
//
template <typename... Args>
  class multi_stepper_term;

template <Dense_array U, Multistep_scheme T, Diff_operator<U> O>
  class multi_stepper_term<U, T, O>
  {
  public:
    using term_t = multi_diff_term<U, T, O>;
    static constexpr bool implicit() { return term_t::implicit(); }

  private:
    term_t term;

  public:
    multi_stepper_term( const U& init, T ts_term, O&& diff_op )
      : term( init, ts_term, std::forward<O>(diff_op) )
    {}
 
    decltype(auto) calculate_rhs()
    {
      return term.calculate_rhs();
    }

    void roll_back( const U& u )
    {
      term.roll_back( u );
    }

    template <typename F, typename... X>
    requires Implicit_multistep_scheme<T>()
      decltype(auto) solve_implicit( F&& f, X&&... args )
      {
        return f( std::forward<X>(args)..., 
                  term.time_scheme().coef_f()[0],
                  std::forward<decltype(term.diff_op())>( term.diff_op() ) );
      }
 
    template <typename F, typename... X>
    requires Explicit_multistep_scheme<T>()
      decltype(auto) solve_implicit( F&& f, X&&... args )
      {
        return f( std::forward<X>(args)... );
      }
 };

// -------------------------------------------------------------------------- //
// multi_stepper_term - recursive class
//
template <Dense_array U, Multistep_scheme T, Diff_operator<U> O, typename... Args>
requires Not_empty<Args...>()
  class multi_stepper_term<U, T, O, Args...> : public multi_stepper_term<U, Args...>
  {
  public:
    using base_t = multi_stepper_term<U, Args...>;
    using term_t = multi_diff_term<U, T, O>;
    static constexpr bool implicit() { return term_t::implicit() || base_t::implicit(); }
  
  private:
    term_t term;

  public:
    multi_stepper_term( const U& init, T ts_term, O&& diff_op, Args&&... args  )
      : base_t( init, std::forward<Args>(args)... ),
        term( init, ts_term, std::forward<O>(diff_op) )
    {}

    decltype(auto) calculate_rhs()
    {
      decltype(auto) rhs = base_t::calculate_rhs();
      rhs += term.calculate_rhs();
      return rhs;
    }

    void roll_back( const U& u )
    {
      base_t::roll_back( u );
      term.roll_back( u );
    }

    template <typename... X>
    requires Implicit_multistep_scheme<T>()
      decltype(auto) solve_implicit( X&&... args )
      {
        return base_t::solve_implicit( std::forward<X>(args)..., 
                                       term.time_scheme().coef_f()[0],
                                       std::forward<decltype(term.diff_op())>( term.diff_op() ) );
      }
 
    template <typename... X>
    requires Explicit_multistep_scheme<T>()
      decltype(auto) solve_implicit( X&&... args )
      {
        return base_t::solve_implicit( std::forward<X>(args)... );
      }
 };

// -------------------------------------------------------------------------- //
// multi_stepper_term - source term
//
template <Dense_array U, Dense_array S, typename... Args>
requires Not_empty<Args...>()
  class multi_stepper_term<U, S, Args...>: public multi_stepper_term<U, Args...>
  {
  public:
    using base_t = multi_stepper_term<U, Args...>;
    static constexpr bool implicit() { return base_t::implicit(); }

  private:
    S s;

  public:
    multi_stepper_term( const U& init, const S& src, Args&&... args )
      : base_t( init, std::forward<Args>(args)... ),
        s( src )
    {}
 
    decltype(auto) calculate_rhs()
    {
      decltype(auto) rhs = base_t::calculate_rhs();
      rhs += s;
      return rhs;
    }

    void roll_back( const U& u )
    {
      base_t::roll_back( u );
    }

    template <typename... X>
      decltype(auto) solve_implicit( X&&... args )
      {
        return base_t::solve_implicit( std::forward<X>(args)... );
      }
 };

// -------------------------------------------------------------------------- //
// multi_stepper_transcoef - main class
//
template <typename V, Multistep_scheme T, Dense_array U, typename J, typename... Args>
requires Not_empty<Args...>()
  class multi_stepper_transcoef : public multi_stepper_term<U, Args...> 
  {
  public:
    using base_t     = multi_stepper_term<U, Args...>;
    using value_type = V;
    static constexpr bool implicit() { return base_t::implicit(); }

  protected:
    V dt;             // time step
    T ts;             // time scheme for transient term
    U u[ T::num_u ];  // u[0, 1, 2, 3,,,] = u[n+1, n, n-1, n-2...]
    J j[ T::num_u ];  // transient term coefficients (jacobian) 

  public:
    multi_stepper_transcoef( V dt, T ts_trans, const U& init, const J& init_j, Args&&... args )
      : base_t( init, std::forward<Args>(args)... ),
        dt( dt ),
        ts( ts_trans )
    {
      reset( init, init_j );
    }

    // time step
    //
    void set_time_step( V dt ) { this->dt = dt; }
    V    time_step() const     { return dt; }

    // reset initial condition
    //
    void reset( const U& init, const J& init_j )
    {
      for( std::size_t i = 0; i < T::num_u; ++i )
      {
        u[ i ] = init;
        j[ i ] = init_j;
      }

      for( std::size_t i = 0; i < T::num_u; ++i )
        roll_back( init, init_j ); 
    }

    // advance & solve
    //
    template <bool dummy = true>
    requires not implicit()
      const U& advance( const J& j_new )
      {
        roll_back();
        j[0] = j_new;
        return solve();
      }

    template <typename F>
    requires implicit()
      const U& advance( const J& j_new, F&& f ) // f = implicit solver
      {
        roll_back();
        j[0] = j_new;
        return solve( std::forward<F>(f) );
      }

    // roll back
    //
    void roll_back()
    {
      base_t::roll_back( u[0] );
      for( std::size_t i = T::num_u-1; i >= 1; --i )
      {
        u[ i ] = u[ i-1 ];
        j[ i ] = j[ i-1 ];
      }
    }

    void roll_back( const U& u0, const J& j0 )
    {
      update_current( u0, j0 );
      roll_back();
    }

    void update_current( const U& u0, const J& j0 )
    {
      u[0] = u0;
      j[0] = j0;
    }

    // solve
    //
    template <bool dummy = true>
    requires not implicit()
      decltype(auto) solve() // Explicit
      {
        u[0]  = base_t::calculate_rhs() * dt;
        decltype(auto) a = ts.coef_u();
        for( std::size_t i = 1; i < T::num_u; ++i )
          u[0] -= a[i] * j[i] * u[i];
        u[0] /= a[0] * j[0];
        return u[0];
      }

    template <typename F>
    requires implicit() // Implicit
      decltype(auto) solve( F&& f ) // f = solver
      {
        decltype(auto) rhs = base_t::calculate_rhs(); 
        decltype(auto) a   = ts.coef_u();
        for( std::size_t i = 1; i < T::num_u; ++i )
          rhs -= a[i] * j[i] / dt * u[i];
        J coef_a = a[0] * j[0] / dt;
        u[0] = base_t::solve_implicit( std::forward<F>(f), rhs, coef_a, u[0] );
        return u[0];
      }
  };

//////////////////////////////////////////////////////////////////////////////
// global factory function
//////////////////////////////////////////////////////////////////////////////

template <typename V, Multistep_scheme T, Dense_array U, typename J, typename... Args>
requires Not_empty<Args...>()
  constexpr decltype(auto) make_multi_stepper_transcoef( V dt, T ts_trans, const U& init, const J& init_j, Args&&... args )
  {
    return multi_stepper_transcoef<V, T, U, J, Args...>( dt, 
                                                         ts_trans, 
                                                         init, 
                                                         init_j, 
                                                         std::forward<Args>(args)... );
  }

// -------------------------------------------------------------------------- //
// multi_stepper - main class
//
template <typename V, Multistep_scheme T, Dense_array U, typename... Args>
requires Not_empty<Args...>()
  class multi_stepper : public multi_stepper_transcoef<V, T, U, V, Args...>
  {
  public:
    using base_t = multi_stepper_transcoef<V, T, U, V, Args...>;
    static constexpr bool implicit() { return base_t::implicit(); }

  public:
    multi_stepper( V dt, T ts_trans, const U& init, Args&&... args )
      : base_t( dt, ts_trans, init, 1, std::forward<Args>(args)... )
    {}

    // reset initial condition
    //
    void reset( const U& init )
    {
      base_t::reset( init, 1 );
    }

    // advance & solve
    //
    template <bool dummy = true>
    requires not implicit()
      const U& advance()
      {
        return base_t::advance( 1 );
      }

    template <typename F>
    requires implicit()
      const U& advance( F&& f ) // f = implicit solver
      {
        return base_t::advance( 1, std::forward<F>(f) );
      }

    // roll back
    //
    void roll_back()
    {
      base_t::roll_back();
    }

    void roll_back( const U& u0 )
    {
      base_t::roll_back( u0, 1 );
    }

    void update_current( const U& u0 )
    {
      base_t::update_current( u0, 1 );
    }
  };

//////////////////////////////////////////////////////////////////////////////
// global factory function
//////////////////////////////////////////////////////////////////////////////

template <typename V, Multistep_scheme T, Dense_array U, typename... Args>
requires Not_empty<Args...>()
  constexpr decltype(auto) make_multi_stepper( V dt, T ts_trans, const U& init, Args&&... args )
  {
    return multi_stepper<V, T, U, Args...>( dt, ts_trans, init, std::forward<Args>(args)... );
  }


} // namespace spx

#endif // SPX_MULTI_STEPPER_H
