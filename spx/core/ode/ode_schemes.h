#ifndef SPX_ODE_SCHEMES_H
#define SPX_ODE_SCHEMES_H

namespace spx
{

// General linear multistep scheme:
//
//  	1/dt * ( a1*u(n+1) + a2*u(n) + a3*u(n-1) + ...) = b1*f(n+1) + b2*f(n) + b3*f(n-1) ...
//

// Leap Frog
//
template <typename T>
  struct leap_frog
  {
    using value_type = T;
    static constexpr bool         implicit = false;
    static constexpr std::size_t  num_u    = 3;
    static constexpr std::size_t  num_f    = 1;
    
    static static_vector<T, num_u> coef_u()
    {
      return { T(1)/T(2), T(0), T(-1)/T(2) };
    }
    
    static static_vector<T, num_f> coef_f()
    {
      return { T(1) };
    }
  };

//////////////////////////////////////////////
// Adams-Bashforth Methods
//////////////////////////////////////////////

// forward Euler (AB1)
//
template <typename T>
  struct forward_euler
  {
    using value_type = T;
    static constexpr bool        implicit = false;
    static constexpr std::size_t num_u    = 2;
    static constexpr std::size_t num_f    = 1;
    
    static static_vector<T, num_u> coef_u()
    {
      return { T(1), T(-1) }; 
    }
    
    static static_vector<T, num_f> coef_f()
    {
      return { T(1) };
    }
  };

template <typename T>
  using ab1 = forward_euler<T>;

// AB2
//
template <typename T>
  struct ab2
  {
    using value_type = T;
    static constexpr bool        implicit = false;
    static constexpr std::size_t num_u    = 2;
    static constexpr std::size_t num_f    = 2;
    
    static static_vector<T, num_u> coef_u()
    {
      return { T(1), T(-1) };
    }
    
    static static_vector<T, num_f> coef_f()
    {
      return { T(3)/T(2), T(-1)/T(2) };
    }
  };

// AB3
// 
template <typename T>
  struct ab3
  {
    using value_type = T;
    static constexpr bool        implicit = false;
    static constexpr std::size_t num_u    = 2;
    static constexpr std::size_t num_f    = 3;
    
    static static_vector<T, num_u> coef_u()
    {
      return { T(1), T(-1) };
    }
    
    static static_vector<T, num_f> coef_f()
    {
      return { T(23)/T(12), T(-16)/T(12), T(5)/T(12) };
    }
  };

// AB4
//
template <typename T>
  struct ab4
  {
    using value_type = T;
    static constexpr bool        implicit = false;
    static constexpr std::size_t num_u    = 2;
    static constexpr std::size_t num_f    = 4;
    
    static static_vector<T, num_u> coef_u()
    {
      return { T(1), T(-1) };
    }
    
    static static_vector<T, num_f> coef_f()
    {
      return { T(55)/T(24), T(-59)/T(24), T(37)/T(24), T(-9)/T(24) };
    }
  };

//////////////////////////////////////////////
// Adams-Moulton Methods
//////////////////////////////////////////////

// backward Euler (aka BDF1, AM1)
//
template <typename T>
  struct backward_euler
  {
    using value_type = T;
    static constexpr bool        implicit = true;
    static constexpr std::size_t num_u    = 2;
    static constexpr std::size_t num_f    = 1;

    static static_vector<T, num_u> coef_u()
    {
      return { T(1), T(-1) };
    }

    static static_vector<T, num_f> coef_f()
    {
      return { T(1) };
    }
  };

template <typename T>
  using am1 = backward_euler<T>;

// Crank-Nicolson (aka AM2)
//
template <typename T>
  struct crank_nicolson
  {
    using value_type = T;
    static constexpr bool        implicit = true;
    static constexpr std::size_t num_u    = 2;
    static constexpr std::size_t num_f    = 2;
    
    static static_vector<T, num_u> coef_u()
    {
      return { T(1), T(-1) };
    }

    static static_vector<T, num_f> coef_f()
    {
      return { T(1)/T(2), T(1)/T(2) };
    }
  };

template <typename T>
  using am2 = crank_nicolson<T>;

// theta 
//
template <typename T>
  struct theta
  {
    using value_type = T;
    static constexpr bool        implicit = true;
    static constexpr std::size_t num_u    = 2;
    static constexpr std::size_t num_f    = 2;
    
    static static_vector<T, num_u> coef_u()
    {
      return { T(1), T(-1) };
    }

    static_vector<T, num_f> coef_f() const
    {
      return { v1, v2 };
    }

    theta( T f1, T f2 )
      : v1(f1), v2(f2)
    {}

  private:
    T v1, v2;
  };

// AM3
//
template <typename T>
  struct am3
  {
    using value_type = T;
    static constexpr bool        implicit = true;
    static constexpr std::size_t num_u    = 2;
    static constexpr std::size_t num_f    = 3;

    static static_vector<T, num_u> coef_u()
    {
      return { T(1), T(-1) };
    }

    static static_vector<T, num_f> coef_f()
    {
      return { T(5)/T(12), T(8)/T(12), T(-1)/T(12) };
    }
  };

// AM4
//
template <typename T>
  struct am4
  {
    using value_type = T;
    static constexpr bool        implicit = true;
    static constexpr std::size_t num_u    = 2;
    static constexpr std::size_t num_f    = 4;

    static static_vector<T, num_u> coef_u()
    {
      return { T(1), T(-1) };
    }

    static static_vector<T, num_f> coef_f()
    {
      return { T(9)/T(24), T(19)/T(24), T(-5)/T(24), T(1)/T(24) };
    }
  };

//////////////////////////////////////////////
// Backward Difference Formulas
//////////////////////////////////////////////

// bdf1
//
template <typename T>
  using bdf1 = backward_euler<T>;

// bdf2
//
template <typename T>
  struct bdf2
  {
    using value_type = T;
    static constexpr bool        implicit = true;
    static constexpr std::size_t num_u    = 3;
    static constexpr std::size_t num_f    = 1;

    static static_vector<T, num_u> coef_u()
    {
      return { T(3)/T(2), T(-4)/T(2), T(1)/T(2) };
    }

    static static_vector<T, num_f> coef_f()
    {
      return { T(1) };
    }
  };

// bdf3
//
template <typename T>
  struct bdf3
  {
    using value_type = T;
    static constexpr bool        implicit = true;
    static constexpr std::size_t num_u    = 4;
    static constexpr std::size_t num_f    = 1;
    
    static static_vector<T, num_u> coef_u()
    {
      return { T(11)/T(6), T(-18)/T(6), T(9)/T(6), T(-2)/T(6) };
    }
    
    static static_vector<T, num_f> coef_f()
    {
      return { T(1) };
    }
  };

// bdf4
//
template <typename T>
  struct bdf4
  {
    using value_type = T;
    static constexpr bool        implicit = true;
    static constexpr std::size_t num_u    = 5;
    static constexpr std::size_t num_f    = 1;

    static static_vector<T, num_u> coef_u()
    {
      return { T(25)/T(12), T(-48)/T(12), T(36)/T(12), T(-16)/T(12), T(3)/T(12) };
    }

    static static_vector<T, num_f> coef_f()
    {
      return { T(1) };
    }
  };


//////////////////////////////////////////////
// Runge-Kutta schemes 
//////////////////////////////////////////////

// RK1 = Euler
//
template <typename T>
  struct rk1
  {
    using value_type = T;
    static constexpr std::size_t N = 1;

    static T a( std::size_t s, std::size_t j ) { return 0; }
    static T b( std::size_t s )                { return 1; }
    static T c( std::size_t s )                { return 0; }
  };

// RK2
//
// p = 1/2: midpoint
// p = 2/3: Ralston
// p = 1  : Heun
//
template <typename T>
  struct rk2
  {
    using value_type = T;
    static constexpr std::size_t N = 2;

    T a( std::size_t s, std::size_t j ) { return p; }
    T b( std::size_t s )                { return coef_b[ s ]; }
    T c( std::size_t s )                { return p; }

    rk2( T p = T(1) / T(2) )
      : p( p )
    {
      coef_b = { T(1) - ( T(1)/ T(2) / p ), T(1) / T(2) / p };
    }

  private:
    T p;
    static_vector<T, N> coef_b;
  };

// RK3
//
template <typename T>
  struct rk3
  {
    using value_type = T;
    static constexpr std::size_t N = 3;

    static T a( std::size_t s, std::size_t j ) { return coef_a()[ (s+1)*s/2+j ]; }
    static T b( std::size_t s )                { return coef_b()[ s ]; }
    static T c( std::size_t s )                { return coef_c()[ s ]; }

    static static_vector<T, N*(N-1)/2> coef_a()
    {
      return { T(1)/T(2), 
               T(-1)    , T(2) }; 
    }
 
    static static_vector<T, N> coef_b()
    {
      return { T(1)/T(6), T(2)/T(3), T(1)/T(6) };
    }
 
    static static_vector<T, N-1> coef_c()
    {
      return { T(1)/T(2), T(1) };
    }
  };


// RK4
//
template <typename T>
  struct rk4
  {
    using value_type = T;
    static constexpr std::size_t N = 4;

    static T a( std::size_t s, std::size_t j ) { return coef_a()[ (s+1)*s/2+j ]; }
    static T b( std::size_t s )                { return coef_b()[ s ]; }
    static T c( std::size_t s )                { return coef_c()[ s ]; }

    static static_vector<T, N*(N-1)/2> coef_a()
    {
      return { T(1)/T(2), 
               T(0)     , T(1)/T(2), 
               T(0)     , T(0)     , T(1) }; 
    }
 
    static static_vector<T, N> coef_b()
    {
      return { T(1)/T(6), T(1)/T(3), T(1)/T(3), T(1)/T(6) };
    }
 
    static static_vector<T, N-1> coef_c()
    {
      return { T(1)/T(2), T(1)/T(2), T(1) };
    }
  };

// RK4: 3/8-rule
//
template <typename T>
  struct rk4_38
  {
    using value_type = T;
    static constexpr std::size_t N = 4;

    static T a( std::size_t s, std::size_t j ) { return coef_a()[ (s+1)*s/2+j ]; }
    static T b( std::size_t s )                { return coef_b()[ s ]; }
    static T c( std::size_t s )                { return coef_c()[ s ]; }

    static static_vector<T, N*(N-1)/2> coef_a()
    {
      return {  T(1)/T(3), 
               -T(1)/T(3),  T(1), 
                T(1)     , -T(1),  T(1) }; 
    }
 
    static static_vector<T, N> coef_b()
    {
      return { T(1)/T(8), T(3)/T(8), T(3)/T(8), T(1)/T(8) };
    }
 
    static static_vector<T, N-1> coef_c()
    {
      return { T(1)/T(3), T(2)/T(3), T(1) };
    }
  };


} // namespace spx

#endif // SPX_ODE_SCHEMES_H
