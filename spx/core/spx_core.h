#include "concepts.h"
#include "stencil.h"
//#include "stencil_generator.h"
#include "stencil_array.h"
#include "test_SG.h"
#include "basis_1d_fd.h"
#include "basis_1d_fourier.h"
#include "basis_1d_chebyshev.h"
#include "stencil_gen.h"

// solver
#include "solver/sor.h"
#include "solver/krylov.h"

#include "solver/sor_async.h"

// ode (time marching)
#include "ode/ode_schemes.h"
#include "ode/multi_stepper.h"
#include "ode/runge_kutta.h"

// geo_grid
#include "geogrid/rectlin_grd.h"
#include "geogrid/curvlin_grd.h"
#include "geogrid/gridgen.h"
