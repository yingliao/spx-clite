#ifndef SPX_STENCIL_H
#define SPX_STENCIL_H

#include <unordered_map>

namespace spx
{
  
template <typename T, std::size_t D, typename X = std::ptrdiff_t>
  class stencil
  {
  public:
    using index_type  = X;
    using weight_type = T;
    using idx_vec_t   = static_vector<X, D>;
    using size_type   = std::size_t;

    static constexpr std::size_t rank() { return D; }
    static constexpr idx_vec_t zero_idx() { return idx_vec_t{0}; }
    static stencil<T, D, X> on_node() 
    {
      stencil<T, D, X> s;
      s[ {0} ] = {1};
      return s;
    }
    
  private:
    struct idx_hash;
    
  private:
    std::unordered_map<idx_vec_t, weight_type, idx_hash> data;

  public:
    // Default Constructible
    stencil() = default;
    
    // Copy
    stencil( const stencil& ) = default;
    stencil& operator=( const stencil& ) = default;
    
    // Copy from type U
    //template <typename U>
    //requires Assignable<T&, U>()
    //  stencil( const stencil<U, D, X>& );
      
    // Convert to type U
    template <typename U>
    requires Convertible<T, U>()
      operator stencil<U, D, X>() const { return as_weight_type( U() ); }

    template <typename U>
    requires Convertible<T, U>()
      decltype(auto) as_weight_type( U&& ) const 
      {
        stencil<U, D, X> s;
        for( auto& kv : *this ) {
          s[kv.first] = U( kv.second );
        }
        return s;
      }

    // Move
    stencil( stencil&& ) = default;
    stencil& operator=( stencil&& ) = default;
   
    // size
    std::size_t size() const;
    
    // clear
    void clear();
    
    // range
    template <typename P>
    decltype(auto) make_range( P pred ) const;
    decltype(auto) range() const;
    decltype(auto) range_line( std::size_t dim ) const;
    decltype(auto) range_line_else( std::size_t dim ) const;
    decltype(auto) range_self_else() const;    
    decltype(auto) range_off() const;
    
    // iterator
    decltype(auto) begin() const { return range().begin(); }
    decltype(auto) end()   const { return range().end(); }
    
    //self node has weight?
    bool use_self() const  { return data.find( zero_idx() ) != data.end(); }
    
    // weight of self node
    weight_type self() const;
    
    // add stencil
    //
    template <Input_iterator I>
      void add( I first, I last, weight_type );
      
    template <typename U>
      void add( std::initializer_list<U>, weight_type );
      
    template <Range R>
      void add( R&& shift, weight_type );
      
    template <bool dummy = true>
    requires (D > 1)
      void add( std::size_t dim, index_type, weight_type );

    template <bool dummy = true>
    requires (D == 1)
      void add( index_type shift, weight_type w ) 
      { 
        add( 0, shift, w );
      }      
      
    // access data
    decltype(auto) operator[]( const idx_vec_t& k ) const { return data[k]; }
    decltype(auto) operator[]( const idx_vec_t& k )       { return data[k]; }
    
    // operator overloading
    template <typename U>
    requires Has_plus_assign<T, U>()
      stencil& operator += ( const stencil<U, D, X>& x );
    
    template <typename U>
    requires Has_minus_assign<T, U>()
      stencil& operator -= ( const stencil<U, D, X>& x );
      
    template <typename U>
    requires Has_multiplies_assign<T, U>()
      stencil& operator *= ( const U& c );

    template <typename U>
    requires Has_divides_assign<T, U>()
      stencil& operator /= ( const U& c );
            
    //template <typename U>
    //requires Has_multiplies<T, U>()
    //  decltype(auto) operator () ( const stencil<U, D, X>& x ) const;
    
    template <Expression_iterator E>
    requires Stencil<Value_type<E>>()
      decltype(auto) operator () ( E e ) const; 

    template <Expression_iterator E>
    requires not Stencil<Value_type<E>>() 
          && Linear_combinable<T, Value_type<E>>()
      decltype(auto) operator () ( E e ) const
      {
        using U = Value_type<E>;
        using S = function_result<ops::multiplies, T, U>;
        S s{0};
        for( auto kv_i : *this )
        {
          // shift iterator e by the shifting index kv_i.first
          s += kv_i.second * (*e( kv_i.first ));
        }
        return s;
      }

  private:    
    decltype(auto) make_pred_line( std::size_t dim ) const;
  };

//////////////////////////////////////////////////////////////////////////////
// member types
//////////////////////////////////////////////////////////////////////////////
  
template <typename T, std::size_t D, typename X>
  struct stencil<T, D, X>::idx_hash
  {
    template <typename U>
      size_t operator()( const U& key ) const
      {
        return key[0];
      }
  };
  
//////////////////////////////////////////////////////////////////////////////
// constructor
//////////////////////////////////////////////////////////////////////////////

//template <typename T, std::size_t D, typename X>
//  template <typename U>
//  requires Assignable<T&, U>()
//    stencil<T, D, X>::stencil( const stencil<U, D, X>& s )
//    {
//      for( auto kv : s )
//        data[ kv.first ] = kv.second;
//    }
    
//////////////////////////////////////////////////////////////////////////////
// member functions
//////////////////////////////////////////////////////////////////////////////
  
template <typename T, std::size_t D, typename X>
  inline std::size_t
  stencil<T, D, X>::size() const
  {
    return data.size();    
  }

template <typename T, std::size_t D, typename X>
  inline void
  stencil<T, D, X>::clear()
  {
    data.clear(); 
  }
  
template <typename T, std::size_t D, typename X>
  template <typename P>
    decltype(auto) 
    stencil<T, D, X>::make_range( P pred ) const
    {
      return make_filtered_range( make_filter( data.begin(), 
                                               data.end(), 
                                               pred ) );
    }
  
template <typename T, std::size_t D, typename X>
  decltype(auto) 
  stencil<T, D, X>::range() const 
  { 
    return make_range( []( auto ) { return true; } ); 
  }
    
template <typename T, std::size_t D, typename X>
  decltype(auto) 
  stencil<T, D, X>::range_line( std::size_t dim ) const 
  { 
    return make_range( make_pred_line(dim) ); 
  }
  
template <typename T, std::size_t D, typename X>
  decltype(auto) 
  stencil<T, D, X>::range_line_else( std::size_t dim ) const 
  { 
    auto g = [this, dim]( auto kv ) 
    { 
      auto f = this->make_pred_line( dim );
      return !f(kv); 
    };
    return make_range( g ); 
  }
  
template <typename T, std::size_t D, typename X>
  decltype(auto) 
  stencil<T, D, X>::range_self_else() const
  { 
    return make_range( [&]( auto kv )
    {
      bool b = false;
      for( std::size_t d = 0; d < D; ++d )
        b |= kv.first[ d ] != 0;
      return b;
    } ); 
  }
    
template <typename T, std::size_t D, typename X>
  decltype(auto) 
  stencil<T, D, X>::range_off() const 
  { 
    return make_range( [&]( auto kv )
    {
      bool b = true;
      for( std::size_t d = 0; d < D; ++d )
        b &= kv.first[ d ] != 0;
      return b;
    } ); 
  }    
    
template <typename T, std::size_t D, typename X>
  decltype(auto) 
  stencil<T, D, X>::make_pred_line( std::size_t dim ) const
  {
    return [&]( auto kv ) -> bool
            { 
              auto key = kv.first;
              std::size_t nz = 0;
              for( std::size_t d = 0; d < D; ++d )
                if( (d != dim) && (key[d] == 0) ) 
                  ++nz;
              return (nz >= D-1);
            };
  }    
  
template <typename T, std::size_t D, typename X>
  inline typename stencil<T, D, X>::weight_type
  stencil<T, D, X>::self() const
  {
    decltype(auto) it = data.find( zero_idx() );
    if( it != data.end() )
      return it->second;
    return weight_type{1};
  }
  
template <typename T, std::size_t D, typename X>
  template <Input_iterator I>
    inline void
    stencil<T, D, X>::add( I first, I last, weight_type w )
    {
      using std::begin;
      idx_vec_t idx;
      std::copy( first, last, begin(idx) );      
      data[ idx ] += w;
    }
    
template <typename T, std::size_t D, typename X>
  template <typename U>
    inline void
    stencil<T, D, X>::add( std::initializer_list<U> shift, weight_type w )
    {
      add( shift.begin(), shift.end(), w );
    }
    
template <typename T, std::size_t D, typename X>
  template <Range R>
    inline void
    stencil<T, D, X>::add( R&& shift, weight_type w )
    {
      using std::begin;
      using std::end;
      add( begin(shift), end(shift), w );
    }
    
template <typename T, std::size_t D, typename X>
  template <bool dummy>
  requires (D > 1)
    inline void
    stencil<T, D, X>::add( std::size_t dim, index_type shift, weight_type w)
    {
      idx_vec_t idx;
      for( std::size_t d = 0; d < D; ++d )
        idx[d] = (d == dim) ? shift : index_type(0);
      data[ idx ] += w;
    }

// Operator overloading    
    
template <typename T, std::size_t D, typename X>
  template <typename U>
  requires Has_plus_assign<T, U>()
    inline stencil<T, D, X>& 
    stencil<T, D, X>::operator += ( const stencil<U, D, X>& x )
    {
      for( auto kv : x )
        data[ kv.first ] += kv.second;
      return *this;
    }
  
template <typename T, std::size_t D, typename X>
  template <typename U>
  requires Has_minus_assign<T, U>()
    inline stencil<T, D, X>& 
    stencil<T, D, X>::operator -= ( const stencil<U, D, X>& x )
    {
      for( auto kv : x )
        data[ kv.first ] -= kv.second;
      return *this;
    }
    
template <typename T, std::size_t D, typename X>
  template <typename U>
  requires Has_multiplies_assign<T, U>()
    inline stencil<T, D, X>& 
    stencil<T, D, X>::operator *= ( const U& c )
    {
      for( auto& kv : data )
        kv.second *= c;
      return *this;
    }
    
template <typename T, std::size_t D, typename X>
  template <typename U>
  requires Has_divides_assign<T, U>()
    inline stencil<T, D, X>& 
    stencil<T, D, X>::operator /= ( const U& c )
    {
      for( auto& kv : data )
        kv.second /= c;
      return *this;
    }
    
//template <typename T, std::size_t D, typename X>
//  template <typename U>
//  requires Has_multiplies<T, U>()
//    inline decltype(auto) 
//    stencil<T, D, X>::operator () ( const stencil<U, D, X>& x ) const
//    {
//      using R = function_result<ops::multiplies, T, U>;
//      stencil<R, D, X> s;
//      for( auto kv_i : *this )
//        for( auto kv_j : x )
//        {
//          idx_vec_t idx = kv_i.first + kv_j.first;
//          s[ idx ] += kv_i.second * kv_j.second;
//        }
//      return s;
//    }

template <typename T, std::size_t D, typename X>
  template <Expression_iterator E>
  requires Stencil<Value_type<E>>()
    inline decltype(auto) 
    stencil<T, D, X>::operator () ( E e ) const 
    {
      using U = typename Value_type<E>::weight_type;
      using R = function_result<ops::multiplies, T, U>;
      stencil<R, D, X> s;
      for( auto kv_i : *this )
      {
        // shift iterator e by the shifting index kv_i.first
        decltype(auto) x_j = *e.shift( kv_i.first );
        for( auto kv_j : x_j )
        {
          idx_vec_t idx = kv_i.first + kv_j.first;
          s[ idx ] += kv_i.second * kv_j.second;
        }
      }
      return s;
    }

//////////////////////////////////////////////////////////////////////////////
// operator overloading - out class
//////////////////////////////////////////////////////////////////////////////

// +

template <typename T, typename U, std::size_t D, typename X>
requires Has_plus<T, U>()
  decltype(auto) operator + ( const stencil<T, D, X>& s1,  const stencil<U, D, X>& s2 )
  {
    using R = function_result<ops::plus, T, U>;
    stencil<R, D, X> s = s1;
    s += s2;
    return s;
  }

// -

template <typename T, typename U, std::size_t D, typename X>
requires Has_minus<T, U>()
  decltype(auto) operator - ( const stencil<T, D, X>& s1,  const stencil<U, D, X>& s2 )
  {
    using R = function_result<ops::minus, T, U>;
    stencil<R, D, X> s = s1;
    s -= s2;
    return s;
  }

// *

template <typename T, typename U, std::size_t D, typename X>
requires Has_multiplies<T, U>()
  decltype(auto) operator * ( const stencil<T, D, X>& s1, U&& c )
  {
    using R = function_result<ops::multiplies, T, U>;
    stencil<R, D, X> s = s1;
    s *= c;
    return s;
  }

template <typename T, typename U, std::size_t D, typename X>
requires Has_multiplies<T, U>()
  decltype(auto) operator * ( U&& c, const stencil<T, D, X>& s1 )
  {
    return s1 * c;
  }

// /

template <typename T, typename U, std::size_t D, typename X>
requires Has_divides<T, U>()
  decltype(auto) operator / ( const stencil<T, D, X>& s1, U&& c )
  {
    using R = function_result<ops::divides, T, U>;
    stencil<R, D, X> s = s1;
    s /= c;
    return s;
  }
  
// unary minus

template <typename T, std::size_t D, typename X>
requires Has_unary_minus<T>()
  decltype(auto) operator - ( const stencil<T, D, X>& s1 )
  {
    using R = function_result<ops::unary_minus, T>;
    stencil<R, D, X> s = s1;
    s *= T{-1};
    return s;
  }  
              
         
} // namespace spx

#endif // SPX_STENCIL_H
