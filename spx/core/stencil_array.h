#ifndef SPX_STENCIL_ARRAY_H
#define SPX_STENCIL_ARRAY_H

namespace spx
{
template <Sparse_data_generator SG>
  class stencil_array : public dense_array<Value_type<SG>, sparse_storage<SG>>
  {
  public:
    using base_t     = dense_array<Value_type<SG>, sparse_storage<SG>>;
    using data_gen_t = SG;
    using base_t::base_t;

    template <Stencil_generator T>
      decltype(auto) operator()( T&& sg ) 
      {
        return as_expr(*this)( as_expr( std::forward<T>(sg) ) ); 
      }

    template <Expressible T>
    requires Linear_combinable<Value_type<T>, typename Value_type<SG>::weight_type>()
      decltype(auto) operator()( const T& t ) 
      {
        return as_expr(*this)( as_expr(t) ); 
      }

    template <Indexable... Args>
      decltype(auto) operator()( Args&&... args )
      {  
        return base_t::operator()( std::forward<Args>(args)... );  
      }
      
    template <Indexable... Args>
      decltype(auto) operator()( Args&&... args ) const
      {  
        return base_t::operator()( std::forward<Args>(args)... );  
      }
  };

//////////////////////////////////////////////////////////////////////////////
// global factory function
//////////////////////////////////////////////////////////////////////////////

template <Sparse_data_generator SG>
  constexpr decltype(auto) make_stencil_array( SG&& sg )
  {
    return stencil_array<SG>( std::move(sg) );
  }

template <typename... Args>
  constexpr decltype(auto) make_stencil_array( stencil_array<Args...>&& x )
  {
    return x;
  }

} // namespace spx

#endif // SPX_STENCIL_ARRAY_H
