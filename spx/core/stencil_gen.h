#ifndef SPX_STENCIL_GEN_H
#define SPX_STENCIL_GEN_H

namespace spx
{

// -------------------------------------------------------------------------- //
// stencil_gen
//
// A stencil generator for any Stencil_generator 
//
//template <Stencil_generator SG>
//  struct stencil_gen
//  {
//    using self_t          = stencil_gen<SG>;
//    using value_type      = Value_type<SG>; 
//    using weight_type     = typename value_type::weight_type; 
//    using index_type      = typename value_type::index_type;
//    using expr_type       = decltype( as_expr( std::declval<SG>() ) );
//    using descriptor_type = Main_type<decltype( std::declval<expr_type>().descriptor() )>;
//
//  private:
//    expr_type       iter;
//    descriptor_type desc;
//
//  public:
//    stencil_gen( const SG& sg  )
//      : iter( as_expr(sg) ), desc( iter.descriptor() )
//    {}
//
//    const descriptor_type& descriptor() const { return desc; }
// 
//    decltype(auto) get( std::ptrdiff_t id ) const
//    {
//      decltype(auto) idx = desc.index_of( id );
//      return *(this->iter(idx));
//    }
//  };
//
//template <Stencil_generator SG>
//requires Expression_iterator<SG>()
//  decltype(auto) make_stencil_array( SG expr )
//  {
//    stencil_gen<SG> gen( expr );
//    return make_stencil_array( std::move(gen) );
//  }

// -------------------------------------------------------------------------- //
// stencil_func_gen
//
// A stencil generator by function callback
//
template <Descriptor Desc, typename F>
  struct stencil_func_gen
  {
    using self_t          = stencil_func_gen<Desc, F>;
    using value_type      = Main_type<decltype(std::declval<F>()( std::declval<Desc&>(), std::ptrdiff_t() ))>;
    using weight_type     = typename value_type::weight_type; 
    using index_type      = typename value_type::index_type;
    using descriptor_type = Desc; 

  private:
    Desc desc;
    F    f;

  public:
    stencil_func_gen( const Desc& desc, F&& f )
      : desc( desc ), f( std::forward<F>(f) )
    {}

    const descriptor_type& descriptor() const { return desc; }
 
    decltype(auto) get( std::ptrdiff_t id ) const
    {
      return f( descriptor(), id );
    }
  };

template <Descriptor Desc, typename F>
  decltype(auto) make_stencil_array( const Desc& desc, F&& f )
  {
    auto gen = stencil_func_gen<Desc, F>( desc, std::forward<F>(f) );
    return make_stencil_array( std::move(gen) );
  }

template <std::size_t D, typename F>
  decltype(auto) make_stencil_array( std::initializer_list<std::size_t> shape, F&& f )
  {
    using Desc = c_descriptor<D>;
    auto gen   = stencil_func_gen<Desc, F>( Desc( shape ), std::forward<F>(f) );
    return make_stencil_array( std::move(gen) );
  }

template <typename T, Descriptor Desc>
  decltype(auto) make_on_node( const Desc& desc )
  {
    using S  = stencil<T, Desc::rank()>;
    auto f   = []( auto& desc, auto id ){ return S::on_node(); };
    using F  = decltype(f);
    auto gen = stencil_func_gen<Desc, F>( desc, std::forward<F>(f) );
                                          
    return make_stencil_array( std::move(gen) );
  }

template <typename T, std::size_t D>
  decltype(auto) make_on_node( std::initializer_list<std::size_t> shape )
  {
    using Desc = c_descriptor<D>;
    using S    = stencil<T, D>;
    auto f     = []( auto& desc, auto id ){ return S::on_node(); };
    using F    = decltype(f);
    auto gen   = stencil_func_gen<Desc, F>( shape, std::forward<F>(f) );
    return make_stencil_array( std::move(gen) );
  }

// half-on_node
//
template <typename T, Descriptor Desc>
  decltype(auto) make_half_on_node( const Desc& desc, std::size_t axis = 0 )
  {
    using S    = stencil<T, Desc::rank()>;
    using V    = typename S::idx_vec_t;

    auto f = [d = axis]( auto& desc, auto id )
             {
               V idx0{0}; 
               V idx1{0}; idx1[ d ] =  1;
               V idx2{0}; idx2[ d ] = -1;
               static S s1;
               s1[ idx0 ] = 0.5; 
               s1[ idx1 ] = 0.5;
               static S s2;
               s2[ idx0 ] = 0.5; 
               s2[ idx2 ] = 0.5;

               decltype(auto) idx = desc.index_of( id );
               bool last = ( idx[ d ] == desc.ubound( d ) );
               return last ? s2 : s1;
             };
    using F  = decltype(f);
    auto gen = stencil_func_gen<Desc, F>( desc, std::forward<F>(f) );
    return make_stencil_array( std::move(gen) );
  }

template <Stencil_generator SG>
requires Expression_iterator<SG>()
  decltype(auto) make_stencil_array( SG expr )
  {
    return make_stencil_array( expr.descriptor(),
                               [e = expr]( auto&& desc, auto idx ) -> decltype(auto)
                               {
                                return *e( desc.index_of( idx ) );
                               } );
  }

// -------------------------------------------------------------------------- //
// stencil_selector
//
// A stencil generator by a selection function that picks one of the 
// Stencil_generator... SG
//
template <typename F, Expression_iterator... E>
  struct stencil_selector
  {
    using self_t          = stencil_selector<F, E...>;
    using descriptor_type = Main_type<decltype( std::declval<expr_func_impl::expr_descriptor>()( std::declval<E>()... ) )>;
    using idx_vec_t       = decltype( std::declval<descriptor_type>().index_of( std::ptrdiff_t() ) );
    using value_type      = Main_type<decltype( std::declval<F>()( std::declval<const descriptor_type&>(), 
                                                                   std::declval<idx_vec_t>(),
                                                                   std::declval<E>()... ) )>;
    using weight_type     = typename value_type::weight_type; 
    using index_type      = typename value_type::index_type;
 
  private:
    F                f;
    std::tuple<E...> exprs;
    descriptor_type  desc;

  public:
    stencil_selector( F&& f, E... e )
      : f( std::forward<F>(f) ),
        exprs( std::make_tuple(e...) ),
        desc( tuple_unpack_and_subst( expr_func_impl::expr_descriptor(), exprs ) )
    {}

    const descriptor_type& descriptor() const { return desc; }
 
    decltype(auto) get( std::ptrdiff_t id ) const
    {
      decltype(auto) idx = desc.index_of( id );
      decltype(auto) stn = tuple_unpack_and_subst( [&]( auto&... e ) 
                                                   { return f( desc, idx, e(idx)... ); },
                                                   exprs );
      return stn;
    }
  };

template <typename F, Stencil_generator... SG>
requires Not_empty<SG...>()
  decltype(auto) make_stencil_array( F&& f, SG&&... sg )
  {
    auto gen = stencil_selector<F, decltype( as_expr( std::forward<SG>(sg) ) )...>( 
               std::forward<F>(f), 
               as_expr( std::forward<SG>(sg) )... );
    return make_stencil_array( std::move(gen) );
  }

// -------------------------------------------------------------------------- //
// stencil_1d_to_nd
//
// A stencil generator that wraps N axes of Basis_1d (providing differential 
// stencils on 1D domain) to ND space
//
template <Basis_1d B, std::size_t N>
  struct stencil_1d_to_nd
  {
    using weight_type     = typename B::stencil_t::weight_type;
    using index_type      = typename B::stencil_t::index_type;
    using value_type      = stencil<weight_type, N, index_type>;
    using idx_vec_t       = typename value_type::idx_vec_t;
    using descriptor_type = c_descriptor<N>;

  private:
    descriptor_type desc;
    B               basis;
    std::size_t     wrap_d = 0;
    
  public:
    stencil_1d_to_nd() = default;

    // Copyable
    stencil_1d_to_nd( const stencil_1d_to_nd& ) = default;
    stencil_1d_to_nd& operator=( const stencil_1d_to_nd& ) = default;

    // Movable
    stencil_1d_to_nd( stencil_1d_to_nd&& ) = default;
    stencil_1d_to_nd& operator=( stencil_1d_to_nd&& ) = default;

    // Constructors
    template <typename D>
      stencil_1d_to_nd( std::initializer_list<D> shape, const B& basis, std::size_t axis = 0 ) 
        : desc( shape ), basis( basis ), wrap_d( axis )
      {}

    template <Range_of_type<std::size_t> R>
      stencil_1d_to_nd( R&& shape, const B& basis, std::size_t axis = 0 ) 
        : desc( std::forward<R>(shape) ), basis( basis ), wrap_d( axis )
      {}

    // Descriptor
    const descriptor_type& descriptor() const { return desc; }

    // Basis
    const B& diff_basis() const { return basis; }

    // Diff. directin
    std::size_t diff_axis() const { return wrap_d; }

    // Data generator
    decltype(auto) get( std::ptrdiff_t id ) const
    {
      decltype(auto) idx = desc.index_of( id );
      decltype(auto) stn = basis.diff_stencil()[ idx[ wrap_d ] ];

      value_type s;
      idx_vec_t  k(0);
      for( auto& kv : stn )
      {
        k[ wrap_d ] = kv.first[0];
        s[ k ] = kv.second;
      }
      return s;
    }
};

template <std::size_t N, typename D, typename B>
requires Basis_1d<Main_type<B>>()
  decltype(auto) make_stencil_array( std::initializer_list<D> shape, const B& basis, std::size_t axis = 0  ) 
  {
    auto gen = stencil_1d_to_nd<Main_type<B>, N>( shape, basis, axis );
    return make_stencil_array( std::move(gen) );
  }

template <std::size_t N, Range_of_type<std::size_t> R, typename B>
requires Basis_1d<Main_type<B>>()
  decltype(auto) make_stencil_array( R&& shape, const B& basis, std::size_t axis = 0  ) 
  {
    auto gen = stencil_1d_to_nd<Main_type<B>, N>( std::forward<R>(shape), basis, axis );
    return make_stencil_array( std::move(gen) );
  }


} // namespace spx

#endif // SPX_STENCIL_GEN_H
