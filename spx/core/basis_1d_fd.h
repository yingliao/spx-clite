#ifndef SPX_BASIS_1D_FD_H
#define SPX_BASIS_1D_FD_H

namespace spx
{

// -------------------------------------------------------------------------- //
// basis_1d_fd
//
//
template <typename T, pbc_type P>
  class basis_1d_fd 
  {
  public:
    using value_type  = T;
    using weight_type = T;
    using index_type  = std::ptrdiff_t;
    using stencil_t   = stencil<weight_type, 1, index_type>;
    
    constexpr static bool periodic() { return P == pbc_type::periodic; }

  protected:
    d_vec<T>    coord;
    T           len;
    std::size_t acrcy    = 1; // order of accuracy 
    std::size_t diff_od  = 1; // order of differentiation
    std::size_t diff_max = 1; // order of differentiation (max.)
    std::vector<d_vec<stencil_t>> stencils; 

  public:
    // Default Constructible
    basis_1d_fd() = default;

    // Movable
    basis_1d_fd( basis_1d_fd&& ) = default;
    basis_1d_fd& operator=( basis_1d_fd&& ) = default;

    // Copyable
    basis_1d_fd( const basis_1d_fd& ) = default;
    basis_1d_fd& operator=( const basis_1d_fd& ) = default;

    // Constructors
    //
    basis_1d_fd( const d_vec<T>& coord, T len = T{-1} )
    {
      update( coord, len );
    }
 
    // Order of accuracy 
    //
    std::size_t accuracy() const
    { 
      return acrcy; 
    }
    
    void set_accuracy( std::size_t o ) 
    {
      if (o != acrcy) 
      {
        acrcy = o;
        update_stencil();
      }
    }

    // Order of differentiation
    //
    void set_diff_order( std::size_t o )
    {
      if ( o >= diff_max ) 
      {
        diff_max = o;
        update_stencil();
      }
      diff_od = o;
    }

    // Update coordinates
    //
    void update( const d_vec<T>& x, T pbc_len = T{-1} )
    {
      coord = x;
      len = pbc_len > 0 ? pbc_len : coord[ last() ] - coord[ first() ];
      update_stencil();
    }
   
    // Member data access
    //
    const d_vec<T>& coords() const { return coord; } // coordinates
    T               length() const { return len; }   // bounding length

    // Get Stencil
    //
    const d_vec<stencil_t>& diff_stencil() const
    {
      return stencils[ diff_od ];
    }

    void print() const;
  
  private:
    template <bool dummy = true>
    requires basis_1d_fd<T, P>::periodic()
      void update_stencil();  // periodic

    template <bool dummy = true>
    requires not basis_1d_fd<T, P>::periodic()
      void update_stencil(); // non-periodic

    void init_stencils()
    {
      stencils.clear();
      for( std::size_t o = 0; o <= diff_max; ++o )
        stencils.push_back( d_vec<stencil_t>( coord.size() ) );
    }
 };

//////////////////////////////////////////////////////////////////////////////
// type alias 
//////////////////////////////////////////////////////////////////////////////

template <typename T>
  using basis_fd = basis_1d_fd<T, pbc_type::non_periodic>;

template <typename T>
  using basis_fd_pbc = basis_1d_fd<T, pbc_type::periodic>;

//////////////////////////////////////////////////////////////////////////////
// global factory function
//////////////////////////////////////////////////////////////////////////////

template <typename T>
  constexpr decltype(auto) make_fd_basis( const d_vec<T>& coord )
  {
    return basis_fd<T>( coord );
  }

template <typename T>
  decltype(auto) make_fd_basis( const d_vec<T>& coord, std::size_t o )
  {
    basis_fd<T> b( coord );
    b.set_diff_order( o );
    return b;
  }

template <typename T>
  decltype(auto) make_fd_basis( const d_vec<T>& coord, std::size_t o, std::size_t a )
  {
    basis_fd<T> b( coord );
    b.set_diff_order( o );
    b.set_accuracy( a );
    return b;
  }

template <typename T, typename L>
requires Convertible<L, T>()
  constexpr decltype(auto) make_fd_basis_pbc( const d_vec<T>& coord, L pbc_len )
  {
    return basis_fd_pbc<T>( coord, T(pbc_len) );
  }

template <typename T, typename L>
requires Convertible<L, T>()
  decltype(auto) make_fd_basis_pbc( const d_vec<T>& coord, L pbc_len, std::size_t o )
  {
    basis_fd_pbc<T> b( coord, T(pbc_len) );
    b.set_diff_order( o );
    return b;
  }

template <typename T, typename L>
requires Convertible<L, T>()
  decltype(auto) make_fd_basis_pbc( const d_vec<T>& coord, L pbc_len, std::size_t o, std::size_t a )
  {
    basis_fd_pbc<T> b( coord, T(pbc_len) );
    b.set_diff_order( o );
    b.set_accuracy( a );
    return b;
  }

//////////////////////////////////////////////////////////////////////////////
// member function
//////////////////////////////////////////////////////////////////////////////

template <typename T, pbc_type P>
  template <bool dummy>
  requires basis_1d_fd<T, P>::periodic()
    inline void basis_1d_fd<T, P>::update_stencil()
    {
      decltype(auto) x = coords();
      decltype(auto) l = length();
		  // SPX_PRE_CONDITION( node_id < x.size() && node_id >= 0 );
      
      // periodic id function
      auto period_id = [ &x ]( std::ptrdiff_t i ) -> std::ptrdiff_t
      {
        auto n = std::ptrdiff_t( x.size() );
        return (i >= 0) ? (i % n) : n + (i % n);
      };

      init_stencils();

      // loop nodes
      for( std::size_t n = 0; n < x.size(); ++n )
      {
        for( std::size_t o = 0; o <= diff_max; ++o )
        {
          std::size_t stn_pts = o + acrcy;
          std::ptrdiff_t l_id  = n - (stn_pts / 2) + (stn_pts % 2 == 0 ? 1 : 0);
          std::ptrdiff_t u_id  = n + (stn_pts / 2);

          d_array<T, 1> x_s( u_id - l_id + 1 );
          auto j = l_id;
          for( std::size_t i = 0; i < x_s.size(); ++i, ++j )
          {
            x_s( i ) = x( period_id(j) );
          }
        
          decltype(auto) C = fd_diff_matrix( x_s, x_s(n - l_id), o, l );

          // update buffered map
          decltype(auto) stn = stencils[o][n];
          for( auto i = l_id; i <= u_id; ++i )
          {
            auto v = C( o, i-l_id ); 
            if( !almost_zero(v) )
              stn[ {period_id(i) - n} ] = v;
          }
        }
      }
   }

template <typename T, pbc_type P>
  template <bool dummy>
  requires not basis_1d_fd<T, P>::periodic()
    void basis_1d_fd<T, P>::update_stencil()
    {
      decltype(auto) x = coords();
		  // SPX_PRE_CONDITION( node_id < x.size() && node_id >= 0 );
	
      init_stencils();

      // update buffered map
      auto update_stencil =
        [this]( auto o, std::ptrdiff_t n, std::ptrdiff_t l_id, std::ptrdiff_t u_id, auto& C )
      {
        decltype(auto) stn = stencils[o][n];
        for( auto i = l_id; i <= u_id; ++i )
        {
          auto v = C( o, i-l_id ); 
          if( !almost_zero(v) )
            stn[ {i - n} ] = v;
        }
      };

      // loop nodes
      for( std::size_t n = 0; n < x.size(); ++n  )
      {
        for( std::size_t o = 0; o <= diff_max; ++o )
        {
          std::size_t stn_pts = o + acrcy;
    		  
          if( n < stn_pts / 2 )
		      {
			      decltype(auto) x_s = x( slice(0, stn_pts-1) );
			      decltype(auto) C = fd_diff_matrix( x_s, x_s(n), o );
            update_stencil( o, n, 0, stn_pts-1, C );
		      } 
		      else if( n >=  x.size() - stn_pts / 2 )
		      {
			      std::ptrdiff_t l_id = x.size() - stn_pts;
			      decltype(auto) x_s = x( slice( l_id, last() ) );
			      decltype(auto) C = fd_diff_matrix( x_s, x_s(n - l_id), o );
            update_stencil( o, n, l_id, x.size()-1, C );
		      }
		      else
		      {
			      std::ptrdiff_t l_id  = n - (stn_pts / 2) + (stn_pts % 2 == 0 ? 1 : 0);
			      std::ptrdiff_t u_id  = n + (stn_pts / 2);
			      decltype(auto) x_s = x( slice(l_id, u_id) );
			      decltype(auto) C = fd_diff_matrix( x_s, x_s(n - l_id), o );
            update_stencil( o, n, l_id, u_id, C );
		      }
        }
      }
    }

template <typename T, pbc_type P>
  void basis_1d_fd<T, P>::print() const
  {
    // print
    for( std::size_t o = 0; o <= this->diff_max; ++o ) 
    {
      std::cout << "ORDER = " << o << std::endl;
      for( std::size_t n = 0; n < this->coords().size(); ++n )
      {
        std::cout << "NODE = " << n << std::endl;
        decltype(auto) stn = this->stencils[o][n];
        for( auto& kv : stn )
        {
          std::cout << "  [";
          for(auto k: kv.first)
            std::cout << k << ", ";
          std::cout << "]" << " : " << kv.second << std::endl;
        }
      }
      std::cout << std::endl;
    }
  }



// -------------------------------------------------------------------------- //
// basis_1d_fd_backward
//
//
template <typename T, pbc_type P>
  class basis_1d_fd_backward : public basis_1d_fd<T, P>
  {
  public:
    using base_t = basis_1d_fd<T, P>;

  public:
    // Default Constructible
    basis_1d_fd_backward() = default;

    // Movable
    basis_1d_fd_backward( basis_1d_fd_backward&& ) = default;
    basis_1d_fd_backward& operator=( basis_1d_fd_backward&& ) = default;

    // Copyable
    basis_1d_fd_backward( const basis_1d_fd_backward& ) = default;
    basis_1d_fd_backward& operator=( const basis_1d_fd_backward& ) = default;

    // Constructors
    //
    basis_1d_fd_backward( const d_vec<T>& coord, T len = T{-1} )
    {
      update( coord, len );
    }
 
    // Order of differentiation
    //
    void set_diff_order( std::size_t o )
    {}

    // Update coordinates
    //
    void update( const d_vec<T>& x, T pbc_len = T{-1} )
    {
      base_t::update( x, pbc_len );

      // fix stencils
      decltype(auto) stn_vec = base_t::stencils[1];
      std::size_t N = stn_vec.size();
 
      typename base_t::stencil_t stn_pbc;
      if( base_t::periodic() )
      {
        for( auto kv : stn_vec[ N-1 ] )
          stn_pbc[ {kv.first[0] + N - 1} ] = kv.second;
      }
      
      for( std::size_t i = N-1; i >= 1; --i )
      {
        typename base_t::stencil_t stn_curr;
        for( auto kv : stn_vec[ i-1 ] )
          stn_curr[ {kv.first[0] - 1} ] = kv.second;
        stn_vec[ i ] = stn_curr;
      }
      
      if( base_t::periodic() )
        stn_vec[ 0 ] = stn_pbc;
    }

  };

//////////////////////////////////////////////////////////////////////////////
// type alias 
//////////////////////////////////////////////////////////////////////////////

template <typename T>
  using basis_fd_backward = basis_1d_fd_backward<T, pbc_type::non_periodic>;

template <typename T>
  using basis_fd_backward_pbc = basis_1d_fd_backward<T, pbc_type::periodic>;

//////////////////////////////////////////////////////////////////////////////
// global factory function
//////////////////////////////////////////////////////////////////////////////

template <typename T>
  constexpr decltype(auto) make_fd_backward( const d_vec<T>& coord )
  {
    return basis_fd_backward<T>( coord );
  }

template <typename T, typename L>
requires Convertible<L, T>()
  constexpr decltype(auto) make_fd_backward_pbc( const d_vec<T>& coord, L pbc_len )
  {
    return basis_fd_backward_pbc<T>( coord, T(pbc_len) );
  }

} // namespace spx

#endif // SPX_BASIS_1D_FD_H

