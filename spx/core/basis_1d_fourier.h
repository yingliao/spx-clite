#ifndef SPX_BASIS_1D_FOURIER_H
#define SPX_BASIS_1D_FOURIER_H

#include <complex>

namespace spx
{

template <typename T>
  class basis_1d_fourier
  {
  public:
    using value_type  = T;
    using weight_type = std::complex<T>;
    using index_type  = std::ptrdiff_t;
    using stencil_t   = stencil<weight_type, 1, index_type>;
    
    constexpr static bool periodic() { return true; }
    
    // Default Constructible
    basis_1d_fourier() = default;

    // Movable
    basis_1d_fourier( basis_1d_fourier&& ) = default;
    basis_1d_fourier& operator=( basis_1d_fourier&& ) = default;

    // Copyable
    basis_1d_fourier( const basis_1d_fourier& ) = default;
    basis_1d_fourier& operator=( const basis_1d_fourier& ) = default;

    // Constructors
    // 
    basis_1d_fourier( std::size_t pts, T dx )
    {
      update( pts, dx );
    }
    
    // Order of differentiation
    //
    void set_diff_order( std::size_t o )
    {
      if ( o >= diff_max )
      {
        diff_max = o;
        update_stencil();
      }
      diff_od = o;
    }

    // Update coordinates
    //
    void update( std::size_t pts, T dx )
    {
      this->nx = pts;
      this->dx = dx;
      update_stencil();
    }
    
    // Member data access
    //
    decltype(auto) coords() const { return arange(0, length(), dx); }
    T              length() const { return T(nx) * dx; }

    // Get Stencil
    //
    const d_vector<stencil_t>& diff_stencil() const
    {
      return stencils[ diff_od ];
    }

    void print() const;
  
  private:
    std::size_t nx;           // number of points
    T           dx;           // mesh size
    std::size_t diff_od  = 1; // order of diff.
    std::size_t diff_max = 1; // order of diff. (max.)
    std::vector<d_vector<stencil_t>> stencils;
    
    void update_stencil();

    void init_stencils()
    {
      stencils.clear();
      for( std::size_t o = 0; o <= diff_max; ++o )
      {
        stencils.push_back( d_vector<stencil_t>(nx) );
      }
    }
  };

//////////////////////////////////////////////////////////////////////////////
// type alias 
//////////////////////////////////////////////////////////////////////////////

template <typename T>
  using basis_fourier = basis_1d_fourier<T>;

//////////////////////////////////////////////////////////////////////////////
// global factory function
//////////////////////////////////////////////////////////////////////////////

template <typename T>
  constexpr decltype(auto) make_fourier_basis( std::size_t pts, T dx )
  {
    return basis_fourier<T>( pts, dx );
  }

template <typename T>
  decltype(auto) make_fourier_basis( std::size_t pts, T dx, std::size_t o )
  {
    auto b = basis_fourier<T>( pts, dx );
    b.set_diff_order( o );
    return b;
  }

//////////////////////////////////////////////////////////////////////////////
// member function
//////////////////////////////////////////////////////////////////////////////

template <typename T>
  inline void basis_1d_fourier<T>::update_stencil()
  {
    using W = typename basis_1d_fourier<T>::weight_type;
    
    auto L = length();
    auto C = pi_2<T> / L;
    
    int N = nx;
    bool even = N % 2 == 0;
    
    init_stencils();
    
    for( std::size_t o = 0; o <= diff_max; ++o )
    {
      std::size_t n = 0;
      auto f = [&]( auto k ) 
      {
        stencils[o][n++][ {0} ] = (o == 0) ? 
          W{1, 0} : std::pow( C * i<T> * T(k), o );
      };

      for( int k = 0; k <= (even ? N/2-1 : (N-1)/2); f(k++) );
      for( int k = (even ? -N/2 : -(N-1)/2); k < 0; f(k++) );
    }
  }


template <typename T>
  void basis_1d_fourier<T>::print() const
  {
    // print
    for( std::size_t o = 0; o <= diff_max; ++o ) 
    {
      std::cout << "ORDER = " << o << std::endl;
      for( std::size_t n = 0; n < nx; ++n )
      {
        std::cout << "NODE = " << n << std::endl;
        decltype(auto) stn = stencils[o][n];
        for( auto& kv : stn )
        {
          std::cout << "  [";
          for(auto k: kv.first)
            std::cout << k << ", ";
          std::cout << "]" << " : " << kv.second << std::endl;
        }
      }
      std::cout << std::endl;
    }
  }

} // namespace spx

#endif // SPX_BASIS_1D_FOURIER_H

