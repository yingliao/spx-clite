#ifndef SPX_FD_H
#define SPX_FD_H

namespace spx
{

template <Vector V, typename T>
  d_array<T, 2> fd_diff_matrix( const V& x, 
                                T ksi,
                                std::size_t highest_order = 1, 
                                T pbc_len = -1 )
  {
    bool is_pbc = pbc_len > 0;
    T    pbc_length = is_pbc ? pbc_len : x[ x.size()-1 ] - x[0];
    
    decltype(auto) pbc_diff = [ is_pbc, pbc_length ]( auto l ) {
      return is_pbc ? 
        pbc_impl<pbc_type::periodic>::diff( l, pbc_length ) :
        pbc_impl<pbc_type::non_periodic>::diff( l, pbc_length );
    };

    int N = x.size() - 1;
    int M = highest_order;
    T C1 = (T)1;
    T C4 = pbc_diff( x[0] - ksi );

    d_array<T, 2> C{ M+1, N+1 };
    C = (T)0;
    C(0, 0) = (T)1;
    for( int i = 1; i <= N; i++ )
    {
      int MN = i < M ? i : M;
      T C2 = (T)1;
      T C5 = C4;
      C4 = pbc_diff( x[i] - ksi );
      for( int j = 0; j <= i-1; j++ )
      {
        T C3 = pbc_diff( x[i] - x[j] );
        C2 *= C3;
        if( j == i-1 )
        {
          for( int k = MN; k >= 1; k--)
            C(k, i) = C1 * ( (T)k * C(k-1, i-1) - C5 * C(k, i-1) ) / C2;
          C(0, i) = -C1 * C5 * C(0, i-1) / C2;
        }
        for( int k = MN; k >= 1; k-- )
          C(k, j) = ( C4 * C(k, j) - (T)k * C(k-1, j) ) / C3;
        C(0, j) = C4 * C(0, j) / C3;
      }
      C1 = C2;
    }
    return C;
  }

} // namespace spx

#endif // SPX_FD_H
