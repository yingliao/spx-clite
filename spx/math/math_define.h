#ifndef SPX_MATH_DEFINE_H
#define SPX_MATH_DEFINE_H

#include <complex>
#include <cmath>

namespace spx
{

template <typename T>
  constexpr T epsilon = std::numeric_limits<T>::epsilon();

template <Floating_point_type T>
  constexpr auto max_precision = std::numeric_limits<T>::max_digits10;

template <typename T>
  constexpr std::complex<T> i = { T(0), T(1) };

template <typename T>
  constexpr T pi = std::atan( T(1) ) * T(4);

template <typename T>
  constexpr T pi_2 = T(2) * pi<T>;

template <typename T>
  constexpr T pi_4 = T(4) * pi<T>;

template <typename T>
  constexpr T one_div_log2 = T(1) / std::log( T(2) );

template <typename T>
  constexpr bool invalid( T v )
  {	
    return std::isnan( v ) || std::isinf( v ); 
  }

} //!< namespace spx

#endif //!< SPX_MATH_DEFINE_H
