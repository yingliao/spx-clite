#ifndef SPX_MATH_CONCEPTS_H
#define SPX_MATH_CONCEPTS_H

namespace spx
{

template <typename T>
  concept bool Vector()
  {
    return Range<T>()
        && requires( T t ) {
             { t.size() } -> Size_type<T>;
             // { t[ std::ptrdiff_t() ] } -> Value_type<T>;
             requires Convertible<decltype(t[ std::ptrdiff_t() ]), Value_type<T>>();
             { *(t.data()) } -> Value_type<T>;             
             requires Random_access_iterator<decltype(t.data())>();
             requires not Ranked_storage<T>() 
                   || (Ranked_storage<T>() && T::rank() == 1);
           };
  }

template <typename T>
  concept bool Matrix()
  {
    return Dense_array<T>()
        && requires( T t ) {
             requires (T::rank() == 2);
           };
  }

template <typename T>
  concept bool Static_vector()
  {
    return Has_static_size<T>()
        && Vector<T>();
  }

template <typename T>
  concept bool Static_matrix()
  {
    return Has_static_size<T>()
        && Matrix<T>();
  }

namespace type_impl
{
  // check matrix NxN 
  template <std::size_t N, typename T, typename Desc, std::size_t N0, std::size_t N1>
  requires (N == N0) && (N == N1)
    constexpr defined_t check_nxn( size_constant<N>, g_static_array<T, Desc, N0, N1>&& );
}

template <typename T, std::size_t N>
  concept bool Static_matrix_NxN()
  {
    return Static_matrix<T>()
        && requires() {
             { type_impl::check_nxn( size_constant<N>(), Main_type<T>() ) };
           };
  }

template <typename T>
  concept bool Complex()
  {
    return requires( T t ) {
             requires Convertible<T, std::complex<Value_type<T>>>();
           }; 
  }

template <typename T>
  concept bool Double()
  {
    return Same<T, double>();    
  }

template <typename T>
  concept bool Float()
  {
    return Same<T, float>();    
  }

template <typename T>
  concept bool Complex_double()
  {
    return Same<Value_type<T>, double>()
        && Complex<T>();
  }

template <typename T>
  concept bool Complex_float()
  {
    return Same<Value_type<T>, float>()
        && Complex<T>();
  }

} // namespace spx

#endif // SPX_MATH_CONCEPTS_H

