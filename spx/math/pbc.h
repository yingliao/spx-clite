#ifndef SPX_PBC_H
#define SPX_PBC_H

namespace spx
{

enum class pbc_type{ periodic, non_periodic };

template <pbc_type>
  struct pbc_impl;

template <>
  struct pbc_impl<pbc_type::periodic>
  {
    template<typename T, typename U>
      static constexpr decltype(auto) diff( T dx, U len )
      {
        return dx >= len / U{2} ?
          dx - len : 
            dx < -len / U{2} ?
              dx + len : dx;
      }
  };

template <>
  struct pbc_impl<pbc_type::non_periodic>
  {
    template<typename T, typename U>
      static constexpr decltype(auto) diff( T dx, U len )
      {
        return dx;
      }
  };

} // namepsace spx

#endif // SPX_PBC_H

