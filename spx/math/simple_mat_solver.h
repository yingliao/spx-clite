#ifndef SPX_SIMPLE_MAT_SOLVER_H
#define SPX_SIMPLE_MAT_SOLVER_H

#include <cmath>
#include <complex>

namespace spx
{

template <typename T>
  bool constexpr operator > (std::complex<T> a, std::complex<T> b) 
  { 
    return std::real( std::sqrt( a * std::conj(a) ) ) > 
           std::real( std::sqrt( b * std::conj(b) ) ); 
  }

template <Matrix A, Vector B>
  void lu_solve( A& a, B& b )
  {
    using T = Value_type<A>;

    //LU decomp
    int n = b.size();
    int *indx = new int[n];

    T d;
    int imax = 0;
    T dum, sum;
    T big, temp;
    T *vv = new T[n];

    d = (T)1.0;
    for ( int i = 0; i < n; i++ ) 
    {
      big = (T)0.0;
      for ( int j = 0; j < n; j++ )
      {
        temp = std::abs( a(i, j) );
        if ( temp > big ) 
          big = temp;
      }
      if ( big == (T)0.0 ) 
      {
        std::cerr << "Singular matrix in routine LUDCMP" << std::endl;
        return;
      }
      vv[i] = (T)1.0 / big;
    }
    
    for ( int j = 0; j < n; j++ ) 
    {
      for (int i = 0; i < j; i++ ) 
      {
        sum = a(i, j);
        for (int k = 0; k < i; k++ ) 
          sum -= a(i, k) * a(k, j);		
        a(i, j) = sum;
      }
      big = (T)0.0;
      for ( int i = j; i < n; i++ ) 
      {
        sum = a(i, j);
        for ( int k = 0; k < j; k++ )
          sum -= a(i, k) * a(k, j);
        a(i, j) = sum;
        if ( ( dum = vv[i] * std::abs(sum) ) > big ) 
        {
          big = dum;
          imax = i;
        }
      }
      if ( j != imax ) 
      {
        for ( int k = 0; k < n; k++ ) 
        {
          dum = a(imax, k);
          a(imax, k) = a(j, k);
          a(j, k) = dum;
        }
        d = -d;
        vv[imax] = vv[j];
      }
      indx[j] = imax;
      if ( a(j, j) == (T)0.0 )
        a(j, j) = (T)1.0e-20;
      if ( j != n-1 )
      {
        dum = (T)1.0 / a(j,j);
        for ( int i = j + 1; i < n; i++ )
          a(i, j) = a(i, j) * dum;
      }
    }

    //back substitute
    int ii = 0, ip;

    for ( int i = 0; i < n; i++)
    {
      ip = indx[i];
      sum = b[ip];
      b[ip] = b[i];
      if ( ii != 0 )
        for ( int j = ii - 1; j < i; j++)
          sum -= a(i, j) * b[j];
      else if ( sum != (T)0.0 )
        ii = i + 1;
      b[i] = sum;
    }

    for ( int i = n - 1; i >= 0; i-- )
    {
      sum = b[i];
      for ( int j = i + 1; j < n; j++)
        sum -= a(i, j) * b[j];
      b[i] = sum / a(i, i);
    }

    delete vv;
    delete indx;
  }

template <Matrix A, Vector B>
  void tdma_solve( A& a, B& b )
  {
    using T = Value_type<A>;
    auto  N = b.size();

    B av( N-1 ), bv( N ), cv( N-1 );
    for( std::size_t i = 0; i < N-1; ++i )
    {
      av[i] = a( i+1,   i );
      bv[i] = a(   i,   i );
      cv[i] = a(   i, i+1 );
    }
    bv[ N-1 ] = a( N-1, N-1 );

    for( std::size_t k = 1; k < N; ++k )
    {
      T m = av [k-1 ] / bv[ k-1 ];
      bv[ k ] -= m * cv[ k-1 ];
      b[ k ]  -= m *  b[ k-1 ];
    }
    
    b[ N-1 ] /= bv[ N-1 ];
    for( int k = N-2; k >= 0; --k )
      b[ k ] = ( b[ k ] - cv[ k ] * b[ k+1 ] ) / bv[ k ];
  }
} // namespace spx

#endif // SPX_SIMPLE_MAT_SOLVER_H
