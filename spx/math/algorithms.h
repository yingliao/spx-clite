#ifndef SPX_ALGORITHMS_H
#define SPX_ALGORITHMS_H

#include <limits>
#include <cmath>

namespace spx
{

// Algorithm 139: AlmostEqual
// Testing Equality of Two Floating Point Numbers
//
template <typename T>
requires not Integral_type<T>()
  constexpr bool almost_equal( T a, T b )
  {
    using R = decltype(std::abs( a - b ));

    return ( a == T(0) || b == T(0) ) ?
      std::abs( a - b ) <= epsilon<R> :
    ( std::abs( a - b ) <= epsilon<R> * std::abs(a) ) && 
    ( std::abs( a - b ) <= epsilon<R> * std::abs(b) );
  }

template <Integral_type T>
  constexpr bool almost_equal( T a, T b )
  {
    return a == b;
  }
 
template <typename T>
  constexpr bool almost_zero( T t )
  {
    return almost_equal( t, T{0} );
  }

template <typename T = double>
  d_vector<T> linspace2( double dx, std::size_t N, double begin = 0.0 )
  {
    d_vector<T> v( N );
    for( std::size_t i = 0; i < N; ++i )
      v[i] = begin + T(i) * dx;
    return v;
  }

template <typename T = double>
  d_vector<T> linspace( double begin, double end, std::size_t N )
  {
    auto dx = ( end - begin ) / T( N - 1 );
    auto v  = linspace2( dx, N, begin );
    return v;
  }

template <typename T = double>
  d_vector<T> arange( double begin, double end, double dx )
  {
    std::size_t N = std::floor( ( end - begin ) / dx );
    d_vector<T> v( N );
    for( std::size_t i = 0; i < N; ++i )
      v[i] = begin + T(i) * dx;
    return v;
  }

template <Dense_array V>
  decltype(auto) norm2( const V& r )
  {
    using T = Value_type<V>;
    using std::sqrt;
    using std::abs;
    T r2 = T{0};
    sum( r * r, r2 );
    T sqr2 = sqrt( r2 );
    T err  = abs( sqr2 );
    return err;
  }

template <Dense_array V>
  decltype(auto) rmse( const V& r )
  {
    using T = Value_type<V>;
    using std::sqrt;
    using std::abs;
    T r2 = T{0};
    sum( r * r, r2 );
    r2 /= T( r.size() );
    T sqr2 = sqrt( r2 );
    T err  = abs( sqr2 );
    return err;
  }

template <Dense_array V>
  decltype(auto) mean( const V& r )
  {
    using T = Value_type<V>;
    T r2 = T{0};
    sum( r, r2 );
    r2 /= T( r.size() );
    return r2;
  }

template <typename T, std::size_t N, typename... Args>
  decltype(auto) zeros( Args&&... args  )
  {
    auto a = d_arr<T, N>( std::forward<Args>(args)... );
    auto z = T(0);
    a = as_const( z );
    return a;
  }

// meshgrid 
//
template <Vector V, Vector... Args>
  decltype(auto) meshgrid( const V& x0, const Args&... x )
  {
    using T = Value_type<V>;
    constexpr auto D = 1 + sizeof...(Args);
    
    return make_callback_array<D>( 
              { x0.size(), x.size()... }, 
              [&]( auto& desc, auto id )
              {
                using S  = static_vector<T, D>;
                auto idx = desc.index_of(id);
                return varargs_trans( [&]( auto&&... u ) { return S{ u... }; }, 
                                      [&]( auto&& y, auto d ) { return y[ idx[d] ]; }, 
                                      x0, x... ); 
              } );
  }

template <Matrix A, Vector V>
  V MxV( const A& a, const V& x )
  {
    V ax( x.size() );
    for( std::size_t r = 0; r < a.extent(0); ++r )
      ax[ r ] = spx::sum( a( r, slice_all() ) * x );
    return ax;
  }

template <Matrix M>
  M MxM( const M& a, const M& x )
  {
    M ax( x.extents() );
    for( auto r = a.lbounds()[0]; r <= a.ubounds()[0]; ++r )
      for( auto c = a.lbounds()[1]; c <= a.ubounds()[1]; ++c )
        ax( r, c ) = spx::sum( a( r, slice_all() ) * x( slice_all(), c ) );
    return ax;
  }


// Algorithm 30: BarycentricWeights
// Weights for Lagrange Interpolation
//
template <Vector V>
  decltype(auto) barycentric_weights( const V& x )
  {
    using T = Value_type<V>;
    std::size_t  N = x.size() - 1;
    V w( x.size() );
    w = T(1);
    for( std::size_t j = 1; j <= N; j++ )
      for( std::size_t k = 0; k <= j-1; k++ )
      {
        w[k] *= x[k] - x[j];
        w[j] *= x[j] - x[k];
      }
    w = T(1) / w;
    return w;
  }

// Algorithm 37: PolynomialDerivativeMatrix
// First Derivative Approximation Matrix
//
// Uses Algorithms:
//   Algorithm 30(BarycentricWeights)
//
template <Vector V>
  decltype(auto) polynomial_derivative_matrix( const V& x )
  {
    using T = Value_type<V>;

    auto w = barycentric_weights(x);
    d_arr<T, 2> D( x.size(), x.size() );
    std::size_t N = x.size() - 1;
    for( std::size_t i = 0; i <= N; i++ )
    {
      D( i, i ) = T(0);
      for( std::size_t j = 0; j <= N; j++ )
      {
        if( j == i ) continue;
        D( i, j )  = w[j] / w[i] / (x[i] - x[j]);
        D( i, i ) -= D( i, j );
      }
    }
    return D;
  }

// Algorithm 38: mthOrderPolynomialDerivativeMatrix
// Derivative Matrix for mth Order Derivatives
//
// Uses Algorithms:
//   Algorithm 30(BarycentricWeights)
//   Algorithm 37(PolynomialDerivativeMatrix)
//
// Parameters:
//   O: order
//
template <Vector V, typename T = Value_type<V>>
  decltype(auto) mth_order_polynomial_derivative_matrix( const V& x, T len = T(1), std::size_t O = 1 )
  {
    auto w = barycentric_weights( x );
    auto D = polynomial_derivative_matrix( x );
    T basis_len = x[ last() ] - x[ first() ]; 
    
    if( O == 1)
    {
      D *= basis_len / len;
      return D;
    }
    
    auto N = x.size() - 1;
    d_arr<T, 2> D_minus_1 = D;
    for( std::size_t k = 2; k <= O; k++ )
    {
      for( std::size_t i = 0; i <= N; i++ )
      {
        D( i, i ) = T(0);
        for( std::size_t j = 0; j <= N; j++ )
        {
          if( j == i ) continue;
          D( i, j )  = T(k) / (x[i] - x[j]) * ( w[j] / w[i] * D_minus_1( i, i ) - D_minus_1( i, j ) );
          D( i, i ) -= D( i, j );
        }
      }
      D_minus_1 = D;
    }

    D *= std::pow( basis_len / len, T(O) );
    return D;
  }


// chebyshev_quad:
//  
//   Chebyshev Quadrature:
//   Algorithm 26: ChebyshevGaussNodesAndWeights
//   Algorithm 27: ChebyshevGaussLobattoNodesAndWeights
//  
//   return: d_arr<static_vector<T, 2>> is a 1D array of static_vector<T, 2>. 
//           Each static_vector<T, 2> is a pair of (xn, wn)
//
template <typename T>
  struct chebyshev_quad
  {
    static decltype(auto) gauss_nodes_and_weights( std::size_t N )
    {
      --N;
      auto x = d_vec<T>( N+1 );
      auto w = d_vec<T>( N+1 );
      for( std::size_t j = 0; j <= N; ++j )
      {
        x[ j ] = -std::cos( T( 2*j+1 ) / T( 2*N+2 ) * pi<T> );
        w[ j ] =  pi<T> / T( N+1 );
      }

      d_vec<static_vector<T, 2>> x_and_w( N+1 );
      x_and_w[0] = x;
      x_and_w[1] = w;
      return x_and_w;
    }

    static decltype(auto) gauss_lobatto_nodes_and_weights( std::size_t N )
    {
      --N;
      auto x = d_vec<T>( N+1 );
      auto w = d_vec<T>( N+1 );
      for( std::size_t j = 0; j <= N; ++j )
      {
        x[ j ] = -std::cos( T(j) / T(N) * pi<T> );
        w[ j ] =  pi<T> / T(N);
      }
      w[ 0 ] /= T(2);
      w[ N ] /= T(2);

      d_vec<static_vector<T, 2>> x_and_w( N+1 );
      x_and_w[0] = x;
      x_and_w[1] = w;
      return x_and_w;
    }
    
    static constexpr T basis_length() { return T(2); }
  };

// Matrix inverse
//
// det: determination of ORIGINAL matrix x
//
template <Matrix A, typename T>
requires Static_matrix_NxN<A, 1>()
  decltype(auto) inv( const A& x, T& det )
  {
    A r( x.extents() );
    det = x( 0, 0 ); 
    r( 0, 0 ) =  T(1) / det;
    return r;
  }

template <Matrix A, typename T>
requires Static_matrix_NxN<A, 2>()
  decltype(auto) inv( const A& x, T& det )
  {
    A r( x.extents() );
    det = ( x(0, 0) * x(1, 1) ) - ( x(0, 1) * x(1, 0) );
    r( 0, 0 ) =  x( 1, 1 );
    r( 0, 1 ) = -x( 0, 1 );
    r( 1, 0 ) = -x( 1, 0 );
    r( 1, 1 ) =  x( 0, 0 );
    r /= det;
    return r;
  }

template <Matrix A, typename T>
requires Static_matrix_NxN<A, 3>()
  decltype(auto) inv( const A& x, T& det )
  {
    A r( x.extents() );
    det = ( x(0, 0) * x(1, 1) * x(2, 2) )
        + ( x(1, 0) * x(2, 1) * x(0, 2) )
        + ( x(2, 0) * x(0, 1) * x(1, 2) )
        - ( x(0, 0) * x(2, 1) * x(1, 2) )
        - ( x(2, 0) * x(1, 1) * x(0, 2) )
        - ( x(1, 0) * x(0, 1) * x(2, 2) );
    r( 0, 0 ) = ( x(1, 1) * x(2, 2) ) - ( x(1, 2) * x(2, 1) );
    r( 0, 1 ) = ( x(0, 2) * x(2, 1) ) - ( x(0, 1) * x(2, 2) );
    r( 0, 2 ) = ( x(0, 1) * x(1, 2) ) - ( x(0, 2) * x(1, 1) );
    r( 1, 0 ) = ( x(1, 2) * x(2, 0) ) - ( x(1, 0) * x(2, 2) );
    r( 1, 1 ) = ( x(0, 0) * x(2, 2) ) - ( x(0, 2) * x(2, 0) );
    r( 1, 2 ) = ( x(0, 2) * x(1, 0) ) - ( x(0, 0) * x(1, 2) );
    r( 2, 0 ) = ( x(1, 0) * x(2, 1) ) - ( x(1, 1) * x(2, 0) );
    r( 2, 1 ) = ( x(0, 1) * x(2, 0) ) - ( x(0, 0) * x(2, 1) );
    r( 2, 2 ) = ( x(0, 0) * x(1, 1) ) - ( x(0, 1) * x(1, 0) );
    r /= det;
    return r;
  }

template <Matrix A, typename T>
requires Static_matrix_NxN<A, 4>()
  decltype(auto) inv( const A& x, T& det )
  {
    A r( x.extents() );

    auto l = [&]( auto i ) -> decltype(auto) { return r( i / 4, i % 4 ); };
    auto m = [&]( auto i ) -> decltype(auto) { return x( i / 4, i % 4 ); };

    l(0) =   m(5)  * m(10) * m(15) - 
             m(5)  * m(11) * m(14) - 
             m(9)  * m(6)  * m(15) + 
             m(9)  * m(7)  * m(14) +
             m(13) * m(6)  * m(11) - 
             m(13) * m(7)  * m(10);

    l(4) =   -m(4)  * m(10) * m(15) + 
              m(4)  * m(11) * m(14) + 
              m(8)  * m(6)  * m(15) - 
              m(8)  * m(7)  * m(14) - 
              m(12) * m(6)  * m(11) + 
              m(12) * m(7)  * m(10);

    l(8) =   m(4)  * m(9) * m(15) - 
             m(4)  * m(11) * m(13) - 
             m(8)  * m(5) * m(15) + 
             m(8)  * m(7) * m(13) + 
             m(12) * m(5) * m(11) - 
             m(12) * m(7) * m(9);

    l(12) =   -m(4)  * m(9) * m(14) + 
               m(4)  * m(10) * m(13) +
               m(8)  * m(5) * m(14) - 
               m(8)  * m(6) * m(13) - 
               m(12) * m(5) * m(10) + 
               m(12) * m(6) * m(9);

    l(1) =   -m(1)  * m(10) * m(15) + 
              m(1)  * m(11) * m(14) + 
              m(9)  * m(2) * m(15) - 
              m(9)  * m(3) * m(14) - 
              m(13) * m(2) * m(11) + 
              m(13) * m(3) * m(10);

    l(5) =   m(0)  * m(10) * m(15) - 
             m(0)  * m(11) * m(14) - 
             m(8)  * m(2) * m(15) + 
             m(8)  * m(3) * m(14) + 
             m(12) * m(2) * m(11) - 
             m(12) * m(3) * m(10);

    l(9) =   -m(0)  * m(9) * m(15) + 
              m(0)  * m(11) * m(13) + 
              m(8)  * m(1) * m(15) - 
              m(8)  * m(3) * m(13) - 
              m(12) * m(1) * m(11) + 
              m(12) * m(3) * m(9);

    l(13) =   m(0)  * m(9) * m(14) - 
              m(0)  * m(10) * m(13) - 
              m(8)  * m(1) * m(14) + 
              m(8)  * m(2) * m(13) + 
              m(12) * m(1) * m(10) - 
              m(12) * m(2) * m(9);

    l(2) =   m(1)  * m(6) * m(15) - 
             m(1)  * m(7) * m(14) - 
             m(5)  * m(2) * m(15) + 
             m(5)  * m(3) * m(14) + 
             m(13) * m(2) * m(7) - 
             m(13) * m(3) * m(6);

    l(6) =   -m(0)  * m(6) * m(15) + 
              m(0)  * m(7) * m(14) + 
              m(4)  * m(2) * m(15) - 
              m(4)  * m(3) * m(14) - 
              m(12) * m(2) * m(7) + 
              m(12) * m(3) * m(6);

    l(10) =   m(0)  * m(5) * m(15) - 
              m(0)  * m(7) * m(13) - 
              m(4)  * m(1) * m(15) + 
              m(4)  * m(3) * m(13) + 
              m(12) * m(1) * m(7) - 
              m(12) * m(3) * m(5);

    l(14) =   -m(0)  * m(5) * m(14) + 
               m(0)  * m(6) * m(13) + 
               m(4)  * m(1) * m(14) - 
               m(4)  * m(2) * m(13) - 
               m(12) * m(1) * m(6) + 
               m(12) * m(2) * m(5);

    l(3) =   -m(1) * m(6) * m(11) + 
              m(1) * m(7) * m(10) + 
              m(5) * m(2) * m(11) - 
              m(5) * m(3) * m(10) - 
              m(9) * m(2) * m(7) + 
              m(9) * m(3) * m(6);

    l(7) =   m(0) * m(6) * m(11) - 
             m(0) * m(7) * m(10) - 
             m(4) * m(2) * m(11) + 
             m(4) * m(3) * m(10) + 
             m(8) * m(2) * m(7) - 
             m(8) * m(3) * m(6);

    l(11) =   -m(0) * m(5) * m(11) + 
               m(0) * m(7) * m(9) + 
               m(4) * m(1) * m(11) - 
               m(4) * m(3) * m(9) - 
               m(8) * m(1) * m(7) + 
               m(8) * m(3) * m(5);

    l(15) =   m(0) * m(5) * m(10) - 
              m(0) * m(6) * m(9) - 
              m(4) * m(1) * m(10) + 
              m(4) * m(2) * m(9) + 
              m(8) * m(1) * m(6) - 
              m(8) * m(2) * m(5);

    det = m(0) * l(0) + m(1) * l(4) + m(2) * l(8) + m(3) * l(12);
    r /= det;
    return r;
  }

template <Matrix A>
requires Static_matrix_NxN<A, 1>()
      || Static_matrix_NxN<A, 2>()
      || Static_matrix_NxN<A, 3>()
      || Static_matrix_NxN<A, 4>()
  decltype(auto) inv( const A& x )
  {
    using T = Value_type<A>;
    T det(0);
    return inv( x, det );
  } 

template <Matrix A, typename... Args>
requires not Static_matrix_NxN<A, 1>()
      && not Static_matrix_NxN<A, 2>()
      && not Static_matrix_NxN<A, 3>()
      && not Static_matrix_NxN<A, 4>()
  decltype(auto) inv( const A& x, Args&&... args )
  {
    A r = x;
    lapack_inv( r, std::forward<Args>(args)... );
    return r;
  }
 
// Dot Product
//

namespace math_impl
{
  template <std::size_t N, typename I, typename J>
  requires (N == 1)
    decltype(auto) dot_impl( I it_i, J it_j )
    {
      return (*it_i) * (*it_j);
    }

  template <std::size_t N, typename I, typename J>
  requires (N > 1)
    decltype(auto) dot_impl( I it_i, J it_j )
    {
      return (*it_i) * (*it_j) 
           + dot_impl<N-1>( ++it_i, ++it_j );
    }
}

template <Expressible T, Expressible U, typename R>
requires not Has_static_size<T>()
      && not Has_static_size<U>()
  inline void dot( const T& t, const U& u, R& r )
  {
    spx::sum( t * u, r );
  }

template <Dense_array T, Expressible U, typename R>
requires Has_static_size<T>()
  inline void dot( const T& t, const U& u, R& r )
  {
    r = math_impl::dot_impl<size<T>>( t.data(), u.data() );
  }

template <Expressible T, Dense_array U, typename R>
requires not Has_static_size<T>()
      && Has_static_size<U>()
  inline void dot( const T& t, const U& u, R& r )
  {
    dot( u, t, r );
  }

template <Expressible T, Expressible U>
  decltype(auto) dot( const T& t, const U& u )
  {
    using R = function_result<ops::plus, Value_type<T>, Value_type<U>>; 
    R r = R{0};
    dot( t, u, r );
    return r;
  }


} // namespace spx

#endif // SPX_ALGORITHMS_H
