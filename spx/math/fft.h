#ifndef SPX_FFT_H
#define SPX_FFT_H

namespace spx
{

template <Vector V>
requires Complex<Value_type<V>>()
  decltype(auto) fft( const V& x )
  {
    using E = Value_type<V>;               // element type (std::complex)
    using T = Value_type<E>;               // value type of element
    using C = typename fftw<T>::complex_t; // fftw complex type

    auto N = x.size();
    
    V f(N);
    int s[1] = { int(N) };
    auto e   = const_cast<E*>( x.data() );
    auto in  = reinterpret_cast<C*>( e );
    auto out = reinterpret_cast<C*>( f.data() );

    auto p = fftw<T>::forward( &s[0], in, out );
    fftw<T>::execute( p );
    fftw<T>::destroy_plan( p );

    return f;
  }

template <Vector V>
requires Complex<Value_type<V>>()
  decltype(auto) ifft( const V& v )
  {
    using E = Value_type<V>;               // element type (std::complex)
    using T = Value_type<E>;               // value type of element
    using C = typename fftw<T>::complex_t; // fftw complex type

    auto N = v.size();

    V x( v ); // copy elements
    for( auto& p: x )
      p /= T(N);

    V f( N );
    int s[1] = { int(N) };
    auto e   = const_cast<E*>( x.data() );
    auto in  = reinterpret_cast<C*>( e );
    auto out = reinterpret_cast<C*>( f.data() );

    auto p = fftw<T>::backward( &s[0], in, out );
    fftw<T>::execute( p );
    fftw<T>::destroy_plan( p );

    return f;
  }

template <Vector V, typename T = Value_type<V>>
requires not Complex<T>()
      && Convertible<T, std::complex<T>>()
  decltype(auto) fft( const V& x )
  {
    using C = std::complex<T>;
    d_vector<C> u( x.size() );
    std::transform( std::begin(x), 
                    std::end(x), 
                    std::begin(u), 
                    []( auto& t ) { return C{t}; } );
    return fft( u );
  }

template <Dense_array A, typename F, typename T = Value_type<A>>
requires (A::rank() >= 2)
      && (Complex<T>() || Convertible<T, std::complex<T>>())
  decltype(auto) fft_nd( const A& arr, F&& fft1d, std::size_t axis )
  {
    using R = type_if<Complex<T>(), A, d_array<std::complex<T>, A::rank()>>; 

    std::size_t D = axis;
    
    auto l_idx      = arr.lbounds();
    auto u_idx      = arr.ubounds();
    auto idx_starts = starting_idx_of_axis( l_idx, u_idx, D );
   
    R out( arr.extents() );

    for( auto idx : idx_starts )
    {
      auto l_id = l_idx[D];
      auto u_id = u_idx[D];
      auto n    = u_id - l_id + 1;
      
      d_vector<T> b( n );
      for( auto id = l_id; id <= u_id; ++id )
      {
        idx[D] = id;
        b[ id - l_id ] = arr( idx ); 
      }

      auto r = fft1d( b );

      for( auto id = l_id; id <= u_id; ++id )
      {
        idx[D] = id;
        out( idx ) = r[ id - l_id ]; 
      }
    }

    return out;
  }

template <Dense_array A, std::size_t D = A::rank()>
requires (D >= 2)
  decltype(auto) fft( const A& arr, std::size_t axis = D-1 )
  {
    return fft_nd( arr, 
                   []( auto& v ) { return fft( v ); }, 
                   axis );
  }
 
template <Dense_array A, std::size_t D = A::rank()>
requires (D >= 2)
  decltype(auto) ifft( const A& arr, std::size_t axis = D-1 )
  {
    return fft_nd( arr, 
                   []( auto& v ) { return ifft( v ); }, 
                   axis );
  }
 
} // namespace spx

#endif // SPX_FFT_H

