#include "tag_types.h"
#include "meta.h"
#include "deduction.h"

#include "origin_typestr.h"
#include "origin_integer.h"
#include "origin_core_traits.h"
#include "origin_core_concepts.h"
#include "origin_iterator_concepts.h"
#include "origin_range_concepts.h"

#include "type_list.h"
