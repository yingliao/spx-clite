#ifndef SPX_TAG_TYPES_H
#define SPX_TAG_TYPES_H

namespace spx
{

//////////////////////////////////////////////////////////////////////////////
// Defined Type
//
// The defined type is a tag class used as the default return type at the 
// place of type checking function, which can be further used in concept.
// For example,
//
// template <typename T>
//   constexpr defined_t check( std::complex<T> );

struct defined_t { };

} // namespace spx

#endif // SPX_TAG_TYPES_H
