#ifndef SPX_ORIGIN_INTEGER_H
#define SPX_ORIGIN_INTEGER_H

#include <spx/type/origin_core_traits.h>

namespace spx
{
  
//template <typename T> concept bool Integral_type();
//template <typename T, typename U> concept bool Same();

namespace type_impl
{
  ////////////////////////////////////////////////////////////////////////////
  // Integer Traits
  //
  // The std::make_signed and std::make_unsigned will fail when the argument
  // type is not integral. We must take care that this does not happen.
  ////////////////////////////////////////////////////////////////////////////


  // Returns the signed version of T if T is an Integer type. 
  // Otherwise, type is non-defined
  template <typename T, bool = Integral_type<T>(), bool = Same<T, bool>()>
    struct make_signed;

  template <typename T>
    struct make_signed<T, true, false> : std::make_signed<T> { };
  

  // Return the unsgiend version of T if T is an Integer type. 
  // Otherwise, type is non-defined
  template <typename T, bool = Integral_type<T>(), bool = Same<T, bool>()>
    struct make_unsigned;

  template <typename T>
    struct make_unsigned<T, true, false> : std::make_unsigned<T> { };

} // namespace type_impl

// An alias for the unsigned integral type with the same width as T.
template <typename T>
  using Make_unsigned = typename type_impl::make_unsigned<T>::type;

// An alias for the signed integral type with the same width as T.
template <typename T>
  using Make_signed = typename type_impl::make_signed<T>::type;

} // namespace spx

#endif // SPX_ORIGIN_INTEGER_H
