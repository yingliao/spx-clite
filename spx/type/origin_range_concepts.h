#ifndef SPX_ORIGIN_RANGE_CONCEPTS_HPP
#define SPX_ORIGIN_RANGE_CONCEPTS_HPP

#include <spx/type/origin_iterator_concepts.h>

namespace spx {

namespace type_impl
{
  // begin
  //
  template <typename T>
    struct std_begin
    {
    private:
      template <typename X>
        static constexpr auto check( X&& x ) -> decltype( std::begin(x) );
      static constexpr subst_failure check( ... );

    public:
      using type = decltype( check( std::declval<T>() ) );
    };

  // end
  //
  template <typename T>
    struct std_end
    {
    private:
      template <typename X>
        static constexpr auto check( X&& x ) -> decltype( std::end(x) );
      static constexpr subst_failure check( ... );

    public:
      using type = decltype( check( std::declval<T>() ) );
    };
}

// Range
template<typename R>
  concept bool Range()
  {
    return requires (R& r) {
      { std::begin(r) };
      { std::end(r) };
      requires Same<typename type_impl::std_begin<R&>::type, 
                    typename type_impl::std_end<R&>::type>();
      //requires Iterator<typename type_impl::std_begin<R&>::type>();
    };
  }

// Iterator_type
template<typename R>
  using Iterator_type = decltype(std::begin(std::declval<R&>()));

// Input_range
template<typename R>
  concept bool Input_range()
  {
    return Range<R>() //&& Input_iterator<Iterator_type<R>>();
        && requires() {
          typename Iterator_type<R>;
          //requires Input_iterator<Iterator_type<R>>();
        };
  }

// Output_range
template<typename R, typename T>
  concept bool Output_range()
  {
    return Range<R>() && Output_iterator<Iterator_type<R>, T>();
  }

// Forward_range
template<typename R>
  concept bool Forward_range()
  {
    return Range<R>() && Forward_iterator<Iterator_type<R>>();
  }

// Bidirectional_range
template<typename R>
  concept bool Bidirectional_range()
  {
    return Range<R>() && Bidirectional_iterator<Iterator_type<R>>();
  }

// Random_access_range
template<typename R>
  concept bool Random_access_range()
  {
    return Range<R>() && Random_access_iterator<Iterator_type<R>>();
  }

} // namespace spx

#endif // SPX_ORIGIN_RANGE_CONCEPTS_HPP
