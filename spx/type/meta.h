#ifndef SPX_META_H
#define SPX_META_H

namespace spx
{
  
template <std::size_t N>
  using size_constant = std::integral_constant<std::size_t, N>;

template <bool B>
  using boolean_constant = std::integral_constant<bool, B>;

} // namespace spx

#endif // SPX_META_H