#ifndef SPX_DEDUCTION_H
#define SPX_DEDUCTION_H

namespace spx
{

struct subst_failure { };

template <bool B, typename T = void>
  using enable_if = typename std::enable_if<B, T>::type;

template <bool B, typename T, typename F>
  using type_if = typename std::conditional<B, T, F>::type;

// All
  
constexpr bool All() 
{ 
  return true; 
}

template <typename... Args>
  constexpr bool All(bool b, Args... args)
  {
    return b && All(args...);
  }

// Some

constexpr bool Some() 
{ 
  return false; 
}

template <typename... Args>
  constexpr bool Some(bool b, Args... args)
  {
    return b || Some(args...);
  }

// None

constexpr bool None() 
{ 
  return true; 
}

template <typename... Args>
  constexpr bool None(bool b, Args... args)
  {
    return !b && None(args...);
  }

// Empty && Not_empty

template <typename... Args>
  concept bool Not_empty()
  {
    return sizeof...(Args) > 0;
  }

template <typename... Args>
  concept bool Empty()
  {
    return sizeof...(Args) == 0;
  }

// Same

template <typename T, typename U = T>
  constexpr bool Same()
  {
    return __is_same_as(T, U);
  }

template <typename T, typename U, typename... Args>
requires Not_empty<Args...>()
  constexpr bool Same()
  {
    return true 
        && Same<T, U>() 
        && Same<U, Args...>();
  }

// Count

constexpr std::size_t Count( bool b )
{
  return b ? 1 : 0;
}

template <typename... Args>
  constexpr std::size_t Count( bool b, Args... args)
  {
    return b ? 1 + Count( args... ) : 0 + Count( args... );
  }


} // namespace spx

#endif // SPX_DEDUCTION_H