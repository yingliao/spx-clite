#ifndef SPX_TYPE_LIST_H
#define SPX_TYPE_LIST_H

#include <tuple>
#include <utility>

namespace spx
{

/**
 *  @brief  check_type_inside. Check if type T in type list t_list
 */

namespace type_impl
{
  template <typename... Args>
    struct is_inside;

  template <typename T, typename U>
    struct is_inside<T, U> 
      : boolean_constant<Same<T, U>()> 
    {};
  
  template <typename T, typename U, typename... Args>
    struct is_inside<T, U, Args...>
      : boolean_constant<Same<T, U>() || is_inside<T, Args...>::value>
    {};
}

template <typename T, typename... Args>
  concept bool Type_inside()
  {
    return type_impl::is_inside<T, Args...>::value;
  }

/**
 *  @tparam trait traits for type properties
 */
template <template <typename...> class Tr, typename... T>
  concept bool Valid()
  {
    return Tr<T...>::value;
  }

/**
 * @brief count number of valid unary trait Tr<Args>...
 */
template <template <typename> class Tr, typename... Args>
  constexpr std::size_t Count_unary_valid()
  {
    return Count( Valid<Tr, Args>()... );
  }

namespace type_impl
{
  enum class pair_order { first, second };
  using pair_sub_first = std::integral_constant<pair_order, pair_order::first>;
  using pair_sub_second = std::integral_constant<pair_order, pair_order::second>;
  
  /**
  * @brief implementation of count_binary_valid
  */
  template <template <typename, typename> class Tr, typename T, typename... Args>
    constexpr std::size_t count_binary_valid( pair_sub_first )
    {
      return Count( Valid<Tr, T, Args>()... );
    }

  template <template <typename, typename> class Tr, typename T, typename... Args>
    constexpr std::size_t count_binary_valid( pair_sub_second )
    {
      return Count( Valid<Tr, Args, T>()... );
    }
}

template<typename T>
  struct sub_to_first : public type_impl::pair_sub_first
  {   using type = T; };

template<typename T>
  struct sub_to_second : public type_impl::pair_sub_second
  {   using type = T; };

template <typename T>
  concept bool Pair_subst()
  {
    return ( Derived<T, type_impl::pair_sub_first>()
          || Derived<T, type_impl::pair_sub_second>() );
        //&& requires() {
        //  typename T::type;
        //};
  }

/**
 * @brief count number of valid binary trait Tr<T,Args>...
 * @tparam T T = other cases, substitute to first for default
 */
template <template <typename, typename> class Tr, typename T, typename... Args>
  requires not Pair_subst<T>() && Not_empty<Args...>()
  constexpr std::size_t Count_binary_valid()
  {
    return type_impl::count_binary_valid<Tr, T, Args...>( type_impl::pair_sub_first() );
  }

/**
 * @brief count number of valid binary trait Tr<T,Args>... or trait Tr<Args,T>...
 * @tparam P P = sub_to_first<T> or P = sub_to_second<T>
 */
template <template <typename, typename> class Tr, typename P, typename... Args>
  requires Pair_subst<P>() && Not_empty<Args...>()
  constexpr std::size_t Count_binary_valid()
  {
    return type_impl::count_binary_valid<Tr, typename P::type, Args...>( P() );
  }

/**
 * @brief count number of T in Args...
 */
template <typename T, typename... Args>
  requires Not_empty<Args...>()
  constexpr std::size_t Count_same_types()
  {
    return Count_binary_valid<std::is_same, sub_to_first<T>, Args...>();
  }

/**
 * @brief expandable
 */
namespace type_impl
{
  template <template <typename...> class X, typename... Args>
    constexpr auto expandable_size( X<Args...>&& ) -> size_constant<(sizeof...(Args))> 
    { 
      return size_constant<(sizeof...(Args))>(); 
    }
    
  template <template <typename...> class X, typename... Args>
    constexpr auto expandable_size( const X<Args...>& ) -> size_constant<(sizeof...(Args))> 
    {
      return size_constant<(sizeof...(Args))>(); 
    }
}

template <typename T>
  concept bool Expandable() 
  { 
    return requires(T t) {
      type_impl::expandable_size( std::forward<T>(t) );
    };
  };

template <Expandable T>
  constexpr std::size_t Expandable_size()
  { 
    return decltype(type_impl::expandable_size( std::declval<T>() ))::value;
  };


/**
* @brief Expandable_parameters
*/

namespace type_impl
{
  namespace expdb_param
  {
    template <template <typename...> class X, typename... Args, typename... Args2>
    requires Same<X<Args...>, X<Args2...>>()
      static defined_t check( X<Args...>&&, Args2&&... );
  }
}

// check if T = X<U...>
template <typename T, typename... U>
  concept bool Expandable_parameters()
  {
    return Expandable<T>()
        && requires( T t, U... u ) {
             type_impl::expdb_param::check( std::forward<T>(t), std::forward<U>(u)... ); 
           };
  }

/**
* @brief append type T to std::tuple<Args...>
*/
namespace type_impl
{
  template <typename T, typename U>
  requires Expandable<T>() || Expandable<U>()
    struct get_appended_type
    {
    private:
      template <template <typename...> class X, typename Y, typename... Args>
        static auto check1( X<Args...>&&, Y&& ) -> X<Args..., Y>;
      static subst_failure check1( ... );
      
      template <typename X, template <typename...> class Y,  typename... Args>
        static auto check2( X&&, Y<Args...>&& ) -> Y<X, Args...>;
      static subst_failure check2( ... );
      
      template <typename T1, typename T2>
      requires  Same<T1, subst_failure>() 
            && !Same<T2, subst_failure>()
        static T2 check( T1&&, T2&& );
        
      template <typename T1, typename T2>
      requires  Same<T2, subst_failure>() 
            && !Same<T1, subst_failure>()
        static T1 check( T1&&, T2&& );
        
      using T1 = decltype( check1( std::declval<T>(), std::declval<U>() ) );
      using T2 = decltype( check2( std::declval<T>(), std::declval<U>() ) );
      
    public:
      using type = decltype( check( std::declval<T1>(), std::declval<T2>() ) );
    };    
}

template <typename T, typename U>
requires Expandable<T>() || Expandable<U>()
  using tuple_append_type = typename type_impl::get_appended_type<T, U>::type;

/**
* @brief repeat_N_types_as_tuple
*/
namespace type_impl
{
  template <typename T, std::size_t N>
  requires (N > 0)
    struct repeat_N_types
    {
      using type = tuple_append_type<typename repeat_N_types<T, N-1>::type, T>;
    };

  template <typename T>
    struct repeat_N_types<T, 1>
    {
      using type = std::tuple<T>;
    };
}

template <typename T, std::size_t N>
requires (N > 0)
  using repeat_type_as_tuple = typename type_impl::repeat_N_types<T, N>::type;

/**
 * @brief replace type T
 */
namespace type_impl
{
  template <typename T, std::size_t I, template <typename...> class X, 
            std::size_t N, typename... Args>
    struct replace_type_expand;

  template <typename T, std::size_t I, template <typename...> class X, 
            std::size_t N, typename U>
    struct replace_type_expand<T, I, X, N, U>
    {
      using _t = type_if<(I==N - 1), T, U>;
      using type = X<_t>;
    };

  template <typename T, std::size_t I, template <typename...> class X, 
            std::size_t N, typename U, typename... Args>
    struct replace_type_expand<T, I, X, N, U, Args...>
    {
      using _t = type_if<(I==N - sizeof...(Args) - 1), T, U>;
      using _tup_next = typename replace_type_expand<T, I, X, N, Args...>::type;
      using type = tuple_append_type<_t, _tup_next>;
    };
    
  template <typename T, std::size_t I, template <typename...> class X, typename... Args>
    constexpr auto replace_type( X<Args...>&& ) -> 
      typename replace_type_expand<T, I, X, sizeof...(Args), Args...>::type;
}

template <typename T,  std::size_t I, typename Tup>
requires Expandable<Tup>() 
      && (I < Expandable_size<Tup>())
  using replace_type = decltype( type_impl::replace_type<T, I>( std::declval<Tup>() ) );

/**
 * @brief remove type T
 */
namespace type_impl
{
  template <std::size_t I, template <typename...> class X, 
            std::size_t N, typename... Args>
    struct remove_type_expand;

  template <std::size_t I, template <typename...> class X, 
            std::size_t N, typename U>
    struct remove_type_expand<I, X, N, U>
    {
      using type = type_if<(I==N - 1), X<>, X<U>>;
    };

  template <std::size_t I, template <typename...> class X, 
            std::size_t N, typename U, typename... Args>
    struct remove_type_expand<I, X, N, U, Args...>
    {
      using _tup_next = typename remove_type_expand<I, X, N, Args...>::type;
      using type = type_if<(I==N - sizeof...(Args) - 1),
                           _tup_next,
                           tuple_append_type<U, _tup_next>>;
    };

  template <std::size_t I, template <typename...> class X, typename... Args>
    constexpr auto remove_type( X<Args...>&& ) ->
      typename remove_type_expand<I, X, sizeof...(Args), Args...>::type;
}

template <std::size_t I, typename Tup>
requires Expandable<Tup>()
      && (I < Expandable_size<Tup>())
  using remove_type = decltype( type_impl::remove_type<I>( std::declval<Tup>() ) );

/**
 * @brief index of T in tuple
 */
template <typename T, typename U, typename... Args>
requires Same<T, U>()
  constexpr std::size_t Index_of_types()
  {
    return 0;
  }

template <typename T, typename U, typename... Args>
requires not Same<T, U>()
      && Type_inside<T, U, Args...>()
  constexpr std::size_t Index_of_types()
  {
    return 1 + Index_of_types<T, Args...>();
  }

namespace type_impl 
{
  template <typename T, template <typename...> class X, typename... Args>
    constexpr auto index_of_types( X<Args...>&& ) ->
      size_constant<Index_of_types<T, Args...>()>;
}

template <typename T, Expandable Tup>
  constexpr std::size_t Index_of_types()
  {
    return decltype( type_impl::index_of_types<T>( std::declval<Tup>() ) )::value;
  }

//
// type at index I
//
template <std::size_t I, typename... Args>
  using type_at = std::tuple_element_t<I, std::tuple<Args...>>;

template <typename... Args>
  using head_type = type_at<0, Args...>;

/**
 * @brief range to tuple
 */
namespace type_impl
{
  template <std::size_t N, std::size_t D, Input_iterator I, Expandable Tup>
  requires (D == N)
    inline Tup
    to_tuple( I iter, Tup&& t)
    {
      return t;
    }

  template <std::size_t N, std::size_t D, Input_iterator I, Expandable Tup>
  requires (D < N)
    inline Tup
    to_tuple( I iter, Tup&& t)
    {
      std::get<D>(t) = *iter;
      return to_tuple<N, D+1>( ++iter, std::forward<Tup>(t) );
    }
}

template <std::size_t N, Range R>
requires (N > 0)
  inline decltype(auto)
  to_tuple( R&& r )
  {
    using std::begin;
    using std::endl;
    return type_impl::to_tuple<N, 0>( begin(r), repeat_type_as_tuple<Value_type<R>, N>() );
  }

/**
 * @brief tuple unpack and substitute into function
 */
namespace type_impl
{
  template <std::size_t I, typename F, Expandable Tup, typename... Args, std::size_t N = Expandable_size<Tup>()>
  requires ( I==N )
    constexpr decltype(auto)
    tpl_upck_sbst( F&& f, Tup& tup, Args&&... args )
    {
      return f( std::forward<Args>( args )... );
    }

  template <std::size_t I, typename F, Expandable Tup, typename... Args, std::size_t N = Expandable_size<Tup>()>
  requires ( I<N )
    constexpr decltype(auto)
    tpl_upck_sbst( F&& f, Tup& tup, Args&&... args)
    {
      return tpl_upck_sbst<I+1>( std::forward<F>(f),
                                 tup,
                                 std::forward<Args>( args )...,
                                 std::forward<decltype(std::get<I>(tup))>( std::get<I>( tup ) ));
    }

  template <typename F, Expandable Tup, std::size_t N = Expandable_size<Tup>()>
  requires ( N>0 )
    constexpr decltype(auto)
    tpl_upck_sbst( F&& f, Tup& tup )
    {
      return tpl_upck_sbst<1>( std::forward<F>(f), 
                               tup,
                               std::forward<decltype(std::get<0>(tup))>( std::get<0>( tup ) ) );
    }
}

template <typename F, typename Tup>
  constexpr decltype(auto)
  tuple_unpack_and_subst( F&& f, Tup& tup )
  {
    return type_impl::tpl_upck_sbst( std::forward<F>(f), tup );
  }
  
// Unary_for_each
// [TODO] put F(T) as Unary_operation concept for both void and non-void return
template <typename F, typename T, typename... Args>
requires Empty<Args...>()
  inline decltype(auto)
  unary_for_each( F&& f, T&& t, Args&&... args )
  {
    return f( std::forward<T>(t) );
  }

// [TODO] put F(T) as Unary_operation concept for both void and non-void return
template <typename F, typename T, typename... Args>
requires Not_empty<Args...>()
  inline decltype(auto)
  unary_for_each( F&& f, T&& t, Args&&... args )
  {
    f( std::forward<T>(t) );
    return unary_for_each( std::forward<F>(f), std::forward<Args>(args)... );
  }

// tuple_for_each -- const std::tuple<Args...>&
template <typename F, typename... Args>
  inline decltype(auto)
  tuple_for_each( F&& f, const std::tuple<Args...>& tup )
  {
    return tuple_unpack_and_subst(
      [&]( auto... args )
      {
        return unary_for_each( std::forward<F>(f), 
                               args... );
      },
      tup );
  }

// tuple_for_each -- std::tuple<Args...>&
template <typename F, typename... Args>
  inline decltype(auto)
  tuple_for_each( F&& f, std::tuple<Args...>& tup )
  {
    return tuple_unpack_and_subst(
      [&]( auto&... args )
      {
        return unary_for_each( std::forward<F>(f), 
                               args... );
      },
      tup );
  }
  
// Unary_for_each -- with ID
// [TODO] put F(T) as Unary_operation concept for both void and non-void return
template <typename F, std::size_t I, typename T, typename... Args>
requires Empty<Args...>()
  inline decltype(auto)
  unary_for_each_with_id( F&& f, size_constant<I>, T&& t, Args&&... args )
  {
    return f( size_constant<I>(), std::forward<T>(t) );
  }

// [TODO] put F(T) as Unary_operation concept for both void and non-void return
template <typename F, std::size_t I, typename T, typename... Args>
requires Not_empty<Args...>()
  inline decltype(auto)
  unary_for_each_with_id( F&& f, size_constant<I>, T&& t, Args&&... args )
  {
    f( size_constant<I>(), std::forward<T>(t) );
    return unary_for_each_with_id( std::forward<F>(f), 
                                   size_constant<I+1>(), 
                                   std::forward<Args>(args)... );
  }

// tuple_for_each_with_id -- const std::tuple<Args...>&
template <typename F, typename... Args>
  inline decltype(auto)
  tuple_for_each_with_id( F&& f, const std::tuple<Args...>& tup )
  {
    return tuple_unpack_and_subst(
      [&]( auto... args )
      {
        return unary_for_each_with_id( std::forward<F>(f), 
                                       size_constant<0>(),
                                       args... );
      },
      tup );
  }

// tuple_for_each_with_id -- std::tuple<Args...>&
template <typename F, typename... Args>
  inline decltype(auto)
  tuple_for_each_with_id( F&& f, std::tuple<Args...>& tup )
  {
    return tuple_unpack_and_subst(
      [&]( auto&... args )
      {
        return unary_for_each_with_id( std::forward<F>(f),
                                       size_constant<0>(),
                                       args... );
      },
      tup );
  }
 
// tuple_append: append object to std::tuple
template <typename T, typename... Args>
  constexpr decltype(auto)
  tuple_append( const std::tuple<Args...>& tup, T&& t )
  {
    return tuple_unpack_and_subst( 
      [&]( Args... args )
      { 
        return std::make_tuple( std::forward<Args>(args)...,
                                std::forward<T>(t) );
      },
      tup );
  }

// Generate std::tuple<T, T, T, U, T, T...>, where T repeats N times
// and U slots at I. T should be default constructible, whereas u is
// the instance of U
// (ex) auto b = tuple_slot_type<int, 5, 2>( 2.4 ); 
// b = make_tuple(int(), int(), 2.4, int(), int())
template <Default_constructible T, std::size_t N, std::size_t I, typename U>
requires (I < N)
  inline decltype(auto)
  tuple_slot_type( U&& u )
  {
    using X = repeat_type_as_tuple<T, N>;
    using R = replace_type<Main_type<U>, I, X>;
    R tup;
    //std::get<I>(tup) = std::forward<U>( u );
    std::get<I>(tup) = u;
    return tup;
  }

namespace type_impl
{

template <typename F, typename G, std::size_t... I, typename... Args>
  constexpr decltype(auto)
  varargs_trans_impl( F&& f, G&& g, std::index_sequence<I...>, Args&&... args )
  {
    return f( g( std::forward<Args>(args), I )... );
  } 
}

// varargs_trans:
//
//    transform a list of var-args and collect/reduce those mapped values
//
// f -> ( auto&&... args ) : collect / reduce function
// g -> ( auto x, int id ) : transform function
//
template <typename F, typename G, typename... Args>
  constexpr decltype(auto)
  varargs_trans( F&& f, G&& g, Args&&... args )
  {
    return type_impl::varargs_trans_impl( std::forward<F>(f),
                                          std::forward<G>(g), 
                                          std::make_index_sequence<sizeof...(Args)>{}, 
                                          std::forward<Args>(args)... );
  }

// varargs_sum:
//
template <typename T>
  constexpr decltype(auto)
  varargs_sum( T&& t )
  {
    return t;
  }

template <typename T, typename... Args>
requires Not_empty<Args...>()
  constexpr decltype(auto)
  varargs_sum( T&& t, Args&&... args )
  {
    return t + varargs_sum( std::forward<Args>(args)... );
  }

} // namespace spx

#endif // SPX_TYPE_LIST_H
