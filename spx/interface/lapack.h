#ifndef SPX_LAPACK_H
#define SPX_LAPACK_H

extern "C"
{

  // solve eigenvalues /eigenvectors
  extern void zgeev_( const char* jobvl, const char* jobvr, int* n, std::complex<double>* a, int* lda, std::complex<double>* w, 
                      std::complex<double>* vl, int* ldvl, std::complex<double>* vr, int* ldvr, 
                      std::complex<double>* work, int* lwork, double* rwork, int* info );

  // LU decomoposition of a general matrix
  extern void sgetrf_( int* m, int* n, float*  a, int* lda, int* ipiv, int* info );
  extern void dgetrf_( int* m, int* n, double* a, int* lda, int* ipiv, int* info );
  extern void cgetrf_( int* m, int* n, std::complex<float>*  a, int* lda, int* ipiv, int* info );
  extern void zgetrf_( int* m, int* n, std::complex<double>* a, int* lda, int* ipiv, int* info );

  // generate inverse of a matrix given its LU decomposition
  extern void sgetri_( int* n, float*  a, int* lda, int* ipiv, float*  work, int* lwork, int* info );
  extern void dgetri_( int* n, double* a, int* lda, int* ipiv, double* work, int* lwork, int* info );
  extern void cgetri_( int* n, std::complex<float>*  a, int* lda, int* ipiv, std::complex<float>*  work, int* lwork, int* info );
  extern void zgetri_( int* n, std::complex<double>* a, int* lda, int* ipiv, std::complex<double>* work, int* lwork, int* info );

  // solve Ax = b using LU-factorized A
  extern void sgetrs_( char* c, int* n, int* nrhs, float*  a, int* lda, int* ipiv, float*  b, int* ldb, int* info );
  extern void dgetrs_( char* c, int* n, int* nrhs, double* a, int* lda, int* ipiv, double* b, int* ldb, int* info );
  extern void cgetrs_( char* c, int* n, int* nrhs, std::complex<float>*  a, int* lda, int* ipiv, std::complex<float>*  b, int* ldb, int* info );
  extern void zgetrs_( char* c, int* n, int* nrhs, std::complex<double>* a, int* lda, int* ipiv, std::complex<double>* b, int* ldb, int* info );
 
  // solve Ax = b
  extern void sgesv_( int* n, int* nrhs, float*  a, int* lda, int* ipiv, float*  b, int* ldb, int* info );
  extern void dgesv_( int* n, int* nrhs, double* a, int* lda, int* ipiv, double* b, int* ldb, int* info );
  extern void cgesv_( int* n, int* nrhs, std::complex<float>*  a, int* lda, int* ipiv, std::complex<float>*  b, int* ldb, int* info );
  extern void zgesv_( int* n, int* nrhs, std::complex<double>* a, int* lda, int* ipiv, std::complex<double>* b, int* ldb, int* info );
}

namespace spx
{

namespace lapack
{
  // LU
  //
  template <Float T, typename... Args>
    void getrf( Args... args ) { sgetrf_( args... ); }

  template <Double T, typename... Args>
    void getrf( Args... args ) { dgetrf_( args... ); }

  template <Complex_float T, typename... Args>
    void getrf( Args... args ) { cgetrf_( args... ); }

  template <Complex_double T, typename... Args>
    void getrf( Args... args ) { zgetrf_( args... ); }

  // Invert based-on LU
  //
  template <Float T, typename... Args>
    void getri( Args... args ) { sgetri_( args... ); }

  template <Double T, typename... Args>
    void getri( Args... args ) { dgetri_( args... ); }

  template <Complex_float T, typename... Args>
    void getri( Args... args ) { cgetri_( args... ); }

  template <Complex_double T, typename... Args>
    void getri( Args... args ) { zgetri_( args... ); }

  // Solve Ax = B using LU-factorized A
  //
  template <Float T, typename... Args>
    void getrs( Args... args ) { sgetrs_( args... ); }

  template <Double T, typename... Args>
    void getrs( Args... args ) { dgetrs_( args... ); }

  template <Complex_float T, typename... Args>
    void getrs( Args... args ) { cgetrs_( args... ); }

  template <Complex_double T, typename... Args>
    void getrs( Args... args ) { zgetrs_( args... ); }

  // Solve Ax = B 
  //
  template <Float T, typename... Args>
    void gesv( Args... args ) { sgesv_( args... ); }

  template <Double T, typename... Args>
    void gesv( Args... args ) { dgesv_( args... ); }

  template <Complex_float T, typename... Args>
    void gesv( Args... args ) { cgesv_( args... ); }

  template <Complex_double T, typename... Args>
    void gesv( Args... args ) { zgesv_( args... ); }

  // Bridging interface
  //
  template <Matrix A, typename F>
    void bridge( A& x, F&& f )
    {
      using T = Value_type<A>;
      
      int  N     = x.extent(0);
      int* ipiv  = new int[ N + 1 ];
      int  lwork = N * N;
      T*   work  = new T[ lwork ];
      int  info  = 0;

      T* a = new T[ N * N ];
      for( int i = 0; i < N; ++i )
        for( int j = 0; j < N; ++j )
          a[ j*N + i ] = x( i, j );

      f( N, a, ipiv, work, lwork, info );

      for( int i = 0; i < N; ++i )
        for( int j = 0; j < N; ++j )
          x( i, j ) = a[ j*N + i ];

      delete [] a;
      delete [] ipiv;
      delete [] work;
    }

  template <Matrix A, Dense_array V, typename F>
    void bridge( A& x, V& b, F&& f )
    {
      using T = Value_type<A>;
      
      int N    = x.extent(0);
      int NRHS = b.size() / N;     
      T*  r    = new T[ N * NRHS ];

      using std::begin;
      using std::end;
      std::copy( begin( b ), end( b ), r );
      
      bridge( x, [&]( auto... args ) { f( NRHS, r, args... ); } );

      std::copy( r, r + (NRHS*N), begin( b ) );
      delete [] r;
    }

  // generate function for Det. calculation
  //
  template <typename T>
    decltype(auto) func_det( T& det )
    {
      return [&]( auto N, auto a, auto ipiv )
      {
        det = T(1);
        for( int i = 0; i < N; ++i )
        {
          T p = (ipiv[i] == (i+1)) ? 1 : -1;
          det *= p * a[ i*N + i ];
        }
      };
    }

} // namespace lapack

// LU decompose

template <Matrix A>
  void lapack_lu( A& x )
  {
    using T = Value_type<A>;
    lapack::bridge( x, 
        []( auto N, auto a, auto ipiv, auto work, auto lwork, auto info )
        {
          lapack::getrf<T>( &N, &N, a, &N, ipiv, &info );
        });
  }

template <Matrix A, typename T = Value_type<A>>
  void lapack_lu( A& x, T& det )
  {
    lapack::bridge( x, 
        [&]( auto N, auto a, auto ipiv, auto work, auto lwork, auto info )
        {
          lapack::getrf<T>( &N, &N, a, &N, ipiv, &info );
          lapack::func_det( det )( N, a, ipiv );
        });
  }

// Matrix inverse

template <Matrix A>
  void lapack_inv( A& x )
  {
    using T = Value_type<A>;
    lapack::bridge( x, 
        []( auto N, auto a, auto ipiv, auto work, auto lwork, auto info )
        {
          lapack::getrf<T>( &N, &N, a, &N, ipiv, &info );
          lapack::getri<T>( &N, a, &N, ipiv, work, &lwork, &info );
        });
  }

template <Matrix A, typename T = Value_type<A>>
  void lapack_inv( A& x, T& det )
  {
    lapack::bridge( x, 
        [&]( auto N, auto a, auto ipiv, auto work, auto lwork, auto info )
        {
          lapack::getrf<T>( &N, &N, a, &N, ipiv, &info );
          lapack::func_det( det )( N, a, ipiv );
          lapack::getri<T>( &N, a, &N, ipiv, work, &lwork, &info );
        });
  }

// Solve Ax = B
//
// RHS formats:
//    [1-dim] rhs = { eq1-x0, eq1-x1, eq1-x2, ..., eq2-x0, eq2-x1, eq2-x2, ... }
//    [2-dim] rhs = { { eq1-x0, eq1-x1, eq1-x2, ... }, 
//                    { eq2-x0, eq2-x1, eq2-x2, ... } };
//
template <Matrix A, Dense_array B>
requires (B::rank() == 1 || B::rank() == 2)
  void lapack_solve( A& a, B& b )
  {
    using T = Value_type<A>;
    lapack::bridge( a, b,
        []( auto NRHS, auto b, auto N, auto a, auto ipiv, auto work, auto lwork, auto info )
        {
          char tr = 'N';
          lapack::getrf<T>( &N, &N, a, &N, ipiv, &info );
          lapack::getrs<T>( &tr, &N, &NRHS, a, &N, ipiv, b, &N, &info );
       });
  }

template <Matrix A, Dense_array B, typename T = Value_type<A>>
requires (B::rank() == 1 || B::rank() == 2)
  void lapack_solve( A& a, B& b, T& det )
  {
    lapack::bridge( a, b,
        [&]( auto NRHS, auto b, auto N, auto a, auto ipiv, auto work, auto lwork, auto info )
        {
          char tr = 'N';
          lapack::getrf<T>( &N, &N, a, &N, ipiv, &info );
          lapack::func_det( det )( N, a, ipiv );
          lapack::getrs<T>( &tr, &N, &NRHS, a, &N, ipiv, b, &N, &info );
       });
  }

} // namespace spx

#endif // SPX_LAPACK_H

