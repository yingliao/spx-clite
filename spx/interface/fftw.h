#ifndef SPX_FFTW_H
#define SPX_FFTW_H

#include <fftw3.h>

namespace spx
{

template <typename T, std::size_t D = 1> 
  struct fftw;

// double
//
template <std::size_t D>
  struct fftw<double, D>
  {
    using complex_t = fftw_complex;
    using plan_t    = fftw_plan;

    static plan_t 
    forward( int* n, complex_t* in, complex_t* out, unsigned flags = FFTW_ESTIMATE )
    {
      return fftw_plan_dft( D, n, in, out, FFTW_FORWARD, flags );
    }

    static plan_t
    backward( int* n, complex_t* in, complex_t* out, unsigned flags = FFTW_ESTIMATE )
    {
      return fftw_plan_dft( D, n, in, out, FFTW_BACKWARD, flags );
    }

    static void execute( const plan_t p )
    {
      fftw_execute( p );
    }

    static void destroy_plan( plan_t p )
    {
      fftw_destroy_plan( p );
    }
  };

// float
//
template <std::size_t D>
  struct fftw<float, D>
  {
    using complex_t = fftwf_complex;
    using plan_t    = fftwf_plan;

    static plan_t 
    forward( int* n, complex_t* in, complex_t* out, unsigned flags = FFTW_ESTIMATE )
    {
      return fftwf_plan_dft( D, n, in, out, FFTW_FORWARD, flags );
    }

    static plan_t
    backward( int* n, complex_t* in, complex_t* out, unsigned flags = FFTW_ESTIMATE )
    {
      return fftwf_plan_dft( D, n, in, out, FFTW_BACKWARD, flags );
    }

    static void execute( const plan_t p )
    {
      fftwf_execute( p );
    }

    static void destroy_plan( plan_t p )
    {
      fftwf_destroy_plan( p );
    }
  };

// long double
//
template <std::size_t D>
  struct fftw<long double, D>
  {
    using complex_t = fftwl_complex;
    using plan_t    = fftwl_plan;

    static plan_t 
    forward( int* n, complex_t* in, complex_t* out, unsigned flags = FFTW_ESTIMATE )
    {
      return fftwl_plan_dft( D, n, in, out, FFTW_FORWARD, flags );
    }

    static plan_t
    backward( int* n, complex_t* in, complex_t* out, unsigned flags = FFTW_ESTIMATE )
    {
      return fftwl_plan_dft( D, n, in, out, FFTW_BACKWARD, flags );
    }

    static void execute( const plan_t p )
    {
      fftwl_execute( p );
    }

    static void destroy_plan( plan_t p )
    {
      fftwl_destroy_plan( p );
    }
  };


} // namespace spx

#endif // SPX_FFTW_H

