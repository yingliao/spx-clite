#ifndef SPX_SYSTEM_TIMER_H
#define SPX_SYSTEM_TIMER_H

#include <chrono>
#include <utility>

namespace spx
{

// U = time_unit
template <typename U, typename T, typename F, typename... Args>
requires Same<function_result<F, Args...>, void>()
  decltype(auto) perform_and_measure_task( T&& time_acumlt, F&& f, Args&&... args ) 
  {
    decltype(auto) t1 = std::chrono::high_resolution_clock::now();
    f( std::forward<Args>(args)... );
    decltype(auto) t2 = std::chrono::high_resolution_clock::now();

    time_acumlt( std::chrono::duration_cast<U>(t2-t1).count() );
  }
  
// U = time_unit
template <typename U, typename T, typename F, typename... Args>
requires not Same<function_result<F, Args...>, void>()
  decltype(auto) perform_and_measure_task( T&& time_acumlt, F&& f, Args&&... args ) 
  {
    decltype(auto) t1 = std::chrono::high_resolution_clock::now();
    decltype(auto)  v = f( std::forward<Args>(args)... );
    decltype(auto) t2 = std::chrono::high_resolution_clock::now();

    time_acumlt( std::chrono::duration_cast<U>(t2-t1).count() );
    return v;
  }
  

template <class U = std::chrono::milliseconds>
  class task_timer
  {
  public:
    using time_rep_t = typename U::rep;

  private:
    time_rep_t task_time       = time_rep_t{0};
    time_rep_t task_total_time = time_rep_t{0};
    
  public:
    task_timer() = default;

    template<class F, class... Args>
    //requires Function<F, Args...>() [FIXME: for empty funtion f()]
      decltype(auto) operator()( F&& f, Args&&... args ) 
      {  
        return perform_and_measure_task<U>(
          [&]( time_rep_t t )
          { 
            task_time = t;
            task_total_time += t;
          }, 
          std::forward<F>(f), std::forward<Args>(args)...);
      }

      time_rep_t get_task_time() const 
      {  return task_time;  }
      
      time_rep_t get_total_time() const 
      {  return task_total_time;  }
      
      void reset() 
      {  task_total_time = 0;  }
    };

// provide some useful type alias

using millisec_timer = task_timer<std::chrono::duration<double, std::milli>>;
using nanosec_timer = task_timer<std::chrono::duration<double, std::nano>>;
using default_timer = nanosec_timer;

} // namespace spx

#endif // SPX_SYSTEM_TIMER_H