#include <spx/first_header.h>

//#include <clay/linear_algebra.h>

//#include <finite_difference/finite_difference.h>
//#include <math/spx_math.h>
//#include <spectral/spectral.h>
//#include <core/core.h>
//#include <geo_grid/spx_geo_grid.h>
//#include <cfd/spx_cfd.h>

#include <spx/type/spx_type.h>
#include <spx/system/spx_system.h>
#include <spx/util/spx_util.h>
#include <spx/array/spx_array.h>
#include <spx/math/spx_math.h>
#include <spx/core/spx_core.h>

#include <spx/util/vtk_writer.h>
#include <spx/util/hdf_writer.h>

