#ifndef SPX_INT_LIST_H
#define SPX_INT_LIST_H

namespace spx
{

template <typename T, T V>
  constexpr decltype(auto) product()
  {
    return V;
  }
  
template <typename T, T V, T... S>
requires Not_empty<std::integral_constant<T, S>...>() 
      && Has_multiplies<T>()
  constexpr decltype(auto) product()
  {
    return V * product<T, S...>();
  }  

} // namespace spx

#endif // SPX_INT_LIST_H