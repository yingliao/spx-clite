#ifndef SPX_HDF_WRITER_H
#define SPX_HDF_WRITER_H

#include "H5Cpp.h"

#ifndef H5_NO_NAMESPACE
  using namespace H5;
#endif
  
namespace spx
{

template <typename T>
requires Same<T, double>()
  decltype(auto) hdf5_file_t( const T&, bool be )
  {
    return be ? PredType::IEEE_F64BE : PredType::IEEE_F64LE;
  }

template <typename T>
requires Same<T, double>()
  decltype(auto) hdf5_mem_t( const T& )
  {
    return PredType::NATIVE_DOUBLE;
  } 

template <typename T>
requires Same<T, float>()
  decltype(auto) hdf5_file_t( const T&, bool be )
  {
    return be ? PredType::IEEE_F32BE : PredType::IEEE_F32LE;
  }

template <typename T>
requires Same<T, float>()
  decltype(auto) hdf5_mem_t( const T& )
  {
    return PredType::NATIVE_FLOAT;
  } 

template <typename T>
requires Same<T, int>()
  decltype(auto) hdf5_file_t( const T&, bool be )
  {
    return be ? PredType::STD_I32BE : PredType::STD_I32LE;
  }

template <typename T>
requires Same<T, int>()
  decltype(auto) hdf5_mem_t( const T& )
  {
    return PredType::NATIVE_INT;
  } 

template <typename T>
requires Same<T, short>()
  decltype(auto) hdf5_file_t( const T&, bool be )
  {
    return be ? PredType::STD_I16BE : PredType::STD_I16LE;
  }

template <typename T>
requires Same<T, short>()
  decltype(auto) hdf5_mem_t( const T& )
  {
    return PredType::NATIVE_SHORT;
  } 

template <typename T>
requires Same<T, long>()
  decltype(auto) hdf5_file_t( const T&, bool be )
  {
    return be ? PredType::STD_I64BE : PredType::STD_I64LE;
  }

template <typename T>
requires Same<T, long>()
  decltype(auto) hdf5_mem_t( const T& )
  {
    return PredType::NATIVE_LONG;
  } 

template <typename T>
requires Same<T, long long>()
  decltype(auto) hdf5_file_t( const T&, bool be )
  {
    return be ? PredType::STD_I64BE : PredType::STD_I64LE;
  }

template <typename T>
requires Same<T, long long>()
  decltype(auto) hdf5_mem_t( const T& )
  {
    return PredType::NATIVE_LLONG;
  } 

template <typename T>
requires Same<T, unsigned int>()
  decltype(auto) hdf5_file_t( const T&, bool be )
  {
    return be ? PredType::STD_U32BE : PredType::STD_U32LE;
  }

template <typename T>
requires Same<T, unsigned int>()
  decltype(auto) hdf5_mem_t( const T& )
  {
    return PredType::NATIVE_UINT;
  } 

template <typename T>
requires Same<T, unsigned short>()
  decltype(auto) hdf5_file_t( const T&, bool be )
  {
    return be ? PredType::STD_U16BE : PredType::STD_U16LE;
  }

template <typename T>
requires Same<T, unsigned short>()
  decltype(auto) hdf5_mem_t( const T& )
  {
    return PredType::NATIVE_USHORT;
  } 

template <typename T>
requires Same<T, unsigned long>()
  decltype(auto) hdf5_file_t( const T&, bool be )
  {
    return be ? PredType::STD_U64BE : PredType::STD_U64LE;
  }

template <typename T>
requires Same<T, unsigned long>()
  decltype(auto) hdf5_mem_t( const T& )
  {
    return PredType::NATIVE_ULONG;
  } 

template <typename T>
requires Same<T, unsigned long long>()
  decltype(auto) hdf5_file_t( const T&, bool be )
  {
    return be ? PredType::STD_U64BE : PredType::STD_U64LE;
  }

template <typename T>
requires Same<T, unsigned long long>()
  decltype(auto) hdf5_mem_t( const T& )
  {
    return PredType::NATIVE_ULLONG;
  } 


namespace hdf5_impl
{
  template <typename X>
  decltype(auto) make_cmplx_t( X&& ft  )
  {
    std::size_t sz = ft.getSize();

    CompType t( sz * 2 );
    t.insertMember( "real", 0,  ft );
    t.insertMember( "imag", sz, ft );
    return t;
  }

  template <typename V, typename X>
  requires Dense_array<V>()
    decltype(auto) make_arr_t( const V& v, const X& ft  )
    {
      using std::begin;
      using std::end;
      constexpr auto D = v.rank();

      decltype(auto) shp = v.extents();
      hsize_t dims[D];
      std::copy( begin( shp ), end( shp ), begin( dims ) );
      return ArrayType( ft, D, dims );
    }

  template <typename V, typename X>
  requires not Dense_array<V>()
    decltype(auto) make_arr_t( const V& v, const X& ft  )
    {
      hsize_t dims[1] = { v.size() };
      return ArrayType( ft, 1, dims );
    }
}

template <typename T>
  decltype(auto) hdf5_file_t( const std::complex<T>&, bool be )
  {
    auto t = hdf5_file_t( T(), be ); 
    return hdf5_impl::make_cmplx_t( t );
  }

template <typename T>
  decltype(auto) hdf5_mem_t( const std::complex<T>& o )
  {
    auto t = hdf5_mem_t( T() );
    return hdf5_impl::make_cmplx_t( t );
  } 

template <typename V>
requires Vector<V>() || Dense_array<V>()
  decltype(auto) hdf5_file_t( const V& v, bool be )
  {
    using std::begin;
    auto t = hdf5_file_t( *begin(v), be );
    return hdf5_impl::make_arr_t( v, t );
  }

template <typename V>
requires Vector<V>() || Dense_array<V>()
  decltype(auto) hdf5_mem_t( const V& v )
  {
    using std::begin;
    auto t = hdf5_mem_t( *begin(v) );
    return hdf5_impl::make_arr_t( v, t );
  }

namespace hdf5_impl
{
  template <typename T>
    struct flat_type
    {
    private:
      template <typename X>
      requires not (Vector<X>() || Dense_array<X>())
        static auto check( X&& ) -> X;

      template <typename X>
      requires Vector<X>() || Dense_array<X>()
        static auto check( X&& ) 
          -> decltype( check( std::declval<Value_type<X>>() ) );

    public:
      using type = decltype( check( std::declval<T>() ) );
    };
}

template <typename T>
  using HDF5_flat_type = typename hdf5_impl::flat_type<T>::type;

template <Dense_array A, typename T = Value_type<A>>
requires not( Vector<T>() || Dense_array<T>() )
  std::size_t hdf5_flat_size( const A& x )
  {
    return x.size();
  }

template <Dense_array A, typename T = Value_type<A>>
requires Vector<T>() || Dense_array<T>()
  std::size_t hdf5_flat_size( const A& x )
  {
    using std::begin;
    return x.size() * hdf5_flat_size( *begin(x) );
  }

// hdf5_flatten

template <Dense_array A, typename I, typename T = Value_type<A>>
requires not( Vector<T>() || Dense_array<T>() )
  decltype(auto) hdf5_flatten( const A& x, I out )
  {
    using std::begin;
    using std::end;
    return std::copy( begin(x), end(x), out );
  }

template <Dense_array A, typename I, typename T = Value_type<A>>
requires Vector<T>() || Dense_array<T>()
  decltype(auto) hdf5_flatten( const A& x, I out )
  {
    for( auto& v : x )
      out = hdf5_flatten( v, out );
    return out;
  }

// hdf5_deserialize

template <Dense_array A, typename I, typename T = Value_type<A>>
requires not( Vector<T>() || Dense_array<T>() )
  decltype(auto) hdf5_deserialize( A& x, I in )
  {
    using std::begin;
    std::copy( in, in + x.size(), begin(x) );
    in += x.size();
    return in;
  }

template <Dense_array A, typename I, typename T = Value_type<A>>
requires Vector<T>() || Dense_array<T>()
  decltype(auto) hdf5_deserialize( A& x, I in )
  {
    for( auto& v : x )
      in = hdf5_deserialize( v, in );
    return in;
  }


// hdf_writer

class hdf_writer
{
private:
  static constexpr bool big_endian() { return false; }

  std::size_t   cnt = 0;
  std::string   nme = "";
  std::string   prefix;
  H5File        file;

public:
  hdf_writer( std::string prefix = "./out_" )
    : prefix( prefix )
  {
    next();
    --cnt;
  }

  ~hdf_writer() { file.close(); }

  void close() { file.close(); }

  void        set_count( std::size_t n ) { cnt = n;    }
  std::size_t count() const              { return cnt; }

  hdf_writer& name( std::string s )
  {
    nme = s;
    return *this;
  }

  hdf_writer& next()
  {
    std::string fname = prefix + digistr( cnt ) + ".h5";
    ++cnt;
    file.close();
    file = H5File( fname, H5F_ACC_TRUNC ); 
    return *this;
  }

  template <Dense_array A> 
    hdf_writer& next( const A& coord )
    {
      next();
      name( "coord" ) << coord;
      return *this;
    }

  template <Dense_array A>
    hdf_writer& operator << ( const A& a )
    {
      using std::begin;
      using std::end;
      constexpr auto D = a.rank();

      decltype(auto) shp = a.extents();
      hsize_t dims[D];
      std::copy( begin( shp ), end( shp ), begin( dims ) );
      DataSpace ds( D, dims );
      
      decltype(auto) o  = *begin( a );
      decltype(auto) ft = hdf5_file_t( o, big_endian() );
      decltype(auto) mt = hdf5_mem_t( o ); 
      DataSet dset = file.createDataSet( nme, ft, ds );

      using U = HDF5_flat_type<A>;
      std::size_t sz = hdf5_flat_size( a );
      std::vector<U> data( sz );
      hdf5_flatten( a, begin( data ) );
 
      dset.write( &data[0], mt );
      dset.close(); 
      return *this;
    }
};

class hdf_reader
{
private:
  H5File        file;

public:
  hdf_reader() {}

  hdf_reader( std::string h5_fname )
  {
    open( h5_fname );
  }

  ~hdf_reader() { file.close(); }
  
  void open( std::string h5_fname )
  {
    file.close();
    file = H5File( h5_fname, H5F_ACC_RDONLY );
  }

  template <typename T, std::size_t D = 1>
    decltype(auto) read( std::string name, T&& t = T() )
    {
      DataSet dset = file.openDataSet( name );
      auto      ds = dset.getSpace();
      size_t    nd = ds.getSimpleExtentNdims(); 
      std::vector<hsize_t> dim( nd );
      ds.getSimpleExtentDims( &dim[0] );
  

      using U = HDF5_flat_type<T>;
      auto        sz_m = dset.getInMemDataSize();
      std::size_t sz_U = sizeof( U );
      std::size_t sz_v = sz_m / sz_U;
      std::vector<U> data( sz_v );
      
      decltype(auto) mt = hdf5_mem_t( t ); 
      dset.read( &data[0], mt );
      dset.close(); 

      auto arr = zeros<T, D>( dim );
      hdf5_deserialize( arr, data.begin() );
      
      return arr;
    }
};

} // namespace spx

#endif // SPX_HDF_WRITER_H
