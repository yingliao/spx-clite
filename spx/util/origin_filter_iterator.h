#ifndef SPX_ORIGIN_FILTER_ITERATOR_H
#define SPX_ORIGIN_FILTER_ITERATOR_H

#include <algorithm>

namespace spx
{
  
//////////////////////////////////////////////////////////////////////////////
// Find Next If
//
// Returns the first iterator i in [first + 1, last) where pred(*i) is true.
//
// Parameters:
//    - first, last -- A bounded readable range
//    - pred        -- A predicate function
//
// Returns:
//   The first iterator in [first + 1, last) where pred(*i) is true.
//
// Remarks: 
//   This algorithm is not a part of the C++ standard library.
template <Input_iterator I, typename P>
requires Predicate<P, Value_type<I>>()
  inline I
  find_next_if(I first, I last, P pred)
  {
    if (first != last)
      return std::find_if(std::next(first), last, pred);
    else
      return last;
  }
    
//////////////////////////////////////////////////////////////////////////////
// Filter Iterator Adaptor
//
// A filter iterator is
//
// Tempate Parameters:
//    I -- The underlying iterator
//    P -- A predicate function
template <Input_iterator I, typename P>
requires Predicate<P, Value_type<I>>()
  class filter_iterator
  {
  public:
    using value_type = Value_type<I>;
    using reference  = typename std::iterator_traits<I>::reference;
    using pointer    = typename std::iterator_traits<I>::pointer;

    // Constructors

    // Construct a filter iterator over the range [first, last).
    filter_iterator(I first, I last, P pred = {});

    // Construct a filter iterator over the empty range [last, last).
    filter_iterator(I last, P pred = {});


    // Properties

    // Returns the underlying iterator.
    I base() const { return first(); }

    // Returns the current iterator.
    const I& first() const { return std::get<0>(data); }
    
    // Returns an iterator past the end of the sequence.
    const I& last() const { return std::get<1>(data); }

    // Returns the predicate function of the filter iterator.
    const P& pred() const { return std::get<2>(data); }


    // Readable
    reference operator*() const;
    pointer operator->() const;


    // Increment
    filter_iterator& operator++();
    filter_iterator operator++(int);

  private:
    I&       first() { return std::get<0>(data); }
    I&       last()  { return std::get<1>(data); }

    void advance();

  private:
    std::tuple<I, I, P> data;
  };


template <Input_iterator I, typename P>
requires Predicate<P, Value_type<I>>()
  inline
  filter_iterator<I, P>::filter_iterator(I first, I last, P pred)
    : data(std::find_if(first, last, pred), last, pred)
  { }

template <Input_iterator I, typename P>
requires Predicate<P, Value_type<I>>()
  inline
  filter_iterator<I, P>::filter_iterator(I last, P pred)
    : data(last, last, pred)
  { }

template <Input_iterator I, typename P>
requires Predicate<P, Value_type<I>>()
  inline auto
  filter_iterator<I, P>::operator*() const -> reference
  {
    return *first();
  }

template <Input_iterator I, typename P>
requires Predicate<P, Value_type<I>>()
  inline auto
  filter_iterator<I, P>::operator->() const -> pointer
  {
    return &operator*();
  }

template <Input_iterator I, typename P>
requires Predicate<P, Value_type<I>>()
  inline filter_iterator<I, P>&
  filter_iterator<I, P>::operator++()
  {
    advance();
    return *this;
  }

template <Input_iterator I, typename P>
requires Predicate<P, Value_type<I>>()
  inline filter_iterator<I, P>
  filter_iterator<I, P>::operator++(int)
  {
    I tmp = first();
    advance();
    return {tmp, last(), pred()};
  }

template <Input_iterator I, typename P>
requires Predicate<P, Value_type<I>>()
  inline void
  filter_iterator<I, P>::advance()
  {
    first() = find_next_if(first(), last(), pred());
  }


// Equality comparable
//
// The behavior is undefined if a.pred() is not the same as b.pred().
template <Input_iterator I, typename P>
requires Predicate<P, Value_type<I>>()
  inline bool
  operator==(const filter_iterator<I, P>& a, const filter_iterator<I, P>& b)
  {
    return a.first() == b.first()
        && a.last() == b.last();
  }

template <Input_iterator I, typename P>
requires Predicate<P, Value_type<I>>()
  inline bool
  operator!=(const filter_iterator<I, P>& a, const filter_iterator<I, P>& b)
  {
    return !(a == b);
  }


// Make Filter

template <Input_iterator I, typename P>
requires Predicate<P, Value_type<I>>()
  inline filter_iterator<I, P>
  make_filter(I first, I last, P pred)
  {
    return {first, last, pred};
  }

template <Input_iterator I, typename P>
requires Predicate<P, Value_type<I>>()
  inline filter_iterator<I, P>
  make_filter(I last, P pred)
  {
    return {last, pred};
  }

//////////////////////////////////////////////////////////////////////////////
// Filter Range Adaptor
//
// A filter range is
//
// Tempate Parameters:
//    I -- The underlying iterator
//    P -- A predicate function
  
template <Input_iterator I, typename P>
requires Predicate<P, Value_type<I>>()
  class filtered_range
  {
  private:
    filter_iterator<I, P> flt_it;
    
  public:
    filtered_range( filter_iterator<I, P> full_ranged_it )
      : flt_it( full_ranged_it )
    {}
    
    filter_iterator<I, P> begin() const 
    { 
      return make_filter( flt_it.first(), flt_it.last(), flt_it.pred() );
    }
    
    filter_iterator<I, P> end() const 
    { 
      return make_filter( flt_it.last(), flt_it.pred() );
    }    
  };

template <Input_iterator I, typename P>
requires Predicate<P, Value_type<I>>()
  inline filtered_range<I, P>
  make_filtered_range( filter_iterator<I, P> full_ranged_it )
  {
    return filtered_range<I, P>( full_ranged_it );
  }
  
} // namespace spx

#endif // SPX_ORIGIN_FILTER_ITERATOR_H
