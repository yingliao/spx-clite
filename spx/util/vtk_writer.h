#ifndef SPX_VTK_WRITER_H
#define SPX_VTK_WRITER_H

#include <fstream>

namespace spx
{

std::string digistr( std::size_t cnt, std::size_t n = 4 )
{
  std::stringstream ss( std::stringstream::out );
  ss.fill( '0' );
  ss.width( n );
  ss << cnt;
  return ss.str();
}

class vtk_writer
{
private:
  std::size_t   cnt = 0;
  std::string   nme = "";
  std::string   prefix;
  std::ofstream out;

public:
  vtk_writer( std::string prefix = "./out_" )
    : prefix( prefix )
  {}

  ~vtk_writer() { out.close(); }
 
  void        set_count( std::size_t n ) { cnt = n;    }
  std::size_t count() const              { return cnt; }

  vtk_writer& name( std::string s )
  {
    nme = s;
    return *this;
  }

  template <Dense_array A>
  requires Vector<Value_type<A>>()
    vtk_writer& next( const A& coord )
    {
      std::string fname = prefix + digistr( cnt ) + ".vtk";
     
      ++cnt;

      if( out.is_open() )
        out.close();

      out = std::ofstream( fname );

      out << "# vtk DataFile Version 3.0" << std::endl;
      out << "vtk output" << std::endl;
      out << "ASCII" << std::endl;

      write_coord( coord );
      
      out.flush();
      return *this;
    }

  template <Dense_array A>
  //requires Vector<Value_type<A>>()
  requires Range<Value_type<A>>()
    vtk_writer& operator << ( const A& a )
    {
      out << "VECTORS " << nme << " double" << std::endl;
      for( auto v : a )
      {
        std::size_t d = 0;
        for( ; d < v.size(); ++d )
          out << float(v[ v.size() - d - 1 ]) << " ";
       
        if ( d < 3 )
        {
          for( std::size_t i = 0; i < 3-d; ++i )
            out << 0 << " ";
        }
        out << std::endl;
      }

      out.flush();
      return *this;
    }

  template <Dense_array A>
  //requires not Vector<Value_type<A>>()
  requires not Range<Value_type<A>>()
    vtk_writer& operator << ( const A& a ) 
    {
      out << "FIELD FieldData 1" << std::endl;
      out << nme << " 1 " << a.size() << " double" << std::endl;
      for( auto v : a )
        out << float(v) << " ";
      out << std::endl;
      
      out.flush();
      return *this;
    }

private:
  template <Dense_array A>
  requires (A::rank() == 1)
    void write_coord( const A& x )
    {
      out << "DATASET STRUCTURED_GRID" << std::endl;
      out << "DIMENSIONS " << x.extent(0) << " 1 1" << std::endl;
      out << "POINTS " << x.size() << " double" << std::endl;
      for( auto v : x )
        out << v[0] << " 0 0" << std::endl;
      out << "CELL_DATA " << (x.extent(0) - 1) << std::endl;
      out << "POINT_DATA " << x.size() << std::endl;
    }

  template <Dense_array A>
  requires (A::rank() == 2)
    void write_coord( const A& x )
    {
      out << "DATASET STRUCTURED_GRID" << std::endl;
      out << "DIMENSIONS " << x.extent(1) << " " << x.extent(0) << " 1" << std::endl;
      out << "POINTS " << x.size() << " double" << std::endl;
      for( auto v : x )
        out << v[1] << " " << v[0] << " 0" << std::endl;
      out << "CELL_DATA " << (x.extent(1) - 1) * (x.extent(0) - 1) << std::endl;
      out << "POINT_DATA " << x.size() << std::endl;
    }

  template <Dense_array A>
  requires (A::rank() == 3)
    void write_coord( const A& x )
    {
      out << "DATASET STRUCTURED_GRID" << std::endl;
      out << "DIMENSIONS " << x.extent(2) << " " << x.extent(1) << " " << x.extent(0) << std::endl;
      out << "POINTS " << x.size() << " double" << std::endl;
      for( auto v : x )
        out << v[2] << " " << v[1] << " " << v[0] << std::endl;
      out << "CELL_DATA " << (x.extent(2) - 1) * (x.extent(1) - 1) * (x.extent(0) - 1) << std::endl;
      out << "POINT_DATA " << x.size() << std::endl;
    }
};

template <typename T>
  T swap_endian(T u)
  {
    union
    {
      T u;
      unsigned char u8[sizeof(T)];
    } source, dest;

    source.u = u;

    for (size_t k = 0; k < sizeof(T); k++)
        dest.u8[k] = source.u8[sizeof(T) - k - 1];

    return dest.u;
  }

class vtk_writer_bin
{
private:
  std::size_t   cnt = 0;
  std::string   nme = "";
  std::string   prefix;
  std::ofstream out;

  using T = double;
  static constexpr auto sz = sizeof(T);
  T v1;
  T v3[3];

public:
  vtk_writer_bin( std::string prefix = "./out_" )
    : prefix( prefix )
  {}

  ~vtk_writer_bin() { out.close(); }
 
  vtk_writer_bin& name( std::string s )
  {
    nme = s;
    return *this;
  }

  template <Dense_array A>
  requires Vector<Value_type<A>>()
    vtk_writer_bin& next( const A& coord )
    {
      std::string fname = prefix + digistr( cnt ) + ".vtk";
     
      ++cnt;

      if( out.is_open() )
        out.close();

      out = std::ofstream( fname, std::ios::binary );

      out << "# vtk DataFile Version 3.0" << std::endl;
      out << "vtk output" << std::endl;
      out << "BINARY" << std::endl;

      write_coord( coord );
      
      out.flush();
      return *this;
    }

  template <Dense_array A>
  requires Range<Value_type<A>>()
  //requires Vector<Value_type<A>>()
    vtk_writer_bin& operator << ( const A& a )
    {
      out << "VECTORS " << nme << " double" << std::endl;
      for( auto v : a )
      {
        std::size_t d = 0;
        for( ; d < v.size(); ++d )
        {
          v1 = swap_endian<T>( v[ v.size() - d - 1 ] );
          out.write( (char*)&v1, sz );
        }
        if ( d < 3 )
        {
          for( std::size_t i = 0; i < 3-d; ++i )
          {
            v1 = 0;
            out.write( (char*)&v1, sz );
          }
        }
      }
      out << std::endl;
      out.flush();
      return *this;
    }

  template <Dense_array A>
  requires not Range<Value_type<A>>()
  //requires not Vector<Value_type<A>>()
    vtk_writer_bin& operator << ( const A& a ) 
    {
      out << "FIELD FieldData 1" << std::endl;
      out << nme << " 1 " << std::to_string( a.size() ) << " double" << std::endl;
      for( auto v : a )
      {
        v1 = swap_endian<T>( v );
        out.write( (char*)&v1, sz );
      }
      out << std::endl;
      out.flush();
      return *this;
    }

private:
  template <Dense_array A>
  requires (A::rank() == 1)
    void write_coord( const A& x )
    {
      out << "DATASET STRUCTURED_GRID" << std::endl;
      out << "DIMENSIONS " << std::to_string( x.extent(0) ) << " 1 1" << std::endl;
      out << "POINTS "     << std::to_string( x.size() )    << " double" << std::endl;
      for( auto v : x )
      {
        v3[0] = swap_endian<T>( v[0] );
        v3[1] = 0;
        v3[2] = 0;
        out.write( (char*)( &v3[0] ), sz*3 );
      }
      out << std::endl;
      out << "CELL_DATA "  << std::to_string( (x.extent(0) - 1) ) << std::endl;
      out << "POINT_DATA " << std::to_string( x.size() ) << std::endl;
    }

  template <Dense_array A>
  requires (A::rank() == 2)
    void write_coord( const A& x )
    {
      out << "DATASET STRUCTURED_GRID" << std::endl;
      out << "DIMENSIONS " << std::to_string( x.extent(1) ) << " " 
                           << std::to_string( x.extent(0) ) << " 1" << std::endl;
      out << "POINTS " << std::to_string( x.size() ) << " double" << std::endl;
      for( auto v : x )
      {
        v3[0] = swap_endian<T>( v[1] );
        v3[1] = swap_endian<T>( v[0] );
        v3[2] = 0;
        out.write( (char*)( &v3[0] ), sz*3 );
      }
      out << std::endl;
      out << "CELL_DATA "  << std::to_string( (x.extent(1) - 1) * (x.extent(0) - 1) ) << std::endl;
      out << "POINT_DATA " << std::to_string( x.size() ) << std::endl;
    }

  template <Dense_array A>
  requires (A::rank() == 3)
    void write_coord( const A& x )
    {
      out << "DATASET STRUCTURED_GRID" << std::endl;
      out << "DIMENSIONS " << std::to_string( x.extent(2) ) << " " 
                           << std::to_string( x.extent(1) ) << " " 
                           << std::to_string( x.extent(0) ) << std::endl;
      out << "POINTS " << std::to_string( x.size() ) << " double" << std::endl;
      for( auto v : x )
      {
        v3[0] = swap_endian<T>( v[2] );
        v3[1] = swap_endian<T>( v[1] );
        v3[2] = swap_endian<T>( v[0] );
        out.write( (char*)( &v3[0] ), sz*3 );
     }
      out << std::endl;
      out << "CELL_DATA "  << std::to_string( (x.extent(2) - 1) 
                                            * (x.extent(1) - 1) 
                                            * (x.extent(0) - 1) ) << std::endl;
      out << "POINT_DATA " << std::to_string( x.size() ) << std::endl;
    }
};


} // namespace spx

#endif // SPX_VTK_WRITER_H

