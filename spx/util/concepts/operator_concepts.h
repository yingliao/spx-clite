#ifndef SPX_OPERATOR_CONCEPTS_H
#define SPX_OPERATOR_CONCEPTS_H

namespace spx
{
  
//////////////////////////////////////////////////////////////////////////////
// Relational operators
//////////////////////////////////////////////////////////////////////////////

// Returns true if the expression t == u is valid.
template <typename T, typename U = T>
  concept bool Has_equal()
  {
    return requires( T t, U u ) {
             t == u;
           };
  }

// Returns true if the expression t != u is valid.
template <typename T, typename U = T>
  concept bool Has_not_equal()
  {
    return requires( T t, U u ) {
             t != u;
           };
  }

// Returns true if the expression t < u is valid.
template <typename T, typename U = T>
  concept bool Has_less()
  {
    return requires( T t, U u ) {
             t < u;
           };
  }

// Returns true if the expression t > u is valid.
template <typename T, typename U = T>
  concept bool Has_greater()
  {
    return requires( T t, U u ) {
             t > u;
           };
  }

// Returns true if the expression t <= u is valid.
template <typename T, typename U = T>
  concept bool Has_less_equal()
  {
    return requires( T t, U u ) {
             t <= u;
           };
  }

// Return true if the expression t >= u is valid.
template <typename T, typename U = T>
  concept bool Has_greater_equal()
  {
    return requires( T t, U u ) {
             t >= u;
           };
  }    

//////////////////////////////////////////////////////////////////////////////
// Arithmetic operators
//////////////////////////////////////////////////////////////////////////////

// Returns true if the expression t + u is valid.
template <typename T, typename U = T>
  concept bool Has_plus()
  {
    return requires( T t, U u ) {
             t + u;
           };
  }

// Returns true if the expression t - u is valid.
template <typename T, typename U = T>
  concept bool Has_minus()
  {
    return requires( T t, U u ) {
             t - u;
           };
  }

// Returns true if the expression t * u is valid.
template <typename T, typename U = T>
  concept bool Has_multiplies()
  {
    return requires( T t, U u ) {
             t * u;
           };
  }

// Return true if the expression t / u is valid.
template <typename T, typename U = T>
  concept bool Has_divides()
  {
    return requires( T t, U u ) {
             t / u;
           };
  }

// Return true if the expression t % u is valid.
template <typename T, typename U = T>
  concept bool Has_modulus()
  {
    return requires( T t, U u ) {
             t % u;
           };
  }

// Returns true if the expression +t is valid.
template <typename T>
  concept bool Has_unary_plus()
  {
    return requires( T t ) {
             +t;
           };
  }

// Returns true if the expression -t is valid.
template <typename T>
  concept bool Has_unary_minus()
  {
    return requires( T t ) {
             -t;
           };
  }



//////////////////////////////////////////////////////////////////////////////    
// Compound arithmetic assignment operators
//////////////////////////////////////////////////////////////////////////////

// Returns true if the expression t += u is valid.
template <typename T, typename U = T>
  concept bool Has_plus_assign()
  {
    return requires( T t, U u ) {
             t += u;
           };
  }    

// Returns true if the expression t -= u is valid.
template <typename T, typename U = T>
  concept bool Has_minus_assign()
  {
    return requires( T t, U u ) {
             t -= u;
           };
  }    

// Returns true if the expression t *= u is valid.
template <typename T, typename U = T>
  concept bool Has_multiplies_assign()
  {
    return requires( T t, U u ) {
             t *= u;
           };
  }    

// Returns true if the expression t /= u is valid.
template <typename T, typename U = T>
  concept bool Has_divides_assign()
  {
    return requires( T t, U u ) {
             t /= u;
           };
  }    

// Returns true if the expression t %= u is valid.
template <typename T, typename U = T>
  concept bool Has_modulus_assign()
  {
    return requires( T t, U u ) {
             t %= u;
           };
  }        



//////////////////////////////////////////////////////////////////////////////
// Increment and decrement
//////////////////////////////////////////////////////////////////////////////

// Returns true if the expression ++t is valid.
template <typename T>
  concept bool Has_pre_increment()
  {
    return requires( T t ) {
             ++t;
           };
  }

// Returns true if the expostssion t++ is valid.
template <typename T>
  concept bool Has_post_increment()
  {
    return requires( T t ) {
             t++;
           };
  }

// Returns true if the expression --t is valid.
template <typename T>
  concept bool Has_pre_decrement()
  {
    return requires( T t ) {
             --t;
           };
  }

// Returns true if the expostssion t-- is valid.
template <typename T>
concept bool Has_post_decrement()
  {
    return requires( T t ) {
             t--;
           };
  }


//////////////////////////////////////////////////////////////////////////////    
// Logical operators
//////////////////////////////////////////////////////////////////////////////

// Returns true if the expression t && u is valid.
template <typename T, typename U = T>
  concept bool Has_and()
  {
    return requires( T t, U u ) {
             t && u;
           };
  }

// Return true if the expression t || u is valid.
template <typename T, typename U = T>
  concept bool Has_or()
  {
    return requires( T t, U u ) {
             t || u;
           };
  }

// Return true if the expression !t is valid.
template <typename T>
  concept bool Has_not()
  {
    return requires( T t ) {
             !t;
           };
  }   


//////////////////////////////////////////////////////////////////////////////
// Binary oeprators
//////////////////////////////////////////////////////////////////////////////

// Returns true if the expression t & u is valid.
template <typename T, typename U = T>
  concept bool Has_bit_and()
  {
    return requires( T t, U u ) {
             t & u;
           };
  }

// Returns true if the expression t | u is valid.
template <typename T, typename U = T>
  concept bool Has_bit_or()
  {
    return requires( T t, U u ) {
             t | u;
           };
  }

// Returns true if the expression t ^ u is valid.
template <typename T, typename U = T>
  concept bool Has_bit_xor()
  {
    return requires( T t, U u ) {
             t ^ u;
           };
  }

// Returns true if the expression t << u is valid.
template <typename T, typename U = T>
  concept bool Has_left_shift()
  {
    return requires( T t, U u ) {
             t << u;
           };
  }

// Returns true if the expression t >> u is valid.
template <typename T, typename U = T>
  concept bool Has_right_shift()
  {
    return requires( T t, U u ) {
             t >> u;
           };
  }

// Return true if the expression ~t is valid.
template <typename T>
  concept bool Has_complement()
  {
    return requires( T t ) {
             ~t;
           };
  }   


//////////////////////////////////////////////////////////////////////////////
// Binary assignment operators
//////////////////////////////////////////////////////////////////////////////

// Returns true if the expression t &= u is valid.
template <typename T, typename U>
  concept bool Has_bit_and_assign()
  {
    return requires( T t, U u ) {
             t &= u;
           };
  }        

// Returns true if the expression t |= u is valid.
template <typename T, typename U>
  concept bool Has_bit_or_assign()
  {
    return requires( T t, U u ) {
             t |= u;
           };
  }        

// Returns true if the expression t ^= u is valid.
template <typename T, typename U>
  concept bool Has_bit_xor_assign()
  {
    return requires( T t, U u ) {
             t ^= u;
           };
  }        

// Returns true if the expression t <<= u is valid.
template <typename T, typename U>
  concept bool Has_left_shift_assign()
  {
    return requires( T t, U u ) {
             t <<= u;
           };
  }        

// Return true if the expression t >>= u is valid.
template <typename T, typename U>
  concept bool Has_right_shift_assign()
  {
    return requires( T t, U u ) {
             t >>= u;
           };
  }        



//////////////////////////////////////////////////////////////////////////////
// Address, dereference, subscript, and function call
//////////////////////////////////////////////////////////////////////////////

// Returns true if the expression &t is valid.
template <typename T>
  concept bool Has_address()
  {
    return requires( T t ) {
             &t;
           };
  }   

// Returns true if the expression *t is valid.
template <typename T>
  concept bool Has_dereference()
  {
    return requires( T t ) {
             *t;
           };
  }

// Returns true if the expression t[u] is valid.
template <typename T, typename U>
  concept bool Has_subscript()
  {
    return requires( T t, U u ) {
             t[u];
           };
  }

} // namespace spx

#endif // SPX_OPERATOR_CONCEPTS_H
