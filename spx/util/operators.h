#ifndef SPX_OPS_H
#define SPX_OPS_H

namespace spx
{
namespace ops
{
  struct invoke
  {
    template <typename F, typename... Args>
      constexpr decltype(auto)
      operator()( F&& f, Args&&... args )
      {
        return f( std::forward<Args>(args)... );
      }
  };

  struct assign
  {
    template <typename X, typename Y>
    requires Assignable<X, Y>()
      constexpr decltype(auto) 
      operator()(X&& x, Y&& y)
      {
        return x = y;
      }
  };
  
  struct equal
  {
    template <typename X, typename Y>
      constexpr decltype(auto) 
      operator()(X&& x, Y&& y)
      {
        return x == y;
      }
  };

  struct not_equal
  {
    template <typename X, typename Y>
      constexpr decltype(auto) 
      operator()(X&& x, Y&& y)
      {
        return x != y;
      }
  };

  struct less
  {
    template <typename X, typename Y>
      constexpr decltype(auto) 
      operator()(X&& x, Y&& y)
      {
        return x < y;
      }
  };

  struct greater
  {
    template <typename X, typename Y>
      constexpr decltype(auto) 
      operator()(X&& x, Y&& y)
      {
        return x > y;
      }
  };
  
  struct less_equal
  {
    template <typename X, typename Y>
      constexpr decltype(auto) 
      operator()(X&& x, Y&& y)
      {
        return x <= y;
      }
  };

  struct greater_equal
  {
    template <typename X, typename Y>
      constexpr decltype(auto) 
      operator()(X&& x, Y&& y)
      {
        return x >= y;
      }
  };

  struct plus
  {
    template <typename X, typename Y>
    requires Has_plus<X, Y>()
      constexpr decltype(auto) 
      operator()(X&& x, Y&& y)
      {
        return x + y;
      }
  };

  struct minus
  {
    template <typename X, typename Y>
    requires Has_minus<X, Y>()
      constexpr decltype(auto) 
      operator()(X&& x, Y&& y)
      {
        return x - y;
      }
  };

  struct multiplies
  {
    template <typename X, typename Y>
    requires Has_multiplies<X, Y>()    
      constexpr decltype(auto) 
      operator()(X&& x, Y&& y)
      {
        return x * y;
      }
  };

  struct divides
  {
    template <typename X, typename Y>
    requires Has_divides<X, Y>()    
      constexpr decltype(auto) 
      operator()(X&& x, Y&& y)
      {
        return x / y;
      }
  };
  
  struct modulus
  {
    template <typename X, typename Y>
    requires Has_modulus<X, Y>()    
      constexpr decltype(auto) 
      operator()(X&& x, Y&& y)
      {
        return x % y;
      }
  };
  
  struct unary_plus
  {
    template <typename X>
      constexpr decltype(auto) 
      operator()(X&& x)
      {
        return +x;
      }
  };

  struct unary_minus
  {
    template <typename X>
      constexpr decltype(auto) 
      operator()(X&& x)
      {
        return -x;
      }
  };

  struct plus_assign
  {
    template <typename X, typename Y>
    requires Has_plus_assign<X, Y>()
      constexpr decltype(auto) 
      operator()(X&& x, Y&& y)
      {
        return x += y;
      }
  };

  struct minus_assign
  {
    template <typename X, typename Y>
    requires Has_minus_assign<X, Y>()
      constexpr decltype(auto) 
      operator()(X&& x, Y&& y)
      {
        return x -= y;
      }
  };

  struct multiplies_assign
  {
    template <typename X, typename Y>
      constexpr decltype(auto) 
      operator()(X&& x, Y&& y)
      {
        return x *= y;
      }
  };

  struct divides_assign
  {
    template <typename X, typename Y>
      constexpr decltype(auto) 
      operator()(X&& x, Y&& y)
      {
        return x /= y;
      }
  };
  
  struct modulus_assign
  {
    template <typename X, typename Y>
      constexpr decltype(auto) 
      operator()(X&& x, Y&& y)
      {
        return x %= y;
      }
  };

  struct pre_increment
  {
    template <typename X>
      constexpr decltype(auto) 
      operator()(X&& x)
      {
        return ++x;
      }
  };
  
  struct post_increment
  {
    template <typename X>
      constexpr decltype(auto) 
      operator()(X&& x)
      {
        return x++;
      }
  };

  struct pre_decrement
  {
    template <typename X>
      constexpr decltype(auto) 
      operator()(X&& x)
      {
        return --x;
      }
  };
  
  struct post_decrement
  {
    template <typename X>
      constexpr decltype(auto) 
      operator()(X&& x)
      {
        return x--;
      }
  };
  
  struct binary_and
  {
    template <typename X, typename Y>
      constexpr decltype(auto) 
      operator()(X&& x, Y&& y)
      {
        return x && y;
      }
  };
  
  struct binary_or
  {
    template <typename X, typename Y>
      constexpr decltype(auto) 
      operator()(X&& x, Y&& y)
      {
        return x || y;
      }
  };
  
  struct unary_not
  {
    template <typename X>
      constexpr decltype(auto) 
      operator()(X&& x)
      {
        return !x;
      }
  };

  struct bit_and
  {
    template <typename X, typename Y>
    requires Has_bit_and<X, Y>()
      constexpr decltype(auto) 
      operator()(X&& x, Y&& y)
      {
        return x & y;
      }
  };

  struct bit_or
  {
    template <typename X, typename Y>
    requires Has_bit_or<X, Y>()
      constexpr decltype(auto) 
      operator()(X&& x, Y&& y)
      {
        return x | y;
      }
  };

  struct bit_xor
  {
    template <typename X, typename Y>
    requires Has_bit_xor<X, Y>()    
      constexpr decltype(auto) 
      operator()(X&& x, Y&& y)
      {
        return x ^ y;
      }
  };

  struct left_shift
  {
    template <typename X, typename Y>
    requires Has_left_shift<X, Y>()    
      constexpr decltype(auto) 
      operator()(X&& x, Y&& y)
      {
        return x << y;
      }
  };
  
  struct right_shift
  {
    template <typename X, typename Y>
    requires Has_right_shift<X, Y>()    
      constexpr decltype(auto) 
      operator()(X&& x, Y&& y)
      {
        return x >> y;
      }
  };

  struct complement
  {
    template <typename X>
      constexpr decltype(auto) 
      operator()(X&& x)
      {
        return ~x;
      }
  };

  struct bit_and_assign
  {
    template <typename X, typename Y>
      constexpr decltype(auto) 
      operator()(X&& x, Y&& y)
      {
        return x &= y;
      }
  };

  struct bit_or_assign
  {
    template <typename X, typename Y>
      constexpr decltype(auto) 
      operator()(X&& x, Y&& y)
      {
        return x |= y;
      }
  };

  struct bit_xor_assign
  {
    template <typename X, typename Y>
      constexpr decltype(auto) 
      operator()(X&& x, Y&& y)
      {
        return x ^= y;
      }
  };
  
  struct left_shift_assign
  {
    template <typename X, typename Y>
      constexpr decltype(auto) 
      operator()(X&& x, Y&& y)
      {
        return x <<= y;
      }
  };

  struct right_shift_assign
  {
    template <typename X, typename Y>
      constexpr decltype(auto) 
      operator()(X&& x, Y&& y)
      {
        return x >>= y;
      }
  };

  struct address
  {
    template <typename X>
      constexpr decltype(auto) 
      operator()(X&& x)
      {
        return &x;
      }
  };

  struct dereference
  {
    template <typename X>
      constexpr decltype(auto) 
      operator()(X&& x)
      {
        return *x;
      }
  };

  struct subscript
  {
    template <typename X, typename Y>
      constexpr decltype(auto) 
      operator()( X&& x, Y&& y )
      {
        return x[y];
      }
  };

} // namespace ops
} // namespace spx

#endif // SPX_OPS_H
