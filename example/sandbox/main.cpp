#include <iostream>
#include <complex>
#include <tuple>
#include <spx/spx.h>

using namespace std;
using namespace spx;

using T = double;
using R = complex<T>;
constexpr size_t D = 3;

int main()
{
  struct Inp
  {
    size_t Nz = 48;
    size_t Ny = 32;
    size_t Nx = 32;
    T      H  = pi<T>;
    T      L1 = pi<T> * 2.0;
    T      L2 = pi<T> * 2.0;
  };
  Inp inp;

  // numerical params
  //
  size_t Ns[D] = {inp.Nz, inp.Ny, inp.Nx };
  T       L[D] = { inp.H, inp.L1, inp.L2 };
  T    xmin[D] = {-inp.H,      0,      0 };
  T      dx[D] = { L[0]/T(Ns[0]), L[1]/T(Ns[1]), L[2]/T(Ns[2]) };
  size_t  N[D] = {Ns[0]+1, Ns[1], Ns[2] };

  // PBC domain
  //
  //auto gen = two_sided_vinokur<T>{ 1.52788, 0.39725 };
  auto gen = two_sided_vinokur<T>{ 3.02788, 0.19725 };
  auto x0b = gen( xmin[0], xmin[0]+L[0], Ns[0] );
  auto x1b = linspace( xmin[1], xmin[1]+L[1]-dx[1], N[1] );
  auto x2b = linspace( xmin[2], xmin[2]+L[2]-dx[2], N[2] );

  auto grd_eta = make_rectlin_grid( x1b, x2b );
  auto grd_rec = make_rectlin_grid( x0b, x1b, x2b );
  auto crd_eta = grd_eta.coords();

  d_arr<T, 2> x1  = crd_eta[0];
  d_arr<T, 2> x2  = crd_eta[1];
  d_arr<T, 2> eta = 0.2 * sin( x1 ) * cos( x2 );
  
  auto crd_crv = grd_rec.perturb_surface( eta, 0 ); // along axis-0

  hdf_writer out( "out_" );
  out.next( crd_crv );
  out.next( grd_rec.coords() );

  return 0;
}
