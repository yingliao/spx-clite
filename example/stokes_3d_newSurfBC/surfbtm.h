
template <typename T, std::size_t D>
  struct surf_coef_helper
  {
    using value_type = T;
    static constexpr std::size_t rank() { return D; }
    
    d_arr<T, D> eta;
    d_arr<T, D> eta_d1, eta_d1d1, eta_d1_2; 
    d_arr<T, D> eta_d2, eta_d2d2, eta_d2_2; 
    d_arr<T, D> eta_d1d2;

    d_arr<T, D> s00, s01, s02, s11, s22;
    
    d_arr<T, D> g0, g1, g2, a, b;

    d_arr<T, D> c0, c1, c2, c3, c4, c5, c6, c7, c8;
    d_arr<T, D> d0, d1, d2, d3, d4, d5, d6, d7, d8;

    d_arr<T, D> u0_dksi0, u0_dksi1, u0_dksi2;
    d_arr<T, D> u1_dksi0, u1_dksi1, u1_dksi2;
    d_arr<T, D> u2_dksi0, u2_dksi1, u2_dksi2;

    template <typename G, typename B, 
              typename Q0, typename Q1, typename Q2, typename Q3>
      void update_grd( G&& crv_grd, const B& eta_n,
                       Q0&&  eta_dx1, Q1&&  eta_dx1dx1, 
                       Q2&&  eta_dx2, Q3&&  eta_dx2dx2 )
      {
        using R = std::complex<T>;

        d_arr<R, 2> ETA      = to_freq( eta_n );
        d_arr<R, 2> ETA_d1   = eta_dx1( ETA );
        d_arr<R, 2> ETA_d1d1 = eta_dx1dx1( ETA );
        d_arr<R, 2> ETA_d2   = eta_dx2( ETA );
        d_arr<R, 2> ETA_d2d2 = eta_dx2dx2( ETA );
        d_arr<R, 2> ETA_d1d2 = eta_dx2( ETA_d1 );

        eta      = eta_n;
        eta_d1   = to_real( ETA_d1 );
        eta_d2   = to_real( ETA_d2 );
        eta_d1d1 = to_real( ETA_d1d1 );
        eta_d2d2 = to_real( ETA_d2d2 );
        eta_d1d2 = to_real( ETA_d1d2 );
        eta_d1_2 = eta_d1 * eta_d1;
        eta_d2_2 = eta_d2 * eta_d2;

        decltype(auto) dksi_dx = crv_grd.b_contra();
        s00 = make_callback_array( dksi_dx[0][0] )( last(), all(), all() );
        s01 = make_callback_array( dksi_dx[0][1] )( last(), all(), all() );
        s02 = make_callback_array( dksi_dx[0][2] )( last(), all(), all() );
        s11 = make_callback_array( dksi_dx[1][1] )( last(), all(), all() );
        s22 = make_callback_array( dksi_dx[2][2] )( last(), all(), all() );

        g0 = T(1) + eta_d1_2 + eta_d2_2;
        g1 = sqrt( ( T(1) + eta_d1_2 ) * g0 );
        g2 = sqrt( ( T(1) + eta_d2_2 ) * g0 );
        a  = eta_d2 * ( T(1) + eta_d2_2 - eta_d1_2 );
        b  = eta_d1 * ( T(1) + eta_d1_2 - eta_d2_2 );

        c0 = T(1) / ( s00 * g0 * g0 );
        c1 = s11 / ( eta_d1_2 - eta_d2_2 - T(1) );
        c2 = T(2) * eta_d1 * eta_d2 * s22;
        c3 = s11 * eta_d1 * ( T(3) + eta_d1_2 + T(3)*eta_d2_2 );
        c4 = s22 * a;
        c5 = s11 * a;
        c6 = s22 * eta_d1 * ( T(1) + eta_d1_2 - eta_d2_2 );
        c7 = ( T(1) + eta_d2_2 ) * g1;
        c8 = -eta_d1 * eta_d2 * g2;

        d0 = c0;
        d1 = T(2) * eta_d1 * eta_d2 * s11;
        d2 = s22 * ( eta_d2_2 - eta_d1_2 - T(1) );
        d3 = s11 * eta_d2 * ( T(1) + eta_d2_2 - eta_d1_2 );
        d4 = s22 * b;
        d5 = s11 * b;
        d6 = s22 * eta_d2 * ( T(3) + T(3)*eta_d1_2 + eta_d2_2 );
        d7 = -eta_d1 * eta_d2 * g1;
        d8 = ( T(1) + eta_d1_2 ) * g2;
      }

    template <Dense_array A, 
              typename O0, typename O1, typename F0, typename F1, typename F2>
      void update_vel( const A& u0s, const A& u1, const A& u2,
                       O0&& d1s, O1&& d2s, 
                       F0&& cl_to_sg, F1&& sg_to_cl, F2&& cl_d0_to_sg )
      {
        using R = complex<T>;

        A u0c = sg_to_cl( u0s );
        A u1s = cl_to_sg( u1 );
        A u2s = cl_to_sg( u2 );

        auto U0s = to_freq( u0s );
        auto U1s = to_freq( u1s );
        auto U2s = to_freq( u2s );

        // U0
        //
        auto U0_dksi1_e = make_callback_array( d1s( U0s ) );
        auto U0_dksi2_e = make_callback_array( d2s( U0s ) );

        d_arr<R, D> U0_dksi1 = U0_dksi1_e( last(), all(), all() );
        d_arr<R, D> U0_dksi2 = U0_dksi2_e( last(), all(), all() );

        u0_dksi0 = cl_d0_to_sg( u0c )( last(), all(), all() );
        u0_dksi1 = to_real( U0_dksi1 );
        u0_dksi2 = to_real( U0_dksi2 );

        // U1
        //
        auto U1_dksi1_e = make_callback_array( d1s( U1s ) );
        auto U1_dksi2_e = make_callback_array( d2s( U1s ) );
 
        d_arr<R, D> U1_dksi1 = U1_dksi1_e( last(), all(), all() );
        d_arr<R, D> U1_dksi2 = U1_dksi2_e( last(), all(), all() );
 
        u1_dksi0 = cl_d0_to_sg( u1 )( last(), all(), all() );
        u1_dksi1 = to_real( U1_dksi1 );
        u1_dksi2 = to_real( U1_dksi2 );

        // U2
        //
        auto U2_dksi1_e = make_callback_array( d1s( U2s ) );
        auto U2_dksi2_e = make_callback_array( d2s( U2s ) );

        d_arr<R, D> U2_dksi1 = U2_dksi1_e( last(), all(), all() );
        d_arr<R, D> U2_dksi2 = U2_dksi2_e( last(), all(), all() );

        u2_dksi0 = cl_d0_to_sg( u2 )( last(), all(), all() );
        u2_dksi1 = to_real( U2_dksi1 );
        u2_dksi2 = to_real( U2_dksi2 );
    }

    template <typename O>
      void dump( O& out )
      {
        out.name( "g0" ) << g0;
        out.name( "g1" ) << g1;
        out.name( "g2" ) << g2;
        out.name( "a" )  << a;
        out.name( "b" )  << b;
        
        out.name( "c0" ) << c0;
        out.name( "c1" ) << c1;
        out.name( "c2" ) << c2;
        out.name( "c3" ) << c3;
        out.name( "c4" ) << c4;
        out.name( "c5" ) << c5;
        out.name( "c6" ) << c6;
        out.name( "c7" ) << c7;
        out.name( "c8" ) << c8;

        out.name( "d0" ) << d0;
        out.name( "d1" ) << d1;
        out.name( "d2" ) << d2;
        out.name( "d3" ) << d3;
        out.name( "d4" ) << d4;
        out.name( "d5" ) << d5;
        out.name( "d6" ) << d6;
        out.name( "d7" ) << d7;
        out.name( "d8" ) << d8;

        out.name( "s00" ) << s00;
        out.name( "s01" ) << s01;
        out.name( "s02" ) << s02;
        out.name( "s11" ) << s11;
        out.name( "s22" ) << s22;
      }
  };

// -----------------------  Surface / Bottom BC  -------------------------------------------
 
template <typename S, Dense_array A, typename O0, typename F0>
  decltype(auto) surf_u0_bc( const S& sf, const A& r0s, O0&& d0c_l, F0&& sg_to_cl )
  {
    using T = Value_type<A>;

    auto r0c      = sg_to_cl( r0s );
    auto r0c_d0_e = make_callback_array( d0c_l( r0c ) );
    d_arr<T, 2> r0_d0 = r0c_d0_e( last(), all(), all() );

    d_arr<T, 2> tmp = sf.s01 * sf.u1_dksi0 
                    + sf.s11 * sf.u1_dksi1 
                    + sf.s02 * sf.u2_dksi0 
                    + sf.s22 * sf.u2_dksi2;

    d_arr<T, 2> rhs = r0_d0 - tmp / sf.s00;
    dealias( rhs );
    return rhs;
  }

template <typename S, Dense_array A, typename O0, typename B, typename T = Value_type<S>>
  decltype(auto) surf_u1_bc( const S& sf, const A& r1, O0&& d0c_l, const B& t1, const B& t2, T mu = 1.0 )
  {
    auto r1_d0_e = make_callback_array( d0c_l( r1 ) );
    d_arr<T, 2> r1_d0 = r1_d0_e( last(), all(), all() );

    d_arr<T, 2> tmp = sf.c1 * sf.u0_dksi1
                    + sf.c2 * sf.u0_dksi2
                    + sf.c3 * sf.u1_dksi1
                    + sf.c4 * sf.u1_dksi2
                    + sf.c5 * sf.u2_dksi1
                    + sf.c6 * sf.u2_dksi2
                    + sf.c7 * t1 / mu
                    + sf.c8 * t2 / mu;

    d_arr<T, 2> rhs = r1_d0 + sf.c0 * tmp; 
    dealias( rhs );
    return rhs;
  }

template <typename S, Dense_array A, typename O0>
  decltype(auto) surf_u1_bc( const S& sf, const A& r1, O0&& d0c_l )
  {
    return surf_u1_bc( sf, r1, std::forward<O0>( d0c_l ), 0.0, 0.0 );
  }


template <typename S, Dense_array A, typename O0, typename B, typename T = Value_type<S>>
  decltype(auto) surf_u2_bc( const S& sf, const A& r2, O0&& d0c_l, const B& t1, const B& t2, T mu = 1.0 )
  {
    auto r2_d0_e = make_callback_array( d0c_l( r2 ) );
    d_arr<T, 2> r2_d0 = r2_d0_e( last(), all(), all() );

    d_arr<T, 2> tmp = sf.d1 * sf.u0_dksi1
                    + sf.d2 * sf.u0_dksi2
                    + sf.d3 * sf.u1_dksi1
                    + sf.d4 * sf.u1_dksi2
                    + sf.d5 * sf.u2_dksi1
                    + sf.d6 * sf.u2_dksi2
                    + sf.d7 * t1 / mu
                    + sf.d8 * t1 / mu;

    d_arr<T, 2> rhs = r2_d0 + sf.c0 * tmp; 
    dealias( rhs );
    return rhs;
  }

template <typename S, Dense_array A, typename O0>
  decltype(auto) surf_u2_bc( const S& sf, const A& r2, O0&& d0c_l )
  {
    return surf_u2_bc( sf, r2, std::forward<O0>( d0c_l ), 0.0, 0.0 );  
  }

template <Dense_array A, typename O0, Dense_array B, typename G>
  decltype(auto) btm_dx0_bc( const A& r_prj, O0&& d0c_l, const B& du, G&& crv_grd )
  {
    using T = Value_type<A>;

    decltype(auto) dksi_dx = crv_grd.b_contra();
    auto           s00     = make_callback_array( dksi_dx[0][0] );
    d_arr<T, 2>    s00_btm = s00( first(), all(), all() );
 
    auto r_d0 = make_callback_array( d0c_l( r_prj ) );
    d_arr<T, 2> r_d0_btm = r_d0( first(), all(), all() );

    d_arr<T, 2> rhs = r_d0_btm + du / s00_btm;
    dealias( rhs );
    return rhs;
  }

template <typename V, typename S, typename T, typename A>
  decltype(auto) surf_pressure( V& vtk, const S& sf, T mu, T rho, T g, T gamma, const A& ext_stress )
  {
    // kappa
    //
    d_arr<T, 2> tmp1      = T(1) + sf.eta_d1_2;
    d_arr<T, 2> tmp2      = T(1) + sf.eta_d2_2;
    d_arr<T, 2> kappa_up  = tmp1 * sf.eta_d2d2 
                          + tmp2 * sf.eta_d1d1 
                          - T(2) * sf.eta_d1 * sf.eta_d2 * sf.eta_d1d2;
    d_arr<T, 2> kappa_dwn = pow( T(1) + sf.eta_d1_2 + sf.eta_d2_2, 1.5 );
    d_arr<T, 2> kappa     = kappa_up / kappa_dwn;
 
    d_arr<T, 2> Q11 = -( sf.eta_d1_2 + 1 ) * sf.s11 * sf.u1_dksi1;
    d_arr<T, 2> Q22 = -( sf.eta_d2_2 + 1 ) * sf.s22 * sf.u2_dksi2;
    d_arr<T, 2> Q01 = -sf.eta_d1           * sf.s11 * sf.u0_dksi1;
    d_arr<T, 2> Q02 = -sf.eta_d2           * sf.s22 * sf.u0_dksi2;
    d_arr<T, 2> Q12 =  sf.eta_d1 * sf.eta_d2 * ( sf.s22 * sf.u1_dksi2 + sf.s11 * sf.u2_dksi1 );
    d_arr<T, 2> Q   = Q11 + Q22 + Q01 + Q02 + Q12;

    d_arr<T, 2> M  = 2.0 * mu / ( 1 + sf.eta_d1_2 + sf.eta_d2_2 );
                M *= Q;
    
    d_arr<T, 2> eta = sf.eta;
    d_arr<T, 2> p   = M + rho * g * eta - gamma * kappa - ext_stress;
    dealias( p );

    eta *= rho*g;
    vtk.name( "kappa" )     << kappa;
    vtk.name( "Q11" )       << Q11;
    vtk.name( "Q22" )       << Q22;
    vtk.name( "Q01" )       << Q01;
    vtk.name( "Q02" )       << Q02;
    vtk.name( "Q12" )       << Q12;
    vtk.name( "Q" )         << Q;
    vtk.name( "M" )         << M;
    vtk.name( "rho_g_eta" ) << eta;

    return p;
  }

template <typename V, typename S, typename T>
  decltype(auto) surf_pressure( V& vtk, const S& sf, T mu, T rho, T g, T gamma )
  {
    return surf_pressure( vtk, sf, mu, rho, g, gamma, 0.0 );  
  }

template <Dense_array A, Dense_array B, typename G, typename T>
  decltype(auto) btm_phi( G&& crv_grd, const A& uh0s, const B& u0_btm, T alpha_0, T dt )
  {
    decltype(auto) dksi_dx = crv_grd.b_contra();
    auto           s00     = make_callback_array( dksi_dx[0][0] );
    d_arr<T, 2>    s00_btm = s00( first(), all(), all() );
 
    d_arr<T, 2> uh0_btm = uh0s( first(), all(), all() );
    d_arr<T, 2> phi_btm = alpha_0 / dt / s00_btm * ( uh0_btm - u0_btm );

    return phi_btm;
 }
