
template <typename T>
  struct exct_stokes
  {
   
    T g;
    T gma;
    T h;
    T ak;
    T lambda;
    T eps;

    T k;
    T a0;
    T kh;
    T omg;
    T c;
    T nu; 
    T prd;

    T m  = 0.5;
    T n  = 1.23;
    T kx;
    T ky;
    T kk;
    T cc; 
 
    exct_stokes( T nu, T g, T gma, T h, T ak = 0.3, T lambda = pi_2<T>, T eps = 0 )
      : nu( nu ), g( g ), gma( gma ), h( h ), ak( ak ), lambda( lambda ), eps( eps )
    {
      k   = pi_2<T> / lambda;
      a0  = ak / k;
      kh  = k * h;

      T tanh_kh      = std::tanh( kh );
      T tanh_kh_inv2 = 1.0 / tanh_kh / tanh_kh;
      T omg_lin      = std::sqrt( g * k * tanh_kh + gma * k*k*k );
      omg = omg_lin * (1 + ak*ak * ( 9.0/8.0 * std::pow( tanh_kh_inv2 - 1.0, 2 ) + tanh_kh_inv2 ) );
      //omg = k * (1 + 0.5 * ak * ak) * std::sqrt( g/k );

      c   = omg / k;
      T re  = c / k / nu;
      prd = pi_2<T> / omg;
      
      kx = k + m;
      ky = n;
      kk = std::sqrt( kx*kx + ky*ky );
      cc = eps * a0 /  std::sqrt( kk );
      
      cout << " c  = "  << c   << endl;
      cout << " re = "  << re  << endl;
      cout << " k  = "  << k   << endl;
      cout << " a0 = "  << a0  << endl;
      cout << " omg = " << omg << endl;
      cout << " T = "   << prd << endl;
    }

    template <Dense_array A>
      decltype(auto) eta( T t, const A& x )
      {
        constexpr auto D = x.rank();
        //T tanh_kh      = std::tanh( kh );
        //T tanh_kh_inv1 = 1.0 / tanh_kh;
        //T tanh_kh_inv2 = tanh_kh_inv1 * tanh_kh_inv1;
        //T tanh_kh_inv4 = tanh_kh_inv2 * tanh_kh_inv2;
        //T tanh_kh_inv6 = tanh_kh_inv4 * tanh_kh_inv2;
        
        d_arr<T, D> th  = k * x[0] - omg * t;
        /*
        T coef1 = 0.25*a0*a0*k * tanh_kh_inv1 * ( 3.0 * tanh_kh_inv2 - 1.0 );
        T coef2 = 3.0/8.0 *a0*a0*a0*k*k * ( tanh_kh_inv4 - 3.0*tanh_kh + 3.0 );
        T coef3 = 3.0/64.0*a0*a0*a0*k*k * ( 8.0*tanh_kh_inv6 + std::pow(tanh_kh_inv2-1.0, 2.0) ); 
        d_arr<T, D> eta = a0 * cos( th )
                        + coef1 * cos( 2.0*th )
                        - coef2 * cos( th )
                        + coef3 * cos( 3.0*th );
        */
        d_arr<T, D> eta = a0 * ( cos(th) 
                              + 0.5    * ak *      cos( 2.0*th ) 
                              + 3.0/8.0* ak * ak * cos( 3.0*th ) );

        eta += eps * a0 * sin( kx * x[0] ) * cos( ky * x[1] );
        return eta;
      }

    template <Dense_array A>
      decltype(auto) u2( T t, const A& x )
      {
        constexpr auto D = x.rank();
        d_arr<T, D> r( x.extents() );
        r = 0;
        
        r += ky * cc * cos( kx * x[1] ) * sin( ky * x[2] ) * exp( kk * x[0] );
        return r;
      }

    template <Dense_array A>
      decltype(auto) u1( T t, const A& x )
      {
        constexpr auto D = x.rank();
        //T tanh_kh      = std::tanh( kh );
        //T tanh_kh_inv1 = 1.0 / tanh_kh;
        //T tanh_kh_inv2 = tanh_kh_inv1 * tanh_kh_inv1;
        //T coef1 = ak*g/omg / std::cosh(kh);
        //T coef2 = 0.75*ak*ak*g/omg / tanh_kh * std::pow(tanh_kh_inv2-1.0, 2.0);
        //T coef3 = 3.0/64.0*ak*ak*ak*g/omg * (tanh_kh_inv2-1.0) * (tanh_kh_inv2+3.0) * (9.0*tanh_kh_inv2-13.0) / std::cosh(3.0*kh);

        d_arr<T, D> th = k * x[1] - omg * t;
        //d_arr<T, D> dp = k * (x[0] + h);
        //d_arr<T, D> r  = coef1 * cosh(     dp ) * cos(     th )
        //               + coef2 * cosh( 2.0*dp ) * cos( 2.0*th )
        //               + coef3 * cosh( 3.0*dp ) * cos( 3.0*th );
        
        d_arr<T, D> r  = a0 * omg * cos( th ) * exp( k * x[0] ); 

        r += kx * cc * sin( kx * x[1] ) * cos( ky * x[2] ) * exp( kk * x[0] );
        return r;
      }
 
    template <Dense_array A>
      decltype(auto) u0( T t, const A& x )
      {
        constexpr auto D = x.rank();
        //T tanh_kh      = std::tanh( kh );
        //T tanh_kh_inv1 = 1.0 / tanh_kh;
        //T tanh_kh_inv2 = tanh_kh_inv1 * tanh_kh_inv1;
        //T coef1 = ak*g/omg / std::cosh(kh);
        //T coef2 = 0.75*ak*ak*g/omg / tanh_kh * std::pow(tanh_kh_inv2-1.0, 2.0);
        //T coef3 = 3.0/64.0*ak*ak*ak*g/omg * (tanh_kh_inv2-1.0) * (tanh_kh_inv2+3.0) * (9.0*tanh_kh_inv2-13.0) / std::cosh(3.0*kh);

        d_arr<T, D> th = k * x[1] - omg * t;
        //d_arr<T, D> dp = k * (x[0] + h);
        //d_arr<T, D> r  = coef1 * sinh(     dp ) * sin(     th )
        //               + coef2 * sinh( 2.0*dp ) * sin( 2.0*th )
        //               + coef3 * sinh( 3.0*dp ) * sin( 3.0*th );
        d_arr<T, D> r  = a0 * omg * sin( th ) * exp( k * x[0] );

        r += -kk * cc * cos( kx * x[1] ) * cos( ky * x[2] ) * exp( kk * x[0] );
        return r;
      }
 };


