
template <typename T, 
          typename F0, typename F1, typename F2, typename F3, typename G0, typename G1, 
          Stencil_array O1, Stencil_array O2, Stencil_array O3,
          Stencil_array O5, Stencil_array O6, Stencil_array O7>
  struct poisson_helper
  {
    using R = std::complex<T>;

  private:
    T  tol = 1.e-10;
    
    T  hc;

    F0 cl_to_sg;
    F1 sg_to_cl;
    F2 cl_d0_to_sg;
    F3 sg_d0_to_cl;

    G0 crv_grd_c;
    G1 crv_grd_s;

    O1 d0d0c;
    O2 d1c;
    O3 d2c;
    
    O5 d0d0s;
    O6 d1s;
    O7 d2s;

    d_arr<T, 3> c0_c;
    d_arr<T, 3> c0_s;

  public:
    poisson_helper( T h, F0&& f0, F1&& f1, F2&& f2, F3&& f3, G0&& g0, G1&& g1, 
                    O1&& d0d0_c, O2&& dksi1_c, O3&& dksi2_c,
                    O5&& d0d0_s, O6&& dksi1_s, O7&& dksi2_s )
      : hc( h ),
        cl_to_sg(    std::forward<F0>( f0 ) ),
        sg_to_cl(    std::forward<F1>( f1 ) ),
        cl_d0_to_sg( std::forward<F2>( f2 ) ),
        sg_d0_to_cl( std::forward<F3>( f3 ) ),
        crv_grd_c( std::forward<G0>( g0 ) ),
        crv_grd_s( std::forward<G1>( g1 ) ),
        d0d0c( std::forward<O1>( d0d0_c    ) ),
        d1c(   std::forward<O2>( dksi1_c   ) ),
        d2c(   std::forward<O3>( dksi2_c   ) ),
        d0d0s( std::forward<O5>( d0d0_s    ) ),
        d1s(   std::forward<O6>( dksi1_s   ) ),
        d2s(   std::forward<O7>( dksi2_s   ) )
    {
      update_grid_coef();
    }
   
    void update_grid_coef()
    {
      c0_c = dksi0_coef_c();
      c0_s = dksi0_coef_s(); 
    }

    decltype(auto) make_laplace_diag_c()
    {
      decltype(auto) g  = crv_grd_c.g_contra();
      return make_stencil_array( hc      * d0d0c
                               + g[1][1] * d1c( d1c )
                               + g[2][2] * d2c( d2c ) );
                               //+ c0      * dksi0 );
    }
 
    decltype(auto) make_laplace_diag_s()
    {
      decltype(auto) g  = crv_grd_s.g_contra();
      return make_stencil_array( hc      * d0d0s
                               + g[1][1] * d1s( d1s )
                               + g[2][2] * d2s( d2s ) );
                               //+ c0      * dksi0 );
    }
   
    template <Dense_array A>
      decltype(auto) eval_laplace_off_c( const A& u ) // clpt
      {
        d_arr<T, 3> u_s    = cl_to_sg( u );
        d_arr<T, 3> u_d0   = sg_d0_to_cl( u_s ); 

        d_arr<R, 3> U_d0   = to_freq( u_d0 );
        d_arr<R, 3> U_d0d1 = d1c( U_d0 );
        d_arr<R, 3> U_d0d2 = d2c( U_d0 );
        
        d_arr<T, 3> u_d0d0 = d0d0c( u );
        d_arr<T, 3> u_d0d1 = to_real( U_d0d1 );
        d_arr<T, 3> u_d0d2 = to_real( U_d0d2 );

        decltype(auto) g  = crv_grd_c.g_contra();

        d_arr<T, 3> v = T(2) * ( g[0][1] * u_d0d1 + g[0][2] * u_d0d2 ) 
                      + c0_c * u_d0
                      + (g[0][0] - hc) * u_d0d0;
        return v;
      }

    template <Dense_array A>
      decltype(auto) eval_laplace_off_s( const A& u ) // stgr
      {
        d_arr<T, 3> u_c    = sg_to_cl( u );
        d_arr<T, 3> u_d0   = cl_d0_to_sg( u_c );

        d_arr<R, 3> U_d0   = to_freq( u_d0 );
        d_arr<R, 3> U_d0d1 = d1s( U_d0 );
        d_arr<R, 3> U_d0d2 = d2s( U_d0 );
        
        d_arr<T, 3> u_d0d0 = d0d0s( u );
        d_arr<T, 3> u_d0d1 = to_real( U_d0d1 );
        d_arr<T, 3> u_d0d2 = to_real( U_d0d2 );

        decltype(auto) g  = crv_grd_s.g_contra();

        d_arr<T, 3> v = T(2) * ( g[0][1] * u_d0d1 + g[0][2] * u_d0d2 ) 
                      + c0_s * u_d0
                      + (g[0][0] - hc) * u_d0d0;
        return v;
      }

    template <Dense_array A>
      decltype(auto) eval_laplace_c( const A& u )
      {
        auto lap_diag = this->make_laplace_diag_c();
        d_arr<R, 3> U          = to_freq( u );
        d_arr<R, 3> LAP_U_DIAG = lap_diag( U );
        d_arr<T, 3> v = this->eval_laplace_off_c( u ) + to_real( LAP_U_DIAG );
        return v;
      }

    template <Dense_array A, Dense_array B, typename O>
      decltype(auto) solve_dn( const A& rhs, const B& u_bc_top, const B& u_bc_btm, 
                               const A& u0,  O&& dksi0_c )
      {
        auto RHS      = to_freq( rhs );
        auto U_bc_top = to_freq( u_bc_top );
        auto U_bc_btm = to_freq( u_bc_btm );
        auto U0       = to_freq( u0 );

        auto on_node = make_on_node<R>( RHS.descriptor() );
        //auto btm = make_stencil_array( dksi0( j * dksi_dx[0][0] * on_node ) / j ); 
        auto btm = make_stencil_array( R(1) * dksi0_c ); 
        auto lap = this->make_laplace_diag_c();
        auto prb = make_stencil_array(
          []( auto& desc, auto idx, auto it_lap, auto it_top, auto it_btm )
          {
            if( idx[0] == desc.ubound(0) )
              return *it_top;
            else if( idx[0] == desc.lbound(0) )
              return *it_btm;
            else
              return *it_lap;
          },
          lap, 
          on_node, 
          btm );

        d_arr<R, 3> U_n(  RHS.extents() );
        d_arr<T, 3> u_cp( RHS.extents() );
        U_n  = U0;
        u_cp = to_real( U_n );

        T tol_n = tol * std::sqrt( T( U_n.size() ) );
        cout << "TOL = " << tol_n << endl;

        auto RHS_it_f = [&]( auto& U )
        {
          d_arr<T, 3> lap_off = this->eval_laplace_off_c( to_real( U ) );
          d_arr<R, 3> RHS_it  = RHS - to_freq( lap_off );
          RHS_it( last(),  all(), all() ) = U_bc_top;
          RHS_it( first(), all(), all() ) = U_bc_btm;
          return RHS_it;
        };

        /*
        auto krv = make_iter_solver( 1.e-6 );
        U_n = krv( [&]( auto& U )
            {
              d_arr<R, 3> RHS_it = RHS_it_f( U );
              d_arr<R, 3> RSD_it = RHS_it - prb( U );
              RSD_it *= RSD_it;
              return RSD_it;
            }, 
            U_n );
        u_cp = to_real( U_n );
        */

        auto tdma = []( auto& a, auto& b ){ tdma_solve( a, b ); };

        for( size_t it = 0; it < 300; ++it )
        {
          auto RHS_it = RHS_it_f( U_n );
          lsor_async( prb, RHS_it, U_n, 0, 1.0, tdma );
       
          auto u_n = to_real( U_n );
          d_arr<T, 3> ee = u_n - u_cp;
          T err = std::abs( norm2( ee ) ); 
          u_cp  = u_n;

          d_arr<R, 3> RHS_chk = RHS_it_f( U_n );
          d_arr<R, 3> RSD_chk = RHS_chk - prb( U_n );
          d_arr<R, 3> RSD_lap = RSD_chk( slice(first()+1, last()-1), all(), all() );
          d_arr<T, 3> rsd_nrm = abs( to_real(RSD_lap) );
          auto it_rsd_max = std::max_element( rsd_nrm.begin(), rsd_nrm.end() );

          cout << "[Iter = " << it << "] Err. L2-Norm = " << err
               << "\t Max-Rsdu = " << *it_rsd_max << endl;

          //if( err < tol_n )
          if( *it_rsd_max < tol )
            break;
        }
        return u_cp;
      }

  private:
    decltype(auto) dksi0_coef_c() // clpt
    {
      decltype(auto) s_c = crv_grd_c.b_contra();
      decltype(auto) s_s = crv_grd_s.b_contra();
      
      // dksi0 -coef
      //
      d_arr<T, 3> s00_s  = s_s[0][0];
      d_arr<T, 3> s01_s  = s_s[0][1];
      d_arr<T, 3> s02_s  = s_s[0][2];
      d_arr<T, 3> s00_d0 = sg_d0_to_cl( s00_s );
      d_arr<T, 3> s01_d0 = sg_d0_to_cl( s01_s ); 
      d_arr<T, 3> s02_d0 = sg_d0_to_cl( s02_s );

      d_arr<T, 3> s01    = s_c[0][1];
      d_arr<T, 3> s02    = s_c[0][2];
      d_arr<R, 3> S01    = to_freq( s01 );
      d_arr<R, 3> S02    = to_freq( s02 );
      d_arr<R, 3> S01_d1 = d1c( S01 );
      d_arr<R, 3> S02_d2 = d2c( S02 );
      d_arr<T, 3> s01_d1 = to_real( S01_d1 );
      d_arr<T, 3> s02_d2 = to_real( S02_d2 );
      d_arr<T, 3> c0_c   = s_c[0][0] * s00_d0 + s_c[0][1] * s01_d0 + s_c[0][2] * s02_d0
                         + s_c[1][1] * s01_d1 + s_c[2][2] * s02_d2;
      return c0_c; 
    }

    decltype(auto) dksi0_coef_s() // stgr
    {
      decltype(auto) s_c = crv_grd_c.b_contra();
      decltype(auto) s_s = crv_grd_s.b_contra();
      
      // dksi0 -coef
      //
      d_arr<T, 3> s00_c  = s_c[0][0];
      d_arr<T, 3> s01_c  = s_c[0][1];
      d_arr<T, 3> s02_c  = s_c[0][2];
      d_arr<T, 3> s00_d0 = cl_d0_to_sg( s00_c );
      d_arr<T, 3> s01_d0 = cl_d0_to_sg( s01_c );
      d_arr<T, 3> s02_d0 = cl_d0_to_sg( s02_c );

      d_arr<T, 3> s01    = s_s[0][1];
      d_arr<T, 3> s02    = s_s[0][2];
      d_arr<R, 3> S01    = to_freq( s01 );
      d_arr<R, 3> S02    = to_freq( s02 );
      d_arr<R, 3> S01_d1 = d1s( S01 );
      d_arr<R, 3> S02_d2 = d2s( S02 );
      d_arr<T, 3> s01_d1 = to_real( S01_d1 );
      d_arr<T, 3> s02_d2 = to_real( S02_d2 );
      d_arr<T, 3> c0_s   = s_s[0][0] * s00_d0 + s_s[0][1] * s01_d0 + s_s[0][2] * s02_d0
                         + s_s[1][1] * s01_d1 + s_s[2][2] * s02_d2;
      return c0_s; 
    }
  };
 
template <typename T,
          typename F0, typename F1, typename F2, typename F3, typename G0, typename G1, 
          Stencil_array O1, Stencil_array O2, Stencil_array O3,
          Stencil_array O5, Stencil_array O6, Stencil_array O7>
 decltype(auto) make_poisson_helper( T h, F0&& f0, F1&& f1, F2&& f2, F3&& f3, G0&& g0, G1&& g1, 
                                     O1&& d0d0_c, O2&& dksi1_c, O3&& dksi2_c,
                                     O5&& d0d0_s, O6&& dksi1_s, O7&& dksi2_s )
  {
    return poisson_helper<T, 
                          F0, F1, F2, F3, G0, G1, 
                          O1, O2, O3,
                          O5, O6, O7>( h,
                                       std::forward<F0>( f0 ),
                                       std::forward<F1>( f1 ),
                                       std::forward<F2>( f2 ),
                                       std::forward<F3>( f3 ),
                                       std::forward<G0>( g0 ), 
                                       std::forward<G1>( g1 ), 
                                       std::forward<O1>( d0d0_c ),
                                       std::forward<O2>( dksi1_c ),
                                       std::forward<O3>( dksi2_c ),
                                       std::forward<O5>( d0d0_s ),
                                       std::forward<O6>( dksi1_s ),
                                       std::forward<O7>( dksi2_s ) );
  }


