#include <iostream>
#include <spx/spx.h>

using namespace std;
using namespace spx;

int main()
{
  using T = double;
  using C = complex<T>;
  using V = static_vector<T, 2>;
  using M = static_array<C, 2, 2>;

  d_arr<C, 2> a( 3, 5 );
  int i = 0;
  for( auto& v : a )
    v = C( i++ );

  M m{ 1, 2, 3, 4 };
  d_arr<M, 2> b( 3, 2 );
  for( auto& v : b )
    v = m;

  auto c = make_sparse_array<M, 2>( 3, 2 );
  c.insert_on();
  c( 1, 1 ) = m;
  c.insert_off();

  hdf_writer hdf;
  hdf.name( "a" ) << a;
  hdf.name( "b" ) << b;
  hdf.name( "c" ) << c;
  hdf.close();
  
  hdf_reader hdf_i( "out_0000.h5" );
  auto a_in = hdf_i.read<C, 2>( "a" );
  cout << endl << endl;
  auto b_in = hdf_i.read<M, 2>( "b" );

  for( auto& x : a_in )
    cout << x << endl;

  for( auto& x : b_in )
    cout << x(0,0) << "\t" << x(0,1) << "\t" << x(1,0) << "\t" << x(1,1) << endl;


  return 0;
}
