#include <iostream>
#include <iomanip>
#include <vector>
#include <fstream>
#include <spx/spx.h>

using namespace std;
using namespace spx;

template <typename T>
  void print( const T& a )
  {
    for( std::size_t i = 0; i < a.extent(0); ++i ) {
      for( std::size_t j = 0; j < a.extent(1); ++j ) {
        std::cout << a(i, j) << "\t";
      }
      std::cout << std::endl;
    }
  }

template <Stencil S>
  void print( const S& stencil )
  {
    for( auto& kv : stencil ) 
    {
      cout << "[";
      for ( auto k : kv.first ) 
        cout << k << ", ";
      cout << "] --> " << kv.second << endl;
    }
  }

template <typename T, typename F>
  decltype(auto) solver_sor( T t_nxt, F&& u_ext )
  {
    return [&]( auto& rhs, auto a0, auto& ut, auto beta_0, auto&& op )
    {
      using U = Main_type<decltype(ut)>;
      using R = Value_type<U>;
      U u_cp( ut.extents() );
      u_cp = ut;
      auto on_node = make_on_node<R>( u_cp.descriptor() );
      auto lhs = make_stencil_array( a0 * on_node - beta_0 * op );
      auto gen = make_stencil_array(
          []( auto& desc, auto idx, auto it_lhs, auto it_drch )
          {
            if( idx[0] == desc.lbound(0) || idx[0] == desc.ubound(0) ) {
              return *it_drch;
            } else {
              return *it_lhs;  
            }
          },
          lhs, 
          on_node );
 
      // rhs( first() ) = u_ext( t_nxt )( first() );
      // rhs( last() )  = u_ext( t_nxt )( last() );

      // auto prob = gen;
      auto prob = lhs; 
      
      // psor( prob, rhs, u_cp );
      // adi( prob, rhs, u_cp );

      // psor_updater
      //
      auto update = psor_updater( prob, rhs, u_cp );
      for( std::size_t it = 0; it < 50; ++it )
      {
        cout << "Internal Iter = " << it << endl;
        for( std::size_t p = 0; p < u_cp.size(); ++p )
          update( {p} );
      }

      return u_cp;
    };
  }

template <typename T, typename F>
  decltype(auto) solver_iter( T t_nxt, F&& u_ext )
  {
    return [&]( auto& rhs, auto a0, auto& ut, auto beta_0, auto&& op )
    {
      using U = Main_type<decltype(ut)>;
      using R = Value_type<U>;
      
      auto lhs = [&]( auto& x )
      {
        U r = a0 * x - beta_0 * op(x);
        return r;
      };

      auto bc = [&]( auto& ut )
                {
                  ut( first() ) = u_ext( t_nxt )( first() );
                  ut( last() )  = u_ext( t_nxt )( last() );
                };
 
      auto f = make_iter_solver( 1.e-7, 1000 );

      // use BC for FDM
      auto u_cp = f( lhs, rhs, bc );
      
      // don't use BC for Fourier (FREQ domain)
      // auto u_cp = f( lhs, rhs );
      
      return u_cp;
    };
  }


int main()
{
  using T = double;
  using R = complex<T>;
  // using R = T;
  size_t N = 128;
  auto L  = 10;
  auto dx = L / T(N);
  auto x  = linspace( 0, L-dx, N );
  auto b1 = make_fd_basis_pbc( x, L, 1 );
  auto b2 = make_fd_basis_pbc( x, L, 2 );
  auto f1 = make_fourier_basis( N, dx, 1 );
  auto f2 = make_fourier_basis( N, dx, 2 );
  auto k  = 2 * pi_2<T> / L;
  auto dt = dx;
  size_t num_it = 5;
 
  auto u = [&]( T t ) 
  { 
    d_vector<T> u_ext = spx::sin( k*x ) * std::exp( -k*k*t );
    u_ext += T(10);
    // d_vector<T> u_ext = spx::sin( k*x + k*t ); 
    return u_ext;
  };

  auto u_dt = [&]( T t )
  {
    d_vec<T> du = (-k*k) * spx::sin( k*x ) * std::exp( -k*k*t );
    // return du;
    return fft( du );
  };

  d_vector<R> u_num{ N };
  u_num = fft( u(0) ); 
  // u_num = u(0); 

  auto d0   = make_stencil_array<1>( {N}, f1, 0 );
  auto d0d0 = make_stencil_array<1>( {N}, f2, 0 );
 
  //auto frac = make_multi_stepper_transcoef( dt, bdf1<T>(), u_num, 1.0,
  //                                              am2<T>(),  d0d0 );

  auto frac = make_multi_stepper( dt, bdf1<T>(), u_num, 
                                       am2<T>(), d0d0 );

  auto rk = make_runge_kutta( dt, rk4_38<T>(), u_num );

  auto save = [&]( auto& ouf, auto& u_num, std::size_t i )
  {
    T t = T(i) * dt;
    auto u_ext = u( t );
    for( auto v : u_ext )
      ouf << v << endl;
    for( auto v : u_num )
      ouf << std::real(v) << endl;
  };

  ofstream ouf( "out.txt" );
  ouf << N << endl;
  ouf << num_it << endl;
  for( auto v : x )
    ouf << v << endl;
  for( std::size_t i = 0; i < num_it; ++i )
  {
    auto t_nxt = T(i+1) * dt;
    auto solver = solver_sor( t_nxt, u );
    // auto solver = solver_iter( t_nxt, u );
    auto rk_f = [&]( auto n, auto dtn, auto& un )
    {
      auto tn = T(i) * dt + dtn;
      auto du = u_dt( tn );
      cout << "t = " << tn << endl;
      return du;
    };

    // u_num = ifft( frac.advance() );
    u_num = ifft( frac.advance( solver ) );
    frac.roll_back();
    // u_num = frac.advance( solver );
    // u_num = ifft( rk.advance( rk_f ) );

    save( ouf, u_num, i+1 );
    auto u_ext = u( t_nxt );
    auto mse = spx::sum( (u_num-u_ext) * (u_num-u_ext) ) / T(u_num.size());
    cout << "MSE = " << std::abs(mse) << endl << endl;
  }
  ouf.close();

  return 0;
}
