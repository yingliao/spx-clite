import numpy as np
import matplotlib.pyplot as plt

f = open("out.txt")

N = int( f.readline() )
num_it = int( f.readline() )

u_ext = np.zeros( [num_it, N] )
u_num = np.zeros( [num_it, N] )

x = np.zeros(N)
for j in range(N):
  x[j] =  float(f.readline())

for i in range(num_it):
  for j in range(N):
    s = [t for t in f.readline().split() ]
    u_ext[i, j] = float( s[0] )

  for j in range(N):
    s = [t for t in f.readline().split() ]
    u_num[i, j] = float( s[0] )

f.close()

plt.hold(True)
for it in range(num_it):
  plt.plot( x, u_ext[it, :], "b" )
  plt.plot( x, u_num[it, :], "g" )
plt.hold(False)
# plt.ylim([-1, 1])
plt.show()

