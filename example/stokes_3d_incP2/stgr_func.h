// ----------------------- Staggered Functions ------------------------------------

decltype(auto) make_clpt_to_stgr()
{
  return []( auto&& a )
  {
    using A = Main_type<decltype(a)>;
    A s = 0.5 * ( a( slice( first(), last()-1 ), all(), all() )
                + a( slice( first()+1, last() ), all(), all() ) );
    return s;
  };
}

template <typename T, typename O>
  decltype(auto) make_stgr_to_clpt( O&& d0s_h )
  {
    return [&]( auto&& p )
    {
      auto ksi0_s  = d0s_h.data_gen().diff_basis().coords();
      T dx0_btm = ksi0_s[ first()+1 ] - ksi0_s[ first()  ];
      T dx0_top = ksi0_s[ last()    ] - ksi0_s[ last()-1 ];

      auto p_d0 = make_callback_array( d0s_h( p ) );
      d_arr<T, 2> p_d0_btm = p_d0( first(), all(), all() );
      d_arr<T, 2> p_d0_top = p_d0( last(),  all(), all() );

      d_arr<T, 2> p_btm = p( first(), all(), all() ) - dx0_btm * p_d0_btm;
      d_arr<T, 2> p_top = p( last(),  all(), all() ) + dx0_top * p_d0_top;

      auto shp = p.extents();
      d_arr<T, 3> ps( {shp[0]+1, shp[1], shp[2]} );
   
      ps( slice( first()+1, last()-1 ), all(), all() ) = 
          0.5 * ( p( slice( first()+1, last() ), all(), all() )
                + p( slice( first(), last()-1 ), all(), all() ) );
   
      ps( first(), all(), all() ) = 0.5 * ( p_btm + p( first(), all(), all() ) );
      ps( last() , all(), all() ) = 0.5 * ( p_top + p( last(),  all(), all() ) );

      return ps;
    };
  }

template <typename T, typename O0, typename O1>
  decltype(auto) make_sg_d0_to_cl( O0&& d0s_h, O1&& d0s_l )
  {
    return [&]( auto& phi )
    {
      auto ksi0_s = d0s_h.data_gen().diff_basis().coords();
      T   dx0_btm = ksi0_s[ first()+1 ] - ksi0_s[ first()  ];
      T   dx0_top = ksi0_s[ last()    ] - ksi0_s[ last()-1 ];

      auto phi_d0_e = make_callback_array( d0s_h( phi ) );
      d_arr<T, 2> phi_d0_btm = phi_d0_e( first(), all(), all() );
      d_arr<T, 2> phi_d0_top = phi_d0_e( last(),  all(), all() );

      d_arr<T, 2> phi_btm = phi( first(), all(), all() ) - dx0_btm * phi_d0_btm;
      d_arr<T, 2> phi_top = phi( last(),  all(), all() ) + dx0_top * phi_d0_top;

      d_arr<T, 3> phi_d0 = d0s_l( phi );
        
      auto   shp   = phi.extents();
      size_t N[3]  = { shp[0]+1, shp[1], shp[2] };
      auto phi_d0c = d_arr<T, 3>( N );
      phi_d0c( slice( first()+1, last()-1 ), all(), all() ) =
        phi_d0( slice( first(), last()-1 ), all(), all() );

      phi_d0c( first(), all(), all() ) = ( phi( first(), all(), all() ) - phi_btm ) / dx0_btm;
      phi_d0c( last(),  all(), all() ) = ( phi_top - phi( last(), all(), all() )  ) / dx0_top;
      return phi_d0c;
    };
  }

template <typename T, typename O0>
  decltype(auto) make_cl_d0_to_sg( O0&& d0c_l )
  {
    return [&]( auto& phi )
    {
      d_arr<T, 3> phi_d0_c = d0c_l( phi );
      d_arr<T, 3> phi_d0_s = phi_d0_c( slice( first(), last()-1 ), all(), all() );
      return phi_d0_s;
    };
  }

