#include "stdafx.h"
//#include "ext_linvis_wave.h"
#include "ext_stokes_wave.h"

template <size_t I>
requires (I == 2)
void test( string prefix, string fname )
{
  constexpr std::size_t D = 3;
  
  using T = double;
  using R = std::complex<T>;
 
  input_parser inp( fname );

  // phsical params
  //
  T rho = inp.rho;
  T gma = inp.gma;
  T g   = inp.g;

  // exact solution
  //
  T nu  = inp.nu;
  T H   = inp.L0;
  T lmd = inp.lmd;
  //exct_lin_vis<T> ext( re, g, gma, ak );
  exct_stokes<T> ext( nu, g, gma, H, inp.ak, lmd, inp.eps );
  T mu  = rho * nu;

  // numerical params
  //
  size_t Ns[D] = {inp.Nz, inp.Ny, inp.Nx };
  T       L[D] = {     H, inp.L1, inp.L2 };
  T    xmin[D] = {    -H,      0,      0 };
  T      dx[D] = { L[0]/T(Ns[0]), L[1]/T(Ns[1]), L[2]/T(Ns[2]) };
  size_t  N[D] = {Ns[0]+1, Ns[1], Ns[2] };

  T        dt = inp.cfl / (1/dx[0] + 1/dx[1] + 1/dx[2]);
  size_t it_N = inp.n_prd * ext.prd / dt;

  cout << "dt = "     << dt   << endl;
  cout << "N_iter = " << it_N << endl;

  // PBC domain
  //
  auto gen = two_sided_vinokur<T>{ 1.52788, 0.39725 };
  auto x0s = gen( xmin[0], xmin[0]+L[0], Ns[0] );
  auto x1  = linspace( xmin[1], xmin[1]+L[1]-dx[1], N[1] );
  auto x2  = linspace( xmin[2], xmin[2]+L[2]-dx[2], N[2] );

  // collocated grid
  //
  auto make_clpt = [&]( const d_vec<T>& x0s, T& dx0_btm, T& dx0_top )
  {
    dx0_btm = x0s[ first()+1 ] - x0s[ first()  ];
    dx0_top = x0s[ last()    ] - x0s[ last()-1 ];
    auto x0 = d_vec<T>( N[0] );
    x0[ slice(1, last()-1) ] = 0.5 * ( x0s[ slice(first()+1, last()  ) ]
                                     + x0s[ slice(first(),   last()-1) ] );
    x0[ first() ] = x0[ first()+1 ] - dx0_btm;
    x0[ last()  ] = x0[ last() -1 ] + dx0_top;
    return x0;
  };

  T dx0_btm, dx0_top;
  d_vec<T> x0 = make_clpt( x0s, dx0_btm, dx0_top );
 
  // coord array
  //
  auto rec_grd_c = make_rectlin_grid( x0,  x1, x2 );
  auto rec_grd_s = make_rectlin_grid( x0s, x1, x2 );
  auto grd_eta   = make_rectlin_grid( x1, x2 );
  auto crd_eta   = grd_eta.coords();

  // eta operators
  //
  auto b1_o1      = make_fourier_basis( N[1], dx[1], 1 );
  auto b1_o2      = make_fourier_basis( N[1], dx[1], 2 );
  auto b2_o1      = make_fourier_basis( N[2], dx[2], 1 );
  auto b2_o2      = make_fourier_basis( N[2], dx[2], 2 );
  auto eta_dx1    = make_stencil_array<D-1>( { N[1], N[2] }, b1_o1, 0 );
  auto eta_dx1dx1 = make_stencil_array<D-1>( { N[1], N[2] }, b1_o2, 0 );
  auto eta_dx2    = make_stencil_array<D-1>( { N[1], N[2] }, b2_o1, 1 );
  auto eta_dx2dx2 = make_stencil_array<D-1>( { N[1], N[2] }, b2_o2, 1 );

  // curvilinear grid
  //
  d_vec<T> ksi0   = x0;
  d_vec<T> ksi0_s = x0s;
  d_vec<T> ksi1   = x1;
  d_vec<T> ksi2   = x2;
  
  auto crvgrd_b0s = make_fd_basis( ksi0_s, 1, 4 ); // 1-st order diff & 4-th order accuracy
  auto crvgrd_b0c = make_fd_basis( ksi0,   1, 4 ); // 1-st order diff & 4-th order accuracy
  auto crvgrd_b1  = make_fd_basis( ksi1,   1, 4 ); // 1-st order diff & 4-th order accuracy
  auto crvgrd_b2  = make_fd_basis( ksi2,   1, 4 ); // 1-st order diff & 4-th order accuracy
  
  auto crvgrd_d0s = make_stencil_array<D>( Ns, crvgrd_b0s, 0 );
  auto crvgrd_d1s = make_stencil_array<D>( Ns, crvgrd_b1,  1 );
  auto crvgrd_d2s = make_stencil_array<D>( Ns, crvgrd_b2,  2 );
  auto crv_grd_s  = make_curvilin_dyn_grid( dt, crvgrd_d0s, crvgrd_d1s, crvgrd_d2s );

  auto crvgrd_d0c = make_stencil_array<D>( N, crvgrd_b0c, 0 );
  auto crvgrd_d1c = make_stencil_array<D>( N, crvgrd_b1,  1 );
  auto crvgrd_d2c = make_stencil_array<D>( N, crvgrd_b2,  2 );
  auto crv_grd_c  = make_curvilin_dyn_grid( dt, crvgrd_d0c, crvgrd_d1c, crvgrd_d2c );
 
  // curvilinear operators -- collocated
  //
  auto ksi_b0c_h  = make_fd_basis( ksi0, 1, 4 );
  auto ksi_b0c_l  = make_fd_basis( ksi0, 1, 1 );
  auto ksi_b0c_o2 = make_fd_basis( ksi0, 2 );
  auto ksi_b1c    = make_fourier_basis( N[1], dx[1], 1 );
  auto ksi_b2c    = make_fourier_basis( N[2], dx[2], 1 );

  auto d0c_h      = make_stencil_array<D>( N, ksi_b0c_h,  0 );
  auto d0c_l      = make_stencil_array<D>( N, ksi_b0c_l,  0 );
  auto d0d0c      = make_stencil_array<D>( N, ksi_b0c_o2, 0 );
  auto d1c        = make_stencil_array<D>( N, ksi_b1c,    1 );
  auto d2c        = make_stencil_array<D>( N, ksi_b2c,    2 );

  // curvilinear operators -- staggered
  //
  auto ksi_b0s_h  = make_fd_basis( ksi0_s, 1, 4 );
  auto ksi_b0s_l  = make_fd_basis( ksi0_s, 1, 1 );
  auto ksi_b0s_o2 = make_fd_basis( ksi0_s, 2 );

  auto d0s_h      = make_stencil_array<D>( Ns, ksi_b0s_h,  0 );
  auto d0s_l      = make_stencil_array<D>( Ns, ksi_b0s_l,  0 );
  auto d0d0s      = make_stencil_array<D>( Ns, ksi_b0s_o2, 0 );
  auto d1s        = make_stencil_array<D>( Ns, ksi_b1c,    1 );
  auto d2s        = make_stencil_array<D>( Ns, ksi_b2c,    2 );

  // staggered functions
  //
  auto cl_to_sg    = make_clpt_to_stgr();
  auto sg_to_cl    = make_stgr_to_clpt<T>( d0s_h );
  auto cl_d0_to_sg = make_cl_d0_to_sg<T>(  d0c_l );
  auto sg_d0_to_cl = make_sg_d0_to_cl<T>(  d0s_h, d0s_l );

  // initial eta
  //
  size_t N2d[2] = { N[1], N[2] }; 
  d_arr<T, D-1> eta( N2d );
  eta = ext.eta( 0, crd_eta ); 

  // initial grid
  //
  auto crd_c = rec_grd_c.perturb_surface( eta, 0 ); // along axis-0
  auto crd_s = rec_grd_s.perturb_surface( eta, 0 ); // along axis-0
  crv_grd_c.update( crd_c );
  crv_grd_s.update( crd_s );

  // initial condition
  //
  d_arr<T, D> u0s( Ns );
  d_arr<T, D> u1(  N );
  d_arr<T, D> u2(  N );
  u0s =           ext.u0( 0, rec_grd_s.coords() );
  u1  = sg_to_cl( ext.u1( 0, rec_grd_s.coords() ) );
  u2  = 0; 

  // initial pressure
  //
  d_arr<T, D> p0( N );
  p0 = 0;
  vector<d_arr<T, D>> p( 3 );
  for( size_t t = 0; t < 3; ++t )
    p[ t ] = p0;

  // time-stacking size
  //
  // t = 0: current solution
  // t = 1 ~ M-1: history
  //
  constexpr size_t M = 2;
  vector<d_arr<T, D>> phi( M );
  for( size_t t = 0; t < M; ++t )
  {
    phi[ t ] = d_arr<T, D>( N );
    phi[ t ] = p[ 0 ];
  }

  // frac coef
  //
  using trns_ts = bdf1<T>;
  using dfus_ts = am2<T>;
  using conv_ts = ab2<T>;
  auto  alpha_0 = trns_ts().coef_u()[0];
  auto  beta_0  = dfus_ts().coef_f()[0];
 
  auto psn_hlp       = make_poisson_helper( H, 
                                            cl_to_sg,    sg_to_cl, 
                                            cl_d0_to_sg, sg_d0_to_cl, 
                                            crv_grd_c,   crv_grd_s, 
                                            d0d0c, d1c, d2c,
                                            d0d0s, d1s, d2s );
  auto lap_diag_c    = psn_hlp.make_laplace_diag_c();
  auto lap_diag_s    = psn_hlp.make_laplace_diag_s();
  auto nu_lap_diag_c = make_stencil_array( nu * lap_diag_c );
  auto nu_lap_diag_s = make_stencil_array( nu * lap_diag_s );

  auto dfus_diag_op  = [&]( size_t d, auto& u )
  {
    d_arr<R, D> U = to_freq( u );
    d_arr<R, D> Q = ( d == 0 ) ? nu_lap_diag_s( U ) : nu_lap_diag_c( U );
    d_arr<T, D> r = to_real( Q );
    return r;
  };

  auto dfus_op = [&]( auto& u )
  {
    d_arr<T, D> r = nu * psn_hlp.eval_laplace_c( u );
    return r;
  };

  auto conv_op = make_conv( u0s, u1, u2, 
                            cl_to_sg, sg_to_cl, cl_d0_to_sg, sg_d0_to_cl, 
                            crv_grd_s, crv_grd_c,
                            d1c, d2c, d1s, d2s, 
                            nu, psn_hlp );
 
  cout << "Preparing Momentum Equation - 0\n";

  auto mom_0 = make_multi_stepper( dt, trns_ts(), u0s, 
                                       dfus_ts(), [&]( auto& x ){ return dfus_diag_op( 0, x ); },
                                       conv_ts(), [&]( auto& x ){ return conv_op( 0, x ); } );
  
  cout << "Preparing Momentum Equation - 1\n";

  auto mom_1 = make_multi_stepper( dt, trns_ts(), u1,
                                       dfus_ts(), [&]( auto& x ){ return dfus_diag_op( 1, x ); },
                                       conv_ts(), [&]( auto& x ){ return conv_op( 1, x ); } );

  cout << "Preparing Momentum Equation - 2\n";

  auto mom_2 = make_multi_stepper( dt, trns_ts(), u2,
                                       dfus_ts(), [&]( auto& x ){ return dfus_diag_op( 2, x ); },
                                       conv_ts(), [&]( auto& x ){ return conv_op( 2, x ); } );

  // eta RK-solver
  //
  auto conv_eta  = [&]( auto& x ) 
  {
    auto X = to_freq( x );
    
    d_arr<T, 2> u0_surf = u0s( last(),   all(), all() );
    d_arr<T, 2> u1_surf = 0.5 * ( u1( last(),   all(), all() )
                                  + u1( last()-1, all(), all() ) );
    d_arr<T, 2> u2_surf = 0.5 * ( u2( last(),   all(), all() )
                                  + u2( last()-1, all(), all() ) );
    d_arr<R, 2> D1 = eta_dx1( X );
    d_arr<T, 2> d1 = to_real( D1 ); 

    d_arr<R, 2> D2 = eta_dx2( X );
    d_arr<T, 2> d2 = to_real( D2 ); 

    d_arr<T, 2> rhs = u0_surf - u1_surf * d1 - u2_surf * d2;
    dealias( rhs );
    return rhs;
  };
 
  auto rk = make_runge_kutta( dt, rk2<T>(1), eta );

  // project function
  //
  auto phi_dx     = d_phi_func<T>( cl_to_sg, cl_d0_to_sg, sg_d0_to_cl, 
                                   crv_grd_c, crv_grd_s, d1c, d2c );
  auto proj_dp    = proj_dp_func( dt, alpha_0, beta_0, crv_grd_c, dfus_op );

  // VTK write out - curvilin
  //
  vtk_writer_bin out_grd( prefix + "/out_grd_" );
  auto vtk = [&]( auto tn )
  {
    cout << "write VTK\n";
    out_grd.next( crd_c );
    out_grd.name( "u" )     << make_vec( u0s, u1, u2, sg_to_cl );
    out_grd.name( "p" )     << p[0];
    out_grd.name( "phi" )   << phi[0];
    out_grd.name( "J" )     << crv_grd_c.jacobian();
    //out.name( "w" )       << crv_grd_c.grid_vel();
    //out.name( "w_con" )   << crv_grd_c.grid_vel_contra();
  };

  // VTK write out - exact solution
  //
  vtk_writer_bin out_ext( prefix + "/out_ext_" );
  auto vtk_ext = [&]( auto tn )
  {
    auto eta_e = ext.eta( tn, crd_eta );
    auto crd_e = rec_grd_c.perturb_surface( eta_e, 0 ); // along axis-0
    out_ext.next( crd_e );
    crd_e = rec_grd_c.coords();
    out_ext.name( "u0" ) << ext.u0( tn, crd_e );
    out_ext.name( "u1" ) << ext.u1( tn, crd_e );
  };

  // VTK write out - debug
  //
  vtk_writer_bin vtk_dbg_c( prefix + "/out_dbg_c_" );
  vtk_writer_bin vtk_dbg_s( prefix + "/out_dbg_s_" );

  // VTK write out - eta
  //
  vtk_writer_bin out_eta( prefix + "/out_eta_" );
  auto vtk_eta = [&]( auto tn )
  {
    out_eta.next( crd_eta );
    out_eta.name( "eta" )     << eta;
    out_eta.name( "eta_ext" ) << ext.eta( tn, crd_eta );
  };

  // VTN write out - eta debug
  //
  vtk_writer_bin vtk_dbg_eta( prefix + "/out_dbg_eta_" );

  // VTK write out - t0
  //
  vtk( 0 );
  vtk_dbg_c.next( crd_c );
  vtk_dbg_s.next( crd_s );
  vtk_eta( 0 );
  vtk_ext( 0 );
  vtk_dbg_eta.next( crd_eta );

  cout << "Ready to Go!!\n";
 
  surf_coef_helper<T, 2> sf_coef;
  
  for( std::size_t i = 1; i <= it_N; ++i )
  {
    T t_nxt = T(i) * dt;
    cout << "\n\ni = " << i << "\t t = " << t_nxt << endl;

    auto rk_f = [&]( auto n, auto dtn, auto& eta_n )
    {
      auto tn = T(i) * dt + dtn;
      cout << "t_iter = " << tn << endl;

      // update eta
      //
      eta = eta_n;
      
      // update grid
      //
      crd_c = rec_grd_c.perturb_surface( eta, 0 ); // along axis-0
      crd_s = rec_grd_s.perturb_surface( eta, 0 ); // along axis-0
      crv_grd_c.update( crd_c );
      crv_grd_s.update( crd_s );
      vtk_dbg_c.next( crd_c );
      vtk_dbg_s.next( crd_s );
      vtk_dbg_eta.next( crd_eta );
      vtk_dbg_eta.name( "eta" ) << eta;

      // update possion equation grid-dependent coef
      //
      psn_hlp.update_grid_coef();

      // update surface grid-dependent coef. - eta
      //
      sf_coef.update_grd( crv_grd_s, eta, eta_dx1, eta_dx1dx1, eta_dx2, eta_dx2dx2 );

      // extra-polation for phi & p
      //
      if( n == 1 )
        extra_polate( phi );
      vtk_dbg_c.name( "phi_h" ) << phi[0];

      // prepare projection parts
      //
      d_arr<T, D> r0s( Ns );
      d_arr<T, D> r1(  N  );
      d_arr<T, D> r2(  N  ); 
      r0s = dt / alpha_0 * phi_dx( 0, phi[0] );
      r1  = dt / alpha_0 * phi_dx( 1, phi[0] );
      r2  = dt / alpha_0 * phi_dx( 2, phi[0] ); 

d_arr<T, D> uh0s, uh1, uh2;

d_arr<T, 2> u0_sf_new = u0s( last(), all(), all() );
d_arr<T, 2> u1_sf_new = 0.5 * ( u1( last(), all(), all() ) + u1( last()-1, all(), all() ) );
d_arr<T, 2> u2_sf_new = 0.5 * ( u2( last(), all(), all() ) + u2( last()-1, all(), all() ) );
d_arr<T, 2> u0_sf_old;
d_arr<T, 2> u1_sf_old;
d_arr<T, 2> u2_sf_old;

T err_sf     = std::numeric_limits<T>::max();
T err_sf_old = std::numeric_limits<T>::max();

do{ 
u0_sf_old  = u0_sf_new;
u1_sf_old  = u1_sf_new;
u2_sf_old  = u2_sf_new;
err_sf_old = err_sf;

      // project BC for u_hat
      // 
      // surface
      //
      sf_coef.update_vel( u0s, u1, u2, d1s, d2s, cl_to_sg, sg_to_cl, cl_d0_to_sg );

      d_arr<T, 2> u0_top = surf_u0_bc( sf_coef, r0s, d0c_l, sg_to_cl );
      d_arr<T, 2> u1_top = surf_u1_bc( sf_coef, r1,  d0c_l );
      d_arr<T, 2> u2_top = surf_u2_bc( sf_coef, r2,  d0c_l );

      // bottom
      //
      d_arr<T, 2> u0_btm = zeros<T, 2>( N2d );
      d_arr<T, 2> u1_btm = btm_dx0_bc( r1, d0c_l, zeros<T, 2>(N2d), crv_grd_s );
      d_arr<T, 2> u2_btm = btm_dx0_bc( r2, d0c_l, zeros<T, 2>(N2d), crv_grd_s );

      d_arr<T, 3> dbg_uh0 = zeros<T, 3>( N );
      d_arr<T, 3> dbg_uh1 = zeros<T, 3>( N );
      d_arr<T, 3> dbg_uh2 = zeros<T, 3>( N );
      dbg_uh0( first(), all(), all() ) = u0_btm;
      dbg_uh1( first(), all(), all() ) = u1_btm;
      dbg_uh2( first(), all(), all() ) = u2_btm;
      dbg_uh0( last(),  all(), all() ) = u0_top;
      dbg_uh1( last(),  all(), all() ) = u1_top;
      dbg_uh2( last(),  all(), all() ) = u2_top;
      vtk_dbg_c.name( "uh_bc" ) << make_vec( dbg_uh0, dbg_uh1, dbg_uh2, sg_to_cl );

      // u_hat solvers - SOR
      //
      d_arr<T, 3> bf0 = -phi_dx( 0, p[0] );
      d_arr<T, 3> bf1 = -phi_dx( 1, p[0] );
      d_arr<T, 3> bf2 = -phi_dx( 2, p[0] );
      auto uh0_slvr = uh_slvr_sor( 0, u0_top, u0_btm, bf0, d0s_h, nu_lap_diag_s );
      auto uh1_slvr = uh_slvr_sor( 1, u1_top, u1_btm, bf1, d0c_l, nu_lap_diag_c );
      auto uh2_slvr = uh_slvr_sor( 2, u2_top, u2_btm, bf2, d0c_l, nu_lap_diag_c );

      // solve momentum eqs. for u_hat
      //
      cout << "Solving Momemtum Equations - U_hat_0 \n";
      uh0s = mom_0.solve( uh0_slvr );
      cout << "Solving Momemtum Equations - U_hat_1 \n";
      uh1  = mom_1.solve( uh1_slvr );
      cout << "Solving Momemtum Equations - U_hat_2 \n";
      uh2  = mom_2.solve( uh2_slvr );
      
      // extra-polate U0 / U1 / U2
      //
      u0s = uh0s - r0s;
      u1  = uh1  - r1;
      u2  = uh2  - r2; 

u0_sf_new = u0s( last(), all(), all() );
u1_sf_new = 0.5 * ( u1( last(), all(), all() ) + u1( last()-1, all(), all() ) );
u2_sf_new = 0.5 * ( u2( last(), all(), all() ) + u2( last()-1, all(), all() ) );
u0_sf_old -= u0_sf_new;
u1_sf_old -= u1_sf_new;
u2_sf_old -= u2_sf_new;
T err0 = std::abs( norm2( u0_sf_old ) );
T err1 = std::abs( norm2( u1_sf_old ) );
T err2 = std::abs( norm2( u2_sf_old ) );

err_sf = ( err0 + err1 + err2 ) / 3.0;
cout << "----> Surface L2-Err. = " << err_sf << endl;

} while( err_sf > 1.e-3 * std::sqrt( T(u0_sf_new.size()) )
     &&  err_sf <= err_sf_old );

      sf_coef.dump( vtk_dbg_eta );
      vtk_dbg_c.name( "uh" )        << make_vec( uh0s, uh1, uh2, sg_to_cl );
      vtk_dbg_c.name( "u_extra" )   << make_vec( u0s,  u1,  u2,  sg_to_cl ); 
      vtk_dbg_s.name( "uh0s" )      << uh0s;
      vtk_dbg_s.name( "u0s_extra" ) << u0s;

      // divergence of u_hat
      //
      auto div_uh = eval_divergence( vtk_dbg_c, uh0s, uh1, uh2, 
                                     cl_to_sg, sg_d0_to_cl, crv_grd_c, d1c, d2c );
      vtk_dbg_c.name( "div_uh" ) << div_uh;

      // prepapre phi - lhs / rhs / bc
      //
      // top BC --> from pressure
      //
      sf_coef.update_vel( u0s, u1, u2, d1s, d2s, cl_to_sg, sg_to_cl, cl_d0_to_sg );

      // p_sf: @( n + 1 )
      d_arr<T, 2> p_sf       = surf_pressure( vtk_dbg_eta, sf_coef, mu, rho, g, gma ) / rho;
      d_arr<T, 2> p_sf_n2    = p[0]( last(), all(), all() ); // p @(n - 0.5)
      d_arr<T, 2> p_sf_n1    = p[1]( last(), all(), all() ); // p @(n - 1.5)
      d_arr<T, 2> p_sf_int   = ( 8.0 * p_sf + 10.0 * p_sf_n2 - 3.0 * p_sf_n1 ) / 15.0; // n+0.5
      d_arr<T, 2> dp_sf      = p_sf_int - p_sf_n2;
      d_arr<T, 3> phi_prj    = dt * beta_0 / alpha_0 * dfus_op( phi[0] );
      d_arr<T, 2> phi_bc_top = dp_sf + phi_prj( last(), all(), all() );

      // bottom BC --> from u0
      //
      //d_arr<T, 2> phi_bc_btm = btm_phi( crv_grd_s, uh0s, u0_btm, alpha_0, dt );
      d_arr<T, 2> phi_bc_btm = zeros<T, 2>( N2d );

      // rhs
      d_arr<T, D> phi_rhs = alpha_0 / dt * div_uh;

      auto phi_bc_dbg = d_arr<T, D>( N );
      phi_bc_dbg = 0;
      phi_bc_dbg( first(), all(), all() ) = phi_bc_btm;
      phi_bc_dbg( last(),  all(), all() ) = phi_bc_top;
      vtk_dbg_c.name( "phi_bc"  ) << phi_bc_dbg;
      vtk_dbg_c.name( "phi_rhs" ) << phi_rhs;

      d_arr<T, 2> u0_top     = u0s( last(), all(), all() );
      d_arr<T, 2> u1_top     = 0.5*( u1( last(),   all(), all() ) 
                                   + u1( last()-1, all(), all() ) );
      d_arr<T, 2> u2_top     = 0.5*( u2( last(),   all(), all() ) 
                                   + u2( last()-1, all(), all() ) );
      vtk_dbg_eta.name( "p_sf" )       << p_sf;
      vtk_dbg_eta.name( "phi_bc_top" ) << phi_bc_top;
      vtk_dbg_eta.name( "u0_extra" )   << u0_top;
      vtk_dbg_eta.name( "u1_extra" )   << u1_top;
      vtk_dbg_eta.name( "u2_extra" )   << u2_top;

      // solve PHI
      //
      cout << "Solving Laplace PHI\n";
      phi[0] = psn_hlp.solve_dn( phi_rhs, phi_bc_top, phi_bc_btm, phi[0], d0c_l );
      vtk_dbg_c.name( "phi" ) << phi[0];

      // update p & u
      //
      cout << "Updating p & u\n";
      p[0] += proj_dp( phi[0] );
      u0s   = uh0s - dt / alpha_0 * phi_dx( 0, phi[0] );
      u1    = uh1  - dt / alpha_0 * phi_dx( 1, phi[0] );
      u2    = uh2  - dt / alpha_0 * phi_dx( 2, phi[0] ); 
      vtk_dbg_c.name( "p" )   << p[0];
      vtk_dbg_c.name( "u" )   << make_vec( u0s, u1, u2, sg_to_cl ); 
      vtk_dbg_s.name( "u0s" ) << u0s;

      auto dEta = conv_eta( eta_n );
      return dEta;
    };

    eta = rk.advance( rk_f );
    vtk_eta( t_nxt );

    // roll-back
    //
    cout << "Rolling back\n";
    crv_grd_c.roll_back();
    crv_grd_s.roll_back();
    mom_0.roll_back( u0s );
    mom_1.roll_back( u1  );
    mom_2.roll_back( u2  );
    for( size_t n = M-1; n >= 1; --n )
      phi[ n ] = phi[ n-1 ];
    for( size_t n = 2;   n >= 1; --n )
      p[ n ]   = p[ n-1 ];

    vtk( t_nxt );
    vtk_ext( t_nxt );
  }
}

int main( int argc, char* argv[] )
{
  if( argc == 2 )
  {
    string fname( argv[1] );
    test<2>( "out/", fname );
  }
  else
    cout << "USAGE: ./main INPUT_FILE\n";
  return 0;
}
