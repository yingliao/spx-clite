#include <iostream>
#include <fstream>
#include <spx/spx.h>

using namespace std;
using namespace spx;

#include "helper.h"
#include "stgr_func.h"
#include "surfbtm.h"
#include "optrs.h"
#include "poisson.h"
#include "io_func.h"

