#include <iostream>
#include <type_traits>
#include <tuple>
#include <complex>
#include <vector>
#include <algorithm>

#include <spx/spx.h>

using namespace std;
using namespace spx;
using namespace spx::expr_func_impl;

template <std::size_t N>
  using CVec = static_vector<std::complex<double>, N>;
    
template <typename T>
  void print( T& t )
  {
    auto iter = t.dense_begin();
    for( ; iter != t.dense_end(); ++iter )
    {
      for( auto i : iter.index() )
        cout << i << ", ";
      cout << " --> " << *iter << endl;
    }
  }
  
void testND()
{
  using T = double;
  dynamic_array<T, 4> q{ 3, 3, 3, 3};
  size_t i = 0;
  for( auto& v : q )
    v = T(i++);
  
  dynamic_array<T, 4> k{ 3, 3, 3, 3};
  /*
  cout << "common_stride = 1 for all dimensions" << endl;
  eval( ops::assign(), k, q + q );
  print( k );
  */
  
  k = T(0);
  decltype(auto) k1 = k( slice(first(), last(), 2), slice(first(), last(), 2), slice_all(), slice_all() );
  decltype(auto) q1 = q( slice(first(), last(), 2), slice(first(), last(), 2), slice_all(), slice_all() );
  decltype(auto) expr = make_expr_func( ops::assign(), as_expr(k1), q1+500+500+q1 );
  eval( expr );
  print( k );
}

void testMultiComp()
{
  using T = double;
  using Mat22 = static_array<T, 2, 2>;
  dynamic_array<Mat22, 3> q{5, 5, 5};
  dynamic_array<double, 3> a{5, 5, 5};
  dynamic_array<double, 3> b{5, 5, 5};
  a = 1.0;
  b = 2.0;
  
  decltype(auto) el = as_expr(q)[0][0];
  decltype(auto) er = as_expr(a);
  decltype(auto) expr = make_expr_func( ops::assign(), el, er );
  eval( expr );
  
  decltype(auto) expr2 = make_expr_func( ops::assign(), as_expr(q)[1][1], as_expr(q)[0][0] * 1.5 );
  eval( expr2 );
  
  q  = a + b;
  
  auto iter = q.dense_begin();
  for( ; iter != q.dense_end(); ++iter )
  {
    for( auto i : iter.index() )
      cout << i << ", ";
    cout << " --> ";
    for( auto v : *iter )
      cout << v << "  ";
    cout << endl;
  }
  
//   decltype(auto) expr3 = q == q;
//   int count = 0;
//   auto f = [&count]( auto expr ) 
//   { 
//     bool eq = true;
//     auto g = [&eq]( bool b ) { eq &= b; };
//     eval( expr, g );    
//     if( eq ) ++count; 
//   };
//   eval( expr3, f );
//   cout << "count true = " << count << endl;
  
  bool bb = q==q;
  cout << " q==q --> " << boolalpha << bb << endl;
  
  dynamic_array<double, 3> r = a + b;
  print( r );
}

int main()
{
  //testND();
  testMultiComp();
  
//   dynamic_array<double, 2> a{ 10, 5 };
//   int i = 0;
//   for( auto& v : a )
//     v = double(++i);
//   
//   using Vec2 = static_vector<int, 2>;
//   decltype(auto) q = a( slice(first(), last(), 3), slice(first(), last(), 2) );
//   print( q );
//   decltype(auto) it = q.dense_begin() + std::vector<int>{1,2};
//   cout << "CHECK = " << *it << endl;
//   
//   using T = decltype( q.dense_begin() );
//   cout << boolalpha << Binary_expressible<T, Vec2>() << endl;
//   cout << boolalpha << Binary_expressible_F<ops::plus, T, Vec2>() << endl;
}