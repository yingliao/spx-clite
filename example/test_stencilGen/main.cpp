#include <iostream>
#include <type_traits>
#include <tuple>
#include <complex>
#include <vector>
#include <algorithm>

#include <spx/spx.h>

using namespace std;
using namespace spx;

template <Stencil S>
  void print( const S& stencil )
  {
    for( auto& kv : stencil ) 
    {
      cout << "[";
      for ( auto k : kv.first ) 
        cout << k << ", ";
      cout << "] --> " << kv.second << endl;
    }
  }
/*
template <StencilGenerator T>
  void print( const T& sg )
  {
    for( int i = 0; i < 10; ++i )
    {
      for( int j = 0; j < 10; ++j )
      {
        cout << "Index = " << i << ", " << j << endl;
        decltype(auto) stencil = sg.stencil_at( {i, j} );
        print( stencil );
        cout << endl;
      }
    }
  }
*/

int main()
{
  using SG = stencil_gen2;
  SG sgen( {10, 10} );
  stencil_array<SG> sg( std::move(sgen) );

  dynamic_array<typename SG::weight_type, 2> coef(10, 10);
  coef = 10;

  decltype(auto) expr = coef * sg;
  // decltype(auto) st = expr[ s_arr.descriptor()(5,5) ];

  decltype(auto) st = expr( vector<int>{5, 5} );
  // decltype(auto) st = s_arr( 5, 5 );
  print( *st );

  // using SG = stencil_gen;
  // SG sg(10, 10);

  // cout << boolalpha << StencilGenerator<SG>() << endl;
  // print(sg);

  using A = c_static_array<double, 10, 10>;
  // A coef = {2};

  // cout << boolalpha << Binary_expressible<A, SG, ops::plus>() << endl;
  // cout << boolalpha << Binary_expressible<A, A, ops::plus>() << endl;
  // cout << boolalpha << Binary_expressible<double, A, ops::plus>() << endl;
  // cout << boolalpha << Binary_expressible<double, double, ops::plus>() << endl;
  // cout << boolalpha << Binary_expressible<SG, SG, ops::plus>() << endl;
  // cout << boolalpha << Binary_expressible<SG, double, ops::plus>() << endl;

  decltype(auto) sg2 = (-sg);
  cout << typestr<decltype(sg2)>() << endl;
  //cout << boolalpha << StencilGenerator<Main_type<decltype(sg2)>>() << endl;
  decltype(auto) stn1 = sg2( vector<int>{2, 2} );
  print(*stn1);

  cout << " ========= " << endl;

  decltype(auto) sg3 = sg(sg(sg)); 
  decltype(auto) stn2 = sg3( vector<int>{3, 3} );
  print(*stn2);

  cout << " ========= " << endl;
  decltype(auto) sg4 = (sg + sg)(sg + sg); // -( coef* (sg + sg));
  decltype(auto) stn3 = sg4( vector<int>{4, 4} );
  //cout << typestr<decltype(sg4)>() << endl;
  print(*stn3);

  cout << " ========= " << endl;
  decltype(auto) v = (sg + sg)( coef );
  d_array<decltype(*v), 2> r = v;
  for( auto& x : r )
    cout << x << endl;

  cout << " ========= " << endl;
  decltype(auto) sg5 = sg + sg;
  decltype(auto) sgen2 = make_decorated_stencil_gen(sg5, [](){ return 1; } );
  stencil_array<decltype(sgen2)> sg6( std::move(sgen2) );
  cout << "composite" << endl;
  print( sg6(4, 4) );
  cout << "decorated" << endl;
  print( sg6(0, 0) );
  
  //cout << boolalpha << Stencil_generator<stencil_array<SG>>() << endl;
  return 0;
}

