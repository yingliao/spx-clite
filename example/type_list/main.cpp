#include <iostream>
#include <type_traits>
#include <tuple>
#include <complex>
#include <vector>
#include <algorithm>

#include <spx/spx.h>

using namespace std;
using namespace spx;

template <typename T, typename... Args>
requires Type_inside<T, Args...>()
  void foo() 
  {}

template <template <typename> class Tr, typename T>
requires Valid<Tr, T>()
  void foo1()
  {}
  
template <std::size_t I>
requires (I==1)
void test()
{
  cout << boolalpha << Type_inside<bool, bool>() << endl; // true
  cout << boolalpha << Type_inside<bool, float, double>() << endl; // false
  cout << boolalpha << Type_inside<bool, float, double, bool>() << endl; // true
  foo<bool, bool>(); // OK
  //foo<bool, float, double>(); // fail
  foo<bool, float, double, bool>(); // OK
  
  cout << boolalpha << Valid<std::is_floating_point, double>() << endl;
  foo1<std::is_floating_point, double>();

  cout << Count_binary_valid<is_same, sub_to_first<bool>, bool>() << endl;
  cout << Count_binary_valid<is_same, bool, bool>() << endl;
  
  cout << boolalpha << Expandable<tuple<>>() << endl;
  cout << boolalpha << Expandable<bool>() << endl;

  cout << Expandable_size<tuple<bool>>() << endl;
  cout << Expandable_size<tuple<>>() << endl;
  
  using T1 = tuple_append_type<tuple<vector<bool>>, vector<double>>;
  cout << typestr<T1>() << endl;
  //using T2 = tuple_append_type<tuple<bool>, tuple<tuple<double>>>;
  //cout << typestr<T2>() << endl;
  
  /*
  using T3 = repeat_type_as_tuple<bool, 1>;
  cout << typestr<T3>() << endl;
  
  using T4 = replace_type<double, 2, tuple<bool, bool, bool, bool>>;
  cout << typestr<T4>() << endl;
  
  using T5 = remove_type<1, tuple<bool, double, bool>>;
  cout << typestr<T5>() << endl;
  
  cout << Index_of_types<bool, bool>() << endl;
  cout << Index_of_types<bool, double, double, bool, float, float>() << endl;
  cout << Index_of_types<bool, tuple<double, double, bool, float, float>>() << endl;
  
  decltype(auto) t1 = to_tuple<3>( vector<int>{1,2,3} );
  vector<int> v{1,2,3};
  decltype(auto) t2 = to_tuple<3>( v );
  cout << typestr<decltype(t1)>() << endl;
  cout << typestr<decltype(t2)>() << endl;
  
  auto f = []( int, int, double ) -> float { return 1.2; };
  decltype(auto) t3 = tuple_unpack_and_subst( f, make_tuple( 1, 2, 3.3 ) );
  cout << typestr<decltype(t3)>() << endl;
  cout << t3 << endl;
  
  int i = 1;
  double d = 1.3;
  const int& ir = i;
  decltype(auto) t4 = make_tuple( ir, 100, d );
  cout << typestr<decltype(t4)>() << endl;
  
  decltype(auto) t5 = tuple_append( t4, "fd" );
  cout << typestr<decltype(t5)>() << endl;
  
  decltype(auto) t6 = tuple_slot_type<int, 5, 2>( 2.4 ); 
  cout << typestr<decltype(t6)>() << endl;
  */
}

template <std::size_t I>
requires (I==2)
void test()
{
  using T = double;
  using S = static_vector<T, 3>;
  
  S s1 = { 1, 2, 3 };
  S s2 = { 4, 5, 6 };
  S s3 = { 7, 8, 9 };

  auto r = 
  varargs_trans( []( auto&&... x ) { return S{ x... }; },
                 []( auto&& x, auto id ) { return x[id]; },
                 s1, s2, s3 );

  for( auto v : r )
    cout << v << endl;
}

int main()
{
  test<2>();
  return 0;
}
