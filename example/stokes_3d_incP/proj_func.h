// -----------------------  Project Functions  -------------------------------------------

template <typename T,  typename F0, typename F1, typename G0, typename G1, 
            typename O0, typename O1, typename O2, typename O3, typename O4>
  decltype(auto) d_phi_func(  T dt, T alpha_0,
                              F0&& cl_to_sg,  F1&& cl_d0_to_sg,
                              G0&& crv_grd_c, G1&& crv_grd_s, 
                              O0&& dksi0_c_l, O1&& dksi0_c_h, O2&& dksi0_s_l, 
                              O3&& dksi1,     O4&& dksi2 )
  {
    return [&]( std::size_t i, auto& phi )
    {
      constexpr auto D = phi.rank();
      using A = Main_type<decltype(phi)>;
      using R = complex<T>;

      decltype(auto) dksi_dx_c = crv_grd_c.b_contra();
      decltype(auto) dksi_dx_s = crv_grd_s.b_contra();

      A t;

      if( i == 0 ) // u: stgr
      {
        d_arr<T, D> phi_d0s = cl_d0_to_sg( phi ); 
        t = dksi_dx_s[0][0] * phi_d0s;
      }
      else if( i == 1 )
      {
        d_arr<T, D> phi_s  = cl_to_sg( phi );
        d_arr<R, D> PHI_d1 = dksi1( to_freq( phi ) );
        d_arr<T, D> phi_d0 = dksi0_s_l( phi_s );
        d_arr<T, D> phi_d1 = to_real( PHI_d1 );
        t = dksi_dx_c[0][1] * phi_d0( slice(first(), last()-1), all(), all() )
          + dksi_dx_c[1][1] * phi_d1;
      }
      else if( i == 2 )
      {
        d_arr<T, D> phi_s  = cl_to_sg( phi );
        d_arr<R, D> PHI_d2 = dksi2( to_freq( phi ) );
        d_arr<T, D> phi_d0 = dksi0_s_l( phi_s );
        d_arr<T, D> phi_d2 = to_real( PHI_d2 );
        t = dksi_dx_c[0][2] * phi_d0( slice(first(), last()-1), all(), all() )
          + dksi_dx_c[2][2] * phi_d2;
      }

      return t;
    };
  }

template <typename T, typename G, typename O>
  decltype(auto) proj_dp_func( T dt, T alpha_0, T beta_0, G&& crv_grd_c, O&& dfus_op )
  {
    return [&]( auto& phi )
    {
      using A = Main_type<decltype(phi)>;
      A dp = phi - dt * beta_0 / alpha_0 * dfus_op( phi );
      return dp;
    };
  }


