from vtk import *
from vtk.util import numpy_support as vtknp
import matplotlib.pyplot as plt
from numpy import *

def u0_ext( t, x, z ):
  return  -cos(x) * sin(z) * exp(-2*t)

def u2_ext( t, x, z ):
  return   sin(x) * cos(z) * exp(-2*t)

class vtkProc():
  def __init__( self, dt, prefix, stride = 1 ):
    self.dt     = dt
    self.prefix = prefix
    self.t0     = int( 0.24 / dt )
    self.t1     = self.t0 + 1
    f0 = prefix + "/out_grd_%04d.vtk" % self.t0
    f1 = prefix + "/out_grd_%04d.vtk" % self.t1

    print f0
    print f1

    r0 = vtkStructuredGridReader()
    r0.SetFileName( f0 )
    r0.ReadAllVectorsOn()
    r0.ReadAllScalarsOn()
    r0.Update()
    o0 = r0.GetOutput()

    r1 = vtkStructuredGridReader()
    r1.SetFileName( f1 )
    r1.ReadAllVectorsOn()
    r1.ReadAllScalarsOn()
    r1.Update()
    o1 = r1.GetOutput()

    self.shp = o0.GetDimensions()
    print "shape = ", self.shp

    u_vtk_0 = o0.GetPointData().GetArray("u")
    u_raw_0 = vtknp.vtk_to_numpy( u_vtk_0 )
    u0_0 = u_raw_0[:, 0].reshape( self.shp[::-1] )[:, 4, :][::stride, :]
    u1_0 = u_raw_0[:, 1].reshape( self.shp[::-1] )[:, 4, :][::stride, :]
    u2_0 = u_raw_0[:, 2].reshape( self.shp[::-1] )[:, 4, :][::stride, :]

    u_vtk_1 = o1.GetPointData().GetArray("u")
    u_raw_1 = vtknp.vtk_to_numpy( u_vtk_1 )
    u0_1 = u_raw_1[:, 0].reshape( self.shp[::-1] )[:, 4, :][::stride, :]
    u1_1 = u_raw_1[:, 1].reshape( self.shp[::-1] )[:, 4, :][::stride, :]
    u2_1 = u_raw_1[:, 2].reshape( self.shp[::-1] )[:, 4, :][::stride, :]

    w1 = (0.24 % self.dt) / self.dt
    w0 = 1.0 - w1
    print self.t0*dt, w0
    print self.t1*dt, w1
    self.u0 = w0 * u0_0 + w1 * u0_1
    self.u1 = w0 * u1_0 + w1 * u1_1
    self.u2 = w0 * u2_0 + w1 * u2_1


vtk1 = vtkProc( 0.0140250, "case_s2/out_1", 4 )
vtk2 = vtkProc( 0.0218166, "case_s2/out_2", 2 )
vtk3 = vtkProc( 0.0302076, "case_s2/out_3", 1 )

u0_1 = vtk1.u0
u1_1 = vtk1.u1
u2_1 = vtk1.u2

u0_2 = vtk2.u0
u1_2 = vtk2.u1
u2_2 = vtk2.u2

u0_3 = vtk3.u0
u1_3 = vtk3.u1
u2_3 = vtk3.u2


# convergence rate
m_u0 = -log( (abs(u0_2 - u0_1)).max() / (abs(u0_3 - u0_2)).max() ) / log(2)
m_u2 = -log( (abs(u2_2 - u2_1)).max() / (abs(u2_3 - u2_2)).max() ) / log(2)
print "u0, m = %f" % m_u0
print "u2, m = %f" % m_u2


shp1 = vtk1.shp
shp2 = vtk2.shp
shp3 = vtk3.shp

x0_1 = linspace( 0, 2*pi, shp1[0] )
x2_1 = linspace( 0, 2*pi, shp1[2] )[::4]

x0_2 = linspace( 0, 2*pi, shp2[0] )
x2_2 = linspace( 0, 2*pi, shp2[2] )[::2]

x0_3 = linspace( 0, 2*pi, shp3[0] )
x2_3 = linspace( 0, 2*pi, shp3[2] )

x_1, z_1 = meshgrid( x0_1, x2_1 )
x_2, z_2 = meshgrid( x0_2, x2_2 )
x_3, z_3 = meshgrid( x0_3, x2_3 )

u0e_1 = u0_ext( 0.238425, x_1, z_1 )
u2e_1 = u2_ext( 0.238425, x_1, z_1 )

plt.figure()
plt.hold(True)
plt.quiver( x_1, z_1, u0_1, u2_1, color="r", alpha=0.5 )
plt.quiver( x_2, z_2, u0_2, u2_2, color="g", alpha=0.5 )
plt.quiver( x_3, z_3, u0_3, u2_3, color="b", alpha=0.5 )
plt.hold(False)

plt.figure()
plt.suptitle( "u0" )

plt.subplot(121)
plt.title( "Err: grd_1 vs grd_2" )
plt.imshow( u0_2 - u0_1 )
plt.colorbar()

plt.subplot(122)
plt.title( "Err: grd_2 vs grd_3" )
plt.imshow( u0_3 - u0_2 )
plt.colorbar()

plt.figure()
plt.suptitle( "u2" )

plt.subplot(121)
plt.title( "Err: grd_1 vs grd_2" )
plt.imshow( u2_2 - u2_1 )
plt.colorbar()

plt.subplot(122)
plt.title( "Err: grd_2 vs grd_3" )
plt.imshow( u2_3 - u2_2 )
plt.colorbar()


plt.show()


