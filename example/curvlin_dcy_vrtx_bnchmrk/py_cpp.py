def plot( x ):
  import matplotlib.pyplot as plt
  print x
  plt.imshow( x );
  plt.show( block = True )
  return x

if __name__ == "__main__":
  import numpy as np
  x = np.array( [[1,2,3,4], [5,6,7,8], [9,10,11,12], [13,14,15,16]] )
  plot(x)

