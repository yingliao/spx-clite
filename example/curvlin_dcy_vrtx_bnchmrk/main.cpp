#include <iostream>
#include <fstream>
#include <future>
#include <spx/spx.h>

#include "curvlin_ops.h"

using namespace std;
using namespace spx;

#include "py_funcs.h"

template <Vector V>
  void extra_polate( V& v )
  {
    if( v.size() == 1 )
      v[0] = 0;
    else if( v.size() == 2 )
      v[0] = v[1];
    else if( v.size() == 3 )
      v[0] = 2.0 * v[1] - v[2];
    else if( v.size() == 4 )
      v[0] = 3.0 * v[1] - 3.0 * v[2] + v[3];
  }

template <Dense_array A>
  decltype(auto) to_freq( const A& u )
  {
    auto U = fft( u, 1 );
         U = fft( U, 2 );
    return U;
  }

template <Dense_array A>
  decltype(auto) to_real( const A& U )
  {
    using R = Value_type<A>; // complex type
    using T = Value_type<R>;
    constexpr auto D = U.rank();

    auto iU = ifft(  U, 1 );
         iU = ifft( iU, 2 );
    d_arr<T, D> u = real( iU );
    return u;
  }

template <Dense_array A, typename G>
  decltype(auto) to_contra_vel( G&& crv_grd, const A& u0, const A& u1, const A& u2 )
  {
    constexpr auto D = crv_grd.rank();
    using T = Value_type<A>;
    using S = static_vector<T, D>;

    decltype(auto) dksi_dx = crv_grd.b_contra();
    d_arr<S, D> u_con( u0.extents() );
    for( std::size_t k = 0; k < D; ++k )
    {
      d_arr<T, D> uk = u0 * dksi_dx[k][0] 
                     + u1 * dksi_dx[k][1] 
                     + u2 * dksi_dx[k][2];
      u_con[k] = uk;
    }
    return u_con;
  }

template <Dense_array A, typename G>
  decltype(auto) to_contra_vel( size_t k, G&& crv_grd, const A& u0, const A& u1, const A& u2 )
  {
    constexpr auto D = crv_grd.rank();
    using T = Value_type<A>;

    decltype(auto) dksi_dx = crv_grd.b_contra();
    d_arr<T, D> uk = u0 * dksi_dx[k][0] 
                   + u1 * dksi_dx[k][1] 
                   + u2 * dksi_dx[k][2];
    return uk;
  }


template <typename B, Dense_array A, typename G, Stencil_array O0, Stencil_array O1, Stencil_array O2>
  decltype(auto) eval_divergence( B& vtk, const A& u_con, G&& crv_grd, O0&& dksi0, O1&& dksi1, O2&& dksi2 )
  {
    constexpr auto D = u_con.rank();
    using V = Value_type<A>;
    using T = Value_type<V>;

    decltype(auto) j = crv_grd.jacobian();

    //d_arr<T, D> u0   = u_con[0];
    //d_arr<T, D> u0_s = 0.5 * ( u0( slice( first()+1, last() ), all(), all() )
    //                         + u0( slice( first(), last()-1 ), all(), all() ) );
    //d_arr<T, D> j_s  = 0.5 * ( j(  slice( first()+1, last() ), all(), all() )
    //                         + j(  slice( first(), last()-1 ), all(), all() ) );
    //
    //auto u0d0 = zeros<T, D>( u0.extents() );
    //u0d0( slice( first()+1, last() ), all(), all() ) = dksi0( j_s * u0_s );
    //u0d0( first(), all(), all() ) = u0d0( first()+1, all(), all() );
    //u0d0( last(),  all(), all() ) = u0d0( last()-1,  all(), all() );
    //u0d0 /= j;

    d_arr<T, D> u0d0 = dksi0( j * u_con[0] ) / j;
    d_arr<T, D> u1d1 = dksi1( j * u_con[1] ) / j;
    d_arr<T, D> u2d2 = dksi2( j * u_con[2] ) / j;
    d_arr<T, D> div = u0d0 + u1d1 + u2d2; 

    vtk.name( "uh0_con_d0" ) << to_real( u0d0 );
    vtk.name( "uh1_con_d1" ) << to_real( u1d1 );
    vtk.name( "uh2_con_d2" ) << to_real( u2d2 );

    //d_arr<T, D> div = dksi0( j * u_con[0] ) 
    //                + dksi1( j * u_con[1] ) 
    //                + dksi2( j * u_con[2] );
    //div /= j;
    return div;
  }

template <Dense_array X, Dense_array A, typename G, Stencil_array O0, Stencil_array O1, Stencil_array O2>
  decltype(auto) eval_conv( const X& U,const A& u_con, G&& crv_grd, O0&& dksi0, O1&& dksi1, O2&& dksi2 )
  {
    constexpr auto D = u_con.rank();
    using V = Value_type<A>;
    using T = Value_type<V>;
    using R = Value_type<X>;

    decltype(auto) j = crv_grd.jacobian();
    auto u = to_real( U );

    d_arr<T, D> cv0 = dksi0( j * u_con[0] * u ); 
    
    d_arr<T, D> u1  = j * u_con[1] * u;
    d_arr<R, D> U1  = to_freq( u1 );
    d_arr<R, D> cv1 = dksi1( U1 );
 
    d_arr<T, D> u2  = j * u_con[2] * u;
    d_arr<R, D> U2  = to_freq( u2 );
    d_arr<R, D> cv2 = dksi2( U2 );
    
    d_arr<R, D> cnv = to_freq( cv0 ) + cv1 + cv2;
    return cnv;
  }


// -----------------------  Fractional N-S  -------------------------------------------

template <Dense_array A, typename G, Stencil_array O0, Stencil_array O1, Stencil_array O2>
  decltype(auto) solve_poisson( const A& lap_phi, const A& phi, G&& crv_grd, O0&& dksi0, O1&& dksi1, O2&& dksi2 )
  {
    constexpr auto D = lap_phi.rank();
    using R = Value_type<A>;
    using T = Value_type<G>;

    // Numerical Solving
    //
    auto lap = crv_grd.make_laplace( dksi0, dksi1, dksi2 );

    auto on_node = make_on_node<R>( phi.descriptor() );
    auto lhs = make_stencil_array(
        []( auto& desc, auto idx, auto it_lap, auto it_drch )
        {
          if( idx[0] == desc.ubound(0) || idx[0] == desc.lbound(0)) // 0-dir upper/lower surface
          //if( idx[0] == desc.ubound(0) ) // 0-dir upper surface
          {
            return *it_drch;
          }
          else 
          {
            return *it_lap;  
          }
        },
        lap, 
        on_node );
   
    d_arr<R, D> rhs = lap_phi;
                rhs(  last(), all(), all() ) = phi(  last(), all(), all() ); // upper surface
                rhs( first(), all(), all() ) = phi( first(), all(), all() ); // lower surface

    d_arr<R, D> phi_n( phi.extents() );
    d_arr<R, D> tmp_n( phi.extents() );
    phi_n = 0;
    tmp_n = phi_n;

    //lsor( lhs, rhs, phi_n, 0, 1.0 );
    lsor_async( lhs, rhs, phi_n, 0, 1.0 );
    T err = std::abs( norm2( tmp_n -= phi_n ) ); 
    cout << "[Iter = 0] Err. L2-Norm = " << err << endl;
    return phi_n;
  }

template <typename F, typename A, typename T>
  decltype(auto) uh_slvr_sor( F&& bc, const A& uh_bc, T tol )
  {
    return [&]( auto& rhs, auto a0, auto& ut, auto beta_0, auto&& op )
    {
      using R = Value_type<A>;
      A u_cp = ut;
      
      auto on_node = make_on_node<R>( u_cp.descriptor() );
      auto lhs = make_stencil_array( a0 * on_node - beta_0 * op );
      auto prb = make_stencil_array(
          []( auto& desc, auto idx, auto it_lhs, auto it_drch )
          {
            if( idx[0] == desc.lbound(0) || idx[0] == desc.ubound(0) )
              return *it_drch;
            else
              return *it_lhs;
          },
          lhs, on_node );

      bc( rhs, uh_bc );
      
      //lsor( prb, rhs, ut, 0, 1.0 );
      lsor_async( prb, rhs, ut, 0, 1.0 );
      T err = std::abs( norm2( u_cp-=ut ) );
      u_cp = ut;
      cout << "[Iter = 0] Err. L2-Norm = " << err << endl;
      return u_cp;
    };
  }

// -----------------------  Project Functions  -------------------------------------------

  template <typename T, typename G, typename O0, typename O1, typename O2>
  decltype(auto) proj_u_func( T dt, T alpha_0, G&& crv_grd, O0&& dksi0, O1&& dksi1, O2&& dksi2 )
  {
    return [&]( std::size_t i, auto& u, auto& phi, bool frwd )
    {
      using A = Main_type<decltype(u)>;
      decltype(auto) dksi_dx = crv_grd.b_contra();
      decltype(auto) j       = crv_grd.jacobian();

      auto grad = dksi0( j * dksi_dx[0][i] * phi )
                + dksi1( j * dksi_dx[1][i] * phi )
                + dksi2( j * dksi_dx[2][i] * phi );

      T s = frwd ? T(-1) : T(1);
      A r = u + s * dt / alpha_0 * grad / j;
      return r;
    };
  }

template <typename T, typename G, typename O0, typename O1, typename O2>
  decltype(auto) proj_u_con_func( T dt, T alpha_0, G&& crv_grd, O0&& dksi0, O1&& dksi1, O2&& dksi2 )
  {
    return [&]( std::size_t q, auto& u_con, auto& phi, bool frwd )
    {
      constexpr auto D = u_con.rank();
      using A = Main_type<decltype(u_con)>;
      using R = Value_type<Value_type<A>>;

      decltype(auto) g = crv_grd.g_contra();
      T s = frwd ? T(-1) : T(1);

      d_arr<R, D> t = g[q][0] * dksi0( phi )
                    + g[q][1] * dksi1( phi )
                    + g[q][2] * dksi2( phi );

      d_arr<R, D> r = u_con[q] + s * dt / alpha_0 * t;
      return r;
    };
  }

template <typename T, typename G, typename O>
  decltype(auto) proj_p_func( T dt, T alpha_0, T beta_0, G&& crv_grd, O&& dfus_op )
  {
    return [&]( auto& phi )
    {
      using A = Main_type<decltype(phi)>;
      decltype(auto) j = crv_grd.jacobian();
      A p = phi - dt * beta_0 / alpha_0 / j * dfus_op( phi );
      return p;
    };
  }

template <typename T, typename G, typename O>
  decltype(auto) proj_phi_func( T dt, T alpha_0, T beta_0, G&& crv_grd, O&& dfus_op )
  {
    return [&]( auto& p, auto& phi_h )
    {
      using A = Main_type<decltype(p)>;
      decltype(auto) j = crv_grd.jacobian();
      A phi = p + dt * beta_0 / alpha_0 / j * dfus_op( phi_h );
      return phi;
    };
  }

// -----------------------  Exact Solution -------------------------------------------

template <typename T>
  decltype(auto) u0_ext_func( T nu )
  {
    return [&]( auto t, auto& x ) {
      constexpr auto D = x.rank();
      d_arr<T, D> u = -cos( x[2] ) * sin( x[0] ) * std::exp( T(-2) * nu * t );
      return u;
    };
  }

template <typename T>
  decltype(auto) u1_ext_func( T nu )
  {
    return [&]( auto t, auto& x ) {
      constexpr auto D = x.rank();
      auto u = make_const_array<D>( T(0), x.extents() );
      return u;
    };
  }

template <typename T>
  decltype(auto) u2_ext_func( T nu )
  {
    return [&]( auto t, auto& x ) {
      constexpr auto D = x.rank();
      d_arr<T, D> u = sin( x[2] ) * cos( x[0] ) * std::exp( T(-2) * nu * t );
      return u;
    };
  }

template <typename T>
  decltype(auto) p_ext_func( T rho, T nu )
  {
    return [&]( auto t, auto& x ) {
      constexpr auto D = x.rank();
      d_arr<T, D> v = cos( T(2) * x[0] ) + cos( T(2) * x[2] );
      d_arr<T, D> p = rho / T(4) * v * std::exp( T(-4) * nu * t );
      return p;
    };
  }

template <typename A>
  decltype(auto) stgr_0( A& a )
  {
    A r( a.extents() );
    r( first(), all(), all() ) = 0.5 * ( a( first(), all(), all() ) 
                                                   + a( first()+1, all(), all() ) );
    r( last(),  all(), all() ) = 0.5 * ( a( last(),  all(), all() ) 
                                                   + a( last()-1,  all(), all() ) );
    r( slice(first()+1, last()-1), all(), all() ) = 
        0.25 * a( slice(first(),   last()-2), all(), all() )
      + 0.5  * a( slice(first()+1, last()-1), all(), all() )
      + 0.25 * a( slice(first()+2, last()),   all(), all() );
    return r;
  }


template <size_t I>
requires (I == 2)
//void test( string prefix, double dt, size_t N0 = 51 )
void test( string prefix, size_t N0 )
{
  constexpr std::size_t D = 3;
  
  using T = double;
  using R = std::complex<T>;
  using S = static_vector<T, D>;

  T         H = pi_2<T>;
  size_t N[D] = { N0, 32/4, 64 };
  T      L[D] = {  H,  pi<T>/4.0,  pi_2<T> };
  T   xmin[D] = { -H,  0,  0 };
  T     dx[D] = { L[0]/T(N[0]-1), L[1]/T(N[1]), L[2]/T(N[2]) };
 
  T       cfl = 0.5;
  T        dt = cfl / (1/dx[0] + 1/dx[2]);
  size_t it_N = 0.24 / dt + 1;

  // phsical params
  //
  T nu  = 1;
  T rho = 1;

  // PBC domain
  //
  auto x1 = linspace( xmin[1], xmin[1]+L[1]-dx[1], N[1] );
  auto x2 = linspace( xmin[2], xmin[2]+L[2]-dx[2], N[2] );

  auto grd_gen = [&]( auto t )
  {
    //T p = 1 - 0.9 * std::cos( 2.0 * t * pi_2<T> );
    //auto g   = two_sided_vinokur<T>{ p, p };
    //auto x0  = g( xmin[0], xmin[0]+L[0], N[0] );
    auto x0  = linspace( xmin[0], xmin[0]+L[0], N[0] );
    d_arr<S, D> crd = meshgrid( x0, x1, x2 );
    return crd;
  };

  // curvlinear grid
  //
  auto ksi0 = linspace( xmin[0], xmin[0]+L[0],       N[0] );
  auto ksi1 = linspace( xmin[1], xmin[1]+L[1]-dx[1], N[1] ); 
  auto ksi2 = linspace( xmin[2], xmin[2]+L[2]-dx[2], N[2] );

  //auto crvgrd_b0 = make_chebyshev_basis( N[0] );
  auto crvgrd_b0 = make_fd_basis( ksi0, 1, 4 ); // 1-st order diff & 4-th order accuracy
  auto crvgrd_b1 = make_fd_basis( ksi1, 1, 4 ); // 1-st order diff & 4-th order accuracy
  auto crvgrd_b2 = make_fd_basis( ksi2, 1, 4 ); // 1-st order diff & 4-th order accuracy
  
  auto crvgrd_dksi0 = make_stencil_array<D>( N, crvgrd_b0, 0 );
  auto crvgrd_dksi1 = make_stencil_array<D>( N, crvgrd_b1, 1 );
  auto crvgrd_dksi2 = make_stencil_array<D>( N, crvgrd_b2, 2 );
 
  auto crv_grd = make_curvilin_dyn_grid( dt, crvgrd_dksi0, crvgrd_dksi1, crvgrd_dksi2 );
  
  // curvilinear operators
  //
  auto ksi_b0_o1 = make_fd_basis( ksi0, 1, 4 );
  auto ksi_b1_o1 = make_fourier_basis( N[1], dx[1] / L[1] * pi_2<T>, 1 );
  auto ksi_b2_o1 = make_fourier_basis( N[2], dx[2] / L[2] * pi_2<T>, 1 );
 
  auto dksi0 = make_stencil_array<D>( N, ksi_b0_o1, 0 );
  auto dksi1 = make_stencil_array<D>( N, ksi_b1_o1, 1 );
  auto dksi2 = make_stencil_array<D>( N, ksi_b2_o1, 2 );
  
  d_vec<T> ksi0_s = 0.5 * ( ksi0( slice( 1, last() ) ) + ksi0( slice( 0, last()-1 ) ) );
  auto ksi_b0_s   = make_fd_basis( ksi0_s, 1, 1 );
  auto dksi0_s    = make_stencil_array<D>( {N[0]-1, N[1], N[2]}, ksi_b0_s, 0 );

  // initial grid
  //
  auto crd0 = grd_gen( 0 ); 
  crv_grd.update( crd0 );

  // exact solution as IC & BC
  //
  auto p_ext  = p_ext_func( rho, nu );
  auto u0_ext = u0_ext_func( nu );
  auto u1_ext = u1_ext_func( nu );
  auto u2_ext = u2_ext_func( nu );

  // initial condition
  //
  d_arr<T, D> u0 = u0_ext( 0, crd0 );
  d_arr<T, D> u1 = u1_ext( 0, crd0 );
  d_arr<T, D> u2 = u2_ext( 0, crd0 );

  auto U0 = to_freq( u0 );
  auto U1 = to_freq( u1 );
  auto U2 = to_freq( u2 );

  auto u_con = to_contra_vel( crv_grd, u0, u1, u2 );
  auto U_CON = to_contra_vel( crv_grd, U0, U1, U2 );

  // initial pressure
  //
  using A = d_arr<R, D>;
  A P0 = to_freq( p_ext( 0, crd0 ) );
  vector<A> P( 3 );
  for( size_t t = 0; t < 3; ++t )
    P[ t ] = P0;

  // time-stacking size
  //
  // t = 0: current solution
  // t = 1 ~ M-1: history
  //
  constexpr size_t M = 2;
  vector<A> PHI( M );
  for( size_t t = 0; t < M; ++t )
  {
    PHI[ t ] = A( N );
    PHI[ t ] = P[ 0 ];
  }

  // frac coef
  //
  using trns_ts = bdf1<T>;
  using dfus_ts = am2<T>;
  using conv_ts = ab2<T>;
  auto  alpha_0 = trns_ts().coef_u()[0];
  auto  beta_0  = dfus_ts().coef_f()[0];
 
  auto lap_j    = crv_grd.make_laplace_j( dksi0, dksi1, dksi2 );
  auto dfus_op  = make_stencil_array( nu * lap_j ); 

  auto conv_op  = [&]( auto& x ) 
  {
    using V    = Value_type<decltype(x)>; 
    auto w_con = crv_grd.grid_vel_contra();
    auto cnv_u = eval_conv( x, u_con, crv_grd, dksi0, dksi1, dksi2 );
    auto cnv_w = eval_conv( x, w_con, crv_grd, dksi0, dksi1, dksi2 );
    d_arr<V, D> r = -( cnv_u - cnv_w );
    return r;
  };
  
  cout << "Preparing Momentum Equation - 0\n";

  auto mom_0 = make_multi_stepper_transcoef( dt, trns_ts(), U0, crv_grd.jacobian(),
                                                 dfus_ts(), dfus_op,
                                                 conv_ts(), conv_op );

  cout << "Preparing Momentum Equation - 1\n";

  auto mom_1 = make_multi_stepper_transcoef( dt, trns_ts(), U1, crv_grd.jacobian(),
                                                 dfus_ts(), dfus_op,
                                                 conv_ts(), conv_op );

  cout << "Preparing Momentum Equation - 2\n";

  auto mom_2 = make_multi_stepper_transcoef( dt, trns_ts(), U2, crv_grd.jacobian(),
                                                 dfus_ts(), dfus_op,
                                                 conv_ts(), conv_op );

  // project function
  //
  auto proj_u     = proj_u_func( dt, alpha_0, crv_grd, dksi0, dksi1, dksi2 );
  auto proj_u_con = proj_u_con_func( dt, alpha_0, crv_grd, dksi0, dksi1, dksi2 );
  auto proj_phi   = proj_phi_func( dt, alpha_0, beta_0, crv_grd, dfus_op );
  auto proj_p     = proj_p_func(   dt, alpha_0, beta_0, crv_grd, dfus_op );

  // BC function
  //
  auto bc = []( auto& a, auto& b )
  {
    // drch-BC on upper / lower
    a( first(), all(), all() ) = b( first(), all(), all() );
    a( last(),  all(), all() ) = b( last(),  all(), all() );
  };

  // VTK write out
  //
  vtk_writer out_grd( prefix + "/out_grd_" );
  auto vtk = [&]( auto tn, auto& out, auto& grd )
  {
    cout << "write VTK\n";
    out.next( grd );

    d_arr<S, D> u( u0.extents() );
    
    u[0] = u0_ext( tn, grd );
    u[1] = u1_ext( tn, grd );
    u[2] = u2_ext( tn, grd );
    out.name( "u_ext" ) << u;
    out.name( "p_ext" ) << p_ext( tn, grd );

    u[0] = u0;
    u[1] = u1;
    u[2] = u2;
    out.name( "u" )       << u;
    out.name( "u_con" )   << u_con; 
    out.name( "p" )       << to_real( P[0] );
    out.name( "phi" )     << to_real( PHI[0] );
    out.name( "J" )       << crv_grd.jacobian();
    //out.name( "w" )       << crv_grd.grid_vel();
    //out.name( "w_con" )   << crv_grd.grid_vel_contra();
  };

  vtk( 0, out_grd, crd0 );

  cout << "Ready to Go!!\n";

  vtk_writer vtk_dbg( prefix + "/out_dbg_" );
  auto vtk_vec = [&]( auto name, auto&& s0, auto&& s1, auto&& s2 )
  {
    d_arr<S, D> u( s0.extents() );    
    u[0] = s0;
    u[1] = s1;
    u[2] = s2;
    vtk_dbg.name( name ) << u;
  };
  vtk_dbg.next( crd0 );

  for( std::size_t i = 1; i <= it_N; ++i )
  {
    T tn = T(i) * dt;
    cout << "i = " << i << "\t t = " << tn << endl;

    // advance grid
    //
    auto crd = grd_gen( tn );
    crv_grd.advance( crd );
    vtk_dbg.next( crd );

    // extra-polation for phi & p
    //
    extra_polate( PHI );

    // project to u_hat
    //
    auto U0_ext = to_freq( u0_ext( tn, crd ) );
    auto U1_ext = to_freq( u1_ext( tn, crd ) );
    auto U2_ext = to_freq( u2_ext( tn, crd ) );
    auto UH0_bc = proj_u( 0, U0_ext, PHI[0], false ); // backward 
    auto UH1_bc = proj_u( 1, U1_ext, PHI[0], false ); // backward 
    auto UH2_bc = proj_u( 2, U2_ext, PHI[0], false ); // backward 
    vtk_vec( "u_ext", to_real(U0_ext), to_real(U1_ext), to_real(U2_ext) );
    vtk_dbg.name( "phi_h" ) << to_real( PHI[0] );
    vtk_vec( "uh_bc", to_real(UH0_bc), to_real(UH1_bc), to_real(UH2_bc) );

    // u_hat solvers - SOR
    //
    T tol = 1.e-7 * sqrt( T(U0.size()) );
    auto UH0_slvr = uh_slvr_sor( bc, UH0_bc, tol );
    auto UH1_slvr = uh_slvr_sor( bc, UH1_bc, tol );
    auto UH2_slvr = uh_slvr_sor( bc, UH2_bc, tol );

    // solve momentum eqs. for u_hat
    //
    cout << "Solving Momemtum Equations - U_hat_0 \n";
    auto UH0 = mom_0.solve( UH0_slvr );
    cout << "Solving Momemtum Equations - U_hat_1 \n";
    auto UH1 = mom_1.solve( UH1_slvr );
    cout << "Solving Momemtum Equations - U_hat_2 \n";
    auto UH2 = mom_2.solve( UH2_slvr );

    // divergence of u_hat
    //
    auto UH_CON = to_contra_vel( crv_grd, UH0, UH1, UH2 );
    auto div_UH = eval_divergence( vtk_dbg, UH_CON, crv_grd, dksi0, dksi1, dksi2 );
    A UH0_CON = UH_CON[0];
    A UH1_CON = UH_CON[1];
    A UH2_CON = UH_CON[2];
    vtk_vec( "uh_con", to_real(UH0_CON), to_real(UH1_CON), to_real(UH2_CON) );
    vtk_dbg.name( "div_uh_con" ) << to_real( div_UH );

    // prepapre phi - lhs / rhs / bc
    //
    d_arr<T, D> p       = p_ext( tn, crd ) / rho;
                P[0]    = to_freq( p );
    A           P_int   = ( T(3) * P[0] + T(6) * P[1] - T(1) * P[2] ) / T(8);
    auto        PHI_bc  = proj_phi( P_int, PHI[0] );
    A           PHI_rhs = alpha_0 / dt * div_UH;
    vtk_dbg.name( "phi_bc" )  << to_real( PHI_bc );
    vtk_dbg.name( "phi_rhs" ) << to_real( PHI_rhs );

    // solve PHI
    //
    cout << "Solving Laplace PHI\n";
    PHI[0] = solve_poisson( PHI_rhs, PHI_bc, crv_grd, dksi0, dksi1, dksi2 );
    vtk_dbg.name( "phi" ) << to_real( PHI[0] );

    // update p & u
    //
    P[0] = proj_p( PHI[0] );
    vtk_dbg.name( "p" ) << to_real( P[0] );
    U0   = proj_u( 0, UH0, PHI[0], true ); // forward
    U1   = proj_u( 1, UH1, PHI[0], true ); // forward 
    U2   = proj_u( 2, UH2, PHI[0], true ); // forward 
    u0   = to_real( U0 );
    u1   = to_real( U1 );
    u2   = to_real( U2 );

    // update flow convection
    //
    auto U_CON_0 = proj_u_con( 0, UH_CON, PHI[0], true ); // forward
    auto U_CON_1 = proj_u_con( 1, UH_CON, PHI[0], true ); // forward
    auto U_CON_2 = proj_u_con( 2, UH_CON, PHI[0], true ); // forward
    u_con[0] = to_real( U_CON_0 );
    u_con[1] = to_real( U_CON_1 );
    u_con[2] = to_real( U_CON_2 );
    U_CON[0] = U_CON_0;
    U_CON[1] = U_CON_1;
    U_CON[2] = U_CON_2;

    // roll-back
    //
    cout << "Rolling back\n";
    mom_0.roll_back( U0, crv_grd.jacobian() );
    mom_1.roll_back( U1, crv_grd.jacobian() );
    mom_2.roll_back( U2, crv_grd.jacobian() );
    for( size_t n = M-1; n >= 1; --n )
      PHI[ n ] = PHI[ n-1 ];
    for( size_t n = 2;   n >= 1; --n )
      P[ n ]   = P[ n-1 ];

    vtk( tn, out_grd, crd );
 }
}

int main( int argc, char* argv[] )
{
  // spatial
  //
  test<2>( argv[2], atoi(argv[1]) );
  
  // temporal
  //
  //test<2>( argv[2], atof( argv[1] ) );

  return 0;
}
