from vtk import *
from vtk.util import numpy_support as vtknp
import matplotlib.pyplot as plt
from numpy import *

class vtkProc():
  def __init__( self, fname ):
    self.r0 = vtkStructuredGridReader()
    self.r0.SetFileName( fname )
    self.r0.ReadAllVectorsOn()
    self.r0.ReadAllScalarsOn()
    self.r0.Update()

    self.o0  = self.r0.GetOutput()
    self.shp = self.o0.GetDimensions()

  def __getitem__( self, key ):
    u_vtk = self.o0.GetPointData().GetArray( key )
    u_raw = vtknp.vtk_to_numpy( u_vtk )
    if u_vtk.GetNumberOfComponents() == 1:
      p = u_raw.reshape( self.shp[::-1] )[:, 4, :]
      return p
    else:
      u0 = u_raw[:, 0].reshape( self.shp[::-1] )[:, 4, :]
      u1 = u_raw[:, 1].reshape( self.shp[::-1] )[:, 4, :]
      u2 = u_raw[:, 2].reshape( self.shp[::-1] )[:, 4, :]
      return [u2, u1, u0]

vtk_1 = vtkProc( "case_t1/out_1/out_grd_0030.vtk" )
vtk_2 = vtkProc( "case_t1/out_2/out_grd_0024.vtk" )
vtk_3 = vtkProc( "case_t1/out_3/out_grd_0020.vtk" )
vtk_4 = vtkProc( "case_t1/out_4/out_grd_0300.vtk" )

shp = vtk_1.shp

x0 = linspace( 0, 2*pi, shp[0] )
x2 = linspace( 0, 2*pi, shp[2] )

u0_1, u1_1, u2_1 = vtk_1["u"]
u0_2, u1_2, u2_2 = vtk_2["u"]
u0_3, u1_3, u2_3 = vtk_3["u"]
u0_4, u1_4, u2_4 = vtk_4["u"]

e0_14 = abs( u0_1 - u0_4 ).max()
e0_24 = abs( u0_2 - u0_4 ).max()
e0_34 = abs( u0_3 - u0_4 ).max()

e2_14 = abs( u2_1 - u2_4 ).max()
e2_24 = abs( u2_2 - u2_4 ).max()
e2_34 = abs( u2_3 - u2_4 ).max()

n0 = 0.5 * ( log(e0_24 / e0_14) / log(1.25) + log(e0_34 / e0_24) / log(1.2) )
n2 = 0.5 * ( log(e2_24 / e2_14) / log(1.25) + log(e2_34 / e2_24) / log(1.2) )

print "n0  = ", n0
print "n2  = ", n2


