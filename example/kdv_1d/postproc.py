import numpy as np
import matplotlib.pyplot as plt

def parse( fname ):
  data = {}
  with open( fname ) as f:
    tmp = []
    for line in f:
      tmp.append( line )
      if len( tmp ) == 4:
        name = tmp[0].strip()
        shp  = [ float(x) for x in tmp[2].split() ]
        arr  = np.array( [ float(x) for x in tmp[3].split() ] )
        arr.reshape( shp )
        data[ name ] = arr
        tmp[:] = []
  return data


d1 = parse( "out.txt" )

d = {}
d.update( d1 )

x = d.pop('x')

lgnd = []
plt.figure()
plt.hold(True)
for k, v in d.iteritems():
  lgnd.append( k )
  plt.plot( x, v )
plt.hold(False)
plt.legend( lgnd )
plt.show()

