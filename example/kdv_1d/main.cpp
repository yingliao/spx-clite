#include <iostream>
#include <complex>
#include <spx/spx.h>

using namespace std;
using namespace spx;

template <typename A>
  void write_txt( ofstream& out, string name, const A& a )
  {
    out << name << endl;
    out << a.rank() << endl;
    for( auto v : a.extents() )
      out << v << " ";
    out << endl;
    for( auto v : a )
      out << v << " ";
    out << endl;
  }

int main()
{
  using T = double;
  using R = complex<T>;

  size_t N  =  256;
  T      L  =  240.0;
  T      xm = -120.0;
  T      dx = L / T(N);
  T      dt = 0.05*dx;
  auto   x  = linspace( xm, xm+L-dx, N );

  auto b1   = make_fourier_basis( N, dx, 1 );
  auto b3   = make_fourier_basis( N, dx, 3 );
  auto Dx   = make_stencil_array<1>( {N}, b1, 0 );
  auto Dxxx = make_stencil_array<1>( {N}, b3, 0 );

  auto u_ext = [&]( auto t, auto a )
  {
    d_vec<T> ph = 0.5 * ( a*x - a*a*a*t );
    d_vec<T> r  = 3.0 * a*a * sech( ph ) * sech( ph );
    return r;
  };

  d_vec<T> u  = u_ext( -700.0, 0.35 ) + u_ext( -200, 0.18 );

  ofstream out( "out.txt" );
  write_txt( out, "x",   x );
  write_txt( out, "un_" + digistr(0), u );

  auto U = fft( u );

  auto rk = make_runge_kutta( dt, rk4_38<T>(), U );
  for( size_t it = 1; it < 30000; ++it )
  {
    T t_nxt = T(it) * dt;
    cout << "[Iter = "<< it << "] t = " << t_nxt << endl;

    auto f = [&]( auto n, auto dtn, auto Un )
    {
      d_vec<R> U_dxxx = Dxxx( Un );
      d_vec<R> U_dx   = Dx( Un );

      d_vec<T> u_dxxx = real( ifft( U_dxxx ) );
      d_vec<T> u_dx   = real( ifft( U_dx ) );
      d_vec<T> u      = real( ifft( Un ) );
      d_vec<T> rhs    = -( u * u_dx + u_dxxx );
      auto RHS = fft( rhs );
      return RHS;
    };

    U = rk.advance( f );
    u = real( ifft( U ) );
    if( it % 3000 == 0 )
      write_txt( out, "un_" + digistr(it), u );
  }

  out.close();
  return 0;
}
