from vtk import *
from vtk.util import numpy_support as vtknp
import matplotlib.pyplot as plt
from numpy import *

def u1_ext( t, x, z ):
  return  -cos(x) * sin(z) * exp(-2*t)

def u0_ext( t, x, z ):
  return   sin(x) * cos(z) * exp(-2*t)

class vtkProc():
  def __init__( self, dt, prefix, stride = 1 ):
    self.dt     = dt
    self.prefix = prefix
    self.t0     = int( 0.24 / dt )
    self.t1     = self.t0 + 1
    f0 = prefix + "/out_grd_%04d.vtk" % self.t0
    f1 = prefix + "/out_grd_%04d.vtk" % self.t1

    print f0
    print f1

    r0 = vtkStructuredGridReader()
    r0.SetFileName( f0 )
    r0.ReadAllVectorsOn()
    r0.ReadAllScalarsOn()
    r0.Update()
    o0 = r0.GetOutput()

    r1 = vtkStructuredGridReader()
    r1.SetFileName( f1 )
    r1.ReadAllVectorsOn()
    r1.ReadAllScalarsOn()
    r1.Update()
    o1 = r1.GetOutput()

    self.u0,     self.u1     = self.read( o0, o1, "u", stride )
    self.u0_ext, self.u1_ext = self.read( o0, o1, "u_ext", stride )

    self.u0_err = self.u0 - self.u0_ext
    self.u1_err = self.u1 - self.u1_ext

    self.shp = self.u0.shape

    self.u0_emax = abs( self.u0_err ).max()
    self.u1_emax = abs( self.u1_err ).max()

    print "u0_emax = ", self.u0_emax
    print "u1_emax = ", self.u1_emax

  def read( self, o0, o1, name, stride ):
    s = o0.GetDimensions()

    u_vtk_0 = o0.GetPointData().GetArray(name)
    u_raw_0 = vtknp.vtk_to_numpy( u_vtk_0 )
    u0_0 = u_raw_0[:, 0].reshape( s[::-1] )[0, :, :][::stride, :]
    u1_0 = u_raw_0[:, 1].reshape( s[::-1] )[0, :, :][::stride, :]

    u_vtk_1 = o1.GetPointData().GetArray(name)
    u_raw_1 = vtknp.vtk_to_numpy( u_vtk_1 )
    u0_1 = u_raw_1[:, 0].reshape( s[::-1] )[0, :, :][::stride, :]
    u1_1 = u_raw_1[:, 1].reshape( s[::-1] )[0, :, :][::stride, :]

    w1 = (0.24 % self.dt) / self.dt
    w0 = 1.0 - w1

    u0 = w0 * u0_0 + w1 * u0_1
    u1 = w0 * u1_0 + w1 * u1_1

    return [u0, u1]

vtk0 = vtkProc( 0.00815998, "case_s3/out_0", 4 )
vtk1 = vtkProc( 0.0140250,  "case_s3/out_1", 4 )
vtk2 = vtkProc( 0.0218166,  "case_s3/out_2", 2 )
vtk3 = vtkProc( 0.0302076,  "case_s3/out_3", 1 )

u0_1 = vtk1.u0
u1_1 = vtk1.u1

u0_2 = vtk2.u0
u1_2 = vtk2.u1

u0_3 = vtk3.u0
u1_3 = vtk3.u1

# convergence rate
m_u0 = -log( (abs(u0_2 - u0_1)).max() / (abs(u0_3 - u0_2)).max() ) / log(2)
m_u1 = -log( (abs(u1_2 - u1_1)).max() / (abs(u1_3 - u1_2)).max() ) / log(2)
print "u0, m = %f" % m_u0
print "u1, m = %f" % m_u1

shp1 = vtk1.shp
shp2 = vtk2.shp
shp3 = vtk3.shp

x0_1 = linspace( 0, 2*pi, shp1[0] )
x1_1 = linspace( 0, 2*pi, shp1[1] )

x0_2 = linspace( 0, 2*pi, shp2[0] )
x1_2 = linspace( 0, 2*pi, shp2[1] )

x0_3 = linspace( 0, 2*pi, shp3[0] )
x1_3 = linspace( 0, 2*pi, shp3[1] )

x_1, y_1 = meshgrid( x1_1, x0_1 )
x_2, y_2 = meshgrid( x1_2, x0_2 )
x_3, y_3 = meshgrid( x1_3, x0_3 )

u0e = u0_ext( 0.24, x_1, y_1 )
u1e = u1_ext( 0.24, x_1, y_1 )

plt.figure()
plt.hold(True)
plt.quiver( x_1, y_1, u0_1, u1_1, color="r", alpha=0.5 )
plt.quiver( x_2, y_2, u0_2, u1_2, color="g", alpha=0.5 )
plt.quiver( x_3, y_3, u0_3, u1_3, color="b", alpha=0.5 )
plt.hold(False)

plt.figure()
plt.suptitle( "u0" )

plt.subplot(121)
plt.title( "Err: grd_1 vs grd_2" )
plt.imshow( u0_2 - u0_1 )
plt.colorbar()

plt.subplot(122)
plt.title( "Err: grd_2 vs grd_3" )
plt.imshow( u0_3 - u0_2 )
plt.colorbar()

plt.figure()
plt.suptitle( "u1" )

plt.subplot(121)
plt.title( "Err: grd_1 vs grd_2" )
plt.imshow( u1_2 - u1_1 )
plt.colorbar()

plt.subplot(122)
plt.title( "Err: grd_2 vs grd_3" )
plt.imshow( u1_3 - u1_2 )
plt.colorbar()

plt.show()


