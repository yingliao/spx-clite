from vtk import *
from vtk.util import numpy_support as vtknp
import matplotlib.pyplot as plt
from numpy import *

class vtkProc():
  def __init__( self, fname ):
    self.r0 = vtkStructuredGridReader()
    self.r0.SetFileName( fname )
    self.r0.ReadAllVectorsOn()
    self.r0.ReadAllScalarsOn()
    self.r0.Update()

    self.o0  = self.r0.GetOutput()
    self.shp = self.o0.GetDimensions()[::-1][1::]

  def __getitem__( self, key ):
    s = self.o0.GetDimensions();
    u_vtk = self.o0.GetPointData().GetArray( key )
    u_raw = vtknp.vtk_to_numpy( u_vtk )
    if u_vtk.GetNumberOfComponents() == 1:
      p = u_raw.reshape( s[::-1] )[0, :, :]
      return p
    else:
      u0 = u_raw[:, 0].reshape( s[::-1] )[0, :, :]
      u1 = u_raw[:, 1].reshape( s[::-1] )[0, :, :]
      return [u1, u0]

vtk_1 = vtkProc( "case_t1/out_1/out_grd_0030.vtk" )
vtk_2 = vtkProc( "case_t1/out_2/out_grd_0024.vtk" )
vtk_3 = vtkProc( "case_t1/out_3/out_grd_0020.vtk" )
vtk_4 = vtkProc( "case_t1/out_4/out_grd_0300.vtk" )

shp = vtk_1.shp

x0 = linspace( 0, 2*pi, shp[0] )
x1 = linspace( 0, 2*pi, shp[1] )

u0_1, u1_1 = vtk_1["u"]
u0_2, u1_2 = vtk_2["u"]
u0_3, u1_3 = vtk_3["u"]
u0_4, u1_4 = vtk_4["u"]

ue0_1, ue1_1 = vtk_1["u_ext"]
ue0_2, ue1_2 = vtk_2["u_ext"]
ue0_3, ue1_3 = vtk_3["u_ext"]
ue0_4, ue1_4 = vtk_4["u_ext"]

print "case-1"
print "u0_max = ", abs(u0_1 - ue0_1).max()
print "u1_max = ", abs(u1_1 - ue1_1).max()
print "case-2"
print "u0_max = ", abs(u0_2 - ue0_2).max()
print "u1_max = ", abs(u1_2 - ue1_2).max()
print "case-3"
print "u0_max = ", abs(u0_3 - ue0_3).max()
print "u1_max = ", abs(u1_3 - ue1_3).max()
print "case-4"
print "u0_max = ", abs(u0_4 - ue0_4).max()
print "u1_max = ", abs(u1_4 - ue1_4).max()


e0_14 = abs( u0_1 - u0_4 ).max()
e0_24 = abs( u0_2 - u0_4 ).max()
e0_34 = abs( u0_3 - u0_4 ).max()

e1_14 = abs( u1_1 - u1_4 ).max()
e1_24 = abs( u1_2 - u1_4 ).max()
e1_34 = abs( u1_3 - u1_4 ).max()

n0 = 0.5 * ( log(e0_24 / e0_14) / log(1.25) + log(e0_34 / e0_24) / log(1.2) )
n1 = 0.5 * ( log(e1_24 / e1_14) / log(1.25) + log(e1_34 / e1_24) / log(1.2) )

print "n0  = ", n0
print "n1  = ", n1


