template <typename G, Stencil_array O0, Stencil_array O1, Stencil_array O2>
  struct poisson_helper
  {
    using T = Value_type<Main_type<G>>;
    using R = std::complex<T>;
    static constexpr auto D = Main_type<G>::D;

  private:
    T  tol = 1.e-4;
    
    T  h;
    G  crv_grd;
    O0 dksi0;
    O1 dksi1;
    O2 dksi2;
    
    d_arr<T, D> c0;

  public:
    poisson_helper( T h, G&& g, O0&& dksi0, O1&& dksi1, O2&& dksi2 )
      : h( h ),
        crv_grd( std::forward<G>( g ) ),
        dksi0( std::forward<O0>( dksi0 ) ),
        dksi1( std::forward<O1>( dksi1 ) ),
        dksi2( std::forward<O2>( dksi2 ) )
    {
      c0 = dksi0_coef();
    }
    
    decltype(auto) make_laplace_diag()
    {
      decltype(auto) g  = crv_grd.g_contra();
      return make_stencil_array( h       * dksi0( dksi0 )
                               + g[1][1] * dksi1( dksi1 )
                               + g[2][2] * dksi2( dksi2 ) );
                               //+ c0      * dksi0 );
    }
    
    template <Dense_array A>
      decltype(auto) eval_laplace_off( const A& U )
      {
        d_arr<R, D> U_d0   = dksi0( U );
        d_arr<R, D> U_d0d1 = dksi0( dksi1( U ) );
        d_arr<R, D> U_d0d2 = dksi0( dksi2( U ) );
        
        d_arr<T, D> u_d0   = to_real( U_d0 );
        d_arr<T, D> u_d0d1 = to_real( U_d0d1 );
        d_arr<T, D> u_d0d2 = to_real( U_d0d2 );

        d_arr<R, D> U_d0d0 = dksi0( dksi0( U ) );
        d_arr<T, D> u_d0d0 = to_real( U_d0d0 );

        decltype(auto) g  = crv_grd.g_contra();

        d_arr<T, D> v = T(2) * ( g[0][1] * u_d0d1 + g[0][2] * u_d0d2 ) 
                      + c0 * u_d0
                      + (g[0][0] - h) * u_d0d0;
        d_arr<R, D> V = to_freq( v );
        return V;
      }

    template <Dense_array A>
      decltype(auto) eval_laplace( const A& U )
      {
        d_arr<R, D> U_d0   = dksi0( U );
        d_arr<R, D> U_d1   = dksi1( U );
        d_arr<R, D> U_d2   = dksi2( U );
        d_arr<R, D> U_d0d0 = dksi0( U_d0 );
        d_arr<R, D> U_d0d1 = dksi0( U_d1 );
        d_arr<R, D> U_d0d2 = dksi0( U_d2 );
        d_arr<R, D> U_d1d1 = dksi1( U_d1 );
        d_arr<R, D> U_d2d2 = dksi2( U_d2 );
       
        d_arr<T, D> u_d0   = to_real( U_d0 );
        d_arr<T, D> u_d0d0 = to_real( U_d0d0 );
        d_arr<T, D> u_d0d1 = to_real( U_d0d1 );
        d_arr<T, D> u_d0d2 = to_real( U_d0d2 );
        d_arr<T, D> u_d1d1 = to_real( U_d1d1 );
        d_arr<T, D> u_d2d2 = to_real( U_d2d2 );

        decltype(auto) g  = crv_grd.g_contra();

        d_arr<T, D> v = T(2) * ( g[0][1] * u_d0d1 + g[0][2] * u_d0d2 ) 
                      + c0 * u_d0
                      + g[0][0] * u_d0d0 
                      + g[1][1] * u_d1d1
                      + g[2][2] * u_d2d2;

        d_arr<R, D> V = to_freq( v );
        return V;
      }

    template <Dense_array A, Dense_array B>
      decltype(auto) solve_dn( const A& rhs, const B& u_bc_top, const B& u_bc_btm, const A& u0 )
      {
        auto RHS      = to_freq( rhs );
        auto U_bc_top = to_freq( u_bc_top );
        auto U_bc_btm = to_freq( u_bc_btm );
        auto U0       = to_freq( u0 );

        auto on_node = make_on_node<R>( RHS.descriptor() );
        //auto btm = make_stencil_array( dksi0( j * dksi_dx[0][0] * on_node ) / j ); 
        auto btm = make_stencil_array( R(1) * dksi0 ); 
        auto lap = this->make_laplace_diag();
        auto prb = make_stencil_array(
          []( auto& desc, auto idx, auto it_lap, auto it_top, auto it_btm )
          {
            if( idx[0] == desc.ubound(0) )
              return *it_top;
            else if( idx[0] == desc.lbound(0) )
              return *it_btm;
            else
              return *it_lap;
          },
          lap, 
          on_node, 
          btm );

        d_arr<R, D> U_n(  RHS.extents() );
        d_arr<T, D> u_cp( RHS.extents() );
        U_n  = U0;
        u_cp = to_real( U_n );

        T tol_n = tol * std::sqrt( T( U_n.size() ) );
        cout << "TOL = " << tol_n << endl;

        for( size_t it = 0; it < 300; ++it )
        {
          d_arr<R, D> LAP_off = this->eval_laplace_off( U_n );
          d_arr<R, D> RHS_it  = RHS - LAP_off;
          RHS_it( last(),  all(), all() ) = U_bc_top;
          RHS_it( first(), all(), all() ) = U_bc_btm;

          lsor_async( prb, RHS_it, U_n, 0, 1.0 );
       
          auto u_n = to_real( U_n );
          d_arr<T, D> ee = u_n - u_cp;
          T err = std::abs( norm2( ee ) ); 
          u_cp  = u_n;
          cout << "[Iter = " << it << "] Err. L2-Norm = " << err << endl;

          if( err < tol_n )
            break;
        }
        return u_cp;
      }

  private:
    decltype(auto) dksi0_coef()
    {
      decltype(auto) s = crv_grd.b_contra();
      
      // dksi0 -coef
      //
      d_arr<T, D> s00_d0 = dksi0( s[0][0] );
      d_arr<T, D> s01_d0 = dksi0( s[0][1] );
      d_arr<T, D> s02_d0 = dksi0( s[0][2] );
      d_arr<T, D> s01    = s[0][1];
      d_arr<T, D> s02    = s[0][2];
      d_arr<R, D> S01    = to_freq( s01 );
      d_arr<R, D> S02    = to_freq( s02 );
      d_arr<R, D> S01_d1 = dksi1( S01 );
      d_arr<R, D> S02_d2 = dksi2( S02 );
      d_arr<T, D> s01_d1 = to_real( S01_d1 );
      d_arr<T, D> s02_d2 = to_real( S02_d2 );
      d_arr<T, D> c0     = s[0][0] * s00_d0 + s[0][1] * s01_d0 + s[0][2] * s02_d0
                         + s[1][1] * s01_d1 + s[2][2] * s02_d2;
      return c0; 
    }
  };
 
template <typename T, typename G, Stencil_array O0, Stencil_array O1, Stencil_array O2>
  decltype(auto) make_poisson_helper( T h, G&& g, O0&& dksi0, O1&& dksi1, O2&& dksi2 )
  {
    return poisson_helper<G, O0, O1, O2>( h,
                                          std::forward<G>(g), 
                                          std::forward<O0>( dksi0 ),
                                          std::forward<O1>( dksi1 ),
                                          std::forward<O2>( dksi2 ) );
  }


