#include "stdafx.h"
//#include "ext_linvis_wave.h"
#include "ext_stokes_wave.h"

template <size_t I>
requires (I == 2)
void test( string prefix, double ak )
{
  constexpr std::size_t D = 3;
  
  using T = double;
  using R = std::complex<T>;
 
  // phsical params
  //
  T rho = 1e3;
  T gma = 0;//0.07197;
  T g   = 9.81;

  // exact solution
  //
  T re  = 1000;
  //exct_lin_vis<T> ext( re, g, gma, ak );
  exct_stokes<T> ext( re, g, gma, ak );
  T nu  = ext.nu; //1.e-6;
  T mu  = rho * nu;

  // numerical params
  //
  T         H = pi_2<T>;
  size_t N[D] = { 97,     128,          16 };
  T      L[D] = {  H, pi_4<T>, pi_4<T>/4.0 };
  T   xmin[D] = { -H, 0, 0 };
  T     dx[D] = { L[0]/T(N[0]), L[1]/T(N[1]), L[2]/T(N[2]) };
 
  T       cfl = 0.4;
  T        dt = cfl / (1/dx[0] + 1/dx[1] + 1/dx[2]);
  size_t it_N = 9.0 / dt;

  // PBC domain
  //
  auto gen = two_sided_vinokur<T>{ 1.52788, 0.39725 };
  auto x0  = gen( xmin[0], xmin[0]+L[0], N[0] );
  auto x1  = linspace( xmin[1], xmin[1]+L[1]-dx[1], N[1] );
  auto x2  = linspace( xmin[2], xmin[2]+L[2]-dx[2], N[2] );

  auto rec_grd_c = make_rectlin_grid( x0, x1, x2 );
  auto grd_eta   = make_rectlin_grid( x1, x2 );
  auto crd_eta   = grd_eta.coords();

  // eta operators
  //
  auto b1_o1      = make_fourier_basis( N[1], dx[1], 1 );
  auto b1_o2      = make_fourier_basis( N[1], dx[1], 2 );
  auto b2_o1      = make_fourier_basis( N[2], dx[2], 1 );
  auto b2_o2      = make_fourier_basis( N[2], dx[2], 2 );
  auto eta_dx1    = make_stencil_array<D-1>( { N[1], N[2] }, b1_o1, 0 );
  auto eta_dx1dx1 = make_stencil_array<D-1>( { N[1], N[2] }, b1_o2, 0 );
  auto eta_dx2    = make_stencil_array<D-1>( { N[1], N[2] }, b2_o1, 1 );
  auto eta_dx2dx2 = make_stencil_array<D-1>( { N[1], N[2] }, b2_o2, 1 );

  // curvilinear grid
  //
  auto ksi0      = linspace( xmin[0], xmin[0]+L[0],       N[0] );
  auto ksi1      = linspace( xmin[1], xmin[1]+L[1]-dx[1], N[1] );
  auto ksi2      = linspace( xmin[2], xmin[2]+L[2]-dx[2], N[2] );
  
  auto crvgrd_b0 = make_fd_basis( ksi0, 1, 4 ); // 1-st order diff & 4-th order accuracy
  auto crvgrd_b1 = make_fd_basis( ksi1, 1, 4 ); // 1-st order diff & 4-th order accuracy
  auto crvgrd_b2 = make_fd_basis( ksi2, 1, 4 ); // 1-st order diff & 4-th order accuracy
  
  auto crvgrd_dksi0 = make_stencil_array<D>( N, crvgrd_b0, 0 );
  auto crvgrd_dksi1 = make_stencil_array<D>( N, crvgrd_b1, 1 );
  auto crvgrd_dksi2 = make_stencil_array<D>( N, crvgrd_b2, 2 );
 
  auto crv_grd_c = make_curvilin_dyn_grid( dt, crvgrd_dksi0, crvgrd_dksi1, crvgrd_dksi2 );
  
  // curvilinear operators
  //
  auto ksi_b0_o1 = make_fd_basis( ksi0, 1, 4 );
  auto ksi_b1_o1 = make_fourier_basis( N[1], dx[1], 1 );
  auto ksi_b2_o1 = make_fourier_basis( N[2], dx[2], 1 );
  auto dksi0     = make_stencil_array<D>( N, ksi_b0_o1, 0 );
  auto dksi1     = make_stencil_array<D>( N, ksi_b1_o1, 1 );
  auto dksi2     = make_stencil_array<D>( N, ksi_b2_o1, 2 );

  // initial eta
  //
  d_arr<T, D-1> eta( {N[1], N[2]} );
  eta = ext.eta( 0, crd_eta ); 

  // initial grid
  //
  auto crd_c = rec_grd_c.perturb_surface( eta, 0 ); // along axis-0
  crv_grd_c.update( crd_c );

  // initial condition
  //
  d_arr<T, D> u0( N );
  d_arr<T, D> u1( N );
  d_arr<T, D> u2( N );
  u0 = ext.u0( 0, rec_grd_c.coords() );
  u1 = ext.u1( 0, rec_grd_c.coords() );
  u2 = 0; 

  // initial pressure
  //
  d_arr<T, D> p0( N );
  p0 = 0;
  vector<d_arr<T, D>> p( 3 );
  for( size_t t = 0; t < 3; ++t )
    p[ t ] = p0;

  // time-stacking size
  //
  // t = 0: current solution
  // t = 1 ~ M-1: history
  //
  constexpr size_t M = 2;
  vector<d_arr<T, D>> phi( M );
  for( size_t t = 0; t < M; ++t )
  {
    phi[ t ] = d_arr<T, D>( N );
    phi[ t ] = p[ 0 ];
  }

  // frac coef
  //
  using trns_ts = bdf1<T>;
  using dfus_ts = am2<T>;
  using conv_ts = ab2<T>;
  auto  alpha_0 = trns_ts().coef_u()[0];
  auto  beta_0  = dfus_ts().coef_f()[0];
 
  auto psn_hlp_c     = make_poisson_helper( H, crv_grd_c, dksi0, dksi1, dksi2 );
  auto lap_diag_c    = psn_hlp_c.make_laplace_diag();
  auto nu_lap_diag_c = make_stencil_array( nu * lap_diag_c );

  auto dfus_diag_op  = [&]( size_t d, auto& u )
  {
    d_arr<R, D> U = to_freq( u );
    d_arr<R, D> Q = nu_lap_diag_c( U );
    d_arr<T, D> r = to_real( Q );
    return r;
  };

  auto dfus_op = [&]( auto& u )
  {
    auto        U     = to_freq( u );
    d_arr<R, D> LAP_U = psn_hlp_c.eval_laplace( U );
    d_arr<T, D> r     = nu * to_real( LAP_U );
    return r;
  };

  auto conv_op = [&]( size_t d, auto& u ) 
  {
    d_arr<T, D> u_u0 = u * u0;
    d_arr<T, D> u_u1 = u * u1;
    d_arr<T, D> u_u2 = u * u2;
    
    d_arr<R, D> d1_U_U1 = dksi1( to_freq( u_u1 ) );
    d_arr<R, D> d2_U_U2 = dksi2( to_freq( u_u2 ) );

    d_arr<T, D> d0_u_u0 = dksi0( u_u0 );
    d_arr<T, D> d0_u_u1 = dksi0( u_u1 );
    d_arr<T, D> d0_u_u2 = dksi0( u_u2 );
    d_arr<T, D> d1_u_u1 = to_real( d1_U_U1 ); 
    d_arr<T, D> d2_u_u2 = to_real( d2_U_U2 ); 

    decltype(auto) s = crv_grd_c.b_contra();
    d_arr<T, D> dx0 = s[0][0] * d0_u_u0;
    d_arr<T, D> dx1 = s[0][1] * d0_u_u1 + s[1][1] * d1_u_u1;
    d_arr<T, D> dx2 = s[0][2] * d0_u_u2 + s[1][1] * d2_u_u2;

    // grid-vel
    auto w_con_c = crv_grd_c.grid_vel_contra();
    auto U = to_freq( u );
    d_arr<R, D> d1_U  = dksi1( U );
    d_arr<R, D> d2_U  = dksi2( U );
    d_arr<T, D> d0_u  = dksi0( u );
    d_arr<T, D> d1_u  = to_real( d1_U );
    d_arr<T, D> d2_u  = to_real( d2_U );
    d_arr<T, D> cnv_w = w_con_c[0] * d0_u 
                      + w_con_c[1] * d1_u 
                      + w_con_c[2] * d2_u;

    d_arr<T, D> cnv = dx0 + dx1 + dx2 - cnv_w;

    d_arr<R, D> LAP_U_OFF = psn_hlp_c.eval_laplace_off( to_freq(u) );  // clpt
    d_arr<T, D> lap_u_off = to_real( LAP_U_OFF );

    d_arr<T, D> rr = -cnv + nu * lap_u_off;
    dealias( rr );
    return rr;
  };
 
  cout << "Preparing Momentum Equation - 0\n";

  auto mom_0 = make_multi_stepper( dt, trns_ts(), u0, 
                                       dfus_ts(), [&]( auto& x ){ return dfus_diag_op( 0, x ); },
                                       conv_ts(), [&]( auto& x ){ return conv_op( 0, x ); } );
  
  cout << "Preparing Momentum Equation - 1\n";

  auto mom_1 = make_multi_stepper( dt, trns_ts(), u1,
                                       dfus_ts(), [&]( auto& x ){ return dfus_diag_op( 1, x ); },
                                       conv_ts(), [&]( auto& x ){ return conv_op( 1, x ); } );

  cout << "Preparing Momentum Equation - 2\n";

  auto mom_2 = make_multi_stepper( dt, trns_ts(), u2,
                                       dfus_ts(), [&]( auto& x ){ return dfus_diag_op( 2, x ); },
                                       conv_ts(), [&]( auto& x ){ return conv_op( 2, x ); } );

  // eta RK-solver
  //
  auto conv_eta  = [&]( auto& x ) 
  {
    auto X = to_freq( x );
    
    d_arr<T, D-1> u0_surf = u0( last(), all(), all() );
    d_arr<T, D-1> u1_surf = u1( last(), all(), all() );
    d_arr<T, D-1> u2_surf = u2( last(), all(), all() );

    d_arr<R, D-1> D1  = eta_dx1( X );
    d_arr<T, D-1> d1  = to_real( D1 ); 

    d_arr<R, D-1> D2  = eta_dx2( X );
    d_arr<T, D-1> d2  = to_real( D2 ); 

    d_arr<T, D-1> rhs = u0_surf - u1_surf * d1 - u2_surf * d2;
    dealias( rhs );
    return rhs;
  };
 
  auto rk = make_runge_kutta( dt, rk2<T>(1), eta );

  // project function
  //
  auto proj_u     = proj_u_func(   dt, alpha_0, crv_grd_c, dksi0, dksi1, dksi2 );
  auto proj_phi   = proj_phi_func( dt, alpha_0, beta_0, crv_grd_c, dfus_op );
  auto proj_p     = proj_p_func(   dt, alpha_0, beta_0, crv_grd_c, dfus_op );

  // BC function
  //
  auto bc = []( auto& a, auto& b )
  {
    a( first(), all(), all() ) = b( first(), all(), all() );
    a( last(),  all(), all() ) = b( last(),  all(), all() );
  };

  // VTK write out - curvilin
  //
  vtk_writer_bin out_grd( prefix + "/out_grd_" );
  auto vtk = [&]( auto tn )
  {
    cout << "write VTK\n";
    out_grd.next( crd_c );
    out_grd.name( "u" )     << make_vec( u0, u1, u2 );
    out_grd.name( "p" )     << p[0];
    out_grd.name( "phi" )   << phi[0];
    out_grd.name( "J" )     << crv_grd_c.jacobian();
    //out.name( "w" )       << crv_grd_c.grid_vel();
    //out.name( "w_con" )   << crv_grd_c.grid_vel_contra();
  };

  // VTK write out - exact solution
  //
  vtk_writer_bin out_ext( prefix + "/out_ext_" );
  auto vtk_ext = [&]( auto tn )
  {
    auto eta_e = ext.eta( tn, crd_eta );
    auto crd_e = rec_grd_c.perturb_surface( eta_e, 0 ); // along axis-0
    out_ext.next( crd_e );
    crd_e = rec_grd_c.coords();
    out_ext.name( "u0" ) << ext.u0( tn, crd_e );
    out_ext.name( "u1" ) << ext.u1( tn, crd_e );
  };

  // VTK write out - debug
  //
  vtk_writer_bin vtk_dbg( prefix + "/out_dbg_" );

  // VTK write out - eta
  //
  vtk_writer_bin out_eta( prefix + "/out_eta_" );
  auto vtk_eta = [&]( auto tn )
  {
    out_eta.next( crd_eta );
    out_eta.name( "eta" )     << eta;
    out_eta.name( "eta_ext" ) << ext.eta( tn, crd_eta );
  };

  // VTN write out - eta debug
  //
  vtk_writer_bin vtk_dbg_eta( prefix + "/out_dbg_eta_" );

  // VTK write out - t0
  //
  vtk( 0 );
  vtk_dbg.next( crd_c );
  vtk_eta( 0 );
  vtk_ext( 0 );
  vtk_dbg_eta.next( crd_eta );

  cout << "Ready to Go!!\n";
 
  surf_coef_helper<T, 2> sf_coef;
  
  for( std::size_t i = 1; i <= it_N; ++i )
  {
    T t_nxt = T(i) * dt;
    cout << "\n\ni = " << i << "\t t = " << t_nxt << endl;

    auto rk_f = [&]( auto n, auto dtn, auto& eta_n )
    {
      auto tn = T(i) * dt + dtn;
      cout << "t_iter = " << tn << endl;

      // update eta
      //
      eta = eta_n;
      
      // update grid
      //
      crd_c = rec_grd_c.perturb_surface( eta, 0 ); // along axis-0
      crv_grd_c.update( crd_c );
      vtk_dbg.next( crd_c );
      vtk_dbg_eta.next( crd_eta );
      vtk_dbg_eta.name( "eta" ) << eta;

      // update surface coef. - eta
      //
      auto ETA = to_freq( eta );
      sf_coef.update_grd( crv_grd_c, ETA, eta_dx1, eta_dx1dx1, eta_dx2, eta_dx2dx2 );

      // extra-polation for phi & p
      //
      if( n == 1 )
        extra_polate( phi );
      vtk_dbg.name( "phi_h" ) << phi[0];

      // bottom velocity
      //
      auto        u0_ext = ext.u0( tn, rec_grd_c.coords() );
      auto        u1_ext = ext.u1( tn, rec_grd_c.coords() );
      d_arr<T, 2> u0_btm = u0_ext( first(), all(), all() );
      d_arr<T, 2> u1_btm = u1_ext( first(), all(), all() );

      auto u2_btm = d_arr<T, 2>( {N[1], N[2]} ); 
           u2_btm = 0;
 
      // prepare projection parts
      //
      d_arr<T, D> r0( N );
      d_arr<T, D> r1( N );
      d_arr<T, D> r2( N );
      r0 = 0;
      r1 = 0;
      r2 = 0;
      r0 = proj_u( 0, r0, phi[0], false );
      r1 = proj_u( 1, r1, phi[0], false );
      r2 = proj_u( 2, r2, phi[0], false );

d_arr<T, D> uh0, uh1, uh2;

d_arr<T, 2> u0_sf_new = u0( last(), all(), all() );
d_arr<T, 2> u1_sf_new = u1( last(), all(), all() );
d_arr<T, 2> u2_sf_new = u2( last(), all(), all() );
d_arr<T, 2> u0_sf_old;
d_arr<T, 2> u1_sf_old;
d_arr<T, 2> u2_sf_old;

T err_sf     = std::numeric_limits<T>::max();
T err_sf_old = std::numeric_limits<T>::max();

do{ 
u0_sf_old  = u0_sf_new;
u1_sf_old  = u1_sf_new;
u2_sf_old  = u2_sf_new;
err_sf_old = err_sf;

      // project to u_hat
      // 
      auto uh0_bc = make_sparse_array<T, D>( N );
      auto uh1_bc = make_sparse_array<T, D>( N );
      auto uh2_bc = make_sparse_array<T, D>( N );

      uh0_bc.insert_on();
      uh1_bc.insert_on();
      uh2_bc.insert_on();

      // surface
      //
      sf_coef.update_vel( u0, u1, u2, dksi0, dksi1, dksi2 );

      uh0_bc( last(), all(), all() ) = 
        surf_u0_bc( sf_coef, r0, dksi0 );

      uh1_bc( last(), all(), all() ) = 
        surf_u1_bc( sf_coef, r1, dksi0 );

      uh2_bc( last(), all(), all() ) = 
        surf_u2_bc( sf_coef, r2, dksi0 );

      // bottom
      uh0_bc( first(), all(), all() ) = btm_u_bc( 0, r0, u0_btm );
      uh1_bc( first(), all(), all() ) = btm_u_bc( 1, r1, u1_btm );
      uh2_bc( first(), all(), all() ) = btm_u_bc( 2, r2, u2_btm );

      uh0_bc.insert_off();
      uh1_bc.insert_off();
      uh2_bc.insert_off();

      auto dbg_uh0_bc = d_arr<T, D>( N );
      auto dbg_uh1_bc = d_arr<T, D>( N );
      auto dbg_uh2_bc = d_arr<T, D>( N );
      dbg_uh0_bc = 0;
      dbg_uh1_bc = 0;
      dbg_uh2_bc = 0;
      bc( dbg_uh0_bc, uh0_bc );
      bc( dbg_uh1_bc, uh1_bc );
      bc( dbg_uh2_bc, uh2_bc );
      vtk_dbg.name( "uh_bc" ) << make_vec( dbg_uh0_bc, dbg_uh1_bc, dbg_uh2_bc );

      // u_hat solvers - SOR
      //
      auto uh0_slvr = uh_slvr_sor( 0, uh0_bc, dksi0, nu_lap_diag_c );
      auto uh1_slvr = uh_slvr_sor( 1, uh1_bc, dksi0, nu_lap_diag_c );
      auto uh2_slvr = uh_slvr_sor( 2, uh2_bc, dksi0, nu_lap_diag_c );

      // solve momentum eqs. for u_hat
      //
      cout << "Solving Momemtum Equations - U_hat_0 \n";
      uh0 = mom_0.solve( uh0_slvr );
      cout << "Solving Momemtum Equations - U_hat_1 \n";
      uh1 = mom_1.solve( uh1_slvr );
      cout << "Solving Momemtum Equations - U_hat_2 \n";
      uh2 = mom_2.solve( uh2_slvr );
      
      // extra-polate U0 / U1 / U2
      //
      u0 = proj_u( 0, uh0, phi[0], true ); // forward
      u1 = proj_u( 1, uh1, phi[0], true ); // forward 
      u2 = proj_u( 2, uh2, phi[0], true ); // forward 

u0_sf_new  = u0( last(), all(), all() );
u1_sf_new  = u1( last(), all(), all() );
u2_sf_new  = u2( last(), all(), all() );
u0_sf_old -= u0_sf_new;
u1_sf_old -= u1_sf_new;
u2_sf_old -= u2_sf_new;
T err0 = std::abs( norm2( u0_sf_old ) );
T err1 = std::abs( norm2( u1_sf_old ) );
T err2 = std::abs( norm2( u2_sf_old ) );

err_sf = ( err0 + err1 + err2 ) / 3.0;
cout << "----> Surface L2-Err. = " << err_sf << endl;

} while( err_sf > 1.e-3 * std::sqrt( T(u0_sf_new.size()) )
     &&  err_sf <= err_sf_old );

      sf_coef.dump( vtk_dbg_eta );
      vtk_dbg.name( "uh" )      << make_vec( uh0, uh1, uh2 );
      vtk_dbg.name( "u_extra" ) << make_vec(  u0,  u1,  u2 ); 

      // divergence of u_hat
      //
      auto div_uh = eval_divergence( vtk_dbg, uh0, uh1, uh2, crv_grd_c, dksi0, dksi1, dksi2 );
      vtk_dbg.name( "div_uh" ) << div_uh;

      // prepapre phi - lhs / rhs / bc
      //
      // top BC --> from pressure
      //
      sf_coef.update_vel( u0, u1, u2, dksi0, dksi1, dksi2 );
      d_arr<T, 2> p_sf = surf_pressure( vtk_dbg_eta, sf_coef, mu, rho, g, gma ) / rho;
      p[0]( last(), all(), all() ) = p_sf;
      d_arr<T, 3> p_int      = ( T(3) * p[0] + T(6) * p[1] - T(1) * p[2] ) / T(8);
      d_arr<T, 2> phi_bc_top = proj_phi( p_int, phi[0] )( last(), all(), all() );

      // bottom BC --> from u0
      auto phi_bc_btm = btm_phi( crv_grd_c, uh0, u0_btm, alpha_0, dt );

      // rhs
      d_arr<T, D> phi_rhs = alpha_0 / dt * div_uh;

      auto phi_bc_dbg = d_arr<T, D>( N );
      phi_bc_dbg = 0;
      phi_bc_dbg( first(), all(), all() ) = phi_bc_btm;
      phi_bc_dbg( last(),  all(), all() ) = phi_bc_top;
      vtk_dbg.name( "phi_bc"  ) << phi_bc_dbg;
      vtk_dbg.name( "phi_rhs" ) << phi_rhs;

      d_arr<T, 2> p_int_top  = p_int( last(), all(), all() );
      d_arr<T, 2> u0_top     = u0( last(), all(), all() );
      d_arr<T, 2> u1_top     = u1( last(), all(), all() );
      d_arr<T, 2> u2_top     = u2( last(), all(), all() );
      vtk_dbg_eta.name( "p_sf" )       << p_sf;
      vtk_dbg_eta.name( "p_int" )      << p_int_top;
      vtk_dbg_eta.name( "phi_bc_top" ) << phi_bc_top;
      vtk_dbg_eta.name( "u0_extra" )   << u0_top;
      vtk_dbg_eta.name( "u1_extra" )   << u1_top;
      vtk_dbg_eta.name( "u2_extra" )   << u2_top;

      // solve PHI
      //
      cout << "Solving Laplace PHI\n";
      phi[0] = psn_hlp_c.solve_dn( phi_rhs, phi_bc_top, phi_bc_btm, phi[0] );
      vtk_dbg.name( "phi" ) << phi[0];

      // update p & u
      //
      cout << "Updating p & u\n";
      p[0] = proj_p( phi[0] );
      u0   = proj_u( 0, uh0, phi[0], true ); // forward
      u1   = proj_u( 1, uh1, phi[0], true ); // forward 
      u2   = proj_u( 2, uh2, phi[0], true ); // forward 
      vtk_dbg.name( "p" ) << p[0];
      vtk_dbg.name( "u" ) << make_vec( u0,  u1,  u2 ); 

      auto dEta = conv_eta( eta_n );
      return dEta;
    };

    eta = rk.advance( rk_f );
    vtk_eta( t_nxt );

    // roll-back
    //
    cout << "Rolling back\n";
    crv_grd_c.roll_back();
    mom_0.roll_back( u0 );
    mom_1.roll_back( u1 );
    mom_2.roll_back( u2 );
    for( size_t n = M-1; n >= 1; --n )
      phi[ n ] = phi[ n-1 ];
     for( size_t n = 2;   n >= 1; --n )
      p[ n ]   = p[ n-1 ];

    vtk( t_nxt );
    vtk_ext( t_nxt );
  }
}

int main( int argc, char* argv[] )
{
  test<2>( "out/", stod( argv[1] ) );
  return 0;
}
