template <Vector V>
  void extra_polate( V& v )
  {
    if( v.size() == 1 )
      v[0] = 0;
    else if( v.size() == 2 )
      v[0] = v[1];
    else if( v.size() == 3 )
      v[0] = 2.0 * v[1] - v[2];
    else if( v.size() == 4 )
      v[0] = 3.0 * v[1] - 3.0 * v[2] + v[3];
  }

template <Dense_array A>
  decltype(auto) to_freq( const A& u )
  {
    constexpr auto D = u.rank();
    size_t s = (D == 2) ? 0 : 1;
    auto U = fft( u, s   );
         U = fft( U, s+1 );
    return U;
  }

template <Dense_array A>
  decltype(auto) to_real( const A& U )
  {
    using R = Value_type<A>; // complex type
    using T = Value_type<R>;
    constexpr auto D = U.rank();

    size_t s = (D == 2) ? 0 : 1;
    auto iU = ifft(  U, s   );
         iU = ifft( iU, s+1 );
    d_arr<T, D> u = real( iU );
    return u;
  }

template <Dense_array A>
requires (A::rank() == 2)
  void dealias( A& u )
  {
    using T = Value_type<Main_type<A>>;
    int N0 = u.extents()[0];  
    int N1 = u.extents()[1];
    
    auto kvec = []( int N )
    {
      bool even = (N % 2) == 0;
      d_vec<T> kv( N );
      int i = 0;
      for( int k = 0; k <= (even ? N/2-1 : (N-1)/2); ++k ) kv[i++] = T(k);
      for( int k = (even ? -N/2 : -(N-1)/2); k < 0;  ++k ) kv[i++] = T(k);
      return kv;
    };

    auto k0 = kvec( N0 );
    auto k1 = kvec( N1 );
    T c     = 0.5;
    T kcut0 = c * 0.5 * T(N0);
    T kcut1 = c * 0.5 * T(N1);
    auto U  = to_freq( u );

    for( int i = 0; i < N0; ++i )
      for( int j = 0; j < N1; ++j )
        if( k0[ i ] < -kcut0 || k0[ i ] > kcut0 
         || k1[ j ] < -kcut0 || k1[ j ] > kcut1 )
          U( i, j ) = 0;

    u = to_real( U );
  }

template <Dense_array A>
requires (A::rank() == 3)
  void dealias( A& u )
  {
    using T = Value_type<Main_type<A>>;
    for( size_t i = 0; i < u.extents()[0]; ++i )
    {
      d_arr<T, 2> t = u( i, all(), all() );
      dealias( t );
      u( i, all(), all() ) = t;
    }
  }


