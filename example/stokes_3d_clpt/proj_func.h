// -----------------------  Project Functions  -------------------------------------------

  template <typename T,  typename G0, typename O0, typename O1, typename O2>
  decltype(auto) proj_u_func( T dt, T alpha_0,
                              G0&& crv_grd_c, O0&& dksi0, O1&& dksi1, O2&& dksi2 )
  {
    return [&]( std::size_t i, auto& u, auto& phi, bool frwd )
    {
      constexpr auto D = u.rank();
      using A = Main_type<decltype(u)>;
      using R = complex<T>;

      decltype(auto) dksi_dx_c = crv_grd_c.b_contra();

      A t;
      A phi_d0 = dksi0( phi );

      if( i == 0 )
      {
        t = dksi_dx_c[0][0] * phi_d0;
      }
      else if( i == 1 )
      {
        d_arr<R, D> PHI_d1 = dksi1( to_freq( phi ) );
        d_arr<T, D> phi_d1 = to_real( PHI_d1 );
        t = dksi_dx_c[0][1] * phi_d0
          + dksi_dx_c[1][1] * phi_d1;
      }
      else if( i == 2 )
      {
        d_arr<R, D> PHI_d2 = dksi2( to_freq( phi ) );
        d_arr<T, D> phi_d2 = to_real( PHI_d2 );
        t = dksi_dx_c[0][2] * phi_d0
          + dksi_dx_c[2][2] * phi_d2;
      }

      T s = frwd ? T(-1) : T(1);
      d_arr<T, D> rr = u + s * dt / alpha_0 * t;
      return rr;
    };
  }

template <typename T, typename G, typename O>
  decltype(auto) proj_p_func( T dt, T alpha_0, T beta_0, G&& crv_grd_c, O&& dfus_op )
  {
    return [&]( auto& phi )
    {
      using A = Main_type<decltype(phi)>;
      A p = phi - dt * beta_0 / alpha_0 * dfus_op( phi );
      return p;
    };
  }

template <typename T, typename G, typename O>
  decltype(auto) proj_phi_func( T dt, T alpha_0, T beta_0, G&& crv_grd_c, O&& dfus_op )
  {
    return [&]( auto& p, auto& phi_h )
    {
      using A = Main_type<decltype(phi_h)>;
      A phi = p + dt * beta_0 / alpha_0 * dfus_op( phi_h );
      return phi; 
    };
  }


