// ----------------------- Staggered Functions ------------------------------------

template <Dense_array A>
  decltype(auto) stgr_to_clpt( const A& a )
  {
    A s = 0.5 * ( a( slice( first(), last()-1 ), all(), all() )
                + a( slice( first()+1, last() ), all(), all() ) );
    return s;
  }

template <typename T, typename O>
  decltype(auto) make_clpt_to_stgr( O&& d0c_h )
  {
    return [&]( auto&& p )
    {
      auto ksi0_s  = d0c_h.data_gen().diff_basis().coords();
      T dx0_btm = ksi0_s[ first()+1 ] - ksi0_s[ first()  ];
      T dx0_top = ksi0_s[ last()    ] - ksi0_s[ last()-1 ];

      auto p_d0 = make_callback_array( d0c_h( p ) );
      d_arr<T, 2> p_d0_btm = p_d0( first(), all(), all() );
      d_arr<T, 2> p_d0_top = p_d0( last(),  all(), all() );

      d_arr<T, 2> p_btm = p( first(), all(), all() ) - dx0_btm * p_d0_btm;
      d_arr<T, 2> p_top = p( last(),  all(), all() ) + dx0_top * p_d0_top;

      auto shp = p.extents();
      d_arr<T, 3> ps( {shp[0]+1, shp[1], shp[2]} );
   
      ps( slice( first()+1, last()-1 ), all(), all() ) = 
          0.5 * ( p( slice( first()+1, last() ), all(), all() )
                + p( slice( first(), last()-1 ), all(), all() ) );
   
      ps( first(), all(), all() ) = 0.5 * ( p_btm + p( first(), all(), all() ) );
      ps( last() , all(), all() ) = 0.5 * ( p_top + p( last(),  all(), all() ) );

      return ps;
    };
  }


