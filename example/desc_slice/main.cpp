#include <iostream>
#include <type_traits>
#include <tuple>
#include <complex>
#include <vector>
#include <algorithm>

#include <spx/spx.h>

using namespace std;
using namespace spx;

struct F
{
  //template <typename B, typename L>
  //B index( B b, L l );
  //template <typename T, typename U>
  //int index( T, U ) = delete;  
  explicit operator bool() { return true;}
  int index( int b, std::size_t l );

};

int trans( int u ) { return u; }

template <typename P, typename F>
  uni_wrap_pos<P, F>
  make_uni_wrap_pos( P&& p, F&& f )
  {
    return uni_wrap_pos<P, F>( std::forward<P>(p), std::forward<F>(f) );
  }

template <Indexable Args>
  void foo( Args ) {}

template <Input_range T>
  void foo2( T ) {}

struct R
{
  static constexpr std::size_t rank() { return 2; }
};

struct DESC : row_major<2>
{
  using size_type = std::size_t;
  using index_type = std::size_t;
  static constexpr size_type rank() { return 2; }
  size_type extent( size_type ) { return 0; }
  size_type stride( size_type ) { return 0; }
  index_type lbound( size_type ) { return 0; }
};


int main()
{
  //using T = decltype(idx_slice({1,2,5,6,8}));
  //Main_type<T> t;
  //cout << Unsigned_type<Main_type<decltype(t.stride()[0])>>() << endl;
  //cout << boolalpha << Nonuniform_id_space<decltype(idx_slice({1,2,5,6,8}))>() << endl;

  cout << boolalpha << Named_position<const F&>() << endl;
  cout << boolalpha << Can_be_signed<int>() << endl;
  cout << boolalpha << Uniform_id_space<uniform_id_space<int, int>>() << endl;

  auto trans2 = []( int u ) -> int { return u; };
  using T = decltype(make_uni_wrap_pos(first(), trans));
  cout << Named_position<T>() << endl;
  auto f = first();
  auto p = make_uni_wrap_pos(f, trans2);
  cout << Named_position<decltype(p)>() << endl;
  cout << p.index( -1, 10 ) << endl;
  auto p1 = period( f + last() );
  cout << p1.index( 0, 10 ) << endl;
  auto p2 = period( 42 );
  cout << p2.index( 0, 10 ) << endl;  

  auto pp1 = last() + 2;
  auto pp2 = 2 + last();
  auto pp3 = last() + last();
  auto pp4 = 2 + std::size_t{4};
  cout << pp1.index(0, 10) << endl;
  cout << pp2.index(0, 10) << endl;
  cout << pp3.index(0, 10) << endl;
  cout << typestr<decltype(pp4)>() << endl;
  
  auto v = idx_slice( std::vector<int>{1,2,5,6,8} );
  //auto v = idx_slice(17);
  auto a = any_major<3>::create_by_default<column_major>();
  cout << boolalpha << Descriptor<DESC>() << endl;
  
  c_descriptor<2> desc( {100,50} );
  cout << "Nonuniform desc = " << boolalpha << Nonuniform_descriptor<decltype(desc)>() << endl;
  cout << "size = " << desc.size() << endl;
  
  decltype(auto) ID = desc.index_of( 180 );
  for( auto& v : ID )
    cout << "I = " << v << endl;
  
  cout << descriptor_slice::get( desc, 2, 2 ) << endl;
  cout << desc( 2, 2 ) << endl;
  //foo( 1 );
  foo( std::vector<int>{1,2,3} );
  
  cout << desc( 2, last() ) << endl;
  using D = c_descriptor<1>;
  using S = sliced_type<D, int>;
  cout << typestr<S>() << endl;
  
  flex_descriptor<2, int, row_major> f_desc_1( {-1,-1}, {100,50} );
  flex_descriptor<2, int, row_major> f_desc_2( vector<int>{-1,-1}, vector<size_t>{100,50} );
  cout << "Flex Desc = " << boolalpha << Flex_descriptor<decltype(f_desc_2)>() << endl; // == true
  
  basic_descriptor<2, -1, row_major> b_desc_1( {100,50} );
  cout << b_desc_1( 2, 2 ) << endl;
  
  cout << boolalpha << Indexable_range<vector<int>>() << endl;
  
  using D1 = c_descriptor<4>;
  //using N1 = nonuniform_descriptor<D1>;
  
  using S1 = sliced_type<D1, int, slice_all, int, slice_all>;
  cout << typestr<S1>() << endl;

  using S2 = sliced_type<D1, vector<int>>;
  cout << typestr<S2>() << endl;
  
  using C1 = c_descriptor<4>;
  C1 c1( {50, 100, 150, 200} );
  decltype(auto) c1_s1 = c1( slice( first()+3, last()), slice(4,10), slice_all(), slice_all() );
  cout << typestr<decltype(c1_s1)>() << endl;
  cout << boolalpha << Nonuniform_id_space<decltype(idx_slice({1,2,5,6,8}))>() << endl;
  decltype(auto) c1_s2 = c1( slice( first()+3, last()), slice(4,10), slice_all(), idx_slice({1,2,5,6,8}) );
  cout << typestr<decltype(c1_s2)>() << endl;
  
  cout << "TRY flex_descriptor" << endl;
  using F1 = flex_descriptor<4, int, row_major>;
  F1 f1( {-1, -1, -1, -1}, {50, 100, 150, 200} );
  decltype(auto) f1_s1 = f1( slice( first()+3, last()), slice(4,10), slice_all(), slice_all() );
  cout << typestr<decltype(f1_s1)>() << endl;
  decltype(auto) f1_s2 = f1( slice( first()+3, last()), slice(4,10), slice_all(), idx_slice({1,2,5,6,8}) );
  cout << typestr<decltype(f1_s2)>() << endl;
  
  cout << "TRY nonuniform_descriptor" << endl;
  decltype(auto) n1_s1 = f1_s2( slice( first()+3, last()), slice(4,10), slice_all(), idx_slice({1,2,5,6,8}) );
  cout << typestr<decltype(n1_s1)>() << endl;
  
  using C2 = c_descriptor<4>;
  C2 c2( {50, 100, 150, 200} );
  decltype(auto) c2_s1 = c2( slice( first()+3, last()), slice(4,10), 4, last()-1 );
  cout << typestr<decltype(c2_s1)>() << endl;

  using F2 = flex_descriptor<4, int, row_major>;
  F2 f2( {-1, -1, -1, -1}, {50, 100, 150, 200} );
  decltype(auto) f2_s1 = f2( slice( first()+3, last()), slice(4,10), 4, last()-1 );
  cout << typestr<decltype(f2_s1)>() << endl;
  auto nls = idx_slice({1,2,5,6,8});
  cout << "T1 PRE" << endl;
  decltype(auto) f2_s2 = f2( slice( first()+3, last()), slice(4,10), 4, nls );
  cout << "T1 POST" << endl;
  //cout << typestr<decltype(f2_s2)>() << endl;
  cout << "TRY nonuniform_descriptor" << endl;
  decltype(auto) n1_s2 = f2_s2( 2, slice_all(), slice_all() );
  cout << typestr<decltype(n1_s2)>() << endl;
  decltype(auto) n1_s3 = n1_s2( size_constant<0>(), last() );
  cout << typestr<decltype(n1_s3)>() << endl;

  cout << "TRY Range" << endl;
  using C3 = c_descriptor<4>;
  C3 c3( {50, 100, 150, 200} );
  decltype(auto) c3_s1 = c3( vector<std::size_t>{2,4,5,6} );
  cout << typestr<decltype(c3_s1)>() << endl;
  
  using T1 = std::tuple<spx::uniform_id_space<spx::first, spx::last> >;
  using U1 = spx::uniform_id_space<spx::first, spx::last>;
  cout << boolalpha << Expandable<T1>() << endl;
  cout << boolalpha << Expandable<U1>() << endl;
  cout << boolalpha << Expandable_parameters<T1, U1>() << endl;
  using T2 = tuple_append_type<tuple<vector<int>>, vector<int>>;
  using T3 = tuple_append_type<vector<int>, tuple<vector<int>>>;
  cout << typestr<T2>() << endl;
  cout << typestr<T3>() << endl;
  decltype(auto) c3_s2 = c3( vector<slice_all>{slice_all(),slice_all(),slice_all(),slice_all()} );
  cout << typestr<decltype(c3_s2)>() << endl;
  
  decltype(auto) c3_s3 = c3( size_constant<2>(), last() );
  cout << typestr<decltype(c3_s3)>() << endl;
  
  cout << "Any major" << endl;
  using D2 = basic_descriptor<3, -1, any_major>;
  D2 d2( {100,200,300}, vector<size_t>{2,0,1} );
  decltype(auto) d2_s1 = d2( slice_all(), last()-3, slice(10,20) );
  cout << typestr<decltype(d2_s1)>() << endl;
}
