#include <iostream>
#include <iomanip>
#include <vector>
#include <spx/spx.h>

using namespace std;
using namespace spx;

template <typename T>
  void print( const T& a )
  {
    for( std::size_t i = 0; i < a.extent(0); ++i ) {
      for( std::size_t j = 0; j < a.extent(1); ++j ) {
        std::cout << a(i, j) << "\t";
      }
      std::cout << std::endl;
    }
  }

template <Stencil S>
  void print( const S& stencil )
  {
    for( auto& kv : stencil ) 
    {
      cout << "[";
      for ( auto k : kv.first ) 
        cout << k << ", ";
      cout << "] --> " << kv.second << endl;
    }
  }

int main()
{
  using T = double;
  std::size_t n0 = 8;
  std::size_t n1 = 8;
  auto x = linspace( 1, n1, n1 );

  auto p = make_fd_basis_pbc( x, n1 );
  p.set_diff_order( 2 );
  p.diff_stencil();
  p.print();

  cout << endl; 

  auto q = make_fd_basis( x );
  q.set_diff_order( 2 );
  q.diff_stencil();
  q.print(); 

  std::size_t N = n0;
  T L = pi_2<T>;
  T dx = L / T(N);

  auto r = make_fourier_basis( N, dx  );
  r.set_diff_order( 2 );
  r.print();

  auto r2 = make_fourier_basis( N, dx  );
  r2.set_diff_order( 4 );
  r2.print();

  stencil_1d_to_nd<decltype(p), 2> gen1( {n0, n1}, p, 1 );
  auto d1d1 = make_stencil_array( std::move(gen1) );
 
  stencil_1d_to_nd<decltype(r), 2> gen2( {n0, n1}, r, 0 );
  auto d0d0 = make_stencil_array( std::move(gen2) );
 
  stencil_1d_to_nd<decltype(r2), 2> gen2_1( {n0, n1}, r2, 1 );
  auto  d1d1d1d1 = make_stencil_array( std::move(gen2_1) );
  
  auto e = d0d0 + d1d1;
  stencil_gen<decltype(e)> gen3( e );
  auto lap = make_stencil_array( std::move(gen3) );

  stencil<T, 2> dirchlt;
  dirchlt[ {0, 0} ] = T{1};
  auto gen4 = make_stencil_func_gen<2>( {n0, n1}, [&]( auto& desc, auto id ) { return dirchlt; } );
  auto drch = make_stencil_array( std::move(gen4) );
 
  auto gen5 = make_stencil_selector( 
     []( auto& desc, auto idx, auto it_lap, auto it_drch )
      {    
        if (idx[0] == desc.lbound(0) || idx[0] == desc.ubound(0) 
         || idx[1] == desc.lbound(1) || idx[1] == desc.ubound(1) ) 
        {
          cout << idx[0] << ", " << idx[1] << " --> dirichlet\n";
          return (*it_drch).as_weight_type( std::complex<T>() );
        } 
        else 
        {
          cout << idx[0] << ", " << idx[1] << " --> laplace\n";
          return *it_lap;
        }
      },
      d0d0 + d1d1, 
      drch );
  auto prob = make_stencil_array(std::move(gen5) );

  cout << "CHECK:\n\n";
  for( auto i = 0; i < n0; ++i ) {
    for( auto j = 0; j < n1; ++j ) {
      cout << "(" << i << ", " << j << ")" << endl;
      decltype(auto) stn = prob(i, j);
      print( stn );
      cout << endl;
    }
  }
  cout << "DONE\n";

  return 0;
}

