#include <iostream>
#include <vector>

//#include "../../spx/type/tag_types.h"
//#include "../../spx/type/meta.h"
//#include "../../spx/type/deduction.h"
//#include "../../spx/type/origin_iterator_concepts.h"
#include <spx/spx.h>

using namespace std;
using namespace spx;

int main() 
{
  using T = double;
  size_t N[2] = { 5, 5 };

  auto s = make_sparse_array<T, 2>( N );
  cout << s.size() << endl;

  s.insert_on();
  s( slice_all(), first() ) = 10;
  s.insert_off();

  cout << "non_zeros = " << s.data_gen().elems().size() << endl;

  cout << "check elems\n";
  for( auto v : s )
    cout << v << endl;
  cout << endl;

  cout << "non_zeros = " << s.data_gen().elems().size() << endl;
  return 0;
}
