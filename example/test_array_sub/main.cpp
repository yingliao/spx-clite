#include <iostream>
#include <type_traits>
#include <tuple>
#include <complex>
#include <vector>
#include <algorithm>

#include <spx/spx.h>

using namespace std;
using namespace spx;

template <Descriptor Desc>
  void print( const Desc& desc )
  {
    cout << "data_off = " << desc.data_offset() << endl;
    
    cout << "lbound = [";
    for( std::size_t d = 0; d < Desc::rank(); ++d )
      cout << desc.lbound(d) << ", ";
    cout << "]" << endl;
    
    cout << "extent = [";
    for( std::size_t d = 0; d < Desc::rank(); ++d )
      cout << desc.extent(d) << ", ";
    cout << "]" << endl;
    
    cout << "stride = [";
    for( std::size_t d = 0; d < Desc::rank(); ++d )
      cout << desc.stride(d) << ", ";
    cout << "]" << endl;
  }

int main()
{ 
  using A = c_static_array<int, 4, 5, 6>;
  cout << boolalpha << Multi_array<A>() << endl;
  
  A v1{ 15 };
  const A& v1c = v1;
  
  decltype(auto) v2  = v1( 2, first(), 1 );
  decltype(auto) v2c = v1c( 2, first(), 1 );
  cout << typestr(v2) << endl;  // int& 
  cout << typestr(v2c) << endl; // const int&
  
  decltype(auto) v3  = v1( slice(2,3), first(), slice(0,last()) );
  decltype(auto) v3c = v1c( slice(2,3), first(), slice(0,last()) );
  cout << typestr(v3) << endl;  // spx::array_ref<int      , spx::basic_descriptor<2u, 0, spx::row_major> >&
  cout << typestr(v3c) << endl; // spx::array_ref<int const, spx::basic_descriptor<2u, 0, spx::row_major> >&

//   decltype(auto) v4  = v3( first(), last(), first() );
//   decltype(auto) v4c = v3c( first(), last(), first() );
//   cout << typestr(v4) << endl;  // spx::array_ref<int      , spx::basic_descriptor<2u, 0, spx::row_major> >&
//   cout << typestr(v4c) << endl; // spx::array_ref<int const, spx::basic_descriptor<2u, 0, spx::row_major> >&

  using B = fort_static_array<int, 4, 5, 6>;
  B q1{ 15 };
  decltype(auto) vi = make_dense_iterator( v1.descriptor(), v1.data() );
  decltype(auto) qi = make_dense_iterator( q1.descriptor(), q1.data() );
  
  cout << vi.index()[0] << endl;
  cout << *vi++ << *vi++ << *vi++ << endl;
  cout << *qi++ << *qi++ << *qi++ << endl;
  
  for( auto& v : q1 )
    cout << v << ", ";
  cout << endl;
  
  double sum = 0;
  auto f = [&]( auto& v ) -> auto
           { 
             sum += v; 
             return sum; 
          };
  int a=1, b=4, c=5, d=6;
  //unary_for_each( f, 1, 4, 5, 6 );
  //unary_for_each( f, a, b, c, d );
  const auto& tup = make_tuple(a,b,c,d);
  //cout << tuple_for_each( f, make_tuple(1, 2.4, 3.3f) ) << endl;
  cout << tuple_for_each( f, make_tuple(1, 2.4, 3.3f) ) << endl;
  cout << sum << endl;
  
  int i = 0;
  for( auto& v : q1 )
    v = i++;
  print(q1.descriptor());
  for( auto it2 = q1.dense_begin(); it2 != q1.dense_end(); ++it2 )
      cout << it2.index()[0] << ", " << it2.index()[1] << ", " <<  it2.index()[2] << " --> " << *it2 << endl;

  decltype(auto) q1_s = q1( slice(first()+1,last(),2), slice(first()+1,last(),2), slice(first()+1,last(),2) );
  print(q1_s.descriptor());
  auto it = q1_s.begin();
  for( ; it != q1_s.end(); ++it ) {
      cout << it.index()[0] << ", " << it.index()[1] << ", " <<  it.index()[2] << " --> " << *it << endl;
  }

  decltype(auto) q1_s2 = q1_s( slice(first()+1,last()), slice(first()+1,last()), slice(first()+1,last()) );
  print(q1_s2.descriptor());
  auto it3 = q1_s2.begin();
  for( ; it3 != q1_s2.end(); ++it3 ) {
      cout << it3.index()[0] << ", " << it3.index()[1] << ", " <<  it3.index()[2] << " --> " << *it3 << endl;
  }
  
  cout << "BEFORE rebase" << endl;
  using D = flex_descriptor<3, int, column_major>;
  static_array<int, D, 3, 4, 5> p{17};
  for( auto it = p.dense_begin(); it != p.dense_end(); ++it )
    cout << it.index()[0] << ", " << it.index()[1] << ", " <<  it.index()[2] << " --> " << *it << endl;
  p.rebase( vector<int>{1,1,1} );
  cout << "AFTER rebase" << endl;
  for( auto it = p.dense_begin(); it != p.dense_end(); ++it )
    cout << it.index()[0] << ", " << it.index()[1] << ", " <<  it.index()[2] << " --> " << *it << endl;
  
  cout << "Row / column " << endl;
  for( auto v : q1(size_constant<0>(), first()) )
    cout << v << endl;
}
