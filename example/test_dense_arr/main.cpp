#include <iostream>
#include <type_traits>
#include <tuple>
#include <complex>
#include <vector>
#include <algorithm>
#include <unordered_map>

#include <spx/spx.h>

using namespace std;
using namespace spx;


using T = double;

/*
template <Multi_array A>
  void print( A& arr )
  {
    for( auto& v : arr ) 
      cout << v << "\t";
    cout << endl;
  }
*/

void test_ctor()
{
  c_static_array<T, 12> a{1.2};
  c_dynamic_array<T, 2> c(3, 4);
  c_dynamic_array<T, 2> b( c.extents() );
  b = 1.2;
  b.resize(4,4);
  b = a * a;
  //print( a );
  //print( b );
  
  fort_dynamic_vector<T> d = {1,2,3,4,5,6,7};
  auto iter = d.dense_begin();
  for( ; iter != d.dense_end(); ++iter )
  cout << *iter << endl;
}

int main()
{
  test_ctor();
}
