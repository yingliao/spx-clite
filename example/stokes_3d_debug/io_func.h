// vtk function
//
template <Dense_array A, typename F>
  decltype(auto) make_vec( const A& u0, const A& u1, const A& u2, F&& sg_to_cl )
  {
    using T = Value_type<A>;
    using S = static_vector<T, 3>;

    A u0o( u1.extents() );
    if( u0.extents()[0] == u1.extents()[0] - 1 )
    {
      u0o = sg_to_cl( u0 );
      u0o( first(), all(), all() ) = u0( first(), all(), all() );
      u0o( last(),  all(), all() ) = u0( last(),  all(), all() );
    }
    else
      u0o = u0;

    d_arr<S, 3> u( u1.extents() );
    u[0] = u0o;
    u[1] = u1;
    u[2] = u2;
    return u;
  }

struct input_parser
{
  using T = double;
  T rho = 0.999103;
  T gma = 75.0;
  T g   = 980.665;

  T nu  = 0.01;
  T lmd = pi_2<T>;

  T ak  = 0.25;
  T eps = 0.16;

  size_t Nz = 128;
  size_t Ny = 128;
  size_t Nx = 64;

  T L0 = pi_2<T>;
  T L1 = lmd;
  T L2 = 0.5*lmd;

  T cfl   = 0.4;
  T n_prd = 6.0;

  input_parser( string fname ) 
  { 
    parse( fname ); 
  }

private:
  template <typename T>
  requires Same<T, double>()
    decltype(auto) parse_data( string line, T )
    {
      return stod( line );
    }

  template <typename T>
  requires Same<T, size_t>()
    decltype(auto) parse_data( string line, T )
    {
      return stod( line );
    }

  void parse( string fname )
  {
    ifstream inf( fname );

    auto read = [&]( auto t )
    {
      string line;
      while( getline( inf, line ) )  
      {
        if( line[0] == '#' )
          continue;
       line. erase( remove_if( line.begin(), 
                               line.end(), 
                               []( auto c ) { return isspace(c); } ), 
                    line.end() );
        if( line.empty() )
          continue;
        break;
      }
      return this->parse_data( line, t );
    };

    rho = read( T() );
    gma = read( T() );
    g   = read( T() );
    nu  = read( T() );
    lmd = read( T() );
    ak  = read( T() );
    eps = read( T() );

    Nz  = read( size_t() );
    Ny  = read( size_t() );
    Nx  = read( size_t() );

    L0  = read( T() );
    L1  = read( T() );
    L2  = read( T() );

    cfl   = read( T() );
    n_prd = read( T() );

    inf.close();
  }

};
