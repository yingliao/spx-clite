#include <iostream>
#include <type_traits>
#include <tuple>
#include <complex>
#include <vector>
#include <algorithm>

#include <spx/spx.h>

using namespace std;
using namespace spx;

template <Range R>
  void print( R&& r )
  {
    for( auto v : r )
      cout << v << ", ";
    cout << endl;
  }

decltype(auto) f()
{
  using M = static_vector<std::complex<double>, 100000>;
  M v1{ {12.4, 12.6} };
  for( int i = 0; i < 4; ++i )
    v1 += v1;
  return v1;
}

decltype(auto) g()
{
  using M = static_vector<std::complex<double>, 4>;
  M v1{ {12.4, 12.6} };
  for( int i = 0; i < 100000; ++i )
    v1 += v1;
  return v1;
}

int main()
{
  static_vector<int, 4> v1{ 1,2,4,5 };
  static_vector<int, 4> v2( 17 );
  static_vector<int, 4> v3( vector<int>{1,2,4,5} );
  static_vector<int, 4> v1copy = v1;
  
  v1 += 12.7;                 print(v1);
  v1 += vector<int>{1,2,4,5}; print(v1);
  v1 += v3;                   print(v1);
  v1 += {1,2,4,5};            print(v1);

  v1 = 12.7;                 print(v1);
  v1 = vector<int>{7,7,7,7}; print(v1);
  v1 = v3;                   print(v1);
  v1 = {8,8,8,8};            print(v1);

  cout << boolalpha << Binary_assignable_operation<ops::plus, int, int>() << endl;
  
  decltype(auto) rr1 = v1 + complex<int>();        print(rr1);
  decltype(auto) rr2 = v1 + vector<int>{2,4,2,4};  print(rr2);
  decltype(auto) rr3 = v1 + v3;                    print(rr3);

  decltype(auto) rr4 = complex<int>() + v1;        print(rr4);
  decltype(auto) rr5 = vector<int>{2,4,2,4} + v1;  print(rr5);
  decltype(auto) rr6 = v3 + v1;                    print(rr6);

  millisec_timer timer;
  int test_around = 100;
  complex<double> a;
  
  double avg_time1 = 0;
  for( int i = 0; i < test_around; ++i ) 
  {
    decltype(auto) r = timer( f );
    a += *begin(r);
    avg_time1 += timer.get_task_time();
    timer.reset();
  }
  
  timer.reset();
  
  double avg_time2 = 0;
  for( int i = 0; i < test_around; ++i ) 
  {
    decltype(auto) r = timer( g );
    a += *begin(r);
    avg_time2 += timer.get_task_time();
    timer.reset();
  }
  
  cout << "large vector + small loop = " << avg_time1 / double(test_around) << endl;
  cout << "small vector + large loop = " << avg_time2 / double(test_around) << endl;

  cout << a << endl;
  //print( r );

  static_vector<c_static_array<double, 3, 3, 3>, 10000> v;
  cout << sizeof(v) << endl;
  static_vector<double, 27*10000> vk;
  cout << sizeof(vk) << endl;
  
  auto incr = [](auto x) { return x++; }; 
}