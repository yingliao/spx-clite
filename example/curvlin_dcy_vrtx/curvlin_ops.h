#ifndef SPX_CURVLIN_OPS_H
#define SPX_CURVLIN_OPS_H

namespace spx
{

// curvlinear operator - base class
//
template <typename G>
  struct curvlin_ops_base
  {
  private:
    G grd;
  
  public:
    curvlin_ops_base( G&& g )
      : grd( std::forward<G>(g) )
    {}
    
    decltype(auto) crv_grd() const { return grd; }
    decltype(auto) crv_grd()       { return grd; }
  };

// curvlinear operator - CONVECTION
//
template <typename G, Dense_array A, Stencil_array O0, Stencil_array... O>
requires Vector<Value_type<A>>()
  struct curvlin_conv : curvlin_ops_base<G>
  {
    using base_t = curvlin_ops_base<G>;

  private:
    static constexpr auto D = sizeof...(O);

    A  conv_u;
    O0 dof;
    std::tuple<O...> dksi_tup;

  public:
    curvlin_conv( G&& g, const A& init_u, O0&& on_node, O&&... op )
      : base_t( std::forward<G>(g) ),
        conv_u( init_u ),
        dof( std::forward<O0>( on_node ) ),
        dksi_tup( std::forward_as_tuple( std::forward<O>(op)... ) )
    {}

    void set_contra_vel( const A& u )
    {
      conv_u = u;
    }

    decltype(auto) contra_vel( std::size_t d )
    {
      return conv_u[d];
    }

    decltype(auto) make_stencils()
    {
      return make_stencils( size_constant<0>() );
    }

    template <Dense_array X>
      decltype(auto) operator()( X&& x )
      {
        auto op = make_stencils();
        X r = op( x );
        return r;
      }

  private:
    template <std::size_t K>
      decltype(auto) make_stencils_term( size_constant<K> )
      {
        decltype(auto) j      = this->crv_grd().jacobian();
        decltype(auto) uk     = this->contra_vel( K );
        decltype(auto) dksi_k = std::get<K>( dksi_tup );
        return dksi_k( j * uk * dof );
      }

    template <std::size_t I>
    requires (I == D-1)
      decltype(auto) make_stencils( size_constant<I> )
      {
        return make_stencils_term( size_constant<I>() );
      }

    template <std::size_t I>
    requires (I < D-1)
      decltype(auto) make_stencils( size_constant<I> )
      {
        return make_stencils_term( size_constant<I>() )
             + make_stencils( size_constant<I+1>() );
      }
  };

template <typename G, Dense_array A, Stencil_array... O>
requires Vector<Value_type<A>>()
  decltype(auto) make_curvlin_conv( G&& g, const A& init_u, O&&... op )
  {
    using T   = Value_type<Main_type<G>>;
    auto  dof = make_on_node<T>( init_u.descriptor() );
    using O0  = decltype(dof);
    return curvlin_conv<G, A, O0, O...>( std::forward<G>(g),
                                         init_u,
                                         std::move( dof ),
                                         std::forward<O>(op)... );
  }

} // namespace spx

#endif // SPX_CURVLIN_OPS_H
