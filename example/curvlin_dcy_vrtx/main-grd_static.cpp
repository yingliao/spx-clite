#include <iostream>
#include <fstream>
#include <spx/spx.h>

#include "curvlin_ops.h"

using namespace std;
using namespace spx;

template <Vector V>
  void extra_polate( V& v )
  {
    if( v.size() == 1 )
      v[0] = 0;
    else if( v.size() == 2 )
      v[0] = v[1];
    else if( v.size() == 3 )
      v[0] = 2.0 * v[1] - v[2];
    else if( v.size() == 4 )
      v[0] = 3.0 * v[1] - 3.0 * v[2] + v[3];
  }

template <Dense_array A>
  decltype(auto) to_freq( const A& u )
  {
    auto U = fft( u, 1 );
         U = fft( U, 2 );
    return U;
  }

template <Dense_array A>
  decltype(auto) to_real( const A& U )
  {
    using R = Value_type<A>; // complex type
    using T = Value_type<R>;
    constexpr auto D = U.rank();

    auto iU = ifft(  U, 1 );
         iU = ifft( iU, 2 );
    d_arr<T, D> u = real( iU );
    return u;
  }

template <Dense_array A, typename G>
  decltype(auto) to_contra_vel( G&& crv_grd, const A& u0, const A& u1, const A& u2 )
  {
    constexpr auto D = crv_grd.rank();
    using T = Value_type<A>;
    using S = static_vector<T, D>;

    decltype(auto) dksi_dx = crv_grd.b_contra();
    d_arr<S, D> u_con( u0.extents() );
    for( std::size_t k = 0; k < D; ++k )
    {
      d_arr<T, D> uk = u0 * dksi_dx[k][0] 
                     + u1 * dksi_dx[k][1] 
                     + u2 * dksi_dx[k][2];
      u_con[k] = uk;
    }
    return u_con;
  }

template <Dense_array A, typename G, Stencil_array O0, Stencil_array O1, Stencil_array O2>
  decltype(auto) eval_divergence( const A& u_con, G&& crv_grd, O0&& dksi0, O1&& dksi1, O2&& dksi2 )
  {
    constexpr auto D = u_con.rank();
    using V = Value_type<A>;
    using T = Value_type<V>;

    decltype(auto) j = crv_grd.jacobian();
    d_arr<T, D> div = dksi0( j * u_con[0] ) 
                    + dksi1( j * u_con[1] ) 
                    + dksi2( j * u_con[2] );
    div /= j;
    return div;
  }

template <Dense_array A, typename G, Stencil_array O0, Stencil_array O1, Stencil_array O2>
  decltype(auto) solve_poisson( const A& lap_phi, const A& phi, G&& crv_grd, O0&& dksi0, O1&& dksi1, O2&& dksi2 )
  {
    using R = Value_type<A>;
    using T = Value_type<G>;

    // Numerical Solving
    //
    auto lap = crv_grd.make_laplace( dksi0, dksi1, dksi2 );

    auto on_node = make_on_node<R>( phi.descriptor() );
    auto lhs = make_stencil_array(
        []( auto& desc, auto idx, auto it_lap, auto it_drch )
        {
          if( idx[0] == desc.ubound(0) || idx[0] == desc.lbound(0)) // 0-dir upper/lower surface
          //if( idx[0] == desc.ubound(0) ) // 0-dir upper surface
          {
            return *it_drch;
          }
          else 
          {
            return *it_lap;  
          }
        },
        lap, 
        on_node );
   
    d_arr<R, 3> rhs = lap_phi;
                rhs(  last(), slice_all(), slice_all() ) = phi(  last(), slice_all(), slice_all() ); // upper surface
                rhs( first(), slice_all(), slice_all() ) = phi( first(), slice_all(), slice_all() ); // lower surface

    d_arr<R, 3> phi_n( phi.extents() );
    d_arr<R, 3> tmp_n( phi.extents() );
    phi_n = 0;
    tmp_n = phi_n;

    lsor( lhs, rhs, phi_n, 0, 1.0 );
    T err = std::abs( norm2( tmp_n -= phi_n ) ); 
    cout << "[Iter = 0] Err. L2-Norm = " << err << endl;
    return phi_n;
  }

template <typename F, typename A, typename T>
  decltype(auto) uh_slvr_sor( F&& bc, const A& uh_bc, T tol )
  {
    return [&]( auto& rhs, auto a0, auto& ut, auto beta_0, auto&& op )
    {
      using R = Value_type<A>;
      A u_cp = ut;
      
      auto on_node = make_on_node<R>( u_cp.descriptor() );
      auto lhs = make_stencil_array( a0 * on_node - beta_0 * op );
      auto prb = make_stencil_array(
          []( auto& desc, auto idx, auto it_lhs, auto it_drch )
          {
            if( idx[0] == desc.lbound(0) || idx[0] == desc.ubound(0) )
              return *it_drch;
            else
              return *it_lhs;
          },
          lhs, on_node );

      bc( rhs, uh_bc );
      
      lsor( prb, rhs, ut, 0, 1.0 );
      T err = std::abs( norm2( u_cp-=ut ) );
      u_cp = ut;
      cout << "[Iter = 0] Err. L2-Norm = " << err << endl;
      return u_cp;
    };
  }

template <typename T, typename G, typename O0, typename O1, typename O2>
  decltype(auto) proj_u_func( T dt, T alpha_0, G&& crv_grd, O0&& dksi0, O1&& dksi1, O2&& dksi2 )
  {
    return [&]( std::size_t i, auto& u, auto& phi, bool frwd )
    {
      using A = Main_type<decltype(u)>;
      decltype(auto) dksi_dx = crv_grd.b_contra();
      decltype(auto) j       = crv_grd.jacobian();

      auto grad = dksi0( j * dksi_dx[0][i] * phi )
                + dksi1( j * dksi_dx[1][i] * phi )
                + dksi2( j * dksi_dx[2][i] * phi );

      T s = frwd ? T(-1) : T(1);
      A r = u + s * dt / alpha_0 * grad / j;
      return r;
    };
  }

template <typename T, typename G, typename O0, typename O1, typename O2>
  decltype(auto) proj_u_con_func( T dt, T alpha_0, G&& crv_grd, O0&& dksi0, O1&& dksi1, O2&& dksi2 )
  {
    return [&]( std::size_t q, auto& u_con, auto& phi, bool frwd )
    {
      constexpr auto D = u_con.rank();
      using A = Main_type<decltype(u_con)>;
      using R = Value_type<Value_type<A>>;

      decltype(auto) g = crv_grd.g_contra();
      T s = frwd ? T(-1) : T(1);

      d_arr<R, D> t = g[q][0] * dksi0( phi )
                    + g[q][1] * dksi1( phi )
                    + g[q][2] * dksi2( phi );

      d_arr<R, D> r = u_con[q] + s * dt / alpha_0 * t;
      return r;
    };
  }

template <typename T, typename G, typename O>
  decltype(auto) proj_p_func( T dt, T alpha_0, T beta_0, G&& crv_grd, O&& dfus_op )
  {
    return [&]( auto& phi )
    {
      using A = Main_type<decltype(phi)>;
      decltype(auto) j = crv_grd.jacobian();
      A p = phi - dt * beta_0 / alpha_0 / j * dfus_op( phi );
      return p;
    };
  }

template <typename T, typename G, typename O>
  decltype(auto) proj_phi_func( T dt, T alpha_0, T beta_0, G&& crv_grd, O&& dfus_op )
  {
    return [&]( auto& p, auto& phi_h )
    {
      using A = Main_type<decltype(p)>;
      decltype(auto) j = crv_grd.jacobian();
      A phi = p + dt * beta_0 / alpha_0 / j * dfus_op( phi_h );
      return phi;
    };
  }

// -----------------------  Exact Solution -------------------------------------------

template <typename T>
  decltype(auto) u0_ext_func( T nu )
  {
    return [&]( auto t, auto& x ) {
      constexpr auto D = x.rank();
      d_arr<T, D> u = -cos( x[2] ) * sin( x[0] ) * std::exp( T(-2) * nu * t );
      return u;
    };
  }

template <typename T>
  decltype(auto) u1_ext_func( T nu )
  {
    return [&]( auto t, auto& x ) {
      constexpr auto D = x.rank();
      auto u = make_const_array<D>( T(0), x.extents() );
      return u;
    };
  }

template <typename T>
  decltype(auto) u2_ext_func( T nu )
  {
    return [&]( auto t, auto& x ) {
      constexpr auto D = x.rank();
      d_arr<T, D> u = sin( x[2] ) * cos( x[0] ) * std::exp( T(-2) * nu * t );
      return u;
    };
  }

template <typename T>
  decltype(auto) p_ext_func( T rho, T nu )
  {
    return [&]( auto t, auto& x ) {
      constexpr auto D = x.rank();
      d_arr<T, D> v = cos( T(2) * x[0] ) + cos( T(2) * x[2] );
      d_arr<T, D> p = rho / T(4) * v * std::exp( T(-4) * nu * t );
      return p;
    };
  }

template <size_t I>
requires (I == 2)
void test()
{
  using T = double;
  using R = std::complex<T>;
  using S = static_vector<T, 3>;

  T         H = pi_2<T>;
  size_t N[3] = { 20, 32, 64 };
  T      L[3] = {  H,  pi_2<T>,  pi_4<T> };
  T   xmin[3] = { -H,  0,  0 };
  T     dx[3] = { L[0]/T(N[0]), L[1]/T(N[1]), L[2]/T(N[2]) };
 
  //T        dt = 0.002;
  T       cfl = 0.125;
  T        dt = cfl / (1/dx[0] + 1/dx[2]);
  size_t it_N = 80;

  // phsical params
  //
  T nu  = 1;
  T rho = 1;

  // PBC domain
  //
  auto g     = one_sided_compression<T>();
  //auto x0    = g( xmin[0], xmin[0]+L[0], N[0] );
  auto x0    = linspace( xmin[0], xmin[0]+L[0],       N[0] );
  auto x1    = linspace( xmin[1], xmin[1]+L[1]-dx[1], N[1] );
  auto x2    = linspace( xmin[2], xmin[2]+L[2]-dx[2], N[2] );

  auto rec_grd = make_rectlin_grid( x0, x1, x2 );
  auto grd_eta = make_rectlin_grid( x1, x2 );
  auto crd_eta = grd_eta.coords();
  
  T k1  = 2 * pi_2<T> / L[1];
  T k2  = 4 * pi_2<T> / L[2];
  T sig = 0.1 * pi_2<T> / 0.01;
  auto eta_f = [&]( auto t )
  {
    d_arr<T, 2> eta = 0.3 * sin( k1 * (crd_eta[0]-xmin[0]) - sig * t ) 
                          * cos( k2 * (crd_eta[1]-xmin[1]) - sig * t );
    eta = 0;
    return eta;
  };

  // curvlinear grid & operators
  //
  auto ksi_b1_o1 = make_fourier_basis( N[1], dx[1] / L[1] * pi_2<T>, 1 );
  auto ksi_b2_o1 = make_fourier_basis( N[2], dx[2] / L[2] * pi_2<T>, 1 );

  auto ksi0      = x0;
  auto ksi1      = ksi_b1_o1.coords(); 
  auto ksi2      = ksi_b2_o1.coords(); 
  
  //auto crvgrd_b0 = make_chebyshev_basis( x0.size() );
  auto crvgrd_b0 = make_fd_basis( ksi0, 1, 4 ); // 1-st order diff & 4-th order accuracy
  auto crvgrd_b1 = make_fd_basis( ksi1, 1, 4 ); // 1-st order diff & 4-th order accuracy
  auto crvgrd_b2 = make_fd_basis( ksi2, 1, 4 ); // 1-st order diff & 4-th order accuracy
  
  auto crvgrd_dksi0 = make_stencil_array<3>( N, crvgrd_b0, 0 );
  auto crvgrd_dksi1 = make_stencil_array<3>( N, crvgrd_b1, 1 );
  auto crvgrd_dksi2 = make_stencil_array<3>( N, crvgrd_b2, 2 );
 
  auto dksi1 = make_stencil_array<3>( N, ksi_b1_o1, 1 );
  auto dksi2 = make_stencil_array<3>( N, ksi_b2_o1, 2 );
  auto dksi0 = crvgrd_dksi0;

  auto crv_grd = make_curvilin_dyn_grid( dt, crvgrd_dksi0, crvgrd_dksi1, crvgrd_dksi2 );

  // initial grid
  //
  auto eta0 = eta_f( 0 );
  auto crd0 = rec_grd.perturb_surface( eta0, 0 );
  crv_grd.update( crd0 );

  // exact solution as IC & BC
  //
  auto p_ext  = p_ext_func( rho, nu );
  auto u0_ext = u0_ext_func( nu );
  auto u1_ext = u1_ext_func( nu );
  auto u2_ext = u2_ext_func( nu );

  // initial condition
  //
  d_arr<T, 3> u0 = u0_ext( 0, crd0 );
  d_arr<T, 3> u1 = u1_ext( 0, crd0 );
  d_arr<T, 3> u2 = u2_ext( 0, crd0 );

  auto U0 = to_freq( u0 );
  auto U1 = to_freq( u1 );
  auto U2 = to_freq( u2 );

  auto u_con = to_contra_vel( crv_grd, u0, u1, u2 );

  // time-stacking size
  //
  // t = 0: current solution
  // t = 1 ~ M-1: history
  //
  constexpr size_t M = 2;
  using A = d_arr<R, 3>;
  vector<A> PHI( M );
  for( size_t t = 0; t < M; ++t )
  {
    PHI[ t ] = A( N );
    PHI[ t ] = 0;
  }

  // initial pressure
  //
  A P = to_freq( p_ext( 0, crd0 ) );
  
  // frac coef
  //
  using trns_ts = bdf1<T>;
  using dfus_ts = am2<T>;
  using conv_ts = ab2<T>;
  auto  alpha_0 = trns_ts().coef_u()[0];
  auto  beta_0  = dfus_ts().coef_f()[0];
 
  auto lap_j    = crv_grd.make_laplace_j( dksi0, dksi1, dksi2 );
  auto dfus_op  = make_stencil_array( nu * lap_j ); 

  auto conv_grd = make_curvlin_conv( crv_grd, 
                                     crv_grd.grid_vel_contra(), 
                                     dksi0, dksi1, dksi2  );

  auto conv_u   = make_curvlin_conv( crv_grd, 
                                     u_con,
                                     dksi0, dksi1, dksi2  );
 
  auto conv_op  = [&]( auto& x ) 
  {
    using V   = Value_type<decltype(x)>; 
    auto op_w = conv_grd.make_stencils();
    auto op_u = conv_u.make_stencils();
    d_arr<V, 3> r = -( op_u(x) - op_w( x ) );
    return r;
  };
  
  cout << "Preparing Momentum Equation - 0\n";

  auto mom_0 = make_multi_stepper_transcoef( dt, trns_ts(), U0, crv_grd.jacobian(),
                                                 dfus_ts(), dfus_op,
                                                 conv_ts(), conv_op );

  cout << "Preparing Momentum Equation - 1\n";

  auto mom_1 = make_multi_stepper_transcoef( dt, trns_ts(), U1, crv_grd.jacobian(),
                                                 dfus_ts(), dfus_op,
                                                 conv_ts(), conv_op );

  cout << "Preparing Momentum Equation - 2\n";

  auto mom_2 = make_multi_stepper_transcoef( dt, trns_ts(), U2, crv_grd.jacobian(),
                                                 dfus_ts(), dfus_op,
                                                 conv_ts(), conv_op );

  // project function
  //
  auto proj_u     = proj_u_func( dt, alpha_0, crv_grd, dksi0, dksi1, dksi2 );
  auto proj_u_con = proj_u_con_func( dt, alpha_0, crv_grd, dksi0, dksi1, dksi2 );
  auto proj_phi   = proj_phi_func( dt, alpha_0, beta_0, crv_grd, dfus_op );
  auto proj_p     = proj_p_func( dt, alpha_0, beta_0, crv_grd, dfus_op );

  // BC function
  //
  auto bc = []( auto& a, auto& b )
  {
    // drch-BC on upper / lower
    a( first(), slice_all(), slice_all() ) = b( first(), slice_all(), slice_all() );
    a( last(),  slice_all(), slice_all() ) = b( last(),  slice_all(), slice_all() );
  };

  // VTK write out
  //
  vtk_writer out_grd( "out/out_grd_" );
  auto vtk = [&]( auto& out, auto& grd )
  {
    cout << "write VTK\n";

    d_arr<S, 3> u( u0.extents() );
    u[0] = u0;
    u[1] = u1;
    u[2] = u2;

    out.next( grd );
    out.name( "u" )       << u;
    out.name( "u_con" )   << u_con; 
    out.name( "p" )       << to_real( P );
    out.name( "J" )       << crv_grd.jacobian();
    //out.name( "w" )       << crv_grd.grid_vel();
    //out.name( "w_con" )   << crv_grd.grid_vel_contra();
  };

  vtk( out_grd, crd0 );

  cout << "Ready to Go!!\n";

  for( std::size_t i = 0; i < it_N; ++i )
  {
    T tn = T(i) * dt;
    cout << "i = " << i << "\t t = " << tn << endl;

    // advance grid
    //
    auto eta = eta_f( tn );
    auto crd = rec_grd.perturb_surface( eta, 0 );
    crv_grd.advance( crd );

    // update grid convection 
    //
    auto w_con = crv_grd.grid_vel_contra();
    conv_grd.set_contra_vel( w_con );

    // extra-polation for phi & p
    //
    extra_polate( PHI );

    // project to u_hat
    //
    auto U0_ext = to_freq( u0_ext( tn, crd ) );
    auto U1_ext = to_freq( u1_ext( tn, crd ) );
    auto U2_ext = to_freq( u2_ext( tn, crd ) );
    auto UH0_bc = proj_u( 0, U0_ext, PHI[0], false ); // backward 
    auto UH1_bc = proj_u( 1, U1_ext, PHI[0], false ); // backward 
    auto UH2_bc = proj_u( 2, U2_ext, PHI[0], false ); // backward 

    // u_hat solvers - SOR
    //
    T tol = 1.e-7 * sqrt( T(U0.size()) );
    auto UH0_slvr = uh_slvr_sor( bc, UH0_bc, tol );
    auto UH1_slvr = uh_slvr_sor( bc, UH1_bc, tol );
    auto UH2_slvr = uh_slvr_sor( bc, UH2_bc, tol );

    // solve momentum eqs. for u_hat
    //
    cout << "Solving Momemtum Equations - U_hat_0 \n";
    auto UH0 = mom_0.solve( UH0_slvr );
    cout << "Solving Momemtum Equations - U_hat_1 \n";
    auto UH1 = mom_1.solve( UH1_slvr );
    cout << "Solving Momemtum Equations - U_hat_2 \n";
    auto UH2 = mom_2.solve( UH2_slvr );
 
    // divergence of u_hat
    //
    auto UH_CON = to_contra_vel( crv_grd, UH0, UH1, UH2 );
    auto div_UH = eval_divergence( UH_CON, crv_grd, dksi0, dksi1, dksi2 );

    // prepapre phi - lhs / rhs / bc
    //
    auto P_ext  = to_freq( p_ext( tn, crd ) );
    auto PHI_bc = proj_phi( P_ext, PHI[0] );
    d_arr<R, 3> PHI_rhs = alpha_0 / dt * div_UH;

    // solve PHI
    //
    cout << "Solving Laplace PHI\n";
    PHI[0] = solve_poisson( PHI_rhs, PHI_bc, crv_grd, dksi0, dksi1, dksi2 );
    
    // update p & u
    //
    cout << "Updating p & u\n";
    P  = proj_p( PHI[0] );
    U0 = proj_u( 0, UH0, PHI[0], true ); // forward
    U1 = proj_u( 1, UH1, PHI[0], true ); // forward 
    U2 = proj_u( 2, UH2, PHI[0], true ); // forward 
    u0 = to_real( U0 );
    u1 = to_real( U1 );
    u2 = to_real( U2 );

    // update flow convection
    //
    cout << "Updating convection velocity\n";
    auto U_CON_0 = proj_u_con( 0, UH_CON, PHI[0], true ); // forward
    auto U_CON_1 = proj_u_con( 1, UH_CON, PHI[0], true ); // forward
    auto U_CON_2 = proj_u_con( 2, UH_CON, PHI[0], true ); // forward
    u_con[0] = to_real( U_CON_0 );
    u_con[1] = to_real( U_CON_1 );
    u_con[2] = to_real( U_CON_2 );
    conv_u.set_contra_vel( u_con );

    // roll-back
    //
    cout << "Rolling back\n";
    mom_0.roll_back( U0, crv_grd.jacobian() );
    mom_1.roll_back( U1, crv_grd.jacobian() );
    mom_2.roll_back( U2, crv_grd.jacobian() );
    for( size_t n = M-1; n >= 1; --n )
      PHI[ n ] = PHI[ n-1 ];
 
    vtk( out_grd, crd );
 }
}

int main()
{
  test<2>();
  return 0;
}
