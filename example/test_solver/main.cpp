#include <iostream>
#include <iomanip>
#include <vector>
#include <spx/spx.h>

using namespace std;
using namespace spx;

template <typename T>
  void print( const T& a )
  {
    for( std::size_t i = 0; i < a.extent(0); ++i ) {
      for( std::size_t j = 0; j < a.extent(1); ++j ) {
        std::cout << a(i, j) << "\t";
      }
      std::cout << std::endl;
    }
  }

template <Stencil S>
  void print( const S& stencil )
  {
    for( auto& kv : stencil ) 
    {
      cout << "[";
      for ( auto k : kv.first ) 
        cout << k << ", ";
      cout << "] --> " << kv.second << endl;
    }
  }

int main()
{
  using T = double;
  std::size_t n0 = 5;
  std::size_t n1 = 5;
  auto x = linspace( 1, n1, n1 );

  auto q = make_fd_basis( x, 2 );

  auto d0d0 = make_stencil_array<2>( { n0, n1 }, q, 0 );
  auto d1d1 = make_stencil_array<2>( { n0, n1 }, q, 1 ); 
  auto lap  = make_stencil_array( d0d0 + d1d1 );

  auto drch = make_on_node<T, 2>( {n0, n1} ); 
  auto prob = make_stencil_array( 
      []( auto& desc, auto idx, auto it_lap, auto it_drch )
      {   
        if (idx[0] == desc.lbound(0) || idx[0] == desc.ubound(0) 
         || idx[1] == desc.lbound(1) || idx[1] == desc.ubound(1) ) 
        {
          return *it_drch; 
        } 
        else 
        {
          return *it_lap;
        }
      },
      lap, 
      drch );

  //for( auto i = 0; i < n0; ++i ) {
  //  for( auto j = 0; j < n1; ++j ) {
  //    cout << "(" << i << ", " << j << ")" << endl;
  //    decltype(auto) stn = prob(i, j);
  //    print( stn );
  //    cout << endl;
  //  }
  //}

  d_arr<T, 2> u{ n0, n1 };
  size_t i = 0;
  for( auto& v : u )
    v = T( i++ );

  //print( u ); 
  //cout << endl << endl;

  d_arr<T, 2> rhs = prob(u);
  //print( rhs );

  d_arr<T, 2> u_num{ n0, n1 };

  auto iterate = [&]( auto f ) 
  {
    std::size_t max_it = 300;
    
    cout << endl;
    for( std::size_t n = 0; n < max_it; ++n ) 
    {
      f();
      T err = norm2( d_arr<T, 2>( u_num - u ) );
      cout << "[Iter = " << n << "] Err-Norm2 = " << err << endl;
      //print( u_num );
      //cout << endl;
    }
    cout << endl;
  };

  //cout << "Jacobi: " << endl;
  //u_num = T{0};
  //iterate( [&](){ jacobi( prob, rhs, u_num ); } );
  //
  //cout << "Point Gauss-Seidel: " << endl;
  //u_num = T{0};
  //iterate( [&](){ point_gauss_seidel( prob, rhs, u_num ); } );
 
  //cout << "Point Successive over-Relaxation (PSOR): " << endl;
  //u_num = T{0};
  //iterate( [&](){ psor( prob, rhs, u_num, 1.1 ); } );

  //cout << "Line Gauss-Seidel: " << endl;
  //u_num = T{0};
  //iterate( [&](){ line_gauss_seidel( prob, rhs, u_num, 0 ); } );
  
  //cout << "Line Successive over-Relaxation (LSOR): " << endl;
  //u_num = T{0};
  //iterate( [&](){ lsor( prob, rhs, u_num, 0, 1.05 ); } );
  
  //cout << "Alternating Direction Implicit method (ADI): " << endl;
  //u_num = T{0};
  //iterate( [&](){ adi( prob, rhs, u_num, 1.05 ); } );
 
  cout << "Parallel Line Successive over-Relaxation (P-LSOR): " << endl;
  u_num = T{0};
  iterate( [&](){ lsor_async( prob, rhs, u_num, 0, 1.05 ); } );

  return 0;
}

