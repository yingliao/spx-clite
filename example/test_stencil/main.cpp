#include <iostream>
#include <type_traits>
#include <tuple>
#include <complex>
#include <vector>
#include <algorithm>
#include <unordered_map>

#include <spx/spx.h>

using namespace std;
using namespace spx;


using T = double;

template <typename S>
  void print( const S& s )
  {
    for( auto& kv : s )
    {
      for( auto k : kv.first )
        cout << k << ", ";
      cout << " ---> " << kv.second << endl;
    }
  }
  
template <typename A, typename... Args>
  void test_subarray( A&&, Args&&... args )
  {
    cout << "sub-arraying = " << boolalpha << Subarray<A, Args...>() << endl;
  }
  
int main()
{
  using ST = stencil<T, 3>;
  cout << boolalpha << Stencil<ST>() << endl;
  //ST s;
  //s.add( { 0, 0, 0}, -6 );
  //s.add( { 1, 0, 0},  1 );
  //s.add( {-1, 0, 0},  1 );
  //s.add( { 0, 1, 0},  1 );
  //s.add( { 0,-1, 0},  1 );
  //s.add( { 0, 0, 1},  1 );
  //s.add( { 0, 0,-1},  1 );
  //
  //s[ {1,2,3} ] = 12;
  //
  //cout << boolalpha << s.use_self() << endl;
  //
  //ST s1;
  //s1[ { 0, 0, 0} ] =  1;
  //s1[ {-1, 0, 0} ] = -1;
  //
  //stencil<complex<T>, 3> s2;
  //s2[ { 1, 0, 0} ] = { 1};
  //s2[ { 0, 0, 0} ] = {-1};
  
  //s1 += s2;
  //print( s1(s2) );
  //print( s2 *= -1 );

  //decltype(auto) s3 = -s2;
  //print( -s2 );
 
  //test_subarray( static_array<T, 3, 3>(), vector<decltype(slice_all())>() );
  cout << boolalpha << Stencil<ST>() << endl;
}
