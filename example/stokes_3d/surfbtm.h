
template <typename T, std::size_t D>
  struct surf_coef_helper
  {
    using value_type = T;
    static constexpr std::size_t rank() { return D; }
    
    d_arr<T, D> eta;
    d_arr<T, D> eta_d1, eta_d1d1, eta_d1_2; 
    d_arr<T, D> eta_d2, eta_d2d2, eta_d2_2; 
    d_arr<T, D> eta_d1d2;

    d_arr<T, D> s00, s01, s02, s11, s22;
    
    d_arr<T, D> a1, b1, c1, d1;
    d_arr<T, D> a2, b2, c2, d2;

    d_arr<T, D> e1, e2, e3, e4, e5, e6, e7, e8;
    d_arr<T, D> f1, f2, f3, f4, f5, f6, f7, f8;

    d_arr<T, D> u0_dksi0, u0_dksi1, u0_dksi2;
    d_arr<T, D> u1_dksi0, u1_dksi1, u1_dksi2;
    d_arr<T, D> u2_dksi0, u2_dksi1, u2_dksi2;

    template <typename G, typename B, 
              typename Q0, typename Q1, typename Q2, typename Q3>
      void update_grd( G&& crv_grd, const B& ETA,
                       Q0&&  eta_dx1, Q1&&  eta_dx1dx1, 
                       Q2&&  eta_dx2, Q3&&  eta_dx2dx2 )
      {
        B ETA_d1   = eta_dx1( ETA );
        B ETA_d1d1 = eta_dx1dx1( ETA );
        B ETA_d2   = eta_dx2( ETA );
        B ETA_d2d2 = eta_dx2dx2( ETA );
        B ETA_d1d2 = eta_dx2( ETA_d1 );

        eta      = to_real( ETA );
        eta_d1   = to_real( ETA_d1 );
        eta_d2   = to_real( ETA_d2 );
        eta_d1d1 = to_real( ETA_d1d1 );
        eta_d2d2 = to_real( ETA_d2d2 );
        eta_d1d2 = to_real( ETA_d1d2 );
        eta_d1_2 = eta_d1 * eta_d1;
        eta_d2_2 = eta_d2 * eta_d2;

        decltype(auto) dksi_dx = crv_grd.b_contra();
        s00 = make_callback_array( dksi_dx[0][0] )( last(), all(), all() );
        s01 = make_callback_array( dksi_dx[0][1] )( last(), all(), all() );
        s02 = make_callback_array( dksi_dx[0][2] )( last(), all(), all() );
        s11 = make_callback_array( dksi_dx[1][1] )( last(), all(), all() );
        s22 = make_callback_array( dksi_dx[2][2] )( last(), all(), all() );

        a1 = T(2) * eta_d1;
        b1 = -eta_d2;
        c1 = T(1) - eta_d1_2;
        d1 = -eta_d1d2;

        e1 = T(2) * a1 * s00 + c1 * s01 + d1 * s02;
        e2 = c1 * s11;
        e3 = d1 * s22;
        e4 = c1 * s00 + b1 * s02;
        e5 = b1 * s22;
        e6 = a1 * s02 + b1 * s01 + d1 * s00;
        e7 = b1 * s11;
        e8 = a1 * s22;

        a2 = T(2) * eta_d2;
        b2 = -eta_d1;
        c2 = T(1) - eta_d2_2;
        d2 = -eta_d1d2;

        f1 = T(2) * a2 * s00 + c2 * s02 + d2 * s01;
        f2 = d2 * s11;
        f3 = c2 * s22;
        f4 = a2 * s01 + b2 * s02 + d2 * s00;
        f5 = a2 * s11;
        f6 = b2 * s22;
        f7 = c2 * s00 + b2 * s01;
        f8 = b2 * s11;
      }

    template <Dense_array A, typename O0, typename O1, typename O2>
      void update_vel( const A& U0, const A& U1, const A& U2,
                       O0&& dksi0,  O1&& dksi1,  O2&& dksi2 )
      {
        using R = Value_type<A>;

        auto U0_dksi0_e = make_callback_array( dksi0( U0 ) );
        auto U0_dksi1_e = make_callback_array( dksi1( U0 ) );
        auto U0_dksi2_e = make_callback_array( dksi2( U0 ) );

        auto U1_dksi0_e = make_callback_array( dksi0( U1 ) );
        auto U1_dksi1_e = make_callback_array( dksi1( U1 ) );
        auto U1_dksi2_e = make_callback_array( dksi2( U1 ) );
        
        auto U2_dksi0_e = make_callback_array( dksi0( U2 ) );
        auto U2_dksi1_e = make_callback_array( dksi1( U2 ) );
        auto U2_dksi2_e = make_callback_array( dksi2( U2 ) );

        d_arr<R, D> U0_dksi0 = U0_dksi0_e( last(), all(), all() );
        d_arr<R, D> U0_dksi1 = U0_dksi1_e( last(), all(), all() );
        d_arr<R, D> U0_dksi2 = U0_dksi2_e( last(), all(), all() );

        d_arr<R, D> U1_dksi0 = U1_dksi0_e( last(), all(), all() );
        d_arr<R, D> U1_dksi1 = U1_dksi1_e( last(), all(), all() );
        d_arr<R, D> U1_dksi2 = U1_dksi2_e( last(), all(), all() );
        
        d_arr<R, D> U2_dksi0 = U2_dksi0_e( last(), all(), all() );
        d_arr<R, D> U2_dksi1 = U2_dksi1_e( last(), all(), all() );
        d_arr<R, D> U2_dksi2 = U2_dksi2_e( last(), all(), all() );
        
        u0_dksi0 = to_real( U0_dksi0 );
        u0_dksi1 = to_real( U0_dksi1 );
        u0_dksi2 = to_real( U0_dksi2 );

        u1_dksi0 = to_real( U1_dksi0 );
        u1_dksi1 = to_real( U1_dksi1 );
        u1_dksi2 = to_real( U1_dksi2 );

        u2_dksi0 = to_real( U2_dksi0 );
        u2_dksi1 = to_real( U2_dksi1 );
        u2_dksi2 = to_real( U2_dksi2 );
      }

    template <typename O>
      void dump( O& out )
      {
        out.name( "a1" ) << a1;
        out.name( "b1" ) << b1;
        out.name( "c1" ) << c1;
        out.name( "d1" ) << d1;
        
        out.name( "a2" ) << a2;
        out.name( "b2" ) << b2;
        out.name( "c2" ) << c2;
        out.name( "d2" ) << d2;
 
        out.name( "e1" ) << e1;
        out.name( "e2" ) << e2;
        out.name( "e3" ) << e3;
        out.name( "e4" ) << e4;
        out.name( "e5" ) << e5;
        out.name( "e6" ) << e6;
        out.name( "e7" ) << e7;
        out.name( "e8" ) << e8;

        out.name( "f1" ) << f1;
        out.name( "f2" ) << f2;
        out.name( "f3" ) << f3;
        out.name( "f4" ) << f4;
        out.name( "f5" ) << f5;
        out.name( "f6" ) << f6;
        out.name( "f7" ) << f7;
        out.name( "f8" ) << f8;

        out.name( "s00" ) << s00;
        out.name( "s01" ) << s01;
        out.name( "s02" ) << s02;
        out.name( "s11" ) << s11;
        out.name( "s22" ) << s22;
      }
  };

// -----------------------  Surface / Bottom BC  -------------------------------------------
 
template <Dense_array A, typename G, typename O0, typename O1, typename O2>
  decltype(auto) surf_dx( const A& U, std::size_t n, 
                          G&& crv_grd, O0&& dksi0, O1&& dksi1, O2&& dksi2 )
  {
    using R = Value_type<A>;
    using T = Value_type<R>;
    constexpr auto D = U.rank();
    decltype(auto) dksi_dx = crv_grd.b_contra();
    d_arr<T, D-1> sf_u_dx;

    if( n == 0 )
    {
      auto U_dksi0_e = make_callback_array( dksi0( U ) );
      d_arr<R, D-1> U_dksi0 = U_dksi0_e( last(), all(), all() );
      d_arr<T, D-1> u_dksi0 = to_real( U_dksi0 );

      auto s00_e = make_callback_array( dksi_dx[0][0] );
      d_arr<T, D-1> s00 = s00_e( last(), all(), all() );

      sf_u_dx = s00 * u_dksi0;
    }
    else if( n == 1 )
    {
      auto U_dksi0_e = make_callback_array( dksi0( U ) );
      d_arr<R, D-1> U_dksi0 = U_dksi0_e( last(), all(), all() );
      d_arr<T, D-1> u_dksi0 = to_real( U_dksi0 );

      auto U_dksi1_e = make_callback_array( dksi1( U ) );
      d_arr<R, D-1> U_dksi1 = U_dksi1_e( last(), all(), all() );
      d_arr<T, D-1> u_dksi1 = to_real( U_dksi1 );

      auto s01_e = make_callback_array( dksi_dx[0][1] );
      auto s11_e = make_callback_array( dksi_dx[1][1] );
      d_arr<T, D-1> s01 = s01_e( last(), all(), all() );
      d_arr<T, D-1> s11 = s11_e( last(), all(), all() );

      sf_u_dx = s01 * u_dksi0 + s11 * u_dksi1;
    }
    else if( n == 2 )
    {
      auto U_dksi0_e = make_callback_array( dksi0( U ) );
      d_arr<R, D-1> U_dksi0 = U_dksi0_e( last(), all(), all() );
      d_arr<T, D-1> u_dksi0 = to_real( U_dksi0 );

      auto U_dksi2_e = make_callback_array( dksi2( U ) );
      d_arr<R, D-1> U_dksi2 = U_dksi2_e( last(), all(), all() );
      d_arr<T, D-1> u_dksi2 = to_real( U_dksi2 );

      auto s02_e = make_callback_array( dksi_dx[0][2] );
      auto s22_e = make_callback_array( dksi_dx[2][2] );
      d_arr<T, D-1> s02 = s02_e( last(), all(), all() );
      d_arr<T, D-1> s22 = s22_e( last(), all(), all() );
      
      sf_u_dx = s02 * u_dksi0 + s22 * u_dksi2;
    }
    return sf_u_dx;
  } 

//template <Dense_array A, typename G, typename O0, typename O1, typename O2>
//  decltype(auto) surf_u0_bc( const A& R0, const A& U1, const A& U2,
//                             G&& crv_grd, O0&& dksi0,  O1&& dksi1,  O2&& dksi2 )
//  {
//    using R = Value_type<A>;
//    using T = Value_type<R>;
//    constexpr auto D = U1.rank();
//
//    decltype(auto) dksi_dx = crv_grd.b_contra();
//    decltype(auto) j       = crv_grd.jacobian();
//
//    auto r0 = to_real( R0 );
//    auto u1 = to_real( U1 );
//    auto u2 = to_real( U2 );
//
//    d_arr<T, D> trm1 = j * dksi_dx[0][0] * r0;
//    d_arr<T, D> trm2 = j * dksi_dx[0][1] * u1;
//    d_arr<T, D> trm3 = j * dksi_dx[1][1] * u1;
//    d_arr<T, D> trm4 = j * dksi_dx[0][2] * u2;
//    d_arr<T, D> trm5 = j * dksi_dx[2][2] * u2;
//
//    auto          e0    = dksi0( trm1 ) - dksi0( trm2 ) - dksi0( trm4 );
//    auto          ee0   = make_callback_array( e0 );
//    d_arr<T, D-1> rhs_0 = ee0( last(), all(), all() );
//    d_arr<R, D-1> RHS_0 = to_freq( rhs_0 );
//    
//    d_arr<R, D>   TRM3  = to_freq( trm3 );
//    d_arr<R, D>   TRM5  = to_freq( trm5 );
//    auto          e1    = dksi1( TRM3 ) - dksi2( TRM5 );
//    auto          ee1   = make_callback_array( e1 );
//    d_arr<R, D-1> RHS_1 = ee1( last(), all(), all() );
//    d_arr<R, D-1> RHS   = RHS_0 - RHS_1;
//
//    return RHS;
//  }

template <Dense_array A, typename G, typename O0, typename O1, typename O2>
  decltype(auto) surf_u0_bc( const A& R0, const A& U1, const A& U2,
                             G&& crv_grd, O0&& dksi0,  O1&& dksi1,  O2&& dksi2 )
  {
    using R = Value_type<A>;
    using T = Value_type<R>;
    constexpr auto D = U1.rank();

    decltype(auto) dksi_dx = crv_grd.b_contra();

    auto s00_e = make_callback_array( dksi_dx[0][0] );
    auto s01_e = make_callback_array( dksi_dx[0][1] );
    auto s11_e = make_callback_array( dksi_dx[1][1] );
    auto s02_e = make_callback_array( dksi_dx[0][2] );
    auto s22_e = make_callback_array( dksi_dx[2][2] );

    d_arr<T, D-1> s00 = s00_e( last(), all(), all() );
    d_arr<T, D-1> s01 = s01_e( last(), all(), all() );
    d_arr<T, D-1> s11 = s11_e( last(), all(), all() );
    d_arr<T, D-1> s02 = s02_e( last(), all(), all() );
    d_arr<T, D-1> s22 = s22_e( last(), all(), all() );

    auto U1_dksi1_e = make_callback_array( dksi1( U1 ) );
    auto U2_dksi2_e = make_callback_array( dksi2( U2 ) );
    auto R0_dksi0_e = make_callback_array( dksi0( R0 ) );
    auto U1_dksi0_e = make_callback_array( dksi0( U1 ) );
    auto U2_dksi0_e = make_callback_array( dksi0( U2 ) );

    d_arr<R, D-1> U1_dksi1 = U1_dksi1_e( last(), all(), all() );
    d_arr<R, D-1> U2_dksi2 = U2_dksi2_e( last(), all(), all() );
    d_arr<R, D-1> R0_dksi0 = R0_dksi0_e( last(), all(), all() );
    d_arr<R, D-1> U1_dksi0 = U1_dksi0_e( last(), all(), all() );
    d_arr<R, D-1> U2_dksi0 = U2_dksi0_e( last(), all(), all() );
    
    auto u1_dksi1 = to_real( U1_dksi1 );
    auto u2_dksi2 = to_real( U2_dksi2 );
    auto r0_dksi0 = to_real( R0_dksi0 );
    auto u1_dksi0 = to_real( U1_dksi0 );
    auto u2_dksi0 = to_real( U2_dksi0 );

    d_arr<T, D-1> rr = r0_dksi0 - ( s01 * u1_dksi0 
                                  + s11 * u1_dksi1 
                                  + s02 * u2_dksi0 
                                  + s22 * u2_dksi2 ) / s00;
    d_arr<R, D-1> RR = to_freq( rr );
    return RR;
  }

template <typename S, Dense_array A, typename O0>
  decltype(auto) surf_u1_bc( const S& sf_coef, const A& R1, O0&& dksi0 )
  {
    using R = Value_type<A>;
    using T = Value_type<S>;
    constexpr auto D = R1.rank();

    auto R1_dksi0_e = make_callback_array( dksi0( R1 ) );
    d_arr<R, D-1> R1_dksi0 = R1_dksi0_e( last(), all(), all() );
    d_arr<T, D-1> r1_dksi0 = to_real( R1_dksi0 );

    d_arr<T, D-1> tmp = sf_coef.e1 * sf_coef.u0_dksi0
                      + sf_coef.e2 * sf_coef.u0_dksi1
                      + sf_coef.e3 * sf_coef.u0_dksi2
                      + sf_coef.e5 * sf_coef.u1_dksi2
                      + sf_coef.e6 * sf_coef.u2_dksi0
                      + sf_coef.e7 * sf_coef.u2_dksi1
                      + sf_coef.e8 * sf_coef.u2_dksi2;

    d_arr<T, D-1> rhs = r1_dksi0 - tmp / sf_coef.e4; 
    auto          RHS = to_freq( rhs );
    return RHS;
  }

template <typename S, Dense_array A, typename O0>
  decltype(auto) surf_u2_bc( const S& sf_coef, const A& R2, O0&& dksi0 )
  {
    using R = Value_type<A>;
    using T = Value_type<S>;
    constexpr auto D = R2.rank();

    auto R2_dksi0_e = make_callback_array( dksi0( R2 ) );
    d_arr<R, D-1> R2_dksi0 = R2_dksi0_e( last(), all(), all() );
    d_arr<T, D-1> r2_dksi0 = to_real( R2_dksi0 );

    d_arr<T, D-1> tmp = sf_coef.f1 * sf_coef.u0_dksi0
                      + sf_coef.f2 * sf_coef.u0_dksi1
                      + sf_coef.f3 * sf_coef.u0_dksi2
                      + sf_coef.f4 * sf_coef.u1_dksi0
                      + sf_coef.f5 * sf_coef.u1_dksi1
                      + sf_coef.f6 * sf_coef.u1_dksi2
                      + sf_coef.f8 * sf_coef.u2_dksi1;

    d_arr<T, D-1> rhs = r2_dksi0 - tmp / sf_coef.f7; 
    auto          RHS = to_freq( rhs );
    return RHS;
  }


template <Dense_array A, Dense_array B>
  decltype(auto) btm_u_bc( const A& R_prj, const B& U )
  {
    using R = Value_type<A>;
    constexpr auto D = R_prj.rank();

    d_arr<R, D-1> R_btm = R_prj( first(), all(), all() );
    d_arr<R, D-1> UH_btm = U + R_btm;
    return UH_btm;
  }


template <typename V,
          Dense_array A, typename O0, typename O1, typename O2, typename G, 
          typename S, typename T>
  decltype(auto) surf_pressure( V& vtk,
                                const A& U0,  const A& U1, const A& U2, 
                                G&& crv_grd,  
                                O0&&  dksi0,  O1&&  dksi1, O2&& dksi2, 
                                const S& sf_coef,
                                T mu, T rho, T g, T gamma )
  {
    constexpr auto D = sf_coef.rank();
    
    // kappa
    //
    d_arr<T, D> tmp1      = T(1) + sf_coef.eta_d1_2;
    d_arr<T, D> tmp2      = T(1) + sf_coef.eta_d2_2;
    d_arr<T, D> kappa_up  = tmp1 * sf_coef.eta_d2d2 
                          + tmp2 * sf_coef.eta_d1d1 
                          - T(2) * sf_coef.eta_d1 * sf_coef.eta_d2 * sf_coef.eta_d1d2;
    d_arr<T, D> kappa_dwn = pow( T(1) + sf_coef.eta_d1_2 + sf_coef.eta_d2_2, 1.5 );
    d_arr<T, D> kappa     = kappa_up / kappa_dwn;
 
    d_arr<T, D> u0_dx0 = surf_dx( U0, 0, crv_grd, dksi0, dksi1, dksi2 );
    d_arr<T, D> u0_dx1 = surf_dx( U0, 1, crv_grd, dksi0, dksi1, dksi2 );
    d_arr<T, D> u0_dx2 = surf_dx( U0, 2, crv_grd, dksi0, dksi1, dksi2 );
    d_arr<T, D> u1_dx0 = surf_dx( U1, 0, crv_grd, dksi0, dksi1, dksi2 );
    d_arr<T, D> u1_dx1 = surf_dx( U1, 1, crv_grd, dksi0, dksi1, dksi2 );
    d_arr<T, D> u1_dx2 = surf_dx( U1, 2, crv_grd, dksi0, dksi1, dksi2 );
    d_arr<T, D> u2_dx0 = surf_dx( U2, 0, crv_grd, dksi0, dksi1, dksi2 );
    d_arr<T, D> u2_dx1 = surf_dx( U2, 1, crv_grd, dksi0, dksi1, dksi2 );
    d_arr<T, D> u2_dx2 = surf_dx( U2, 2, crv_grd, dksi0, dksi1, dksi2 );

    d_arr<T, D> M1  = sf_coef.eta_d1 * ( u0_dx1 + u1_dx0 );
    d_arr<T, D> M2  = sf_coef.eta_d2 * ( u0_dx2 + u2_dx0 );
    d_arr<T, D> M11 = sf_coef.eta_d1_2 * u1_dx1;
    d_arr<T, D> M22 = sf_coef.eta_d2_2 * u2_dx2;
    d_arr<T, D> M12 = sf_coef.eta_d1 * sf_coef.eta_d2 * ( u1_dx2 + u2_dx1 );

    d_arr<T, D> M  = 2.0 * mu / ( 1 + sf_coef.eta_d1_2 );
                M *= u0_dx0 - M1 - M2 + M11 + M12 + M22;
    
    d_arr<T, D> eta = sf_coef.eta;
    d_arr<T, D> p   = M + rho * g * eta - gamma * kappa;

    eta *= rho*g;
    vtk.name( "kappa" )     << kappa;
    vtk.name( "u0_dx0" )    << u0_dx0;
    vtk.name( "u0_dx1" )    << u0_dx1;
    vtk.name( "u0_dx2" )    << u0_dx2;
    vtk.name( "u1_dx0" )    << u1_dx0;
    vtk.name( "u1_dx1" )    << u1_dx1;
    vtk.name( "u1_dx2" )    << u1_dx2;
    vtk.name( "u2_dx0" )    << u2_dx0;
    vtk.name( "u2_dx1" )    << u2_dx1;
    vtk.name( "u2_dx2" )    << u2_dx2;
    vtk.name( "M1" )        << M1;
    vtk.name( "M2" )        << M2;
    vtk.name( "M11" )       << M11;
    vtk.name( "M22" )       << M22;
    vtk.name( "M12" )       << M12;
    vtk.name( "M" )         << M;
    vtk.name( "rho_g_eta" ) << eta;

    return p;
  }

//template <Dense_array A, Dense_array B, typename G, typename T>
//  decltype(auto) btm_phi( G&& crv_grd, const A& UH0, const B& U0_btm, T alpha_0, T dt )
//  {
//    using R = Value_type<A>;
//    constexpr auto D = UH0.rank();
//
//    d_arr<R, D-1> UH0_btm = UH0( first(), all(), all() );
//    d_arr<R, D-1> PHI_btm = alpha_0 / dt * ( UH0_btm - U0_btm );
//    return PHI_btm;
//  }


template <Dense_array A, Dense_array B, typename G, typename T>
  decltype(auto) btm_phi( G&& crv_grd, const A& UH0, const B& U0_btm, T alpha_0, T dt )
  {
    using R = Value_type<A>;
    constexpr auto D = UH0.rank();

    decltype(auto) dksi_dx = crv_grd.b_contra();
    auto           s00     = make_callback_array( dksi_dx[0][0] );
    d_arr<T, D-1>  s00_btm = s00( first(), all(), all() );
 
    d_arr<R, D-1> UH0_btm = UH0( first(), all(), all() );
    d_arr<T, D-1> uh0_btm = to_real( UH0_btm );
    d_arr<T, D-1> u0_btm  = to_real( U0_btm );

    d_arr<T, D-1> phi_btm = alpha_0 / dt / s00_btm * ( uh0_btm - u0_btm );
    d_arr<R, D-1> PHI_btm = to_freq( phi_btm );
    return PHI_btm;
 }
