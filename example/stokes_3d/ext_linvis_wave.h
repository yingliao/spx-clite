
template <typename T>
  struct exct_lin_vis // linear viscous wave
  {
    T lambda = pi_2<T>;
    T k      = pi_2<T> / lambda;
    T ak     = 0.3;
    T a0     = ak / k;
 
    T c;
    T nu;
    T g;
    T gma;
    T omg;
    T bta;

    exct_lin_vis( T re, T g, T gma )
      : g( g ), gma( gma )
    {
      omg = std::sqrt( g * k + gma * k * k * k ); 
      c   = omg / k;
      nu  = c / k / re;
      bta = std::sqrt( omg / T(2) / nu );
      cout << " c  = "  << c  << endl;
      cout << " nu = "  << nu  << endl;
      cout << " k  = "  << k   << endl;
      cout << " a0 = "  << a0  << endl;
      cout << " omg = " << omg << endl;
      cout << " bta = " << bta << endl;
    }

    template <Dense_array A>
      decltype(auto) eta( T t, const A& x )
      {
        constexpr auto D = x.rank();
        d_arr<T, D> x1  = x[0];
        T           c   = T(2) * nu * k * k * t; 
        d_arr<T, D> eta = a0 * exp( -c ) * sin( k * x1 + omg * t );
        return eta;
      }

    template <Dense_array A>
      decltype(auto) u1( T t, const A& x )
      {
        constexpr auto D = x.rank();
        T c = T(2) * nu * k * k * t; 
        d_arr<T, D> u1_pot = -omg * a0 * exp( k * x[0] - c ) * sin( k * x[1] + omg * t );
        d_arr<T, D> phse   = bta * x[0] + k * x[1] + omg * t; 
        d_arr<T, D> u1_vis = T(2) * nu * k * bta * a0 
                           * exp( bta * x[0] - c )
                           * ( sin( phse ) - cos( phse ) );
        d_arr<T, D> r = u1_pot + u1_vis;
        return r;
      }
 
    template <Dense_array A>
      decltype(auto) u0( T t, const A& x )
      {
        constexpr auto D = x.rank();
        T c = T(2) * nu * k * k * t; 
        d_arr<T, D> u0_pot = omg * a0 * exp( k * x[0] - c ) * cos( k * x[1] + omg * t );
        d_arr<T, D> u0_vis = -T(2) * nu * k * k * a0 
                           * exp( bta * x[0] - c ) 
                           * sin( bta * x[0] + k * x[1] + omg * t );
        d_arr<T, D> r = u0_pot + u0_vis;
        return r;
      }
 };


