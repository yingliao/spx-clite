#include <iostream>
#include <fstream>
#include <spx/spx.h>

using namespace std;
using namespace spx;

template <typename A>
  void write_txt( ofstream& out, string name, const A& a )
  {
    out << name << endl;
    out << a.rank() << endl;
    for( auto v : a.extents() )
      out << v << " ";
    out << endl;
    for( auto v : a )
      out << v << " ";
    out << endl;
  }

template <Vector V>
  void extra_polate( V& v )
  {
    if( v.size() == 1 )
      v[0] = 0;
    else if( v.size() == 2 )
      v[0] = v[1];
    else if( v.size() == 3 )
      v[0] = 2.0 * v[1] - v[2];
    else if( v.size() == 4 )
      v[0] = 3.0 * v[1] - 3.0 * v[2] + v[3];
  }

template <Dense_array A>
  decltype(auto) to_freq( const A& u )
  {
    constexpr auto D = u.rank();
    size_t s = (D == 2) ? 0 : 1;
    auto U = fft( u, s   );
         U = fft( U, s+1 );
    return U;
  }

template <Dense_array A>
  decltype(auto) to_real( const A& U )
  {
    using R = Value_type<A>; // complex type
    using T = Value_type<R>;
    constexpr auto D = U.rank();

    size_t s = (D == 2) ? 0 : 1;
    auto iU = ifft(  U, s   );
         iU = ifft( iU, s+1 );
    d_arr<T, D> u = real( iU );
    return u;
  }

//template <Dense_array A>
//  decltype(auto) to_freq( const A& u )
//  {
//    return u;
//  }
//
//template <Dense_array A>
//  decltype(auto) to_real( const A& U )
//  {
//    return U;
//  }

#include "surfbtm.h"

// ----------------------- Operators / Transformation ------------------------------------

template <Dense_array A, typename O0, typename O1>
  decltype(auto) eval_dksi0( const A& u, O0&& dksi0, O1&& dksi0_s )
  {
    auto ksi0_s  = dksi0_s.data_gen().diff_basis().coords();
    auto ksi0_dx = ksi0_s[1] - ksi0_s[0];

    A u_s( dksi0_s.extents() );
 
    u_s( slice( first()+1, last()-1 ), all(), all() ) = 0.5 *
                ( u( slice( first(),   last()-1 ), all(), all() ) 
                + u( slice( first()+1, last()   ), all(), all() ) );
   
    A u_dksi0 = dksi0( u );
    u_s( first(), all(), all() ) = 
      u_s( first()+1, all(), all() ) - ksi0_dx * u_dksi0( first(), all(), all() );

    u_s( last(),  all(), all() ) = 
      u_s( last() -1, all(), all() ) + ksi0_dx * u_dksi0( last(),  all(), all() );
    
    A u_s_dksi0_s = dksi0_s( u_s );
    A r = u_s_dksi0_s( slice( first(), last()-1 ), all(), all() );
    return r;
  }

template <Dense_array A, typename G>
  decltype(auto) to_contra_vel( G&& crv_grd, const A& u0, const A& u1, const A& u2 )
  {
    constexpr auto D = crv_grd.rank();
    using T = Value_type<A>;
    using S = static_vector<T, D>;

    decltype(auto) dksi_dx = crv_grd.b_contra();
    d_arr<S, D> u_con( u0.extents() );
    for( std::size_t k = 0; k < D; ++k )
    {
      d_arr<T, D> uk = u0 * dksi_dx[k][0] 
                     + u1 * dksi_dx[k][1]
                     + u2 * dksi_dx[k][2];
      u_con[k] = uk;
    }
    return u_con;
  }

template <Dense_array A, typename G, Stencil_array OS, 
          Stencil_array O0, Stencil_array O1, Stencil_array O2>
  decltype(auto) eval_divergence( const A& U_CON, G&& crv_grd, OS&& dksi0_s, 
                                  O0&& dksi0, O1&& dksi1, O2&& dksi2 )
  {
    constexpr auto D = U_CON.rank();
    using V = Value_type<A>;
    using R = Value_type<V>;
    using T = Value_type<V>;

    d_arr<R, D> U0 = U_CON[0];
    d_arr<R, D> U1 = U_CON[1];
    d_arr<R, D> U2 = U_CON[2];

    decltype(auto) j = crv_grd.jacobian();
    d_arr<T, D> j_u0 = j * to_real( U0 );
    d_arr<T, D> j_u1 = j * to_real( U1 );
    d_arr<T, D> j_u2 = j * to_real( U2 );

    d_arr<T, D> u0d0 = eval_dksi0( j_u0, dksi0, dksi0_s );
    //d_arr<T, D> u0d0 = dksi0( j_u0 ); 

    d_arr<R, D> U1D1 = dksi1( to_freq( j_u1 ) );
    d_arr<R, D> U2D2 = dksi2( to_freq( j_u2 ) );

    d_arr<T, D> div = u0d0 + to_real( U1D1 ) + to_real( U2D2 );
    div /= j;
    return to_freq( div );
  }

template <Dense_array X, Dense_array A, typename G, Stencil_array OS, 
          Stencil_array O0, Stencil_array O1, Stencil_array O2>
  decltype(auto) eval_conv( const X& U,
                            const A& u_con, G&& crv_grd, OS&& dksi0_s, 
                            O0&& dksi0, O1&& dksi1, O2&& dksi2 )
  {
    constexpr auto D = u_con.rank();
    using V = Value_type<A>;
    using T = Value_type<V>;
    using R = Value_type<X>;

    decltype(auto) j = crv_grd.jacobian();
    auto u = to_real( U );

    d_arr<T, D> u0  = j * u_con[0] * u; 
    d_arr<T, D> cv0 = eval_dksi0( u0, dksi0, dksi0_s );
    //d_arr<T, D> cv0 = dksi0( u0 ); 
    
    d_arr<T, D> u1  = j * u_con[1] * u;
    d_arr<R, D> U1  = to_freq( u1 );
    d_arr<R, D> cv1 = dksi1( U1 );
 
    d_arr<T, D> u2  = j * u_con[2] * u;
    d_arr<R, D> U2  = to_freq( u2 );
    d_arr<R, D> cv2 = dksi2( U2 );
    
    d_arr<R, D> cnv = to_freq( cv0 ) + cv1 + cv2;
    return cnv;
  }


// -----------------------  Fractional N-S  -------------------------------------------

#include "poisson.h"

template <typename F, typename A, typename T, 
          typename G, typename O0, typename O1, typename O2>
  decltype(auto) uh0_slvr_sor( F&& bc, const A& uh_bc, T tol, 
                               G&& crv_grd, O0&& dksi0, O1&& dksi1, O2&& dksi2 )
  {
    return [&]( auto& rhs, auto a0, auto& ut, auto beta_0, auto&& op )
    {
      using R = Value_type<A>;
      
      auto on_node = make_on_node<R>( ut.descriptor() );

      //decltype(auto) dksi_dx = crv_grd.b_contra();
      //decltype(auto) j       = crv_grd.jacobian();
      //auto top = make_stencil_array( dksi0( j * dksi_dx[0][0] * on_node ) );
      
      auto top = make_stencil_array( R(1) * dksi0 );
      
      auto lhs = make_stencil_array( a0 * on_node - beta_0 * op );
      auto prb = make_stencil_array(
          []( auto& desc, auto idx, auto it_lhs, auto it_btm, auto it_top )
          {
            if( idx[0] == desc.lbound(0) )
              return *it_btm;
            else if( idx[0] == desc.ubound(0) )
              return *it_top;
            else
              return *it_lhs;
          },
          lhs, 
          on_node, 
          // on_node );
          top );

      bc( rhs, uh_bc );
      
      Main_type<decltype(ut)> u_cp = ut;
      for( size_t it = 0; it < 1; ++it )
      {
        //lsor( prb, rhs, ut, 0, 1.0 );
        lsor_async( prb, rhs, ut, 0, 1.0 );
        T err = std::abs( norm2( u_cp-=ut ) ) / T( rhs.size() );
        u_cp = ut;
        cout << "[Iter = " << it << "] Err. L2-Norm = " << err << endl;
        if( err < 1.e-7 )
          break;
      }
      return u_cp;
    };
  }

template <typename F, typename A, typename T, typename G, typename O0>
  decltype(auto) uh_slvr_sor( F&& bc, const A& uh_bc, T tol, G&& crv_grd, O0&& dksi0 )
  {
    return [&]( auto& rhs, auto a0, auto& ut, auto beta_0, auto&& op )
    {
      using R = Value_type<A>;
      
      auto on_node = make_on_node<R>( ut.descriptor() );
      auto top = make_stencil_array( R(1) * dksi0 );
      auto lhs = make_stencil_array( a0 * on_node - beta_0 * op );
      auto prb = make_stencil_array(
          []( auto& desc, auto idx, auto it_lhs, auto it_btm, auto it_top )
          {
            if( idx[0] == desc.lbound(0) )
              return *it_btm;
            else if( idx[0] == desc.ubound(0) )
              return *it_top;
            else
              return *it_lhs;
          },
          lhs, 
          on_node, 
          //on_node );
          top );

      bc( rhs, uh_bc );
      
      Main_type<decltype(ut)> u_cp = ut;
      for( size_t it = 0; it < 1; ++it )
      {
        //lsor( prb, rhs, ut, 0, 1.0 );
        lsor_async( prb, rhs, ut, 0, 1.0 );
        T err = std::abs( norm2( u_cp-=ut ) ) / T( rhs.size() );
        u_cp = ut;
        cout << "[Iter = " << it << "] Err. L2-Norm = " << err << endl;
        if( err < 1.e-7 )
          break;
      }
      return u_cp;
    };
  }

// -----------------------  Project Functions  -------------------------------------------

  template <typename T, typename G, typename OS, typename O0, typename O1, typename O2>
  decltype(auto) proj_u_func( T dt, T alpha_0, G&& crv_grd, OS&& dksi0_s, 
                              O0&& dksi0, O1&& dksi1, O2&& dksi2 )
  {
    return [&]( std::size_t i, auto& U, auto& PHI, bool frwd )
    {
      using A = Main_type<decltype(U)>;
      using R = Value_type<A>;
      constexpr auto D = U.rank();

      decltype(auto) dksi_dx = crv_grd.b_contra();
      decltype(auto) j       = crv_grd.jacobian();

      d_arr<T, D> phi    = to_real( PHI );
      d_arr<T, D> phi_0i = j * dksi_dx[0][i] * phi;
      d_arr<T, D> phi_d0 = eval_dksi0( phi_0i, dksi0, dksi0_s );
      //d_arr<T, D> phi_d0 = dksi0( phi_0i ); 

      d_arr<T, D> phi_1i = j * dksi_dx[1][i] * phi;
      d_arr<T, D> phi_2i = j * dksi_dx[2][i] * phi;
      d_arr<R, D> PHI_d1 = dksi1( to_freq( phi_1i ) );
      d_arr<R, D> PHI_d2 = dksi2( to_freq( phi_2i ) );

      d_arr<T, D> grad = phi_d0 + to_real( PHI_d1 ) + to_real( PHI_d2 );
      T    s    = frwd ? T(-1) : T(1);
      
      d_arr<T, D> rr = s * dt / alpha_0 * grad / j;
      d_arr<R, D> RR = U + to_freq( rr );
      return RR;
    };
  }

template <typename T, typename G, typename OS, typename O0, typename O1, typename O2>
  decltype(auto) proj_u_con_func( T dt, T alpha_0, G&& crv_grd, OS&& dksi0_s, 
                                  O0&& dksi0, O1&& dksi1, O2&& dksi2 )
  {
    return [&]( std::size_t q, auto& u_con, auto& phi, bool frwd )
    {
      constexpr auto D = u_con.rank();
      using A = Main_type<decltype(u_con)>;
      using R = Value_type<Value_type<A>>;

      decltype(auto) g = crv_grd.g_contra();
      T s = frwd ? T(-1) : T(1);

      d_arr<R, D> phi_d0 = eval_dksi0( phi, dksi0, dksi0_s );
      //d_arr<R, D> phi_d0 = dksi0( phi ); 

      d_arr<R, D> t = g[q][0] * phi_d0
                    + g[q][1] * dksi1( phi )
                    + g[q][2] * dksi2( phi );

      d_arr<R, D> r = u_con[q] + s * dt / alpha_0 * t;
      return r;
    };
  }

template <typename T, typename G, typename O>
  decltype(auto) proj_p_func( T dt, T alpha_0, T beta_0, G&& crv_grd, O&& dfus_op )
  {
    return [&]( auto& phi )
    {
      using A = Main_type<decltype(phi)>;
      //decltype(auto) j = crv_grd.jacobian();
      //A p = phi - dt * beta_0 / alpha_0 / j * dfus_op( phi );
      A p = phi - dt * beta_0 / alpha_0 * dfus_op( phi );
      return p;
    };
  }

template <typename T, typename G, typename O>
  decltype(auto) proj_phi_func( T dt, T alpha_0, T beta_0, G&& crv_grd, O&& dfus_op )
  {
    return [&]( auto& p, auto& phi_h )
    {
      using A = Main_type<decltype(phi_h)>;
      //decltype(auto) j = crv_grd.jacobian();
      //auto phi = p + dt * beta_0 / alpha_0 / j * dfus_op( phi_h );
      A phi = p + dt * beta_0 / alpha_0 * dfus_op( phi_h );
      return phi; 
    };
  }

//#include "ext_linvis_wave.h"
#include "ext_stokes_wave.h"

template <size_t I>
requires (I == 2)
void test( string prefix, double ak )
{
  constexpr std::size_t D = 3;
  
  using T = double;
  using R = std::complex<T>;
  //using R = T;
  using S = static_vector<T, D>;
 
  // phsical params
  //
  T rho = 1e3;
  T gma = 0;//0.07197;
  T g   = 9.81;

  // exact solution
  //
  T re  = 1000;
  //exct_lin_vis<T> ext( re, g, gma );
  exct_stokes<T> ext( re, g, gma, ak );
  T nu  = ext.nu; //1.e-6;
  T mu  = rho * nu;

  // numerical params
  //
  T         H = pi_2<T>;
  size_t N[D] = { 97,     128,          16 };
  T      L[D] = {  H, pi_4<T>, pi_4<T>/4.0 };
  T   xmin[D] = { -H, 0, 0 };
  T     dx[D] = { L[0]/T(N[0]), L[1]/T(N[1]), L[2]/T(N[2]) };
 
  T       cfl = 0.4;
  T        dt = cfl / (1/dx[0] + 1/dx[1] + 1/dx[2]);
  size_t it_N = 9.0 / dt;

  // PBC domain
  //
  auto gen = two_sided_vinokur<T>{ 1.52788, 0.39725 };
  auto x0  = gen( xmin[0], xmin[0]+L[0], N[0] );
  // auto x0 = linspace( xmin[0], xmin[0]+L[0],       N[0] );
  auto x1  = linspace( xmin[1], xmin[1]+L[1]-dx[1], N[1] );
  auto x2  = linspace( xmin[2], xmin[2]+L[2]-dx[2], N[2] );

  auto rec_grd = make_rectlin_grid( x0, x1, x2 );
  auto grd_eta = make_rectlin_grid( x1, x2 );
  auto crd_eta = grd_eta.coords();

  // eta operators
  //
  auto b1_o1      = make_fourier_basis( N[1], dx[1], 1 );
  auto b1_o2      = make_fourier_basis( N[1], dx[1], 2 );
  auto b2_o1      = make_fourier_basis( N[2], dx[2], 1 );
  auto b2_o2      = make_fourier_basis( N[2], dx[2], 2 );
  //auto b1_o1      = make_fd_basis_pbc( x1, L[1], 1, 4 );
  //auto b1_o2      = make_fd_basis_pbc( x1, L[1], 2, 3 );
  //auto b2_o1      = make_fd_basis_pbc( x2, L[2], 1, 4 );
  //auto b2_o2      = make_fd_basis_pbc( x2, L[2], 2, 3 );
  auto eta_dx1    = make_stencil_array<D-1>( { N[1], N[2] }, b1_o1, 0 );
  auto eta_dx1dx1 = make_stencil_array<D-1>( { N[1], N[2] }, b1_o2, 0 );
  auto eta_dx2    = make_stencil_array<D-1>( { N[1], N[2] }, b2_o1, 1 );
  auto eta_dx2dx2 = make_stencil_array<D-1>( { N[1], N[2] }, b2_o2, 1 );

  // curvilinear grid
  //
  auto ksi0      = linspace( xmin[0], xmin[0]+L[0],       N[0] );
  auto ksi1      = linspace( xmin[1], xmin[1]+L[1]-dx[1], N[1] );
  auto ksi2      = linspace( xmin[2], xmin[2]+L[2]-dx[2], N[2] );
  
  auto crvgrd_b0 = make_fd_basis( ksi0, 1, 4 ); // 1-st order diff & 4-th order accuracy
  auto crvgrd_b1 = make_fd_basis( ksi1, 1, 4 ); // 1-st order diff & 4-th order accuracy
  auto crvgrd_b2 = make_fd_basis( ksi2, 1, 4 ); // 1-st order diff & 4-th order accuracy
  
  auto crvgrd_dksi0 = make_stencil_array<D>( N, crvgrd_b0, 0 );
  auto crvgrd_dksi1 = make_stencil_array<D>( N, crvgrd_b1, 1 );
  auto crvgrd_dksi2 = make_stencil_array<D>( N, crvgrd_b2, 2 );
 
  auto crv_grd = make_curvilin_dyn_grid( dt, crvgrd_dksi0, crvgrd_dksi1, crvgrd_dksi2 );
  
  // curvilinear operators
  //
  auto ksi_b0_o1 = make_fd_basis( ksi0, 1, 4 );
  auto ksi_b1_o1 = make_fourier_basis( N[1], dx[1], 1 );
  auto ksi_b2_o1 = make_fourier_basis( N[2], dx[2], 1 );
  //auto ksi_b1_o1 = make_fd_basis_pbc( ksi1, L[1], 1, 2 );
  //auto ksi_b2_o1 = make_fd_basis_pbc( ksi2, L[2], 1, 2 );
  auto dksi0     = make_stencil_array<D>( N, ksi_b0_o1, 0 );
  auto dksi1     = make_stencil_array<D>( N, ksi_b1_o1, 1 );
  auto dksi2     = make_stencil_array<D>( N, ksi_b2_o1, 2 );

  auto ksi0_dx    = ksi0[1] - ksi0[0];
  d_vec<T> ksi0_s = linspace( ksi0[ first() ] - 0.5 * ksi0_dx, 
                              ksi0[ last() ]  + 0.5 * ksi0_dx, 
                              N[0] + 1 );
  auto ksi_b0_s   = make_fd_basis( ksi0_s, 1, 1 );
  auto dksi0_s    = make_stencil_array<D>( {N[0] + 1, N[1], N[2]}, ksi_b0_s, 0 );

  // initial eta
  //
  d_arr<T, D-1> eta( {N[1], N[2]} );
  eta = ext.eta( 0, crd_eta ); 
  auto ETA = to_freq( eta );

  // initial grid
  //
  auto crd = rec_grd.perturb_surface( eta, 0 ); // along axis-0
  crv_grd.update( crd );

  // initial condition
  //
  d_arr<T, D> u0( N );
  d_arr<T, D> u1( N );
  d_arr<T, D> u2( N );
  u0 = ext.u0( 0, rec_grd.coords() );
  u1 = ext.u1( 0, rec_grd.coords() );
  u2 = 0; 
  auto U0 = to_freq( u0 );
  auto U1 = to_freq( u1 );
  auto U2 = to_freq( u2 );
  auto u_con = to_contra_vel( crv_grd, u0, u1, u2 );
  auto U_CON = to_contra_vel( crv_grd, U0, U1, U2 );

  // initial pressure
  //
  d_arr<T, D> p0( N );
  p0 = 0;
  vector<d_arr<R, D>> P( 3 );
  for( size_t t = 0; t < 3; ++t )
    P[ t ] = to_freq( p0 );

  // time-stacking size
  //
  // t = 0: current solution
  // t = 1 ~ M-1: history
  //
  constexpr size_t M = 2;
  vector<d_arr<R, D>> PHI( M );
  for( size_t t = 0; t < M; ++t )
  {
    PHI[ t ] = d_arr<R, D>( N );
    PHI[ t ] = P[ 0 ];
  }

  // frac coef
  //
  using trns_ts = bdf1<T>;
  using dfus_ts = am2<T>;
  using conv_ts = ab2<T>;
  auto  alpha_0 = trns_ts().coef_u()[0];
  auto  beta_0  = dfus_ts().coef_f()[0];
 
  auto psn_hlp      = make_poisson_helper( H, crv_grd, dksi0, dksi1, dksi2 );
  
  auto lap_diag     = psn_hlp.make_laplace_diag();
  auto dfus_diag_op = make_stencil_array( nu * lap_diag ); 

  //auto lap_j        = crv_grd.make_laplace_j( dksi0, dksi1, dksi2 );
  //auto dfus_diag_op = make_stencil_array( nu * lap_j ); 

  auto dfus_op = [&]( auto& X )
  {
    auto LAP = psn_hlp.eval_laplace( X );
    LAP *= nu;
    return LAP;
  };

  auto conv_op = [&]( auto& X ) 
  {
    using V    = Value_type<decltype(X)>; 
    auto w_con = crv_grd.grid_vel_contra();
    auto cnv_u = eval_conv( X, u_con, crv_grd, dksi0_s, dksi0, dksi1, dksi2 );
    auto cnv_w = eval_conv( X, w_con, crv_grd, dksi0_s, dksi0, dksi1, dksi2 );
    d_arr<V, D> r = -( cnv_u - cnv_w );
    //return r;
    auto rr  = to_real( r );
         rr /= crv_grd.jacobian();
    d_arr<R, D> RR = to_freq( rr ) - nu * psn_hlp.eval_laplace_off( X );
    return RR;
  };
 
  //cout << "Preparing Momentum Equation - 0\n";

  //auto mom_0 = make_multi_stepper_transcoef( dt, trns_ts(), U0, crv_grd.jacobian(),
  //                                           dfus_ts(), dfus_diag_op,
  //                                           conv_ts(), conv_op );
  //
  //cout << "Preparing Momentum Equation - 1\n";

  //auto mom_1 = make_multi_stepper_transcoef( dt, trns_ts(), U1, crv_grd.jacobian(),
  //                                           dfus_ts(), dfus_diag_op,
  //                                           conv_ts(), conv_op );

  //cout << "Preparing Momentum Equation - 2\n";

  //auto mom_2 = make_multi_stepper_transcoef( dt, trns_ts(), U2, crv_grd.jacobian(),
  //                                           dfus_ts(), dfus_diag_op,
  //                                           conv_ts(), conv_op );

 
  cout << "Preparing Momentum Equation - 0\n";

  auto mom_0 = make_multi_stepper( dt, trns_ts(), U0, 
                                       dfus_ts(), dfus_diag_op,
                                       conv_ts(), conv_op );
  
  cout << "Preparing Momentum Equation - 1\n";

  auto mom_1 = make_multi_stepper( dt, trns_ts(), U1,
                                       dfus_ts(), dfus_diag_op,
                                       conv_ts(), conv_op );

  cout << "Preparing Momentum Equation - 2\n";

  auto mom_2 = make_multi_stepper( dt, trns_ts(), U2,
                                       dfus_ts(), dfus_diag_op,
                                       conv_ts(), conv_op );

  // eta RK-solver
  //
  auto conv_eta  = [&]( auto& X ) 
  {
    d_arr<T, D-1> u0_surf = u0( last(), all(), all() );
    d_arr<T, D-1> u1_surf = u1( last(), all(), all() );
    d_arr<T, D-1> u2_surf = u2( last(), all(), all() );

    d_arr<R, D-1> D1  = eta_dx1( X );
    d_arr<T, D-1> d1  = to_real( D1 ); 

    d_arr<R, D-1> D2  = eta_dx2( X );
    d_arr<T, D-1> d2  = to_real( D2 ); 

    d_arr<T, D-1> rhs = u0_surf - u1_surf * d1 - u2_surf * d2;
    auto          RHS = to_freq( rhs );
    return RHS;
  };
 
  auto rk = make_runge_kutta( dt, rk2<T>(1), ETA );

  // project function
  //
  auto proj_u     = proj_u_func(     dt, alpha_0, crv_grd, dksi0_s, dksi0, dksi1, dksi2 );
  auto proj_u_con = proj_u_con_func( dt, alpha_0, crv_grd, dksi0_s, dksi0, dksi1, dksi2 );
  auto proj_phi   = proj_phi_func( dt, alpha_0, beta_0, crv_grd, dfus_op );
  auto proj_p     = proj_p_func(   dt, alpha_0, beta_0, crv_grd, dfus_op );

  // BC function
  //
  auto bc = []( auto& a, auto& b )
  {
    a( first(), all(), all() ) = b( first(), all(), all() );
    a( last(),  all(), all() ) = b( last(),  all(), all() );
  };

  // VTK write out - curvilin
  //
  vtk_writer out_grd( prefix + "/out_grd_" );
  auto vtk = [&]( auto tn, auto& out, auto& grd )
  {
    cout << "write VTK\n";
    out.next( grd );

    d_arr<S, D> u( u0.extents() );
    u[0] = u0;
    u[1] = u1;
    u[2] = u2;
    out.name( "u" )       << u;
    out.name( "u_con" )   << u_con; 
    out.name( "p" )       << to_real( P[0] );
    out.name( "phi" )     << to_real( PHI[0] );
    out.name( "J" )       << crv_grd.jacobian();
    //out.name( "w" )       << crv_grd.grid_vel();
    //out.name( "w_con" )   << crv_grd.grid_vel_contra();
  };

  // VTK write out - exact solution
  //
  vtk_writer out_ext( prefix + "/out_ext_" );
  auto vtk_ext = [&]( auto tn )
  {
    auto eta_e = ext.eta( tn, crd_eta );
    auto crd_e = rec_grd.perturb_surface( eta_e, 0 ); // along axis-0
    out_ext.next( crd_e );
    crd_e = rec_grd.coords();
    out_ext.name( "u0" ) << ext.u0( tn, crd_e );
    out_ext.name( "u1" ) << ext.u1( tn, crd_e );
  };

  // VTK write out - debug
  //
  vtk_writer vtk_dbg( prefix + "/out_dbg_" );
  auto vtk_vec = [&]( auto name, auto&& s0, auto&& s1, auto&& s2 )
  {
    d_arr<S, D> u( s0.extents() );
    u[0] = s0;
    u[1] = s1;
    u[2] = s2;
    vtk_dbg.name( name ) << u;
  };

  // VTK write out - ETA
  //
  vtk_writer out_eta( prefix + "/out_eta_" );
  auto vtk_eta = [&]( auto tn )
  {
    out_eta.next( crd_eta );
    out_eta.name( "eta" )     << eta;
    out_eta.name( "eta_ext" ) << ext.eta( tn, crd_eta );
  };

  // VTN write out - ETA debug
  //
  vtk_writer vtk_dbg_eta( prefix + "/out_dbg_eta_" );

  // VTK write out - t0
  //
  vtk( 0, out_grd, crd );
  vtk_dbg.next( crd );
  vtk_eta( 0 );
  vtk_ext( 0 );
  vtk_dbg_eta.next( crd_eta );

  cout << "Ready to Go!!\n";
 
  surf_coef_helper<T, D-1> sf_coef;
  
  for( std::size_t i = 1; i <= it_N; ++i )
  {
    T t_nxt = T(i) * dt;
    cout << "\n\ni = " << i << "\t t = " << t_nxt << endl;

    auto rk_f = [&]( auto n, auto dtn, auto& ETA_n )
    {
      auto tn = T(i) * dt + dtn;
      cout << "t_iter = " << tn << endl;

      // update eta
      //
      ETA = ETA_n;
      eta = to_real( ETA ); 
      
      // update grid
      //
      crd = rec_grd.perturb_surface( eta, 0 ); // along axis-0
      crv_grd.update( crd );
      vtk_dbg.next( crd );
      vtk_dbg_eta.next( crd_eta );
      vtk_dbg_eta.name( "eta" ) << eta;

      // update surface coef. - eta
      //
      sf_coef.update_grd( crv_grd, ETA, eta_dx1, eta_dx1dx1, eta_dx2, eta_dx2dx2 );

      // extra-polation for phi & p
      //
      if( n == 1 )
        extra_polate( PHI );
      vtk_dbg.name( "phi_h" ) << to_real( PHI[0] );

      // bottom velocity
      //
      auto          u0_ext = ext.u0( tn, rec_grd.coords() );
      auto          u1_ext = ext.u1( tn, rec_grd.coords() );
      d_arr<T, D-1> u0_btm = u0_ext( first(), all(), all() );
      d_arr<T, D-1> u1_btm = u1_ext( first(), all(), all() );
      auto          U0_btm = to_freq( u0_btm );
      auto          U1_btm = to_freq( u1_btm );

      auto u2_btm = d_arr<T, D-1>( {N[1], N[2]} ); 
           u2_btm = 0;
      auto U2_btm = to_freq( u2_btm );
 
      // prepare projection parts
      //
      d_arr<R, D> R0( N );
      d_arr<R, D> R1( N );
      d_arr<R, D> R2( N );
      R0 = 0;
      R1 = 0;
      R2 = 0;
      R0 = proj_u( 0, R0, PHI[0], false );
      R1 = proj_u( 1, R1, PHI[0], false );
      R2 = proj_u( 2, R2, PHI[0], false );


d_arr<R, D-1> tmp;

tmp = U0( last(), all(), all() );
d_arr<T, D-1> u0_sf_new = to_real( tmp );

tmp = U1( last(), all(), all() );
d_arr<T, D-1> u1_sf_new = to_real( tmp );

tmp = U2( last(), all(), all() );
d_arr<T, D-1> u2_sf_new = to_real( tmp );

d_arr<T, D-1> u0_sf_old;
d_arr<T, D-1> u1_sf_old;
d_arr<T, D-1> u2_sf_old;
T err_sf     = std::numeric_limits<T>::max();
T err_sf_old = std::numeric_limits<T>::max();

d_arr<R, D> UH0, UH1, UH2;

do{ 
u0_sf_old  = u0_sf_new;
u1_sf_old  = u1_sf_new;
u2_sf_old  = u2_sf_new;
err_sf_old = err_sf;

      // project to u_hat
      // 
      auto UH0_bc = make_sparse_array<R, D>( N );
      auto UH1_bc = make_sparse_array<R, D>( N );
      auto UH2_bc = make_sparse_array<R, D>( N );

      UH0_bc.insert_on();
      UH1_bc.insert_on();
      UH2_bc.insert_on();

      // surface
      //
      sf_coef.update_vel( U0, U1, U2, dksi0, dksi1, dksi2 );

      UH0_bc( last(), all(), all() ) = 
        surf_u0_bc( R0, U1, U2, crv_grd, dksi0, dksi1, dksi2 );

      UH1_bc( last(), all(), all() ) = 
        surf_u1_bc( sf_coef, R1, dksi0 );

      UH2_bc( last(), all(), all() ) = 
        surf_u2_bc( sf_coef, R2, dksi0 );

      // bottom
      UH0_bc( first(), all(), all() ) = btm_u_bc( R0, U0_btm );
      UH1_bc( first(), all(), all() ) = btm_u_bc( R1, U1_btm );
      UH2_bc( first(), all(), all() ) = btm_u_bc( R2, U2_btm );

      UH0_bc.insert_off();
      UH1_bc.insert_off();
      UH2_bc.insert_off();

      auto dbg_UH0_bc = d_arr<R, D>( N );
      auto dbg_UH1_bc = d_arr<R, D>( N );
      auto dbg_UH2_bc = d_arr<R, D>( N );
      dbg_UH0_bc = R(0);
      dbg_UH1_bc = R(0);
      dbg_UH2_bc = R(0);
      bc( dbg_UH0_bc, UH0_bc );
      bc( dbg_UH1_bc, UH1_bc );
      bc( dbg_UH2_bc, UH2_bc );
      vtk_vec( "uh_bc", 
               to_real(dbg_UH0_bc), to_real(dbg_UH1_bc), to_real(dbg_UH2_bc) );

      // u_hat solvers - SOR
      //
      T tol = 1.e-7 * sqrt( T(U0.size()) );
      auto UH0_slvr = uh0_slvr_sor( bc, UH0_bc, tol, 
                                    crv_grd, dksi0, dksi1, dksi2 );
      auto UH1_slvr = uh_slvr_sor(  bc, UH1_bc, tol, crv_grd, dksi0 );
      auto UH2_slvr = uh_slvr_sor(  bc, UH2_bc, tol, crv_grd, dksi0 );

      // solve momentum eqs. for u_hat
      //
      cout << "Solving Momemtum Equations - U_hat_0 \n";
      UH0 = mom_0.solve( UH0_slvr );
      cout << "Solving Momemtum Equations - U_hat_1 \n";
      UH1 = mom_1.solve( UH1_slvr );
      cout << "Solving Momemtum Equations - U_hat_2 \n";
      UH2 = mom_2.solve( UH2_slvr );
      
      // extra-polate U0 / U1 / U2
      //
      U0 = proj_u( 0, UH0, PHI[0], true ); // forward
      U1 = proj_u( 1, UH1, PHI[0], true ); // forward 
      U2 = proj_u( 2, UH2, PHI[0], true ); // forward 

tmp = U0( last(), all(), all() );
u0_sf_new = to_real( tmp );

tmp = U1( last(), all(), all() );
u1_sf_new = to_real( tmp );

tmp = U2( last(), all(), all() );
u2_sf_new = to_real( tmp );

u0_sf_old -= u0_sf_new;
u1_sf_old -= u1_sf_new;
u2_sf_old -= u2_sf_new;
T err0 = std::abs( norm2( u0_sf_old ) );
T err1 = std::abs( norm2( u1_sf_old ) );
T err2 = std::abs( norm2( u2_sf_old ) );

err_sf = ( err0 + err1 + err2 ) / 3.0;
cout << "----> Surface L2-Err. = " << err_sf << endl;

} while( err_sf > 1.e-4 * std::sqrt( T(u0_sf_new.size()) )
     &&  err_sf <= err_sf_old );

      sf_coef.dump( vtk_dbg_eta );
      vtk_vec( "uh", to_real(UH0), to_real(UH1), to_real(UH2) );
      
      auto u0_dbg = to_real( U0 );
      auto u1_dbg = to_real( U1 );
      auto u2_dbg = to_real( U2 );
      vtk_vec( "u_extra", u0_dbg, u1_dbg, u2_dbg );
      
      // divergence of u_hat
      //
      auto UH_CON = to_contra_vel( crv_grd, UH0, UH1, UH2 );
      auto div_UH = eval_divergence( UH_CON, crv_grd, dksi0_s, dksi0, dksi1, dksi2 );
      d_arr<R, D> UH0_CON = UH_CON[0];
      d_arr<R, D> UH1_CON = UH_CON[1];
      d_arr<R, D> UH2_CON = UH_CON[2];
      vtk_vec( "uh_con", to_real(UH0_CON), to_real(UH1_CON), to_real(UH2_CON) );
      vtk_dbg.name( "div_uh_con" ) << to_real( div_UH );

      // prepapre phi - lhs / rhs / bc
      //
      // top BC --> from pressure
      //
      auto p_sf = surf_pressure( vtk_dbg_eta,
                                 U0, U1, U2, 
                                 crv_grd, dksi0, dksi1, dksi2, 
                                 sf_coef,
                                 mu, rho, g, gma );
      p_sf /= rho;
      P[0]( last(), all(), all() ) = to_freq( p_sf );
      d_arr<R, D>   P_int          = ( T(3) * P[0] + T(6) * P[1] - T(1) * P[2] ) / T(8);
      d_arr<R, D-1> PHI_bc_top     = proj_phi( P_int, PHI[0] )( last(), all(), all() );
      // bottom BC --> from u0
      auto PHI_bc_btm = btm_phi( crv_grd, UH0, U0_btm, alpha_0, dt );
      // rhs
      d_arr<R, D> PHI_rhs = alpha_0 / dt * div_UH;

      auto PHI_bc_dbg = d_arr<R, D>( N );
      PHI_bc_dbg = R(0);
      PHI_bc_dbg( first(), all(), all() ) = PHI_bc_btm;
      PHI_bc_dbg( last(),  all(), all() ) = PHI_bc_top;
      vtk_dbg.name( "phi_bc"  ) << to_real( PHI_bc_dbg );
      vtk_dbg.name( "phi_rhs" ) << to_real( PHI_rhs );

      d_arr<R, D-1> P_int_top  = P_int( last(), all(), all() );
      d_arr<T, D-1> p_int_top  = to_real( P_int_top ); 
      d_arr<T, D-1> phi_bc_top = to_real( PHI_bc_top );
      d_arr<T, D-1> u0_top     = u0_dbg( last(), all(), all() );
      d_arr<T, D-1> u1_top     = u1_dbg( last(), all(), all() );
      d_arr<T, D-1> u2_top     = u2_dbg( last(), all(), all() );
      vtk_dbg_eta.name( "p_sf" )       << p_sf;
      vtk_dbg_eta.name( "p_int" )      << p_int_top;
      vtk_dbg_eta.name( "phi_bc_top" ) << phi_bc_top;
      vtk_dbg_eta.name( "u0_extra" )   << u0_top;
      vtk_dbg_eta.name( "u1_extra" )   << u1_top;
      vtk_dbg_eta.name( "u2_extra" )   << u2_top;

      // solve PHI
      //
      cout << "Solving Laplace PHI\n";
      PHI[0] = psn_hlp.solve_dn( PHI_rhs, PHI_bc_top, PHI_bc_btm, PHI[0] );
      vtk_dbg.name( "phi" ) << to_real( PHI[0] );

      // update p & u
      //
      cout << "Updating p & u\n";
      P[0] = proj_p( PHI[0] );
      vtk_dbg.name( "p" ) << to_real( P[0] );
      U0   = proj_u( 0, UH0, PHI[0], true ); // forward
      U1   = proj_u( 1, UH1, PHI[0], true ); // forward 
      U2   = proj_u( 2, UH2, PHI[0], true ); // forward 
      u0   = to_real( U0 );
      u1   = to_real( U1 );
      u2   = to_real( U2 );

      // update flow convection
      //
      cout << "Updating convection velocity\n";
      auto U_CON_0 = proj_u_con( 0, UH_CON, PHI[0], true ); // forward
      auto U_CON_1 = proj_u_con( 1, UH_CON, PHI[0], true ); // forward
      auto U_CON_2 = proj_u_con( 2, UH_CON, PHI[0], true ); // forward
      u_con[0] = to_real( U_CON_0 );
      u_con[1] = to_real( U_CON_1 );
      u_con[2] = to_real( U_CON_2 );
      U_CON[0] = U_CON_0;
      U_CON[1] = U_CON_1;
      U_CON[2] = U_CON_2;
      
      d_arr<R, D-1> dX = conv_eta( ETA_n );
      return dX;
    };

    ETA = rk.advance( rk_f );
    eta = to_real( ETA );
    vtk_eta( t_nxt );

    // roll-back
    //
    cout << "Rolling back\n";
    crv_grd.roll_back();
    //mom_0.roll_back( U0, crv_grd.jacobian() );
    //mom_1.roll_back( U1, crv_grd.jacobian() );
    //mom_2.roll_back( U2, crv_grd.jacobian() );
    mom_0.roll_back( U0 );
    mom_1.roll_back( U1 );
    mom_2.roll_back( U2 );
    for( size_t n = M-1; n >= 1; --n )
      PHI[ n ] = PHI[ n-1 ];
     for( size_t n = 2;   n >= 1; --n )
      P[ n ]   = P[ n-1 ];

    vtk( t_nxt, out_grd, crd );
    vtk_ext( t_nxt );
  }
}

int main( int argc, char* argv[] )
{
  test<2>( "out/", stod( argv[1] ) );
  return 0;
}
