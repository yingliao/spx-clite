#include <iostream>
#include <spx/spx.h>
#include <fstream>

using namespace std;
using namespace spx;

template <typename V>
  void foo( const V& b )
  {
    V c( b );

    cout << typestr<decltype(c)>() << endl;

    for( auto v : c )
      cout << v << endl;
    cout << endl;
  }

template <typename O, typename V>
  void write( O& ouf, const V& x  )
  {
    ouf << x.size() << endl;
    for( auto v: x )
      ouf << v << endl;
  }

template <std::size_t I>
requires (I == 1)
void test()
{
  using T = double;
  using S = static_vector<T, 2>;

  d_arr<T, 2> a{ 3, 3 };
  a = { 1, 2, 3, 
        2, 4, 5, 
        3, 5, 4 };

  d_vec<T> b = {4, 5, 6}; // x = 13, -9, 3
  // d_vec<S> b = { S{4, 4}, S{5, 5}, S{6, 6}};

  auto x = iter_solve( a, b, 1.e-8, 10000 );

  cout << "SOL = " << endl;
  for( auto v : x )
    cout << v << endl;
    // cout << v[0] << ", " << v[1] << endl;
  cout << endl;
}

template <std::size_t I>
requires (I == 2)
void test()
{
  using T = double;
  using R = T;

  size_t N = 128;
  auto L = 10;
  auto dx = L / T(N);
  auto x = linspace( 0, L-dx, N );
  auto k = 2 * pi_2<T> / L;

  // auto b1 = make_fd_basis_pbc( x, L, 1 );
  auto b1 = make_fd_basis( x, 1 );
  auto d0 = make_stencil_array<1>( {N}, b1, 0 );
  
  auto u_ext = [&]()
  {
    d_vec<T> u = spx::sin( k*x );
    // d_vec<T> u = x;
    return u;
  };

  auto f = [&]()
  {
    d_vec<T> g = spx::sin( k*x ) * spx::cos( k*x );
    // d_vec<T> g = spx::cos( k*x );
    g *= k;
    // d_vec<T> g = u_ext() * u_ext();
    return g;
  };

  auto bc = [&]( auto& r )
  {
    auto ux = u_ext();
    r( first() ) = ux( first() );
    r( last() )  = ux( last() ); 
  };

  auto rhs = [&]()
  {
    auto r = f();
    bc( r );
    return r;
  };

  auto op_lin = [&]( auto& p )
  {
    d_vec<R> r = p * d0(p);
    // d_vec<R> r = d0(p);
    // d_vec<R> r = p * p;
    r( first() ) = p( first() );
    r( last() )  = p( last() );
    return r;
  };

  auto op_non = [&]( auto& p )
  {
    d_vec<R> r = p * d0(p) - f();
    // d_vec<R> r = d0(p) - f();
    // d_vec<R> r = (p * p) - f();
    auto ux = u_ext();
    r( first() ) = p( first() ) - ux( first() );
    r( last() )  = p( last() )  - ux( last() );
    r *= r; // convex
    return spx::sum(r);
  };

  auto lin = make_iter_solver( 1.e-8, 1000 );
  auto non = make_nonlin_solver( 1.e-8, 1000, true ); 
 
  //d_vec<R> u_num  = lin( op_lin, rhs(), [&]( auto& ut )
  //    {
  //      auto ux = u_ext(); 
  //      auto mse = spx::sum( (ut - ux) * (ut - ux) ) / T( ut.size() );
  //      cout << "MSE = " << std::abs(mse) << endl << endl;
  //      bc( ut );
  //    });

  d_vec<R> u_num = rhs(); // init. val.
  non( op_non, u_num, [&]( auto& ut )
      {
        auto ux = u_ext(); 
        auto mse = spx::sum( (ut - ux) * (ut - ux) ) / T( ut.size() );
        cout << "MSE = " << std::abs(mse) << endl << endl;
        bc( ut );
      } );

  ofstream ouf( "out.txt" );
  write( ouf, u_ext() );
  write( ouf, u_num );
  ouf.close();
}

template <size_t I>
requires (I==3)
  void test()
  {
    using T = double;
    using Q = static_vector<T, 2>;
    using S = static_vector<T, 3>;
    using V = d_vector<Q>;

    V u = { Q{1, 1}, Q{2, 2}, Q{3, 3}};
    V v = { Q{4, 4}, Q{5, 5}, Q{6, 6}};
    Q r{0};
    dot( u, v, r );
    cout << r[0] << ", " << r[1] << endl;
  }

int main()
{
  test<1>();
  // test<2>();
  // test<3>();
  return 0;
}
