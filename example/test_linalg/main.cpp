#include <iostream>
#include <spx/spx.h>
#include <fstream>

using namespace std;
using namespace spx;

template <Matrix A>
  void print( A& a )
  {
    for( size_t i = 0;  i < a.extent(0); ++i )
    {
      for( size_t j = 0; j < a.extent(1); ++j )
        cout << a(i, j) << "  ";
      cout << endl;
    }
    cout << endl;
  }

template <Matrix M>
  void check_lu( M& a )
  {
    using T = Value_type<M>;
    M l( a.extents() ), u( a.extents() );
    for( size_t i = 0;  i < a.extent(0); ++i )
      for( size_t j = 0; j < a.extent(1); ++j )
      {
        if (i > j)
          l(i, j) = a(i, j);
        else if(i == j)
        {
          u(i, j) = a(i, j);
          l(i, j) = T(1);
        }
        else // i < j
          u(i, j) = a(i, j);
      }
    cout << "L = \n";
    print(l);
    cout << "U = \n";
    print(u);
    cout << "LU = \n";
    auto lu = MxM(l, u);
    print( lu );
  }

template <typename T>
  decltype(auto) gen( size_t N )
  {
    vector<T> a;
    for( size_t i = 0; i < N*N; ++i )
      a.push_back( T((i*i)%(N+i) + 1)  );
    return a;
  }

template <typename T>
void test()
{
  using S = static_vector<T, 2>;
  
  constexpr size_t N = 4;
  // d_arr<T, 2> a{ N, N };
  static_array<T, N, N> a;

  a = gen<T>( N );

  //a = {1, 2, 
  //    3, 4};

  // a = { 1, 2, 3, 
  //      2, 4, 5, 
  //      3, 5, 4 };

  //a = { 1, -2, 4, 
  //      -5, 2, 0, 
  //      1, 0, 3 };

  //a = {1, 0, 2, -1,
  //     3, 0, 0,  5,
  //     2, 1, 4, -3,
  //     1, 0, 5,  0};

  // d_vec<T> b = {4, 5, 6}; // x = 13, -9, 3
  // d_vec<S> b = { S{4, 4}, S{5, 5}, S{6, 6}};
  // d_vec<T> b = {4, 5, 6, 7};

  //d_arr<T, 2> b{ N, size_t(2) };
  //b = { 3, 4, 5, 6,
  //      4, 5, 6, 7 };

  cout << "A = \n";
  print(a);

  T det(0);
  // lu( a ); 
  // lu( a, det ); 
  auto inv_a = inv( a, det ); 
  // inv( a, det ); 
  // lapack_solve( a, b );
  // lapack_solve( a, b, det );

  // check_lu( a );

  cout << "det. =  " << det << endl << endl;

  print( inv_a );

  // cout << "SOL = ";
  // for( auto v : b )
  //  cout << v << ", ";
  // cout << endl;
}

void test1()
{
  using T = double;
  using S = static_array<T, 3, 2>;
  using A = d_array<T, 2>;
  cout << boolalpha << Static_matrix_NxN<A, 3>() << endl;
}

int main()
{
  //test<float>();
  test<double>();
  //test<complex<float>>();
  //test<complex<double>>();
  //test1();
  return 0;
}
