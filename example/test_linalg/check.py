import numpy as np

N = 4
v = [ (x*x)%(N+x) + 1 for x in range(N*N)]
a = np.array(v).reshape([N, N])

print "A = ", a
print "det = ", np.linalg.det(a)
print "inv = ", np.linalg.inv(a)
