import matplotlib.pyplot as plt
from numpy import *

f = open("grid2D.txt")
data2 = []
while 1:
	lines = f.readlines(100000)
	if not lines:
		break
	for line in lines:
		l = line.split(' ')[0:-1]
		data2.append(l)

n = 50
m = 100
xx = array( double(data2[0]).reshape(n, m) );
yy = array( double(data2[1]).reshape(n, m) );

f = open("grid2D_geom.txt")
data2 = []
while 1:
	lines = f.readlines(100000)
	if not lines:
		break
	for line in lines:
		l = line.split(' ')[0:-1]
		data2.append(l)

a00 = array( double(data2[0]).reshape(n, m) );
a01 = array( double(data2[1]).reshape(n, m) );
a10 = array( double(data2[2]).reshape(n, m) );
a11 = array( double(data2[3]).reshape(n, m) );

b00 = array( double(data2[4]).reshape(n, m) );
b01 = array( double(data2[5]).reshape(n, m) );
b10 = array( double(data2[6]).reshape(n, m) );
b11 = array( double(data2[7]).reshape(n, m) );

J   = array( double(data2[8]).reshape(n, m) );

plt.hold(True);
for i in range(0, n):
	plt.plot(xx[i,::], yy[i,::], 'b-')
for j in range(0, m):
	plt.plot(xx[::,j], yy[::,j], 'b-')

#plt.quiver( xx, yy, a00, a10, color='g', pivot='tail', width=0.002, headlength=6, scale=1.2e3 )
#plt.quiver( xx, yy, a01, a11, color='g', pivot='tail', width=0.002, headlength=6, scale=1.2e3 )

plt.quiver( xx, yy, b00, b10, pivot='tail', width=0.002, headlength=6, scale=3 )
plt.quiver( xx, yy, b01, b11, pivot='tail', width=0.002, headlength=6, scale=3 )

plt.hold(False);
plt.grid(True)
plt.xlabel("x")
plt.ylabel("y")

plt.figure()
plt.imshow( J, origin='lower' )
plt.colorbar()

plt.show()
