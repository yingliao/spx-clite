#include <iostream>
#include <spx/spx.h>
#include <fstream>

using namespace std;
using namespace spx;

template <Dense_array A>
  void print( A& a )
  {
    for( size_t i = 0;  i < a.extent(0); ++i )
    {
      for( size_t j = 0; j < a.extent(1); ++j )
        cout << "(" << a(i, j)[0] << ", " << a(i, j)[1] << ")  ";
      cout << endl;
    }
    cout << endl;
  }

template <Dense_array A>
  void print2( A& a )
  {
    for( size_t i = 0;  i < a.extent(0); ++i )
    {
      for( size_t j = 0; j < a.extent(1); ++j )
        cout << a(i, j) << ", ";
      cout << endl;
    }
    cout << endl;
  }

template <size_t I>
requires (I==1)
void test()
{
  using T = double;
  using S = static_vector<T, 2>;
  using M = static_array<T, 2, 2>;
  
  auto x0 = linspace( 0, 1, 11 );
  auto x1 = linspace( 5, 9, 5 );

  auto rect = make_rectlin_grid( x0, x1 );
  auto grd = rect.coords();

  print( grd );

  d_arr<S, 2> grdp{ 3, 3 };
  grdp = { S{0, 1}, S{1, 2}, S{2, 3}, 
           S{3, 4}, S{4, 5}, S{5, 6},
           S{6, 7}, S{7, 8}, S{8, 9} };

  d_arr<M, 2> grdm{ 3, 3 };
  grdm = { M{0, 1, 2, 3}, M{1, 2, 3, 4}, M{2, 3, 4, 5}, 
           M{3, 4, 5, 6}, M{4, 5, 6, 7}, M{5, 6, 7, 8},
           M{6, 7, 8, 9}, M{7, 8, 9, 10}, M{8, 9, 10, 11} };

  // d_arr<S, 2> grdp = grd;
  d_arr<T, 2> xx0 = grdm[0][1];
  print2( xx0 );
}

template <size_t I>
requires (I==2)
void test()
{
  using T = double;
  using S = static_vector<T, 2>;

  // one_sided_compression<T> g{ 2.9 };
  // one_sided_expansion<T> g{ 2.9 };
  two_sided_vinokur<T> g{ 0.1, 0.1 };

  auto c = g( 0, 1, 20 );

  for( auto v : c )
    cout << v << ",";
  cout << endl << endl;

  d_vec<T> x = { 1, 2, 3, 4, 5 };
  d_vec<S> y = { S{1, 1}, S{2, 2}, S{3, 3}, S{4, 4}, S{5, 5} };
  d_vec<T> x_int = { 1.1, 1.5, 3.4, 5.0 };
  // auto y_int = linear_interp_1d( x, y, x_int );
  // auto y_int = cal_length( y );
  // auto y_acu = accumulate( y_int );
  
  auto y_grd = gen_grids( y, 10, one_sided_compression<T>{2.9} );

  for( auto v : y_grd )
    cout << v[0] << ", " << v[1] << endl;
}

template <size_t I>
requires (I == 3)
void test()
{
  using T = double;
  using S = static_vector<T, 2>;
  using M = static_array<T, 2, 2>;

  d_vec<S> edge1 = { S{0, 0}, S{10,  10} , S{20, 10} };
  d_vec<S> edge2 = { S{0, 0}, S{15, -10} };
  d_vec<S> edge3 = { S{15, -10}, S{30, 0} };
  d_vec<S> edge4 = { S{20,  10}, S{30, 0} };

  auto g = two_sided_vinokur<T>{ 0.1, 0.1 };
  //auto g = one_sided_expansion<T>();

  std::size_t n[2] = { 50, 100 };

  auto grd1 = gen_grids( edge1, n[0], g );
  auto grd2 = gen_grids( edge2, n[1], g );
  auto grd3 = gen_grids( edge3, n[0], g );
  auto grd4 = gen_grids( edge4, n[1], g );

  auto grd_2d = soni_tfi( grd1, grd2, grd3, grd4 );

  auto ksi0 = g( 0, 1, n[0] );
  auto ksi1 = g( 0, 1, n[1] );
  auto b0 = make_fd_basis( ksi0 );
  auto b1 = make_fd_basis( ksi1 );
  b0.set_accuracy( 2 );
  b1.set_accuracy( 2 );
  auto dksi0 = make_stencil_array<2>( {n[0], n[1]}, b0, 0 );
  auto dksi1 = make_stencil_array<2>( {n[0], n[1]}, b1, 1 );

  auto crv_grd = make_curvilin_grid<T>( dksi0, dksi1 );
  crv_grd.update( grd_2d );

  //auto t = forward_as_tuple( dksi0, dksi1 );
  //d_arr<T, 2> x0 = grd_2d[0];
  //d_arr<M, 2> dx_dksi( grd_2d.extents() );

  //tuple_for_each_with_id( [&]( auto i, auto&& dksi ) {
  //    cout << i() << endl;
  //    d_arr<T, 2> dksi_dx0 = dksi( x0 );
  //    dx_dksi[0][i()] = dksi(x0); // dksi_dx0;
  //    }, t );

  //! output to file
	ofstream out;
	out.open("grid2D.txt", ios::out);
	for( size_t d = 0; d < 2; ++d )
	{
    d_arr<T, 2> x = grd_2d[ d ];
		for( auto& v : x )
			out << v << " ";
		out << endl;
	}
	out.close();

  cout << "dumping basis\n";
	out.open("grid2D_geom.txt", ios::out);
  // dx_dksi
	for( size_t i = 0; i < 2; ++i )
	{
    for( size_t j = 0; j < 2; ++j )
    {
      d_arr<T, 2> x = crv_grd.b_covrnt()[i][j];
		  for( auto& v : x )
			  out << v << " ";
		  out << endl;
	  }
  }

  // dksi_dx
	for( size_t i = 0; i < 2; ++i )
	{
    for( size_t j = 0; j < 2; ++j )
    {
      d_arr<T, 2> x = crv_grd.b_contra()[i][j];
		  for( auto& v : x )
			  out << v << " ";
		  out << endl;
	  }
  }

  // jacobian
  for( auto& v: crv_grd.jacobian() )
    out << v << " ";
  out << endl;

  // g_cov
	for( size_t i = 0; i < 2; ++i )
	{
    for( size_t j = 0; j < 2; ++j )
    {
      d_arr<T, 2> x = crv_grd.g_covrnt()[i][j];
		  for( auto& v : x )
			  out << v << " ";
		  out << endl;
	  }
  }

  // g_con
  for( size_t i = 0; i < 2; ++i )
	{
    for( size_t j = 0; j < 2; ++j )
    {
      d_arr<T, 2> x = crv_grd.g_contra()[i][j];
		  for( auto& v : x )
			  out << v << " ";
		  out << endl;
	  }
  }

  out.close();

}

int main()
{
  test<3>();
  return 0;
}
