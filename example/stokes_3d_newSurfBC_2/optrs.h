// ----------------------- Operators / Transformation ------------------------------------

template <Dense_array A, typename F0, typename F1, typename F2, typename F3, 
         typename G0, typename G1,
         typename O0, typename O1, typename O2, typename O3, 
         typename T,  typename P>
  decltype(auto) make_conv( const A& u0s, const A& u1, const A& u2, 
                            F0&& cl_to_sg,  F1&& sg_to_cl, F2&& cl_d0_to_sg, F3&& sg_d0_to_cl,  
                            G0&& crv_grd_s, G1&& crv_grd_c,
                            O0&& d1c, O1&& d2c, O2&& d1s, O3&& d2s, 
                            T nu, P&& psn_hlp )
  {
    return [&]( size_t d, auto& u )
    {
      using R = complex<T>;
      d_arr<T, 3> cnv;
      if( d == 0 ) // u: stgr
      {
        d_arr<T, 3> u_u1_s  = u * cl_to_sg( u1 );
        d_arr<T, 3> u_u2_s  = u * cl_to_sg( u2 );
        d_arr<R, 3> d1_U_U1 = d1s( to_freq( u_u1_s ) );
        d_arr<R, 3> d2_U_U2 = d2s( to_freq( u_u2_s ) );
        d_arr<T, 3> d1_u_u1 = to_real( d1_U_U1 ); 
        d_arr<T, 3> d2_u_u2 = to_real( d2_U_U2 ); 

        d_arr<T, 3> u0c     = sg_to_cl( u0s );
        d_arr<T, 3> u_c     = sg_to_cl( u );
        d_arr<T, 3> u_u0_c  = u_c * u0c;
        d_arr<T, 3> u_u1_c  = u_c * u1;
        d_arr<T, 3> u_u2_c  = u_c * u2;
        d_arr<T, 3> d0_u_u0 = cl_d0_to_sg( u_u0_c );
        d_arr<T, 3> d0_u_u1 = cl_d0_to_sg( u_u1_c );
        d_arr<T, 3> d0_u_u2 = cl_d0_to_sg( u_u2_c );
        
        decltype(auto) s = crv_grd_s.b_contra();
        d_arr<T, 3> dx0 = s[0][0] * d0_u_u0;
        d_arr<T, 3> dx1 = s[0][1] * d0_u_u1 + s[1][1] * d1_u_u1;
        d_arr<T, 3> dx2 = s[0][2] * d0_u_u2 + s[2][2] * d2_u_u2;

        // grid-vel
        auto w_con_s = crv_grd_s.grid_vel_contra();
        auto U = to_freq( u );
        d_arr<R, 3> d1_U  = d1s( U );
        d_arr<R, 3> d2_U  = d2s( U );
        d_arr<T, 3> d0_u  = cl_d0_to_sg( u_c );
        d_arr<T, 3> d1_u  = to_real( d1_U );
        d_arr<T, 3> d2_u  = to_real( d2_U );
        d_arr<T, 3> cnv_w = w_con_s[0] * d0_u 
                          + w_con_s[1] * d1_u 
                          + w_con_s[2] * d2_u;

        cnv = dx0 + dx1 + dx2 - cnv_w;
      }
      else // u: clpt
      {
        d_arr<T, 3> u_u1_c  = u * u1;
        d_arr<T, 3> u_u2_c  = u * u2;
        d_arr<R, 3> d1_U_U1 = d1c( to_freq( u_u1_c ) );
        d_arr<R, 3> d2_U_U2 = d2c( to_freq( u_u2_c ) );
        d_arr<T, 3> d1_u_u1 = to_real( d1_U_U1 ); 
        d_arr<T, 3> d2_u_u2 = to_real( d2_U_U2 ); 

        d_arr<T, 3> u1s     = cl_to_sg( u1 );
        d_arr<T, 3> u2s     = cl_to_sg( u2 );
        d_arr<T, 3> u_s     = cl_to_sg( u ); 
        d_arr<T, 3> u_u0_s  = u_s * u0s;
        d_arr<T, 3> u_u1_s  = u_s * u1s;
        d_arr<T, 3> u_u2_s  = u_s * u2s;
        d_arr<T, 3> d0_u_u0 = sg_d0_to_cl( u_u0_s );
        d_arr<T, 3> d0_u_u1 = sg_d0_to_cl( u_u1_s );
        d_arr<T, 3> d0_u_u2 = sg_d0_to_cl( u_u2_s );
        
        decltype(auto) s = crv_grd_c.b_contra();
        d_arr<T, 3> dx0 = s[0][0] * d0_u_u0;
        d_arr<T, 3> dx1 = s[0][1] * d0_u_u1 + s[1][1] * d1_u_u1;
        d_arr<T, 3> dx2 = s[0][2] * d0_u_u2 + s[2][2] * d2_u_u2;

        // grid-vel
        auto w_con_c = crv_grd_c.grid_vel_contra();
        auto U = to_freq( u );
        d_arr<R, 3> d1_U  = d1c( U );
        d_arr<R, 3> d2_U  = d2c( U );
        d_arr<T, 3> d0_u  = sg_d0_to_cl( u_s );
        d_arr<T, 3> d1_u  = to_real( d1_U );
        d_arr<T, 3> d2_u  = to_real( d2_U );
        d_arr<T, 3> cnv_w = w_con_c[0] * d0_u 
                          + w_con_c[1] * d1_u 
                          + w_con_c[2] * d2_u;

        cnv = dx0 + dx1 + dx2 - cnv_w;
      }
      
      d_arr<T, 3> lap_u_off = ( d == 0 ) ? 
                              psn_hlp.eval_laplace_off_s( u, true ) : // stgr
                              psn_hlp.eval_laplace_off_c( u, true );  // clpt
      d_arr<T, 3> rr = -cnv + nu * lap_u_off;
      dealias( rr );
      return rr;
    };
  }

template <typename V, Dense_array A, typename F0, typename F1, typename G0, 
          Stencil_array O0, Stencil_array O1>
  decltype(auto) eval_divergence( V& vtk, 
                                  const A& u0s, const A& u1, const A& u2, 
                                  F0&& cl_to_sg, F1&& sg_d0_to_cl, G0&& crv_grd_c, 
                                  O0&& d1c, O1&& d2c )
  {
    constexpr auto D = u0s.rank();
    using T = Value_type<A>;
    using R = complex<T>;

    d_arr<R, D> U1D1 = d1c( to_freq( u1 ) );
    d_arr<R, D> U2D2 = d2c( to_freq( u2 ) );

    d_arr<T, D> u1s   = cl_to_sg( u1 );
    d_arr<T, D> u2s   = cl_to_sg( u2 );
    d_arr<T, D> u0d0  = sg_d0_to_cl( u0s );
    d_arr<T, D> u1d0  = sg_d0_to_cl( u1s ); 
    d_arr<T, D> u2d0  = sg_d0_to_cl( u2s ); 
    d_arr<T, D> u1d1  = to_real( U1D1 );
    d_arr<T, D> u2d2  = to_real( U2D2 );

    decltype(auto) s = crv_grd_c.b_contra();
    d_arr<T, D> s00  = s[0][0];
    d_arr<T, D> s01  = s[0][1];
    d_arr<T, D> s02  = s[0][2];
    d_arr<T, D> s11  = s[1][1];
    d_arr<T, D> s22  = s[2][2];
    d_arr<T, D> div0 = s00 * u0d0;
    d_arr<T, D> div1 = s01 * u1d0 + s11 * u1d1;
    d_arr<T, D> div2 = s02 * u2d0 + s22 * u2d2;
    d_arr<T, D> div  = div0 + div1 + div2;

    vtk.name( "s00" )   << s00;
    vtk.name( "s01" )   << s01;
    vtk.name( "s02" )   << s02;
    vtk.name( "s11" )   << s11;
    vtk.name( "s22" )   << s22;
    vtk.name( "u0d0" )  << u0d0;
    vtk.name( "u1d0" )  << u1d0;
    vtk.name( "u1d1" )  << u1d1;
    vtk.name( "u2d0" )  << u2d0;
    vtk.name( "u2d2" )  << u2d2;

    vtk.name( "div_0" ) << div0;
    vtk.name( "div_1" ) << div1;
    vtk.name( "div_2" ) << div2;

    return div;
  }

// -----------------------  Project Functions  -------------------------------------------

template <typename T,  typename F0, typename F1, typename F2, 
          typename G0, typename G1, typename O0, typename O1>
  decltype(auto) d_phi_func( F0&& cl_to_sg,  F1&& cl_d0_to_sg, F2&& sg_d0_to_cl,
                             G0&& crv_grd_c, G1&& crv_grd_s, 
                             O0&& d1c, O1&& d2c )
  {
    return [&]( std::size_t i, auto& phi )
    {
      constexpr auto D = phi.rank();
      using A = Main_type<decltype(phi)>;
      using R = complex<T>;

      decltype(auto) dksi_dx_c = crv_grd_c.b_contra();
      decltype(auto) dksi_dx_s = crv_grd_s.b_contra();

      A t;

      if( i == 0 ) // u: stgr
      {
        d_arr<T, D> phi_d0s = cl_d0_to_sg( phi ); 
        t = dksi_dx_s[0][0] * phi_d0s;
      }
      else if( i == 1 )
      {
        d_arr<T, D> phi_s  = cl_to_sg( phi );
        d_arr<T, D> phi_d0 = sg_d0_to_cl( phi_s );
        d_arr<R, D> PHI_d1 = d1c( to_freq( phi ) );
        d_arr<T, D> phi_d1 = to_real( PHI_d1 );
        t = dksi_dx_c[0][1] * phi_d0
          + dksi_dx_c[1][1] * phi_d1;
      }
      else if( i == 2 )
      {
        d_arr<T, D> phi_s  = cl_to_sg( phi );
        d_arr<T, D> phi_d0 = sg_d0_to_cl( phi_s );
        d_arr<R, D> PHI_d2 = d2c( to_freq( phi ) );
        d_arr<T, D> phi_d2 = to_real( PHI_d2 );
        t = dksi_dx_c[0][2] * phi_d0;
          + dksi_dx_c[2][2] * phi_d2;
      }

      return t;
    };
  }

template <typename T, typename G, typename O>
  decltype(auto) proj_dp_func( T dt, T alpha_0, T beta_0, G&& crv_grd_c, O&& dfus_op )
  {
    return [&]( auto& phi )
    {
      using A = Main_type<decltype(phi)>;
      A dp = phi - dt * beta_0 / alpha_0 * dfus_op( phi );
      return dp;
    };
  }


// -----------------------  Fractional N-S  -------------------------------------------

template <typename A, typename B, typename O0, typename O1>
  decltype(auto) uh_slvr_sor( size_t d, 
                              const A& uh_top, const A& uh_btm, const B& bf, 
                              O0&& dksi0, O1&& nu_lap_diag )
  {
    return [&]( auto& rhs, auto a0, auto& ut, auto beta_0, auto&& op )
    {
      using T = Value_type<A>;
      
      auto on_node = make_on_node<T>( ut.descriptor() );
      auto lhs = make_stencil_array( a0 * on_node - beta_0 * nu_lap_diag );
      auto prb = make_stencil_array(
          [&]( auto& desc, auto idx, auto it_lhs, auto it_drch, auto it_d0 )
          {
            if( idx[0] == desc.lbound(0) ) // bottom
              return ( d == 0 ) ? *it_drch : *it_d0;
            else if( idx[0] == desc.ubound(0) ) // top
              return *it_d0;
            else
              return *it_lhs;
          },
          lhs, 
          on_node, 
          dksi0 );

      rhs += bf;
      rhs( first(), all(), all() ) = uh_btm;
      rhs( last(),  all(), all() ) = uh_top;

      d_arr<T, 3> u_cp = ut;

      for( size_t it = 0; it < 1; ++it )
      {
        lsor_async( prb, rhs, ut, 0, 1.0 );

        d_arr<T, 3> ee = ut - u_cp;
        T err = std::abs( norm2(ee) );
        err = std::sqrt( err*err / T( u_cp.size() ));
        u_cp = ut;
        cout << "[Iter = " << it << "] Err. RMSE = " << err << endl;
        if( err < 1.e-7 )
          break;
      }
      return u_cp;
    };
  }


