#include <iostream>
#include <complex>
#include <spx/spx.h>

using namespace std;
using namespace spx;

int main()
{
  using T = long double; 
  using C = std::complex<T>;
  
  d_array<C, 2> v{ 2, 5 };
  v = { C{1}, C{2}, C{3}, C{4}, C{5}, 
        C{1}, C{2}, C{3}, C{4}, C{5} }; 
 
  //for( auto x : v )
  //  cout << x << endl;

  //cout << endl;

  //using W = typename fftw<T>::complex_t;
  //W* in  = reinterpret_cast<W*>( v.data() );
  //W* out = reinterpret_cast<W*>( v.data() );

  //auto shape = v.extents();
  //d_vector<int> s( v.size() );
  //std::copy( std::begin(shape), std::end(shape), s.data() );
  //auto n = s.data();
  //
  //cout << "size = " << n[0] << ", " << n[1] << endl << endl;

  //decltype(auto) p = fftw<T, 2>::forward( n, in, out );
  //fftw<T, 2>::execute( p );
  //fftw<T, 2>::destroy_plan( p );
  //
  //for( auto x : v )
  //  cout << x << endl;

  //cout << endl;

  //decltype(auto) q = fftw<T>::backward( n, in, out );
  //fftw<T, 2>::execute( q );
  //fftw<T, 2>::destroy_plan( q );
  //
  //for( auto x : v )
  //  cout << x << endl;

  cout << endl << " ========================== \n\n";

  auto F = fft( d_vector<C>{ C{1}, C{2}, C{3}, C{4}, C{5} } );
  // auto F = fft( vector<T>{ 1, 2, 3, 4, 5 } );
  for( auto x : F )
    cout << x << endl;

  cout << endl;

  auto f = ifft( F );
  for( auto x : f )
    cout << x << endl;

  cout << endl << " ========================== \n\n";

  d_array<T, 2> vv{ 2, 5 };
  vv = { 1, 2, 3, 4, 5,
         6, 7, 8, 9, 10 };

  auto FF = fft( vv );

  for( auto x : FF )
    cout << x << endl;
  cout << endl;

  auto ff = ifft( FF );

  cout << "\nifft: \n";
  for( auto x : ff )
    cout << x << endl;
  cout << endl;

  cout << endl << " ========================== \n\n";
 
  std::size_t n0 = 16;

  std::size_t N = n0;
  T L = pi_2<T>;
  T dx = L / T(N);

  auto p = make_fourier_basis( N, dx  );
  p.set_diff_order( 2 );
  p.print();

  auto x = p.coords();
  d_vector<T> u   =  spx::sin( x );
 
  cout << "u_exact = \n\n";
  for( auto v: u )
    cout << v << ", " << endl;
  cout << endl;

  stencil_1d_to_nd<decltype(p), 1> gen1( {n0}, p, 0 );
  auto d0d0 = make_stencil_array( std::move(gen1) );

  auto U  = fft( u );
  using R = d_vector<Value_type<decltype(U)>>; 
  R RHS = d0d0( U );

  // solve
  R U_num( RHS.size() );
  psor( d0d0, RHS, U_num );
 
  cout << "RHS_(numerical) = \n\n";
  auto rhs = ifft( RHS );
  for( auto v: rhs )
    cout << std::real(v) << ", " << endl;
  cout << endl << endl;

  cout << "u_numerical = \n\n";
  auto u_num = ifft( U_num );
  for( auto v: u_num )
    cout << std::real(v) << ", " << endl;
  cout << endl;

  return 0;
}
