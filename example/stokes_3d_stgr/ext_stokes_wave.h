
template <typename T>
  struct exct_stokes
  {
    T lambda = pi_2<T>;
    T k      = pi_2<T> / lambda;
 
    T c;
    T nu;
    T g;
    T gma;
    T omg;
    T ak;
    T a0;
    T prd;

    T m  = 0.5;
    T n  = 1.23;
    T kx = k + m;
    T ky = n;
    T kk = std::sqrt( kx*kx + ky*ky );
    T eps;
    T cc; 
 
    exct_stokes( T re, T g, T gma, T ak = 0.3, T eps = 0 )
      : g( g ), gma( gma ), ak( ak ), eps( eps )
    {
      a0  = ak / k;
      omg = k * (1 + 0.5 * ak * ak) * std::sqrt( g/k );
      c   = omg / k;
      nu  = c / k / re;
      cc  = eps * a0 /  std::sqrt( kk );
      prd = pi_2<T> / omg;
      
      cout << " c  = "  << c   << endl;
      cout << " nu = "  << nu  << endl;
      cout << " k  = "  << k   << endl;
      cout << " a0 = "  << a0  << endl;
      cout << " omg = " << omg << endl;
      cout << " T = "   << prd << endl;
    }

    template <Dense_array A>
      decltype(auto) eta( T t, const A& x )
      {
        constexpr auto D = x.rank();
        d_arr<T, D> th  = k * x[0] - omg * t;
        d_arr<T, D> eta = a0 * ( cos(th) 
                              + 0.5    * ak *      cos( 2.0*th ) 
                              + 3.0/8.0* ak * ak * cos( 3.0*th ) );

        eta += eps * a0 * sin( kx * x[0] ) * cos( ky * x[1] );
        return eta;
      }

    template <Dense_array A>
      decltype(auto) u2( T t, const A& x )
      {
        constexpr auto D = x.rank();
        d_arr<T, D> r( x.extents() );
        r = 0;
        
        r += ky * cc * cos( kx * x[1] ) * sin( ky * x[2] ) * exp( kk * x[0] );
        return r;
      }

    template <Dense_array A>
      decltype(auto) u1( T t, const A& x )
      {
        constexpr auto D = x.rank();
        d_arr<T, D> th = k * x[1] - omg * t;
        d_arr<T, D> r  = a0 * omg * cos( th ) * exp( k * x[0] ); 

        r += kx * cc * sin( kx * x[1] ) * cos( ky * x[2] ) * exp( kk * x[0] );
        return r;
      }
 
    template <Dense_array A>
      decltype(auto) u0( T t, const A& x )
      {
        constexpr auto D = x.rank();
        d_arr<T, D> th = k * x[1] - omg * t;
        d_arr<T, D> r  = a0 * omg * sin( th ) * exp( k * x[0] );

        r += -kk * cc * cos( kx * x[1] ) * cos( ky * x[2] ) * exp( kk * x[0] );
        return r;
      }
 };


