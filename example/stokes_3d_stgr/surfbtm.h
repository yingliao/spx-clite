
template <typename T, std::size_t D>
  struct surf_coef_helper
  {
    using value_type = T;
    static constexpr std::size_t rank() { return D; }
    
    d_arr<T, D> eta;
    d_arr<T, D> eta_d1, eta_d1d1, eta_d1_2; 
    d_arr<T, D> eta_d2, eta_d2d2, eta_d2_2; 
    d_arr<T, D> eta_d1d2;

    d_arr<T, D> s00, s01, s02, s11, s22;
    
    d_arr<T, D> a1, b1, c1, d1;
    d_arr<T, D> a2, b2, c2, d2;

    d_arr<T, D> e1, e2, e3, e4, e5, e6, e7, e8;
    d_arr<T, D> f1, f2, f3, f4, f5, f6, f7, f8;

    d_arr<T, D> u0_dksi0, u0_dksi1, u0_dksi2;
    d_arr<T, D> u1_dksi0, u1_dksi1, u1_dksi2;
    d_arr<T, D> u2_dksi0, u2_dksi1, u2_dksi2;

    template <typename G, typename B, 
              typename Q0, typename Q1, typename Q2, typename Q3>
      void update_grd( G&& crv_grd, const B& ETA,
                       Q0&&  eta_dx1, Q1&&  eta_dx1dx1, 
                       Q2&&  eta_dx2, Q3&&  eta_dx2dx2 )
      {
        B ETA_d1   = eta_dx1( ETA );
        B ETA_d1d1 = eta_dx1dx1( ETA );
        B ETA_d2   = eta_dx2( ETA );
        B ETA_d2d2 = eta_dx2dx2( ETA );
        B ETA_d1d2 = eta_dx2( ETA_d1 );

        eta      = to_real( ETA );
        eta_d1   = to_real( ETA_d1 );
        eta_d2   = to_real( ETA_d2 );
        eta_d1d1 = to_real( ETA_d1d1 );
        eta_d2d2 = to_real( ETA_d2d2 );
        eta_d1d2 = to_real( ETA_d1d2 );
        eta_d1_2 = eta_d1 * eta_d1;
        eta_d2_2 = eta_d2 * eta_d2;

        decltype(auto) dksi_dx = crv_grd.b_contra();
        s00 = make_callback_array( dksi_dx[0][0] )( last(), all(), all() );
        s01 = make_callback_array( dksi_dx[0][1] )( last(), all(), all() );
        s02 = make_callback_array( dksi_dx[0][2] )( last(), all(), all() );
        s11 = make_callback_array( dksi_dx[1][1] )( last(), all(), all() );
        s22 = make_callback_array( dksi_dx[2][2] )( last(), all(), all() );

        a1 = T(2) * eta_d1;
        b1 = -eta_d2;
        c1 = T(1) - eta_d1_2;
        d1 = -eta_d1d2;

        e1 = T(2) * a1 * s00 + c1 * s01 + d1 * s02;
        e2 = c1 * s11;
        e3 = d1 * s22;
        e4 = c1 * s00 + b1 * s02;
        e5 = b1 * s22;
        e6 = a1 * s02 + b1 * s01 + d1 * s00;
        e7 = b1 * s11;
        e8 = a1 * s22;

        a2 = T(2) * eta_d2;
        b2 = -eta_d1;
        c2 = T(1) - eta_d2_2;
        d2 = -eta_d1d2;

        f1 = T(2) * a2 * s00 + c2 * s02 + d2 * s01;
        f2 = d2 * s11;
        f3 = c2 * s22;
        f4 = a2 * s01 + b2 * s02 + d2 * s00;
        f5 = a2 * s11;
        f6 = b2 * s22;
        f7 = c2 * s00 + b2 * s01;
        f8 = b2 * s11;
      }

    template <Dense_array A, 
              typename O0, typename O1, typename O2, typename O3, typename O4, typename O5>
      void update_vel( const A& u0s, const A& u1, const A& u2,
                       O0&& dksi0_s_l, O1&& dksi1_s, O2&& dksi2_s,
                       O3&& dksi0_c_h, O4&& dksi1_c, O5&& dksi2_c )
      {
        using R = complex<T>;

        auto U0s = to_freq( u0s );
        auto U1  = to_freq( u1 );
        auto U2  = to_freq( u2 );

        // U0
        //
        auto U0_dksi0_e = make_callback_array( dksi0_s_l( U0s ) );
        auto U0_dksi1_e = make_callback_array( dksi1_s(   U0s ) );
        auto U0_dksi2_e = make_callback_array( dksi2_s(   U0s ) );

        d_arr<R, D> U0_dksi0   = U0_dksi0_e( last(),   all(), all() );
        d_arr<R, D> U0_dksi1_u = U0_dksi1_e( last(),   all(), all() );
        d_arr<R, D> U0_dksi1_l = U0_dksi1_e( last()-1, all(), all() );
        d_arr<R, D> U0_dksi2_u = U0_dksi2_e( last(),   all(), all() );
        d_arr<R, D> U0_dksi2_l = U0_dksi2_e( last()-1, all(), all() );

        u0_dksi0 = to_real( U0_dksi0 );
        u0_dksi1 = 0.5 * ( to_real( U0_dksi1_u ) + to_real( U0_dksi1_l ) );
        u0_dksi2 = 0.5 * ( to_real( U0_dksi2_u ) + to_real( U0_dksi2_l ) );

        // U1
        //
        auto U1_dksi0_e = make_callback_array( dksi0_c_h( U1 ) );
        auto U1_dksi1_e = make_callback_array( dksi1_c(   U1 ) );
        auto U1_dksi2_e = make_callback_array( dksi2_c(   U1 ) );
 
        d_arr<R, D> U1_dksi0 = U1_dksi0_e( last(), all(), all() );
        d_arr<R, D> U1_dksi1 = U1_dksi1_e( last(), all(), all() );
        d_arr<R, D> U1_dksi2 = U1_dksi2_e( last(), all(), all() );
 
        u1_dksi0 = to_real( U1_dksi0 );
        u1_dksi1 = to_real( U1_dksi1 );
        u1_dksi2 = to_real( U1_dksi2 );

        // U2
        //
        auto U2_dksi0_e = make_callback_array( dksi0_c_h( U2 ) );
        auto U2_dksi1_e = make_callback_array( dksi1_c(   U2 ) );
        auto U2_dksi2_e = make_callback_array( dksi2_c(   U2 ) );

        d_arr<R, D> U2_dksi0 = U2_dksi0_e( last(), all(), all() );
        d_arr<R, D> U2_dksi1 = U2_dksi1_e( last(), all(), all() );
        d_arr<R, D> U2_dksi2 = U2_dksi2_e( last(), all(), all() );
 
        u2_dksi0 = to_real( U2_dksi0 );
        u2_dksi1 = to_real( U2_dksi1 );
        u2_dksi2 = to_real( U2_dksi2 );
    }

    template <typename O>
      void dump( O& out )
      {
        out.name( "a1" ) << a1;
        out.name( "b1" ) << b1;
        out.name( "c1" ) << c1;
        out.name( "d1" ) << d1;
        
        out.name( "a2" ) << a2;
        out.name( "b2" ) << b2;
        out.name( "c2" ) << c2;
        out.name( "d2" ) << d2;
 
        out.name( "e1" ) << e1;
        out.name( "e2" ) << e2;
        out.name( "e3" ) << e3;
        out.name( "e4" ) << e4;
        out.name( "e5" ) << e5;
        out.name( "e6" ) << e6;
        out.name( "e7" ) << e7;
        out.name( "e8" ) << e8;

        out.name( "f1" ) << f1;
        out.name( "f2" ) << f2;
        out.name( "f3" ) << f3;
        out.name( "f4" ) << f4;
        out.name( "f5" ) << f5;
        out.name( "f6" ) << f6;
        out.name( "f7" ) << f7;
        out.name( "f8" ) << f8;

        out.name( "s00" ) << s00;
        out.name( "s01" ) << s01;
        out.name( "s02" ) << s02;
        out.name( "s11" ) << s11;
        out.name( "s22" ) << s22;
      }
  };

// -----------------------  Surface / Bottom BC  -------------------------------------------
 
template <typename S, Dense_array A, typename O0>
  decltype(auto) surf_u0_bc( const S& sf, const A& r0s, O0&& dksi0_s_l )
  {
    using T = Value_type<A>;

    auto r0s_dksi0_e = make_callback_array( dksi0_s_l( r0s ) );
    d_arr<T, 2> r0s_dksi0 = r0s_dksi0_e( last(), all(), all() );

    d_arr<T, 2> rhs = r0s_dksi0 - ( sf.s01 * sf.u1_dksi0 
                                  + sf.s11 * sf.u1_dksi1 
                                  + sf.s02 * sf.u2_dksi0 
                                  + sf.s22 * sf.u2_dksi2 ) / sf.s00;
    dealias( rhs );
    return rhs;
  }

template <typename S, Dense_array A, typename O0>
  decltype(auto) surf_u1_bc( const S& sf, const A& r1, O0&& dksi0 )
  {
    using T = Value_type<S>;

    auto r1_dksi0_e = make_callback_array( dksi0( r1 ) );
    d_arr<T, 2> r1_dksi0 = r1_dksi0_e( last(), all(), all() );

    d_arr<T, 2> tmp = sf.e1 * sf.u0_dksi0
                    + sf.e2 * sf.u0_dksi1
                    + sf.e3 * sf.u0_dksi2
                    + sf.e5 * sf.u1_dksi2
                    + sf.e6 * sf.u2_dksi0
                    + sf.e7 * sf.u2_dksi1
                    + sf.e8 * sf.u2_dksi2;

    d_arr<T, 2> rhs = r1_dksi0 - tmp / sf.e4; 
    dealias( rhs );
    return rhs;
  }

template <typename S, Dense_array A, typename O0>
  decltype(auto) surf_u2_bc( const S& sf, const A& r2, O0&& dksi0 )
  {
    using T = Value_type<S>;

    auto r2_dksi0_e = make_callback_array( dksi0( r2 ) );
    d_arr<T, 2> r2_dksi0 = r2_dksi0_e( last(), all(), all() );

    d_arr<T, 2> tmp = sf.f1 * sf.u0_dksi0
                    + sf.f2 * sf.u0_dksi1
                    + sf.f3 * sf.u0_dksi2
                    + sf.f4 * sf.u1_dksi0
                    + sf.f5 * sf.u1_dksi1
                    + sf.f6 * sf.u1_dksi2
                    + sf.f8 * sf.u2_dksi1;

    d_arr<T, 2> rhs = r2_dksi0 - tmp / sf.f7; 
    dealias( rhs );
    return rhs;
  }


template <Dense_array A, Dense_array B>
  decltype(auto) btm_u_bc( size_t d, const A& r_prj, const B& u )
  {
    using T = Value_type<A>;

    d_arr<T, 2> r_btm = r_prj( first(), all(), all() );
    if( d == 0 )
    {
      r_btm += r_prj( first()+1, all(), all() );
      r_btm /= 2.0;
    }
    d_arr<T, 2> uh_btm = u + r_btm;
    return uh_btm;
  }

template <Dense_array A, typename O0, Dense_array B, typename G>
  decltype(auto) btm_dx0_bc( const A& r_prj, O0&& dksi0, const B& du, G&& crv_grd )
  {
    using T = Value_type<A>;

    decltype(auto) dksi_dx = crv_grd.b_contra();
    auto           s00     = make_callback_array( dksi_dx[0][0] );
    d_arr<T, 2>    s00_btm = s00( first(), all(), all() );
 
    auto r_dksi0 = make_callback_array( dksi0( r_prj ) );
    d_arr<T, 2> r_dksi0_btm = r_dksi0( first(), all(), all() );

    d_arr<T, 2> rhs = du / s00_btm + r_dksi0_btm;
    return rhs;
  }

template <typename V, typename S, typename T>
  decltype(auto) surf_pressure( V& vtk, const S& sf, T mu, T rho, T g, T gamma )
  {
    // kappa
    //
    d_arr<T, 2> tmp1      = T(1) + sf.eta_d1_2;
    d_arr<T, 2> tmp2      = T(1) + sf.eta_d2_2;
    d_arr<T, 2> kappa_up  = tmp1 * sf.eta_d2d2 
                          + tmp2 * sf.eta_d1d1 
                          - T(2) * sf.eta_d1 * sf.eta_d2 * sf.eta_d1d2;
    d_arr<T, 2> kappa_dwn = pow( T(1) + sf.eta_d1_2 + sf.eta_d2_2, 1.5 );
    d_arr<T, 2> kappa     = kappa_up / kappa_dwn;
 
    d_arr<T, 2> u0_dx0 = sf.s00 * sf.u0_dksi0;
    d_arr<T, 2> u0_dx1 = sf.s01 * sf.u0_dksi0 + sf.s11 * sf.u0_dksi1;
    d_arr<T, 2> u0_dx2 = sf.s02 * sf.u0_dksi0 + sf.s22 * sf.u0_dksi2;
    d_arr<T, 2> u1_dx0 = sf.s00 * sf.u1_dksi0;
    d_arr<T, 2> u1_dx1 = sf.s01 * sf.u1_dksi0 + sf.s11 * sf.u1_dksi1;
    d_arr<T, 2> u1_dx2 = sf.s02 * sf.u1_dksi0 + sf.s22 * sf.u1_dksi2;
    d_arr<T, 2> u2_dx0 = sf.s00 * sf.u2_dksi0;
    d_arr<T, 2> u2_dx1 = sf.s01 * sf.u2_dksi0 + sf.s11 * sf.u2_dksi1;
    d_arr<T, 2> u2_dx2 = sf.s02 * sf.u2_dksi0 + sf.s22 * sf.u2_dksi2;

    d_arr<T, 2> M1  = sf.eta_d1 * ( u0_dx1 + u1_dx0 );
    d_arr<T, 2> M2  = sf.eta_d2 * ( u0_dx2 + u2_dx0 );
    d_arr<T, 2> M11 = sf.eta_d1_2 * u1_dx1;
    d_arr<T, 2> M22 = sf.eta_d2_2 * u2_dx2;
    d_arr<T, 2> M12 = sf.eta_d1 * sf.eta_d2 * ( u1_dx2 + u2_dx1 );

    d_arr<T, 2> M  = 2.0 * mu / ( 1 + sf.eta_d1_2 );
                M *= u0_dx0 - M1 - M2 + M11 + M12 + M22;
    
    d_arr<T, 2> eta = sf.eta;
    d_arr<T, 2> p   = M + rho * g * eta - gamma * kappa;

    eta *= rho*g;
    vtk.name( "kappa" )     << kappa;
    vtk.name( "u0_dx0" )    << u0_dx0;
    vtk.name( "u0_dx1" )    << u0_dx1;
    vtk.name( "u0_dx2" )    << u0_dx2;
    vtk.name( "u1_dx0" )    << u1_dx0;
    vtk.name( "u1_dx1" )    << u1_dx1;
    vtk.name( "u1_dx2" )    << u1_dx2;
    vtk.name( "u2_dx0" )    << u2_dx0;
    vtk.name( "u2_dx1" )    << u2_dx1;
    vtk.name( "u2_dx2" )    << u2_dx2;
    vtk.name( "M1" )        << M1;
    vtk.name( "M2" )        << M2;
    vtk.name( "M11" )       << M11;
    vtk.name( "M22" )       << M22;
    vtk.name( "M12" )       << M12;
    vtk.name( "M" )         << M;
    vtk.name( "rho_g_eta" ) << eta;

    return p;
  }

template <Dense_array A, Dense_array B, typename G, typename T>
  decltype(auto) btm_phi( G&& crv_grd, const A& uh0s, const B& u0_btm, T alpha_0, T dt )
  {
    decltype(auto) dksi_dx = crv_grd.b_contra();
    auto           s00     = make_callback_array( dksi_dx[0][0] );
    d_arr<T, 2>    s00_btm = s00( first(), all(), all() );
 
    d_arr<T, 2> uh0_btm = 0.5 * ( uh0s( first(),   all(), all() )
                                + uh0s( first()+1, all(), all() ) ) ;
    d_arr<T, 2> phi_btm = alpha_0 / dt / s00_btm * ( uh0_btm - u0_btm );

    return phi_btm;
 }
