// ----------------------- Operators / Transformation ------------------------------------

template <typename V, Dense_array A, typename F, typename G0, 
          Stencil_array O0, Stencil_array O2, Stencil_array O3>
  decltype(auto) eval_divergence( V& vtk, 
                                  const A& u0s, const A& u1, const A& u2, 
                                  F&& cl_to_sg, G0&& crv_grd_c, 
                                  O0&& dksi0_s_l, O2&& dksi1, O3&& dksi2 )
  {
    constexpr auto D = u0s.rank();
    using T = Value_type<A>;
    using R = complex<T>;

    d_arr<R, D> U1D1 = dksi1( to_freq( u1 ) );
    d_arr<R, D> U2D2 = dksi2( to_freq( u2 ) );

    d_arr<T, D> u1s   = cl_to_sg( u1 );
    d_arr<T, D> u2s   = cl_to_sg( u2 );
    d_arr<T, D> u0d0s = dksi0_s_l( u0s ); 
    d_arr<T, D> u1d0s = dksi0_s_l( u1s ); 
    d_arr<T, D> u2d0s = dksi0_s_l( u2s ); 
    d_arr<T, D> u0d0  = u0d0s( slice( first(), last()-1 ), all(), all() );
    d_arr<T, D> u1d0  = u1d0s( slice( first(), last()-1 ), all(), all() );
    d_arr<T, D> u2d0  = u2d0s( slice( first(), last()-1 ), all(), all() );
    d_arr<T, D> u1d1  = to_real( U1D1 );
    d_arr<T, D> u2d2  = to_real( U2D2 );

    decltype(auto) s = crv_grd_c.b_contra();
    d_arr<T, D> s00  = s[0][0];
    d_arr<T, D> s01  = s[0][1];
    d_arr<T, D> s02  = s[0][2];
    d_arr<T, D> s11  = s[1][1];
    d_arr<T, D> s22  = s[2][2];
    d_arr<T, D> div0 = s00 * u0d0;
    d_arr<T, D> div1 = s01 * u1d0 + s11 * u1d1;
    d_arr<T, D> div2 = s02 * u2d0 + s22 * u2d2;
    d_arr<T, D> div  = div0 + div1 + div2;

    vtk.name( "s00" )   << s00;
    vtk.name( "s01" )   << s01;
    vtk.name( "s02" )   << s02;
    vtk.name( "s11" )   << s11;
    vtk.name( "s22" )   << s22;
    vtk.name( "u0d0" )  << u0d0;
    vtk.name( "u1d0" )  << u1d0;
    vtk.name( "u1d1" )  << u1d1;
    vtk.name( "u2d0" )  << u2d0;
    vtk.name( "u2d2" )  << u2d2;

    vtk.name( "div_0" ) << div0;
    vtk.name( "div_1" ) << div1;
    vtk.name( "div_2" ) << div2;

    return div;
  }

// -----------------------  Fractional N-S  -------------------------------------------

template <typename A, typename O0, typename O1>
  decltype(auto) uh_slvr_sor( size_t d, const A& uh_bc, O0&& dksi0, O1&& nu_lap_diag )
  {
    return [&]( auto& rhs, auto a0, auto& ut, auto beta_0, auto&& op )
    {
      using T = Value_type<A>;
      using R = complex<T>;
      
      auto on_node = make_on_node<R>( ut.descriptor() );
      auto hl_node = make_half_on_node<R>( ut.descriptor() );
      auto d0R = make_stencil_array( R(1) * dksi0 );
      auto lhs = make_stencil_array( a0 * on_node - beta_0 * nu_lap_diag );
      auto prb = make_stencil_array(
          [&]( auto& desc, auto idx, auto it_lhs, auto it_drch, auto it_drch_hl, auto it_d0R )
          {
            if( idx[0] == desc.lbound(0) ) // bottom
              return ( d == 0 ) ? *it_drch_hl : *it_drch;
            else if( idx[0] == desc.ubound(0) ) // top
              return *it_d0R;
            else
              return *it_lhs;
          },
          lhs, 
          on_node, 
          hl_node,
          d0R );

      rhs( first(), all(), all() ) = uh_bc( first(), all(), all() );
      rhs( last(),  all(), all() ) = uh_bc( last(),  all(), all() );
      auto RHS_it = to_freq( rhs );

      d_arr<R, 3> Ut   = to_freq( ut );
      d_arr<T, 3> u_cp = ut;

      for( size_t it = 0; it < 1; ++it )
      {
        lsor_async( prb, RHS_it, Ut, 0, 1.0 );

        auto ut = to_real( Ut );
        d_arr<T, 3> ee = ut - u_cp;
        T err = std::abs( norm2(ee) );
        err = std::sqrt( err*err / T( u_cp.size() ));
        u_cp = ut;
        cout << "[Iter = " << it << "] Err. RMSE = " << err << endl;
        if( err < 1.e-7 )
          break;
      }
      return u_cp;
    };
  }


