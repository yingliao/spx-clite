// vtk function
//
template <Dense_array A>
  decltype(auto) make_vec( const A& u0, const A& u1, const A& u2 )
  {
    using T = Value_type<A>;
    using S = static_vector<T, 3>;

    A u0o( u1.extents() );
    if( u0.extents()[0] == u1.extents()[0] + 1 )
      u0o = stgr_to_clpt( u0 );
    else
      u0o = u0;

    d_arr<S, 3> u( u1.extents() );
    u[0] = u0o;
    u[1] = u1;
    u[2] = u2;
    return u;
  }


