#  This file is part of the OpenLB library
#
#  Copyright (C) 2007 Mathias Krause
#  Address: Wilhelm-Maybach-Str. 24, 68766 Hockenheim, Germany 
#  E-mail: mathias.j.krause@gmx.de
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public 
#  License along with this program; if not, write to the Free 
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA  02110-1301, USA.


###########################################################################
###########################################################################
## DEFINITIONS TO BE CHANGED

#CXX             := clang++ -std=c++11 -ftemplate-depth-2048
#CXX             := /usr/local/bin/g++ -std=c++1y -ftemplate-depth-2048
#CXX             := /usr/local/bin/g++ -std=c++1z -ftemplate-depth-2048
CXX             := g++ -std=c++1z -ftemplate-depth-2048
#CXX             := mpiCC

OPTIM           := -O3 -Wall
DEBUG           := -g -DOLB_DEBUG

UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S), Linux)
  FFTW_INC        := /opt/fftw/include
  FFTW_LIB        := -L/opt/fftw/lib -lfftw
else
  FFTW_INC        := /usr/local/Cellar/fftw/3.3.4_1/include
  FFTW_LIB        := -L/usr/local/Cellar/fftw/3.3.4_1/lib -lfftw3 -lfftw3f -lfftw3l 
endif

PY_INC          := #-I/Users/liao/anaconda/include/python2.7 -I/Users/liao/anaconda/pkgs/numpy-1.8.1-py27_0/lib/python2.7/site-packages/numpy/core/include
PY_LIB          := #-L/Users/liao/anaconda/lib -lpython2.7 -L/Users/liao/anaconda/pkgs/numpy-1.8.1-py27_0/lib/python2.7/site-packages/numpy/core/lib -lnpymath

HDF5_INC        := -I/opt/hdf5/include
HDF5_LIB        := -L/opt/hdf5/lib -lhdf5 -lhdf5_cpp -lhdf5_hl -lhdf5_hl_cpp

CXXFLAGS        := $(OPTIM) -I$(FFTW_INC) $(PY_INC) $(HDF5_INC)
#CXXFLAGS        := $(DEBUG)

LDFLAGS         := $(FFTW_LIB) -llapack $(PY_LIB) $(HDF5_LIB)
#LDFLAGS         := -lfftw3 -framework vecLib  # Mac OS X

PARALLEL_MODE   := OFF
#PARALLEL_MODE   := MPI
#PARALLEL_MODE   := OMP
#PARALLEL_MODE   := HYBRID

MPIFLAGS        :=
OMPFLAGS        := -fopenmp

#BUILDTYPE       := precompiled
BUILDTYPE       := generic


###########################################################################
## conditional settings

ifeq ($(BUILDTYPE), precompiled)
   CXXFLAGS := -DOLB_PRECOMPILED $(CXXFLAGS)
endif

ifeq ($(PARALLEL_MODE), MPI)
   CXXFLAGS := -DPARALLEL_MODE_MPI $(MPIFLAGS) $(CXXFLAGS)
endif

ifeq ($(PARALLEL_MODE), OMP)
   CXXFLAGS := -DPARALLEL_MODE_OMP $(OMPFLAGS) $(CXXFLAGS)
   LDFLAGS  := $(OMPFLAGS) $(LDFLAGS)
endif

ifeq ($(PARALLEL_MODE), HYBRID)
   CXXFLAGS := -DPARALLEL_MODE_OMP -DPARALLEL_MODE_MPI $(OMPFLAGS) $(MPIFLAGS) $(CXXFLAGS)
   LDFLAGS  := $(OMPFLAGS) $(LDFLAGS)
endif

###########################################################################
## defines shell

SHELL           := /bin/sh

###########################################################################
## dependencies, object, library directory and library name

DEPENDDIR       := build/$(BUILDTYPE)/dep
OBJDIR          := build/$(BUILDTYPE)/obj
LIBDIR          := build/$(BUILDTYPE)/lib
LIB             := 

###########################################################################
## search directories

SUBDIRS         := ../spx

EXAMPLEDIRS     := ../examples/testAlgm

BUILDTYPEDIRS   := build/precompiled \
                   build/generic

SUBDIRSH        := $(foreach d,$(SUBDIRS),$(ROOT)/$(d))
IDIR            := -I$(ROOT)/../

###########################################################################
###########################################################################
