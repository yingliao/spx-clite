
code = """
template <class D, class T>
  template <class U>
  requires #CONCEPT#<T, U>()
    inline decltype(auto) arithmetic<D, T>::operator #OP# ( const U& u )
    {
      return static_cast<D&>(*this).apply( u, ops::#OPS#() );
    }
"""

code2 =  """
template <class D, class T>
  template <Range R>
  requires #CONCEPT#<T, range_value_type<R>>()
    inline decltype(auto) arithmetic<D, T>::operator #OP# ( R&& r )
    {
      return static_cast<D&>(*this).apply( std::forward<R>(r), ops::#OPS#() );
    }
"""

def print_code( template, op, concept, ops ):
    codegen = template
    codegen = codegen.replace("#CONCEPT#", concept)
    codegen = codegen.replace("#OP#", op)
    codegen = codegen.replace("#OPS#", ops)
    print codegen

# arithmetic assignment
print_code( code, "+=", "Has_plus_assign", "plus_assign" )
print_code( code, "-=", "Has_minus_assign", "minus_assign" )
print_code( code, "*=", "Has_multiplies_assign", "multiplies_assign" )
print_code( code, "/=", "Has_divides_assign", "divides_assign" )
print_code( code, "%=", "Has_modulus_assign", "modulus_assign" )
# binary assignment
print_code( code, "&=", "Has_bit_and_assign", "bit_and_assign" )
print_code( code, "|=", "Has_bit_or_assign", "bit_or_assign" )
print_code( code, "^=", "Has_bit_xor_assign", "bit_xor_assign" )
print_code( code, "<<=", "Has_left_shift_assign", "left_shift_assign" )
print_code( code, ">>=", "Has_right_shift_assign", "right_shift_assign" )

# arithmetic assignment
print_code( code2, "+=", "Has_plus_assign", "plus_assign" )
print_code( code2, "-=", "Has_minus_assign", "minus_assign" )
print_code( code2, "*=", "Has_multiplies_assign", "multiplies_assign" )
print_code( code2, "/=", "Has_divides_assign", "divides_assign" )
print_code( code2, "%=", "Has_modulus_assign", "modulus_assign" )
# binary assignment
print_code( code2, "&=", "Has_bit_and_assign", "bit_and_assign" )
print_code( code2, "|=", "Has_bit_or_assign", "bit_or_assign" )
print_code( code2, "^=", "Has_bit_xor_assign", "bit_xor_assign" )
print_code( code2, "<<=", "Has_left_shift_assign", "left_shift_assign" )
print_code( code2, ">>=", "Has_right_shift_assign", "right_shift_assign" )