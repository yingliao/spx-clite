 
code = '''
// OP, FRWD, FUNC

template <typename U, typename T, Descriptor Desc, std::size_t... D>
requires Binary_assignable_operation<ops::FUNC, T, U>()
  inline decltype(auto) 
  operator OP ( const static_array<T, Desc, D...>& v1, 
               const static_array<U, Desc, D...>& v2 )
  {
    using V = function_result<ops::FUNC, T, U>;
    static_array<V, Desc, D...> res = v1;
    res FRWD v2;
    return res;
  }

template <typename X, typename T, Descriptor Desc, std::size_t... D>
requires not Static_array<X>()
      && Binary_assignable_operation<ops::FUNC, T, X>()
  inline decltype(auto)
  operator OP ( const static_array<T, Desc, D...>& v1, 
               const X& v2 )
  {
    using V = function_result<ops::FUNC, T, X>;
    static_array<V, Desc, D...> res = v1;
    res FRWD v2;
    return res;
  }

template <Input_range R, typename T, Descriptor Desc, std::size_t... D>
requires not Static_array<R>()
      && Binary_assignable_operation<ops::FUNC, T, range_value_type<R>>()
  inline decltype(auto)
  operator OP ( const static_array<T, Desc, D...>& v1, 
               const R& v2 )
  {
    using V = function_result<ops::FUNC, T, range_value_type<R>>;
    static_array<V, Desc, D...> res = v1;
    res FRWD v2;
    return res;
  }

template <typename X, typename T, Descriptor Desc, std::size_t... D>
requires not Static_array<X>()
  inline decltype(auto)
  operator OP ( const X& v2, 
               static_array<T, Desc, D...>& v1 )
  {
    return v1 OP v2;
  }
'''

def print_code( op, op_frwd, func_obj ):
    code1 = code
    code1 = code1.replace("OP", op)
    code1 = code1.replace("FRWD", op_frwd)
    code1 = code1.replace("FUNC", func_obj)
    print code1
    
print_code( "+", "+=", "plus" )
print_code( "-", "-=", "minus" )
print_code( "*", "*=", "multiplies" )
print_code( "/", "/=", "divides" )
print_code( "%", "%=", "modulus" )
print_code( "&", "&=", "bit_and" )
print_code( "|", "|=", "bit_or" )
print_code( "^", "^=", "bit_xor" )
print_code( "<<", "<<=", "left_shift" )
print_code( ">>", ">>=", "right_shift" )
