import numpy as np
from stencil import *

class fdm_helper:
  @staticmethod
  def num_stencil_pts( derv_order, order_of_accuracy = 2):
    assert order_of_accuracy > 0, "order of accuracy must be larger than 0"
    return derv_order + order_of_accuracy
    
  @staticmethod
  def stencil( x, ksi, highest_order ):
    N = len( x ) - 1
    M = highest_order
    C = np.zeros( [M+1, N+1, N+1] ) # M-th order
    C[0, 0, 0] = 1
    C1 = 1
    C4 = x[0] - ksi  # TODO: pbc_impl
    
    for i in range(1, N+1):
      MN = min(i, M)
      C2 = 1
      C5 = C4
      C4 = x[i] - ksi # TODO: pbc_impl
      for j in range(0, i):
        C3 = x[i] - x[j] # TODO: pbc_impl
        C2 *= C3
        if i < M:
          C[i, j, i-1] = 0
        C[0, j, i] = C4 * C[0, j, i-1] / C3
        for k in range(1, MN+1):
          C[k, j, i] = (C4 * C[k, j, i-1] - k * C[k-1, j, i-1]) / C3
      C[0, i, i] = -C1 * C5 * C[0, i-1, i-1] / C2
      for k in range(1, MN+1):
        C[k, i, i] = C1 * (k * C[k-1, i-1, i-1] - C5 * C[k, i-1, i-1]) / C2
      C1 = C2
    return C

  @staticmethod
  def stencil_on_nodes( x, ksi, highest_order ):
    N = len( x ) - 1
    M = highest_order
    C1 = 1
    C4 = x[0] - ksi  # TODO: pbc_impl
    C = np.zeros( [M+1, N+1] ) # M-th order
    C[0, 0] = 1
    
    for i in range(1, N+1):
      MN = min( i, M )
      C2 = 1
      C5 = C4
      C4 = x[i] - ksi # TODO: pbc_impl
      for j in range(0, i):
        C3 = x[i] - x[j] # TODO: pbc_impl
        C2 *= C3
        if j == i-1:
          for k in range(1, MN+1)[::-1]: # MN --> 1
            C[k, i] = C1 * (k * C[k-1, i-1] - C5 * C[k, i-1] ) / C2
          C[0, i] = -C1 * C5 * C[0, i-1] / C2
        for k in range(1, MN+1)[::-1]: # MN --> 1
          C[k, j] = ( C4 * C[k, j] - k * C[k-1, j] ) / C3
        C[0, j] = C4 * C[0, j] / C3
      C1 = C2
    return C

class basis:
  def __init__( self ):
    pass

class fdm_basis( basis ):
  def __init__( self, coord ):
    self.coord = coord
    self.stencil = dict() # order --> stencil_bank_1d

  def __make_stencil( self, highest_order, order_of_accuracy = 2 ):
    nroll = fdm_helper.num_stencil_pts( highest_order, order_of_accuracy )
    x = self.coord
    N = len(x)
    
    for k in range(1, highest_order+1):
      self.stencil[ k ] = stencil_bank_1d()

    for i in range(N):
      #print "i = ", i
      if i < nroll/2:
        #print "\t slice %d to %d, query %d " % (0, nroll, i)
        x_slice = x[0:nroll]
        C = fdm_helper.stencil_on_nodes( x_slice, x[i], highest_order)
        rep_shift = i
        id_shift = np.arange(0, nroll) - i
      elif i >= N - nroll/2:
        low_id = N - nroll
        #print "\t slice %d to %d, query %d " % (low_id, low_id+nroll, i-low_id)
        x_slice = x[low_id : low_id+nroll]
        C = fdm_helper.stencil_on_nodes( x_slice, x[i], highest_order)
        rep_shift = i-low_id
        id_shift = np.arange(low_id, low_id+nroll) - i
      else:
        #print "\t slice %d to %d, query %d " % (i-nroll/2, i+nroll/2, nroll/2)
        x_slice = x[i-nroll/2 : i+nroll/2]
        C = fdm_helper.stencil_on_nodes( x_slice, x[i], highest_order)
        rep_shift = nroll/2
        id_shift = np.arange(i-nroll/2, i+nroll/2) - i
        
      for k in range(1, highest_order+1):
        sten = stencil_1d()
        for n_sh, v in zip(id_shift, C[k, :]):
          sten[ n_sh ] = v
        sten.update()
        self.stencil[ k ][ i ] = sten
        
    return self.stencil[ highest_order ]
    
  def diff_stencil( self, order, order_of_accuracy = 2 ):
    if self.stencil is None or len(self.stencil) < order+1:
      return self.__make_stencil( order, order_of_accuracy )
    else:
      return self.stencil[ order ]
      
  def num_grids( self ):
    return len( self.coord )
       
if __name__ == "__main__":
  n = 10
  x = np.linspace(0, n, n+1)
  fdm_x = fdm_basis( x )
  a = fdm_x.diff_stencil( 2 )
  a = fdm_x.diff_stencil( 1 )
  print a.as_matrix()
  