import numpy as np

def laplace( x, y ):
  lap = np.zeros( x.shape )
  lap.fill(10)
  return lap
  
def ext( x, y ):
  return 2*x**2 + 3*y**2
  
def bc( f, XX, YY ):
  nx, ny = XX.shape[1], XX.shape[0]
  u = ext( XX, YY )
  f[0, :] = u[0, :]
  f[ny-1, :] = u[ny-1, :]
  f[:, 0] = u[:, 0]
  f[:, nx-1] = u[:, nx-1]
  return f
  
def rhs( XX, YY ):
  f = laplace( XX, YY )
  f = bc( f, XX, YY )
  return f

def mat_x( nx, dx, dy ):
  m = np.zeros( [nx, nx] )
  m[0, 0] = 1
  m[nx-1, nx-1] = 1
  cx = 1.0 / dx / dx
  cy = 1.0 / dy / dy
  for i in range(1, nx-1):
    m[i, i-1] = 1  * cx
    m[i, i]   = (-2 * cx) + (-2 * cy)
    m[i, i+1] = 1  * cx
  return m
  
def mat_y( ny, dx, dy ):
  m = np.zeros( [ny, ny] )
  m[0, 0] = 1
  m[ny-1, ny-1] = 1
  cx = 1.0 / dx / dx
  cy = 1.0 / dy / dy
  for i in range(1, ny-1):
    m[i, i-1] = 1  * cy
    m[i, i]   = (-2 * cy) + (-2 * cx)
    m[i, i+1] = 1  * cy
  return m

def mat_x_rhs_on_y( ny, dx, dy ):
  m = np.zeros( [ny, ny] )
  m[0, 0] = 1
  m[ny-1, ny-1] = 1
  cy = 1.0 / dy / dy
  for i in range(1, ny-1):
    m[i, i-1] = 1  * cy
    m[i, i]   = 0
    m[i, i+1] = 1  * cy  
  return m

def mat_y_rhs_on_x( nx, dx, dy ):
  m = np.zeros( [nx, nx] )
  m[0, 0] = 1
  m[nx-1, nx-1] = 1
  cx = 1.0 / dx / dx
  for i in range(1, nx-1):
    m[i, i-1] = 1  * cx
    m[i, i]   = 0
    m[i, i+1] = 1  * cx
  return m
  
def sweep_x( u, rhs, my, my_rhs ):
  nx, ny = u.shape[1], u.shape[0]
  f = np.copy( rhs )
  for iy in range(1, ny-1):
    b = np.dot( my_rhs, u[iy, :] )
    f[iy, 1:nx-1] -= b[1:nx-1]
  # solve y
  for ix in range(1, nx-1):
    u[:, ix] = np.linalg.solve( my, f[:, ix] )
  return u
  
def sweep_y( u, rhs, mx, mx_rhs ):
  nx, ny = u.shape[1], u.shape[0]
  f = np.copy( rhs )
  for ix in range(1, nx-1):
    b = np.dot( mx_rhs, u[:,ix] )
    f[1:ny-1, ix] -= b[1:ny-1]
  # solve x
  for iy in range(1, ny-1):
    u[iy, :] = np.linalg.solve( mx, f[iy, :] )
  return u
  
def error( u, u_ext ):
  return np.sqrt( np.mean((u - u_ext)**2) )
  
def main():
  nx, ny = 11, 11
  Lx, Ly = 2, 2
  x_ax = np.linspace( 0, Lx, nx )
  y_ax = np.linspace( 0, Ly, ny )
  dx = x_ax[1] - x_ax[0]
  dy = y_ax[1] - y_ax[0]
  XX, YY = np.meshgrid( x_ax, y_ax )
  u_ext = ext( XX, YY )
  u = np.zeros( [ny, nx] )
  u = bc( u, XX, YY )
  f = rhs( XX, YY )
  
  my = mat_y( ny, dx, dy )
  my_rhs = mat_y_rhs_on_x( nx, dx, dy )
  mx = mat_x( nx, dx, dy )
  mx_rhs = mat_x_rhs_on_y( ny, dx, dy )
  
  print error( u, u_ext )
  for it in range(200):
    print "it = %d" % it
    u = sweep_x( u, f, my, my_rhs ) # solve y
    #print u
    print error( u, u_ext )
    #print (u-u_ext)**2
    
    u = sweep_y( u, f, mx, mx_rhs ) # solve x
    #print u
    print error( u, u_ext )
    #print (u-u_ext)**2
  
if __name__ == "__main__":
  main()
  