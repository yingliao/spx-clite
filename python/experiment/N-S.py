from fdm import *
import numpy as np
import matplotlib.pyplot as plt

n0, n1 = 100, 50
shape = [n0, n1]
x0 = np.linspace( 0, 1, n0 )
x1 = np.linspace( 0, 2, n1 )
x0_ax = fdm_basis( x0 )
x1_ax = fdm_basis( x1 )

x0_stens = x0_ax.diff_stencil( 1, order_of_accuracy = 4 )
x1_stens = x1_ax.diff_stencil( 1, order_of_accuracy = 4 )

t = sten_1d_wrap_on_reclin_2d( shape, x1_stens, 1 )

xx1, xx0 = np.meshgrid( x1, x0 )
u = np.sin( xx1 )
dudx0_ext = np.cos( xx1 )

dudx0_num = np.zeros( shape )
for n_id, sten_id, sten_val in t.iteritems():
  u_cut = u[ sten_id[:,0], sten_id[:,1] ]
  dudx0_num[ n_id[0], n_id[1] ] = np.dot(u_cut, sten_val)
  
err = (dudx0_ext - dudx0_num)**2
print err
plt.imshow( err )
plt.colorbar()
plt.show()
