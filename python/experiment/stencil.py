import numpy as np

class stencil_1d:
  def __init__( self ):
    self.sten_map = dict()
    self.nd_shift = None
    self.nd_value = None
    
  def __setitem__( self, node_shift, value ):
    if node_shift not in self.sten_map:
      self.sten_map[ node_shift ] = 0      
    self.sten_map[ node_shift ] += value

  def __iadd__( self, other ):
    if isinstance(other, stencil_1d):
      for k, v in other.sten_map.iteritems():
        if k not in self.sten_map:
          self.sten_map[ k ] = 0
        self.sten_map[ k ] += v
    else:
      for k, v in self.sten_map.iteritems():
        self.sten_map[ k ] += other
    self.update()
    return self
        
  def __add__( self, other ):
    new_sten = stencil_1d()
    new_sten.sten_map = self.sten_map.copy()
    new_sten += other
    return new_sten
      
  def __str__( self ):
    return "id_shift : " + str(self.nd_shift) + "\nid_value : " + str(self.nd_value)
      
  def update( self ):
    N = len( self.sten_map )
    self.nd_shift = np.array( list( self.sten_map.keys() ), dtype="int") # node ID shift
    self.nd_value = np.array( list( self.sten_map.values() ) )  # values
    return [self.nd_shift, self.nd_value]
    
  def apply( self, node_id ):
    node_idx = self.nd_shift + node_id
    return node_idx, self.nd_value
    
  def as_row( self, node_id, N_total = None ):
    n_id, val = self.apply( node_id )
    if N_total is None:
      N_total = max( n_id ) + 1
    C = np.zeros( N_total )
    C[ n_id ] = val
    return C
      
class stencil_bank_1d:
  def __init__( self ):
    self.stens = dict()
  
  def __getitem__( self, node_id ):
    return self.stens[ node_id ]
  
  def __setitem__( self, node_id, stencil ):
    assert isinstance( stencil, stencil_1d ), ""
    self.stens[ node_id ] = stencil
  
  def apply( self, node_id ):
    return self.stens[ node_id ].apply( node_id )
    
  def as_matrix( self, N = None ):
    if N is None:
      N = max( self.stens.keys() ) + 1
    C = np.zeros( [N, N] )
    for n_id, sten in self.stens.iteritems():
      row = sten.as_row( n_id, N )
      C[ n_id, : ] = row
    return C
  
  def iteritems( self ):
    for n_id, sten in self.stens.iteritems():
      yield sten.apply( n_id )
      
  def __str__( self ):
    s = str()
    for n_id, sten in self.stens.iteritems():
      sten_n_idx, val = sten.apply( n_id )
      s += str( n_id ) + " : " + str( sten_n_idx ) + " <--> " + str( val ) + "\n"
    return s
    
class sten_gen_on_reclin_2d:
  def __init__( self, shape ):
    self.shape = shape

class sten_1d_wrap_on_reclin_2d( sten_gen_on_reclin_2d ):
  def __init__( self, shape, stens_1d, diff_dir ):
    assert isinstance( stens_1d, stencil_bank_1d )
    sten_gen_on_reclin_2d.__init__( self, shape )
    self.stens_1d = stens_1d
    self.diff_dir = diff_dir
    
  def generate( self, i0, i1 ):
    if self.diff_dir == 0:
      n_id, val = self.stens_1d.apply( i0 )
      C = np.zeros( [len( n_id ), 2], dtype="int" )
      C[:,0] = n_id
      C[:,1] = i1
    elif self.diff_dir == 1:
      n_id, val = self.stens_1d.apply( i1 )
      C = np.zeros( [len( n_id ), 3], dtype="int" )
      C[:,0] = i0
      C[:,1] = n_id
    return C, val
    
  def iteritems( self ):
    nx0, nx1 = self.shape
    for i0 in range( nx0 ):
      for i1 in range( nx1 ):
        C, val = self.generate( i0, i1 )
        yield [i0, i1], C, val